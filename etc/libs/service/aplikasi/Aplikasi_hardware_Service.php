<?php
class aplikasi_hardware_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

	public function getTmHardware($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,i_proc_pentium3,i_proc_pentium4,i_proc_dualcore,i_proc_coretwoduo,i_memori_1gb,
										i_memori_2gb,i_hardisk_40gb,i_hardisk_50gb,i_monitor_17lcd,i_monitor_18lcd,i_monitor_17nlcd,
										i_monitor_18nlcd,i_printer,i_scanner,i_ups,c_cctv,c_fasilitas_tv,c_fasilitas_pc,
										c_fasilitas_liflet,c_fasilitas_no,i_watt,c_problem_watt
										from tm_hardware where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"i_proc_pentium3"=>(string)$result[$j]->i_proc_pentium3,
									"i_proc_pentium4"=>(string)$result[$j]->i_proc_pentium4,
									"i_proc_dualcore"=>(string)$result[$j]->i_proc_dualcore,
									"i_proc_coretwoduo"=>(string)$result[$j]->i_proc_coretwoduo,
									"i_memori_1gb"=>(string)$result[$j]->i_memori_1gb,
									"i_memori_2gb"=>(string)$result[$j]->i_memori_2gb,
									"i_hardisk_40gb"=>(string)$result[$j]->i_hardisk_40gb,
									"i_hardisk_50gb"=>(string)$result[$j]->i_hardisk_50gb,
									"i_monitor_17lcd"=>(string)$result[$j]->i_monitor_17lcd,
									"i_monitor_18lcd"=>(string)$result[$j]->i_monitor_18lcd,
									"i_monitor_17nlcd"=>(string)$result[$j]->i_monitor_17nlcd,
									"i_monitor_18nlcd"=>(string)$result[$j]->i_monitor_18nlcd,
									"i_printer"=>(string)$result[$j]->i_printer,
									"i_scanner"=>(string)$result[$j]->i_scanner,
									"i_ups"=>(string)$result[$j]->i_ups,
									"c_cctv"=>(string)$result[$j]->c_cctv,
									"c_fasilitas_tv"=>(string)$result[$j]->c_fasilitas_tv,
									"c_fasilitas_pc"=>(string)$result[$j]->c_fasilitas_pc,
									"c_fasilitas_liflet"=>(string)$result[$j]->c_fasilitas_liflet,
									"c_fasilitas_no"=>(string)$result[$j]->c_fasilitas_no,
									"i_watt"=>(string)$result[$j]->i_watt,
									"c_problem_watt"=>(string)$result[$j]->c_problem_watt);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	public function getTmHardwareServer($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,c_fungsi_sever,n_aplikasi_server,i_jml_server,n_spesifikasi
										from tm_hardware_server where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"c_fungsi_sever"=>(string)$result[$j]->c_fungsi_sever,
									"n_aplikasi_server"=>(string)$result[$j]->n_aplikasi_server,
									"n_spesifikasi"=>(string)$result[$j]->n_spesifikasi,
									"i_jml_server"=>(string)$result[$j]->i_jml_server);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}

	
	public function ubahTmHardware(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"i_proc_pentium3"=>$data['i_proc_pentium3'],
							"i_proc_pentium4"=>$data['i_proc_pentium4'],
							"i_proc_dualcore"=>$data['i_proc_dualcore'],
							"i_proc_coretwoduo"=>$data['i_proc_coretwoduo'],
							"i_memori_1gb"=>$data['i_memori_1gb'],
							"i_memori_2gb"=>$data['i_memori_2gb'],
							"i_hardisk_40gb"=>$data['i_hardisk_40gb'],
							"i_hardisk_50gb"=>$data['i_hardisk_50gb'],
							"i_monitor_17lcd"=>$data['i_monitor_17lcd'],
							"i_monitor_18lcd"=>$data['i_monitor_18lcd'],
							"i_monitor_17nlcd"=>$data['i_monitor_17nlcd'],
							"i_monitor_18nlcd"=>$data['i_monitor_18nlcd'],
							"i_printer"=>$data['i_printer'],
							"i_scanner"=>$data['i_scanner'],
							"i_ups"=>$data['i_ups'],
							"c_cctv"=>$data['c_cctv'],
							"c_fasilitas_tv"=>$data['c_fasilitas_tv'],
							"c_fasilitas_pc"=>$data['c_fasilitas_pc'],
							"c_fasilitas_liflet"=>$data['c_fasilitas_liflet'],
							"c_fasilitas_no"=>$data['c_fasilitas_no'],
							"i_watt"=>$data['i_watt'],
							"c_problem_watt"=>$data['c_problem_watt'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));

		$db->update('tm_hardware',$ubah_data, "id_kuesioner = '".trim($data['id_kuesioner'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	
	
	public function tambahTmHardware(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"i_proc_pentium3"=>$data['i_proc_pentium3'],
							"i_proc_pentium4"=>$data['i_proc_pentium4'],
							"i_proc_dualcore"=>$data['i_proc_dualcore'],
							"i_proc_coretwoduo"=>$data['i_proc_coretwoduo'],
							"i_memori_1gb"=>$data['i_memori_1gb'],
							"i_memori_2gb"=>$data['i_memori_2gb'],
							"i_hardisk_40gb"=>$data['i_hardisk_40gb'],
							"i_hardisk_50gb"=>$data['i_hardisk_50gb'],
							"i_monitor_17lcd"=>$data['i_monitor_17lcd'],
							"i_monitor_18lcd"=>$data['i_monitor_18lcd'],
							"i_monitor_17nlcd"=>$data['i_monitor_17nlcd'],
							"i_monitor_18nlcd"=>$data['i_monitor_18nlcd'],
							"i_printer"=>$data['i_printer'],
							"i_scanner"=>$data['i_scanner'],
							"i_ups"=>$data['i_ups'],
							"c_cctv"=>$data['c_cctv'],
							"c_fasilitas_tv"=>$data['c_fasilitas_tv'],
							"c_fasilitas_pc"=>$data['c_fasilitas_pc'],
							"c_fasilitas_liflet"=>$data['c_fasilitas_liflet'],
							"c_fasilitas_no"=>$data['c_fasilitas_no'],
							"i_watt"=>$data['i_watt'],
							"c_problem_watt"=>$data['c_problem_watt'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_hardware',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	public function hapusTmHardware(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'";
		$db->delete('tm_hardware', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
	public function hapusTmHardwareServer(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'";
		$db->delete('tm_hardware_server', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}
	public function tambahTmHardwareServer(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"c_fungsi_sever"=>$data['c_fungsi_sever'],
							"n_aplikasi_server"=>$data['n_aplikasi_server'],
							"i_jml_server"=>$data['i_jml_server'],
							"n_spesifikasi"=>$data['n_spesifikasi'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_hardware_server',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}

}
?>
