<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_Referensi_Service.php";


class Aplikasi_GlobalController extends Zend_Controller_Action {
	private $referensi_serv;
	private $id;
	private $kdorg;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		//$this->pagu_serv = $registry->get('pagu_service');
		$this->view->basePath = $registry->get('basepath'); 
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
	    $this->user  = 'cdr';
	   		
		$this->ref_serv = Aplikasi_Referensi_Service::getInstance();
	}	
	
    public function indexAction() {
	   
    }
	
	public function globaljsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('globaljs');
    }
	
	public function listautokomplitAction() 
    {
		$this->_helper->viewRenderer->setNoRender(true);
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_POST['kategori'];
		$katakunciCari 	= $_GET['text'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		$this->view->daftarPejabat = $this->ref_serv->pejabatList($dataMasukan);
		
    }
	
	public function daftarpejabatAction() 
    {
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_POST['kategori'];
		$katakunciCari 	= $_POST['cari'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		$this->view->daftarPejabat = $this->ref_serv->pejabatList($dataMasukan);
		
    }
	
	public function daftarpejabat2Action() 
    {
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_POST['kategori'];
		$katakunciCari 	= $_POST['cari'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		$this->view->targetSurat = $_REQUEST['targetSurat'];
		$this->view->field = $_REQUEST['field'];
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		$this->view->daftarPejabat = $this->ref_serv->pejabatList($dataMasukan);
		
    }
	
	public function daftarpejabatdisposisiAction() 
    {
		$rowKe = $_REQUEST['rowKe'];
		$this->view->rowKe = $rowKe;
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_POST['kategori'];
		$katakunciCari 	= $_POST['cari'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		$this->view->daftarPejabat = $this->ref_serv->pejabatList($dataMasukan);
		
    }
	
	public function daftarpegawaiAction() 
    {
		$rowKe 				= $_REQUEST['rowKe'];
		$kdOrg 				= $_REQUEST['kdOrg'];
		$kdJabatan 			= $_REQUEST['kdJabatan'];
		$level 				= $_REQUEST['level'];
		$idRiwayatJabatan 	= $_REQUEST['idRiwayatJabatan'];
		$kdStrukturOrgInduk = $_REQUEST['kdStrukturOrgInduk'];
		
		$this->view->rowKe = $rowKe;
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_REQUEST['kategoriCari'];
		$katakunciCari 	= $_REQUEST['kataKunci'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		
		
		$dataMasukan = array("kategoriCari" 	=> $kategoriCari,
							"katakunciCari" 	=> $katakunciCari,
							"kdOrg"				=> $kdOrg,
							"kdJabatan"			=> $kdJabatan,
							"level"				=> $level,
							"idRiwayatJabatan"	=> $idRiwayatJabatan,
							"kdStrukturOrgInduk"=> $kdStrukturOrgInduk,
							"sortBy" 			=> $sortBy,
							"sort" 				=> $sort);
		$this->view->daftarPegawai = $this->ref_serv->pencarianPegawai($dataMasukan);
		//var_dump($dataMasukan);
    }
	
	public function daftarpegawaimultiselectAction() 
    {
		$tujuan				= $_REQUEST['tujuan'];
		
		$this->view->rowKe = $rowKe;
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_REQUEST['kategoriCari'];
		$katakunciCari 	= $_REQUEST['kataKunci'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		
		
		$dataMasukan = array("kategoriCari" 	=> $kategoriCari,
							"katakunciCari" 	=> $katakunciCari,
							"sortBy" 			=> $sortBy,
							"sort" 				=> $sort);
		$this->view->daftarPegawai = $this->ref_serv->pencarianPegawaiNonPelaksana($dataMasukan);
		
		if ($tujuan == 'memoKepada') {
			$this->view->tujuan = $tujuan;
			$this->_helper->viewRenderer('daftarpegawaimultiselect');
		}
    }
	
	public function daftarpegawaipenerimasuratmasukAction() 
    {
		$kdOrg = $_REQUEST['kdOrg'];
		$kdJabatan = $_REQUEST['kdJabatan'];
		$level = $_REQUEST['level'];
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= $_REQUEST['kategoriCari'];
		$katakunciCari 	= $_REQUEST['kataKunci'];
		$sortBy			= 'nip';
		$sort			= 'asc';
		
		$this->view->kategoriCari	= $kategoriCari;
		$this->view->katakunciCari	= $katakunciCari;
		$dataMasukan = array("kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"kdOrg"			=> $kdOrg,
							"kdJabatan"		=> $kdJabatan,
							"level"		=> $level,
							"sortBy" => $sortBy,
							"sort" => $sort);
							
		$this->view->daftarPegawai = $this->ref_serv->pencarianPegawaiAll($dataMasukan);
    }
}
?>