<?php
class Adm_AdmjenisSurat_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List User
	//======================================================================
	public function cariJenisSuratList(array $dataMasukan, $pageNumber, $itemPerPage) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= strtoupper($dataMasukan['katakunciCari']);
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	    //var_dump($dataMasukan);
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = " where (c_statusdelete != 'Y' or c_statusdelete is null)";
			$whereOpt = " UPPER($kategoriCari) like '%$katakunciCari%' ";
			if($katakunciCari != "") { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = " order by $sortBy $sort ";
			$sqlProses = "select c_srtjenis, n_srtjenis, c_statusdelete, i_entry, d_entry from tr_jenis_surat";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses"." "."$where) a";
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtjenis"  		=>(string)$result[$j]->c_srtjenis,
										"n_srtjenis"  	=>(string)$result[$j]->n_srtjenis,
										"c_statusdelete"=>(string)$result[$j]->c_statusdelete,
										"i_entry"      	=>(string)$result[$j]->i_entry,
										"d_entry"      	=>(string)$result[$j]->d_entry
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function jenisSuratInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_srtjenis"  		=>$dataMasukan['c_srtjenis'],
								"n_srtjenis"  		=>$dataMasukan['n_srtjenis'],
								"i_entry"  			=>$dataMasukan['i_entry']);		
			$db->insert('tr_jenis_surat',$paramInput);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function detailJenisSuratById($c_srtjenis) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		
			$where = " where c_srtjenis = '$c_srtjenis' ";
			$sqlProses = "select c_srtjenis, n_srtjenis, c_statusdelete, d_entry, i_entry from tr_jenis_surat";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			$hasilAkhir = array("c_srtjenis"  		=>(string)$result->c_srtjenis,
								"n_srtjenis"  	=>(string)$result->n_srtjenis,
								"c_statusdelete"=>(string)$result->c_statusdelete,
								"d_entry"      	=>(string)$result->d_entry,
								"i_entry"      	=>(string)$result->i_entry
								);
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function jenisSuratUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("n_srtjenis"  	=>$dataMasukan['n_srtjenis'],
								"i_entry"  		=>$dataMasukan['i_entry']);	
								
			//var_dump($paramInput);
			$where[] = " c_srtjenis = '".$dataMasukan['c_srtjenis']."'";
			
			$db->update('tr_jenis_surat',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function jenisSuratHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_statusdelete"	=> 'Y',
								"i_entry"  			=>$dataMasukan['i_entry']);	
								
			$where[] = " c_srtjenis = '".$dataMasukan['c_srtjenis']."'";
			
			$db->update('tr_jenis_surat',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
		
}
?>
