<?php
class aplikasi_prosedure_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

public function getTmProsedure($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,n_prosedure,e_desc
										from tm_prosedure where 1=1 $cari ");							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"n_prosedure"=>(string)$result[$j]->n_prosedure,
									"e_desc"=>(string)$result[$j]->e_desc);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}	
	public function hapusTmProsedure(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'  ";
		$db->delete('tm_prosedure', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
	
	public function tambahTmProsedure(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"n_prosedure"=>$data['n_prosedure'],
							"e_desc"=>$data['e_desc'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_prosedure',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	


}
?>
