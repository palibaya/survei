<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_deskinfo_Service.php";
require_once "../etc/libs/gd/pChart/pData.class";
require_once "../etc/libs/gd/pChart/pChart.class";
class Aplikasi_ChartController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->deskinfo_serv = aplikasi_deskinfo_Service::getInstance();

        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;
    }

public function indexAction() {
  }
public function chartlistAction()
{
}

public function chartAction() {

        $id_kuesioner = $_REQUEST['idKuesioner'];
        $this->view->id_kuesioner=$id_kuesioner;
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $databln = $this->deskinfo_serv->getTmDeskinfobulan($cari);
        $jmldatabln = count($databln);
        if (count($databln)!=0){

            for ($j = 0; $j < $jmldatabln; $j++) {
                $jmljan=$databln[$j]['v_jan'];
                $jmlfeb=$databln[$j]['v_feb'];
                $jmlmar=$databln[$j]['v_mar'];
                $jmlapr=$databln[$j]['v_apr'];
                $jmlmei=$databln[$j]['v_mei'];
                $jmljun=$databln[$j]['v_jun'];
                $jmljul=$databln[$j]['v_jul'];
                $jmlagt=$databln[$j]['v_agt'];
                $jmlsep=$databln[$j]['v_sep'];
                $jmlokt=$databln[$j]['v_okt'];
                $jmlnop=$databln[$j]['v_nop'];
                $jmldes=$databln[$j]['v_des'];
            }
            if (!$jmljan && !$jmlfeb && !$jmlmar && !$jmlapr && !$jmlmei && !$jmljun && !$jmljul
                 && !$jmlagt && !$jmlsep && !$jmlokt && !$jmlnop && !$jmldes){
                 echo "hhhhh";
                $this->_helper->viewRenderer('chart3dpie2');
            } else {
                $this->view->chart=$this->toChartGd('Deskinfo',$jmljan,$jmlfeb,$jmlmar,$jmlapr,$jmlmei,$jmljun,$jmljul,$jmlagt,$jmlsep,$jmlokt,$jmlnop,$jmldes);
                $this->_helper->viewRenderer('chart3dpie');
            }

        }
else{
    $jmljan=0;
    $jmlfeb=0;
    $jmlmar=0;
    $jmlapr=0;
    $jmlmei=0;
    $jmljun=0;
    $jmljul=0;
    $jmlagt=0;
    $jmlsep=0;
    $jmlokt=0;
    $jmlnop=0;
    $jmldes=0;
        $this->_helper->viewRenderer('chart3dpie2');
}


}

 function toChartGd($caption,$jmljan,$jmlfeb,$jmlmar,$jmlapr,$jmlmei,$jmljun,$jmljul,$jmlagt,$jmlsep,$jmlokt,$jmlnop,$jmldes){
 // Standard inclusions

  // Dataset definition
    $DataSet = new pData;
    $DataSet->AddPoint(array($jmljan,$jmlfeb,$jmlmar,$jmlapr,$jmlmei,$jmljun,$jmljul,$jmlagt,$jmlsep,$jmlokt,$jmlnop,$jmldes),"Serie1");
    $DataSet->AddPoint(array("Januari","Februari ","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember"),"Serie2");
    $DataSet->AddAllSeries();
    $DataSet->SetAbsciseLabelSerie("Serie2");

  // Initialise the graph
    $Test = new pChart(500,300);
    $Test->loadColorPalette("../etc/libs/gd/Sample/tones.txt");

/*  $Test->setColorPalette(0,213,70,66);
    $Test->setColorPalette(2,148,203,33);
         $Test->setColorPalette(3,122,48,160);
    $Test->setColorPalette(1,233,255,17); */



    $Test->drawFilledRoundedRectangle(7,7,493,293,5,240,240,240);
    //$Test->drawRoundedRectangle(5,5,295,195,5,230,230,230);

  // Draw the pie chart
    $Test->setFontProperties("../etc/libs/gd/Fonts/arial.ttf",8);
    $Test->setShadowProperties(4,2,200,200,200);
    $Test->drawFlatPieGraphWithShadow($DataSet->GetData(),$DataSet->GetDataDescription(),230,160,90,PIE_PERCENTAGE,10);
    $Test->drawPieLegend(370,40,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);

  // Write the title
    $Test->setFontProperties("../etc/libs/gd/Fonts/MankSans.ttf",12);
    $Test->drawTitle(220,35,"$caption",0,0,0);

    $Test->Render("../etc/data/pchart/$caption.png");
  }

    public function chartjsAction()
    {
         header('content-type : text/javascript');
         $this->render('chartjs');
    }



}
?>
