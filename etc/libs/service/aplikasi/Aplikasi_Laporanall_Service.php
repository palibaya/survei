<?php
class aplikasi_laporanall_Service {
    private static $instance;

    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

    public function getLaporan($cari)
    {
            $registry = Zend_Registry::getInstance();
            $db = $registry->get('db');
            try
            {
                $db->setFetchMode(Zend_Db::FETCH_OBJ);
                $sql = "SELECT a.id,
                               a.id_pengadilan,
                               a.d_bulan_kuesioner,
                               a.d_tahun_kuesioner,
                               a.n_responden,
                               a.n_jabatan,
                               a.n_pangkat,
                               a.n_golongan,
                               a.no_telepon_responden,
                               a.no_hp_responden,
                               b.i_organisasi,
                               b.n_organisasi,
                               b.n_alamat,
                               b.no_telepon,
                               b.no_fax,
                               b.i_organisasi_parent,
                               c.i_jumlahhakim AS desc1_1,
                               c.i_jumlahpns AS desc1_2,
                               c.i_jumlahpp AS desc1_3,
                               c.i_jumlahhonorer AS desc1_4,
                               c.i_stafit_sertifikasi_software AS desc2_1,
                               c.i_stafit_sertifikasi_hardware AS desc2_2,
                               c.i_stafit_sertifikasi_network AS desc2_3,
                               c.i_user_siap AS desc3_0,
                               c.i_user_sdm AS desc4_0,
                               c.i_user_sai AS desc5_0,
                               c.i_user_rkakl AS desc6_0,
                               c.i_user_sabmn AS desc7_0,
                               c.i_user_website AS desc8_0,
                               c.i_hakim_pc AS desc9_0,
                               c.i_pp_pc AS desc10_0,
                               c.i_pns_pc AS desc11_0,
                               d.i_xp AS desc12_1,
                               d.i_vista AS desc12_2,
                               d.i_seven AS desc12_3,
                               d.i_linux AS desc12_4,
                               d.i_dos AS desc12_5,
                               d.i_ext AS desc12_6,
                               d.i_browser AS desc13_1,
                               d.i_pdfreader AS desc13_2,
                               d.i_office AS desc13_3,
                               d.i_kompresi AS desc13_4,
                               d.i_virus AS desc13_5,
                               d.c_website AS desc16_1,
                               d.n_website AS desc16_2,
                               d.c_sw_platform AS desc16_3_a,
                               d.c_sw_database AS desc16_3_b,
                               d.c_sk AS desc16_4,
                               d.c_struktur AS desc16_5,
                               d.c_nemail AS desc16_6,
                               d.n_sw_email AS desc16_7,
                               e.i_proc_pentium3 AS desc17_1,
                               e.i_proc_pentium4 AS desc17_2,
                               e.i_proc_dualcore AS desc17_3,
                               e.i_proc_coretwoduo AS desc17_4,
                               e.i_memori_1gb AS desc18_1,
                               e.i_memori_2gb AS desc18_2,
                               e.i_hardisk_40gb AS desc19_1,
                               e.i_hardisk_50gb AS desc19_2,
                               e.i_monitor_17lcd AS desc20_1,
                               e.i_monitor_18lcd AS desc20_2,
                               e.i_monitor_17nlcd AS desc20_3,
                               e.i_monitor_18nlcd AS desc20_4,
                               e.i_printer AS desc21_1,
                               e.i_scanner AS desc21_2,
                               e.i_ups AS desc22_0,
                               e.c_cctv AS desc23_0,
                               e.c_fasilitas_tv AS desc24_a,
                               e.c_fasilitas_pc AS desc24_b,
                               e.c_fasilitas_liflet AS desc24_c,
                               e.c_fasilitas_no AS desc24_d,
                               e.i_watt AS desc25_0,
                               e.c_problem_watt AS desc26_0,
                               f.c_lan_cable AS desc28_1,
                               f.c_lan_wireless AS desc28_2,
                               f.c_lan_securty AS desc28_3,
                               f.q_lan AS desc28_4,
                               f.q_lan_hub_client AS desc28_5,
                               f.q_lan_nhub_client AS desc28_6,
                               f.c_lan_internet AS desc28_7,
                               f.q_lan_hub AS desc28_8,
                               f.q_pc_internet AS desc29_0,
                               f.c_internet_pasca AS desc30_0,
                               f.q_internet_isp AS desc31_0,
                               f.v_internet AS desc32_0,
                               f.q_internet_upload AS desc33_1,
                               f.q_internet_download AS desc33_2,
                               h.e_layanan AS desc39_0,
                               h.e_layanan_publik AS desc40_0,
                               h.e_staft_pelaksana AS desc40_1,
                               h.e_sk_tugas AS desc40_2,
                               h.e_mohon_info AS desc41_0,
                               h.e_status_perkara AS desc42_0,
                               i.v_jan ,
                               i.v_feb,
                               i.v_mar,
                               i.v_apr,
                               i.v_mei,
                               i.v_jun,
                               i.v_jul,
                               i.v_agt,
                               i.v_sep,
                               i.v_okt,
                               i.v_nop,
                               i.v_des
                        FROM tm_kuesioner a
                             LEFT JOIN tm_organisasi b ON a.id_pengadilan=b.i_organisasi
                             LEFT JOIN tm_sdm c ON a.id=c.id_kuesioner
                             LEFT JOIN tm_software d ON a.id=d.id_kuesioner
                             LEFT JOIN tm_hardware e ON a.id=e.id_kuesioner
                             LEFT JOIN tm_jaringan f ON a.id=f.id_kuesioner
                             LEFT JOIN tm_deskinfo h ON a.id=h.id_kuesioner
                             LEFT JOIN tm_deskinfo_bln i ON a.id=i.id_kuesioner
                        WHERE 1 $cari
                        ORDER BY a.id ASC,
                                 a.id_pengadilan ASC,
                                 a.d_bulan_kuesioner ASC,
                                 a.d_tahun_kuesioner ASC";

                //echo $sql2;
                $result = $db->fetchAll($sql);

                    $jmlResult = count($result);
                    for ($j = 0; $j < $jmlResult; $j++)
                    {
                        $id_kuesioner=$result[$j]->id;
                        $sofwareadm = $db->fetchAll("select id_kuesioner,c_adm,n_aplikasi,c_platform ,c_database,c_jns_aplikasi
                                                from tm_software_adm where id_kuesioner ='$id_kuesioner' order by c_adm asc");


                        unset($arraysofwareadm);
                        $jmlsofwareadm=0;
                        $jmlsofwareadm = count($sofwareadm);
                        for ($xx = 0; $xx < $jmlsofwareadm; $xx++)
                        {

                            if($sofwareadm[$xx]->c_platform==1){$c_platform="Dotnet";}
                            elseif($sofwareadm[$xx]->c_platform==2){$c_platform="PHP";}
                            elseif($sofwareadm[$xx]->c_platform==3){$c_platform="ASP";}
                            elseif($sofwareadm[$xx]->c_platform==4){$c_platform="VB";}
                            elseif($sofwareadm[$xx]->c_platform==5){$c_platform="Java";}
                            elseif($sofwareadm[$xx]->c_platform==6){$c_platform="VFP7";}
                            elseif($sofwareadm[$xx]->c_platform==7){$c_platform="VFP9";}
                            else{$c_platform="Lain-lain";}
                            if($sofwareadm[$xx]->c_database==1){$c_database="Acces";}
                            elseif($sofwareadm[$xx]->c_database==2){$c_database="MySql";}
                            elseif($sofwareadm[$xx]->c_database==3){$c_database="SqlServer";}
                            elseif($sofwareadm[$xx]->c_database==4){$c_database="DB2";}
                            elseif($sofwareadm[$xx]->c_database==5){$c_database="Oracle";}
                            elseif($sofwareadm[$xx]->c_database==6){$c_database="VFP7";}
                            elseif($sofwareadm[$xx]->c_database==7){$c_database="VFP9";}
                            else{$c_database="Lain-lain";}
                            if($sofwareadm[$xx]->c_database==1){$c_jns_aplikasi="Web";}
                            elseif($sofwareadm[$xx]->c_database==2){$c_jns_aplikasi="Client Server";}
                            else{$c_jns_aplikasi="Standalone";}

                            $arraysofwareadm[$xx] =array("id_kuesioner"=>(string)$sofwareadm[$xx]->id_kuesioner,
                                                "c_adm"=>(string)$sofwareadm[$xx]->c_adm,
                                                "n_aplikasi"=>(string)$sofwareadm[$xx]->n_aplikasi,
                                                "c_platform"=>(string)$c_platform,
                                                "c_database"=>(string)$c_database,
                                                "c_jns_aplikasi"=>(string)$c_jns_aplikasi);
                        }

                        $sofwarekendala = $db->fetchAll("select id_kuesioner,n_aplikasi_kendala,n_kendala
                                                from tm_software_kendala where id_kuesioner ='$id_kuesioner' order by n_aplikasi_kendala asc");

                        unset($arraysofwarekendala);
                        $jmlsofwarekendala=0;
                        $jmlsofwarekendala = count($sofwarekendala);
                        for ($xx = 0; $xx < $jmlsofwarekendala; $xx++)
                        {

                            $arraysofwarekendala[$xx] =array("id_kuesioner"=>(string)$sofwarekendala[$xx]->id_kuesioner,
                                                "n_aplikasi_kendala"=>(string)$sofwarekendala[$xx]->n_aplikasi_kendala,
                                                "n_kendala"=>(string)$sofwarekendala[$xx]->n_kendala);
                        }

                        $kegiatan = $db->fetchAll("select a.id_kuesioner,a.c_kegiatan,n_kegiatan,e_kegiatan,v_anggaran,
                                                e_alamat_prshn,e_mekanisme,i_lama,i_waktu_mulai,
                                                i_waktu_akhir,c_jaminan,e_cara,e_pedoman,
                                                b.c_komponen,e_spesipikasi ,i_jml_komponen,e_keterangan,n_komponen
                                                from tm_kegiatan a,tm_kegiatan_komponen b,tr_kegiatan_komponen c
                                                where a.id_kuesioner=b.id_kuesioner and b.c_komponen=c.c_komponen and a.id_kuesioner ='$id_kuesioner' order by c_kegiatan asc");

                        unset($arraykegiatan);
                        $jmlkegitan=0;
                        $jmlkegitan = count($kegiatan);
                        for ($xx = 0; $xx < $jmlkegitan; $xx++)
                        {
                            $arraykegiatan[$xx] =array("id_kuesioner"=>(string)$kegiatan[$xx]->id_kuesioner,
                                                "c_kegiatan"=>(string)$kegiatan[$xx]->c_kegiatan,
                                                "n_kegiatan"=>(string)$kegiatan[$xx]->n_kegiatan,
                                                "e_kegiatan"=>(string)$kegiatan[$xx]->e_kegiatan,
                                                "v_anggaran"=>(string)$kegiatan[$xx]->v_anggaran,
                                                "e_alamat_prshn"=>(string)$kegiatan[$xx]->e_alamat_prshn,
                                                "e_mekanisme"=>(string)$kegiatan[$xx]->e_mekanisme,
                                                "i_lama"=>(string)$kegiatan[$xx]->i_lama,
                                                "i_waktu_mulai"=>(string)$kegiatan[$xx]->i_waktu_mulai,
                                                "i_waktu_akhir"=>(string)$kegiatan[$xx]->i_waktu_akhir,
                                                "c_jaminan"=>(string)$kegiatan[$xx]->c_jaminan,
                                                "e_cara"=>(string)$kegiatan[$xx]->e_cara,
                                                "e_pedoman"=>(string)$kegiatan[$xx]->e_pedoman,
                                                "c_komponen"=>(string)$kegiatan[$xx]->c_komponen,
                                                "n_komponen"=>(string)$kegiatan[$xx]->n_komponen,
                                                "e_spesipikasi"=>(string)$kegiatan[$xx]->e_spesipikasi,
                                                "i_jml_komponen"=>(string)$kegiatan[$xx]->i_jml_komponen,
                                                "e_keterangan"=>(string)$kegiatan[$xx]->e_keterangan);
                        }


                        $hardwareserver = $db->fetchAll("select id_kuesioner,c_fungsi_sever,n_aplikasi_server,i_jml_server,n_spesifikasi
                                                from tm_hardware_server where id_kuesioner ='$id_kuesioner' order by c_fungsi_sever asc");

                        unset($arrayhardwareserver);
                        $jmlhardwareserver=0;
                        $jmlhardwareserver = count($hardwareserver);
                        for ($xx = 0; $xx < $jmlhardwareserver; $xx++)
                        {
                            if((string)$hardwareserver[$xx]->c_fungsi_sever=='1'){$c_fungsi_sever="Server Database";}
                            if((string)$hardwareserver[$xx]->c_fungsi_sever=='2'){$c_fungsi_sever="Server Aplikasi";}
                            if((string)$hardwareserver[$xx]->c_fungsi_sever=='3'){$c_fungsi_sever="Server Web";}
                            if((string)$hardwareserver[$xx]->c_fungsi_sever=='4'){$c_fungsi_sever="Lain-lain";}

                            $arrayhardwareserver[$xx] =array("c_fungsi_sever"=>$c_fungsi_sever,
                                                "n_aplikasi_server"=>(string)$hardwareserver[$xx]->n_aplikasi_server,
                                                "i_jml_server"=>(string)$hardwareserver[$xx]->i_jml_server,
                                                "n_spesifikasi"=>(string)$hardwareserver[$xx]->n_spesifikasi);
                        }

                        $prosedure = $db->fetchAll("select n_prosedure,e_desc from tm_prosedure where id_kuesioner ='$id_kuesioner' order by n_prosedure asc");
                        unset($arrayprosedure);
                        $jmlprosedure=0;
                        $jmlprosedure = count($prosedure);
                        for ($xx = 0; $xx < $jmlprosedure; $xx++)
                        {
                            $arrayprosedure[$xx] =array("e_desc"=>(string)$prosedure[$xx]->e_desc,
                                                "n_prosedure"=>(string)$prosedure[$xx]->n_prosedure);
                        }

                        $pengembangandipa = $db->fetchAll("select i_tahun_dipa,v_dipa from tm_pengembangan_dipa where  id_kuesioner ='$id_kuesioner' order by i_tahun_dipa asc");
                        unset($arraypengembangandipa);
                        $jmlpengembangandipa=0;
                        $jmlpengembangandipa = count($pengembangandipa);
                        for ($xx = 0; $xx < $jmlpengembangandipa; $xx++)
                        {
                            $arraypengembangandipa[$xx] =array("i_tahun_dipa"=>(string)$pengembangandipa[$xx]->i_tahun_dipa,
                                                "v_dipa"=>(string)$pengembangandipa[$xx]->v_dipa);
                        }

                        $pengembangandonor = $db->fetchAll("select i_tahun_donor,e_donor from tm_pengembangan_donor where  id_kuesioner ='$id_kuesioner' order by i_tahun_donor asc");
                        unset($arraypengembangandonor);
                        $jmlpengembangandonor=0;
                        $jmlpengembangandonor = count($pengembangandonor);
                        for ($xx = 0; $xx < $jmlpengembangandonor; $xx++)
                        {
                            $arraypengembangandonor[$xx] =array("i_tahun_donor"=>(string)$pengembangandonor[$xx]->i_tahun_donor,
                                                "e_donor"=>(string)$pengembangandonor[$xx]->e_donor);
                        }

                        $pengembanganang = $db->fetchAll("select i_tahun_ang,e_kegiatan from tm_pengembangan_thnang where  id_kuesioner ='$id_kuesioner' order by i_tahun_ang asc");
                        unset($arraypengembanganang);
                        $jmlpengembanganang=0;
                        $jmlpengembanganang = count($pengembanganang);
                        for ($xx = 0; $xx < $jmlpengembanganang; $xx++)
                        {
                            $arraypengembanganang[$xx] =array("i_tahun_ang"=>(string)$pengembanganang[$xx]->i_tahun_ang,
                                                "e_kegiatan"=>(string)$pengembanganang[$xx]->e_kegiatan);
                        }
                    $data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
                                    "id"=>(string)$result[$j]->id,
                                    "id_pengadilan"=>(string)$result[$j]->id_pengadilan,
                                    "d_bulan_kuesioner"=>(string)$result[$j]->d_bulan_kuesioner,
                                    "d_tahun_kuesioner"=>(string)$result[$j]->d_tahun_kuesioner,
                                    "n_responden"=>(string)$result[$j]->n_responden,
                                    "n_jabatan"=>(string)$result[$j]->n_jabatan,
                                    "n_pangkat"=>(string)$result[$j]->n_pangkat,
                                    "n_golongan"=>(string)$result[$j]->n_golongan,
                                    "no_telepon_responden"=>(string)$result[$j]->no_telepon_responden,
                                    "no_hp_responden"=>(string)$result[$j]->no_hp_responden,
                                    "i_organisasi"=>(string)$result[$j]->i_organisasi,
                                    "n_organisasi"=>(string)$result[$j]->n_organisasi,
                                    "n_alamat"=>(string)$result[$j]->n_alamat,
                                    "no_telepon"=>(string)$result[$j]->no_telepon,
                                    "no_fax"=>(string)$result[$j]->no_fax,
                                    "i_organisasi_parent"=>(string)$result[$j]->i_organisasi_parent,
                                    "1_1"=>(string)$result[$j]->desc1_1,
                                    "1_2"=>(string)$result[$j]-> desc1_2,
                                    "1_3"=>(string)$result[$j]-> desc1_3,
                                    "1_4"=>(string)$result[$j]-> desc1_4,
                                    "2_1"=>(string)$result[$j]-> desc2_1,
                                    "2_2"=>(string)$result[$j]-> desc2_2,
                                    "2_3"=>(string)$result[$j]-> desc2_3,
                                    "3_0"=>(string)$result[$j]-> desc3_0,
                                    "4_0"=>(string)$result[$j]-> desc4_0,
                                    "5_0"=>(string)$result[$j]-> desc5_0,
                                    "6_0"=>(string)$result[$j]-> desc6_0,
                                    "7_0"=>(string)$result[$j]-> desc7_0,
                                    "8_0"=>(string)$result[$j]-> desc8_0,
                                    "9_0"=>(string)$result[$j]-> desc9_0,
                                    "10_0"=>(string)$result[$j]-> desc10_0,
                                    "11_0"=>(string)$result[$j]-> desc11_0,
                                    "12_1"=>(string)$result[$j]-> desc12_1,
                                    "12_2"=>(string)$result[$j]-> desc12_2,
                                    "12_3"=>(string)$result[$j]-> desc12_3,
                                    "12_4"=>(string)$result[$j]-> desc12_4,
                                    "12_5"=>(string)$result[$j]-> desc12_5,
                                    "12_6"=>(string)$result[$j]-> desc12_6,
                                    "13_1"=>(string)$result[$j]-> desc13_1,
                                    "13_2"=>(string)$result[$j]-> desc13_2,
                                    "13_3"=>(string)$result[$j]-> desc13_3,
                                    "13_4"=>(string)$result[$j]-> desc13_4,
                                    "13_5"=>(string)$result[$j]-> desc13_5,
                                    "16_1"=>(string)$result[$j]-> desc16_1,
                                    "16_2"=>(string)$result[$j]-> desc16_2,
                                    "16_3_a"=>(string)$result[$j]-> desc16_3_a,
                                    "16_3_b"=>(string)$result[$j]-> desc16_3_b,
                                    "16_4"=>(string)$result[$j]-> desc16_4,
                                    "16_5"=>(string)$result[$j]-> desc16_5,
                                    "16_6"=>(string)$result[$j]-> desc16_6,
                                    "16_7"=>(string)$result[$j]-> desc16_7,
                                    "17_1"=>(string)$result[$j]-> desc17_1,
                                    "17_2"=>(string)$result[$j]-> desc17_2,
                                    "17_3"=>(string)$result[$j]-> desc17_3,
                                    "17_4"=>(string)$result[$j]-> desc17_4,
                                    "18_1"=>(string)$result[$j]-> desc18_1,
                                    "18_2"=>(string)$result[$j]-> desc18_2,
                                    "19_1"=>(string)$result[$j]-> desc19_1,
                                    "19_2"=>(string)$result[$j]-> desc19_2,
                                    "20_1"=>(string)$result[$j]-> desc20_1,
                                    "20_2"=>(string)$result[$j]-> desc20_2,
                                    "20_3"=>(string)$result[$j]-> desc20_3,
                                    "20_4"=>(string)$result[$j]-> desc20_4,
                                    "21_1"=>(string)$result[$j]-> desc21_1,
                                    "21_2"=>(string)$result[$j]-> desc21_2,
                                    "22_0"=>(string)$result[$j]-> desc22_0,
                                    "23_0"=>(string)$result[$j]-> desc23_0,
                                    "24_a"=>(string)$result[$j]-> desc24_a,
                                    "24_b"=>(string)$result[$j]-> desc24_b,
                                    "24_c"=>(string)$result[$j]-> desc24_c,
                                    "24_d"=>(string)$result[$j]-> desc24_d,
                                    "25_0"=>(string)$result[$j]-> desc25_0,
                                    "26_0"=>(string)$result[$j]-> desc26_0,
                                    "28_1"=>(string)$result[$j]-> desc28_1,
                                    "28_2"=>(string)$result[$j]-> desc28_2,
                                    "28_3"=>(string)$result[$j]-> desc28_3,
                                    "28_4"=>(string)$result[$j]-> desc28_4,
                                    "28_5"=>(string)$result[$j]-> desc28_5,
                                    "28_6"=>(string)$result[$j]-> desc28_6,
                                    "28_7"=>(string)$result[$j]-> desc28_7,
                                    "28_8"=>(string)$result[$j]-> desc28_8,
                                    "29_0"=>(string)$result[$j]-> desc29_0,
                                    "30_0"=>(string)$result[$j]-> desc30_0,
                                    "31_0"=>(string)$result[$j]-> desc31_0,
                                    "32_0"=>(string)$result[$j]-> desc32_0,
                                    "33_1"=>(string)$result[$j]-> desc33_1,
                                    "33_2"=>(string)$result[$j]-> desc33_2,
                                    "34_1"=>(string)$result[$j]-> desc34_1,
                                    "34_2"=>(string)$result[$j]-> desc34_2,
                                    "39_0"=>(string)$result[$j]-> desc39_0,
                                    "40_0"=>(string)$result[$j]-> desc40_0,
                                    "40_1"=>(string)$result[$j]-> desc40_1,
                                    "40_2"=>(string)$result[$j]-> desc40_2,
                                    "41_0"=>(string)$result[$j]-> desc41_0,
                                    "42_0"=>(string)$result[$j]-> desc42_0,
                                    "v_jan"=>(string)$result[$j]-> v_jan,
                                    "v_feb"=>(string)$result[$j]-> v_feb,
                                    "v_mar"=>(string)$result[$j]-> v_mar,
                                    "v_apr"=>(string)$result[$j]-> v_apr,
                                    "v_mei"=>(string)$result[$j]-> v_mei,
                                    "v_jun"=>(string)$result[$j]-> v_jun,
                                    "v_jul"=>(string)$result[$j]-> v_jul,
                                    "v_agt"=>(string)$result[$j]-> v_agt,
                                    "v_sep"=>(string)$result[$j]-> v_sep,
                                    "v_okt"=>(string)$result[$j]-> v_okt,
                                    "v_nop"=>(string)$result[$j]-> v_nop,
                                    "v_des"=>(string)$result[$j]-> v_des,
                                    "arraykegiatan"=>$arraykegiatan,
                                    "arraysofwareadm"=>$arraysofwareadm,
                                    "arraysofwarekendala"=>$arraysofwarekendala,
                                    "arrayhardwareserver"=>$arrayhardwareserver,
                                    "arrayprosedure"=>$arrayprosedure,
                                    "arraypengembangandipa"=>$arraypengembangandipa,
                                    "arraypengembangandonor"=>$arraypengembangandonor,
                                    "arraypengembanganang"=>$arraypengembanganang,
                                    "jmlkegitan"=>$jmlkegitan
                                    );}

                    return $data;
            } catch (Exception $e)
            {
                    echo $e->getMessage().'<br>';
                    return 'Data tidak ada <br>';
            }
    }

}
?>
