<?php
class Aplikasi_Refsrtsifat_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function srtsifatList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		if(count($dataMasukan) > 0){
			$pageNumber 	= $dataMasukan['pageNumber'];
			$itemPerPage 	= $dataMasukan['itemPerPage'];
			$kategoriCari 	= $dataMasukan['kategoriCari'];
			$katakunciCari 	= $dataMasukan['katakunciCari'];
			$sortBy			= $dataMasukan['sortBy'];
			$sort			= $dataMasukan['sort'];
		}
		else
		{
			$sortBy			= 'c_srtsifat';
			$sort			= 'asc';
		}
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort ";
			$sqlProses = "select c_srtsifat,
								n_srtsifat
							from tr_sifatpenomoran_suratkeluar ";	
							
			if(count($dataMasukan) > 0){
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;
				
				if(($pageNumber==0) && ($itemPerPage==0))
				{	
					$sqlTotal = "select count(*) from ($sqlProses.$where) a";
					$hasilAkhir = $db->fetchOne($sqlTotal);	
				}
				else
				{
					$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
					$result = $db->fetchAll($sqlData);	
				}
			} else {
				$sqlData = $sqlProses.$where.$order;
				$result = $db->fetchAll($sqlData);				
			}
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtsifat"  	=>(string)$result[$j]->c_srtsifat,
									   "n_srtsifat"  	=>(string)$result[$j]->n_srtsifat
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function srtsifatsuratmasukList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		if(count($dataMasukan) > 0){
			$pageNumber 	= $dataMasukan['pageNumber'];
			$itemPerPage 	= $dataMasukan['itemPerPage'];
			$kategoriCari 	= $dataMasukan['kategoriCari'];
			$katakunciCari 	= $dataMasukan['katakunciCari'];
			$sortBy			= $dataMasukan['sortBy'];
			$sort			= $dataMasukan['sort'];
		}
		else
		{
			$sortBy			= 'c_srtsifat';
			$sort			= 'asc';
		}
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort ";
			$sqlProses = "select c_srtsifat,
								n_srtsifat
							from tr_sifat ";	
							
			if(count($dataMasukan) > 0){
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;
				
				if(($pageNumber==0) && ($itemPerPage==0))
				{	
					$sqlTotal = "select count(*) from ($sqlProses.$where) a";
					$hasilAkhir = $db->fetchOne($sqlTotal);	
				}
				else
				{
					$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
					$result = $db->fetchAll($sqlData);	
				}
			} else {
				$sqlData = $sqlProses.$where.$order;
				$result = $db->fetchAll($sqlData);				
			}
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtsifat"  	=>(string)$result[$j]->c_srtsifat,
									   "n_srtsifat"  	=>(string)$result[$j]->n_srtsifat
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
}
?>