<html>
<head>
<title>Sistem Informasi Kuesioner - Mahkamah Agung Republik Indonesia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel='stylesheet' href='<? echo $this->basePath;?>/css/login.css' type='text/css' />
<script type="text/javascript" src="<?echo $this->basePath;?>/js/jquery.js"></script>
<script type="text/javascript" src="<?echo $this->basePath;?>/js/jquery.form.js"></script>
</head>
<script type="text/javascript">
function organisasiTkIList(){
	var idOrgTkI = document.getElementById('id_organisasiTkI').value;	
	var url = '<?php echo $this->basePath; ?>/adm/user/organisasitk2';
	var param = { idOrgTkI:idOrgTkI};
	jQuery.get(url, param, function(data) {
		jQuery("#id_organisasiTk2").html(data);
	});
}
function userList(){
	var id_organisasiTk2 = document.getElementById('id_organisasiTk2').value;	
	var url = '<?php echo $this->basePath; ?>/adm/user/usertk2x';
	var param = { id_organisasiTk2:id_organisasiTk2};
	jQuery.get(url, param, function(data) {
		jQuery("#username").html(data);
	});
}
</script>
<body>
<h1>Sistem Informasi Kuesioner - Mahkamah Agung Republik Indonesia</h1>
  <div id="page">
    <div id="container-login">
		<div id="result" style="display:none;"></div>
	    <div class="container-top"></div>
		<div class="container-right"></div>
		<div class="container-bottom"></div>
		<div class="container-left"></div>
		<div class="container-top-right"></div>
		<div class="container-bottom-right"></div>
		<div class="container-bottom-left"></div>
		<div class="container-top-left"></div>
			  <fieldset class="panel-form">
			  <h2>Login</h2>
			  <h4>Sistem Informasi Kuesioner<br>Mahkamah Agung Republik Indonesia</h4>
		      <form name="login" id="myForm" action="<? echo $this->basePath;?>/home/index/main" method="POST">
			  <div class="panel-login">
				
				<label for="posnusername">User ID</label><input type="text" name="username" class="textbox" id="username" size="23"><br>
				<label for="pospaswword">Password</label> <input type="password" name="password" class="textbox" id="password" size="23"><br>
		        <div class="alignright">
				<input type="submit" name="submit" value="Login" class="buttonsubmit"/>
			    </div>

			   </div>
				<div class="note">
				<? if ($this->pesanKesalahan){
				?>
					<font color="red"><b>
				<? echo $this->pesanKesalahan;?>
					</b></font>
				<?
				} else {
				?>
					Masukkan user id dan password yang sudah terdaftar untuk mengakses Sistem Informasi Kuesioner
				<? } ?>
				</div>	  
		          </fieldset>
		       </form>
		          
			</div>
	</div>
</body>
</html>