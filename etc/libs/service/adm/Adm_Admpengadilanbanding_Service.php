<?php
class Adm_Admpengadilanbanding_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List User
	//======================================================================
	

	public function jenisAjuanInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_srtajuan"  	=>$dataMasukan['c_srtajuan'],
								"n_srtajuan"  	=>$dataMasukan['n_srtajuan'],
								"i_entry"  		=>$dataMasukan['i_entry']);		
			$db->insert('tr_jenis_ajuan',$paramInput);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function detailJenisAjuanById($c_srtajuan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		
			$where = " where c_srtajuan = '$c_srtajuan' ";
			$sqlProses = "select c_srtajuan, n_srtajuan, c_statusdelete, d_entry, i_entry from tr_jenis_ajuan";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			$hasilAkhir = array("c_srtajuan"  		=>(string)$result->c_srtajuan,
								"n_srtajuan"  	=>(string)$result->n_srtajuan,
								"c_statusdelete"=>(string)$result->c_statusdelete,
								"d_entry"      	=>(string)$result->d_entry,
								"i_entry"      	=>(string)$result->i_entry
								);
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function jenisAjuanUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("n_srtajuan"  	 =>$dataMasukan['n_srtajuan'],
								"i_entry"  		=>$dataMasukan['i_entry']);	
								
			//var_dump($paramInput);
			$where[] = " c_srtajuan = '".$dataMasukan['c_srtajuan']."'";
			
			$db->update('tr_jenis_ajuan',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function jenisAjuanHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_statusdelete"=> 'Y',
								"i_entry"  		=>$dataMasukan['i_entry']);	
								
			$where[] = " c_srtajuan = '".$dataMasukan['c_srtajuan']."'";
			
			$db->update('tr_jenis_ajuan',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
		
}
?>
