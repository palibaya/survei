<?php

require_once "service/aplikasi/Aplikasi_Referensi_Service.php";

class Aplikasi_Suratmasukdisposisi_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// Insert Disposisi
	//======================================================================
	public function disposisiInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			for($i=0; $i<count($dataMasukan); $i++) {
				$paramInputDisposisi = array("id_srtmasuk" 			=> $dataMasukan[$i]['id_srtmasuk'],
											"n_teruskandari" 		=> $dataMasukan[$i]['n_teruskandari'],
											"kd_jabatanteruskandari"=> $dataMasukan[$i]['kd_jabatanteruskandari'],
											"kd_orgteruskandari"	=> $dataMasukan[$i]['kd_orgteruskandari'],
											"n_teruskanke" 			=> $dataMasukan[$i]['n_teruskanke'],
											"kd_jabatanteruskanke" 	=> $dataMasukan[$i]['kd_jabatanteruskanke'],
											"kd_orgteruskanke" 		=> $dataMasukan[$i]['kd_orgteruskanke'],
											"e_disposisi" 			=> $dataMasukan[$i]['e_disposisi'],
											"t_disposisi" 			=> new Zend_Db_Expr('NOW()'),
											"i_entry" 				=> $dataMasukan[$i]['i_entry']
											);
											
				$db->insert('tm_disposisi',$paramInputDisposisi);			

				$paramInput = array("c_status_bukasurat"	=> 'Y');
				$where[] = "id = '".$dataMasukan[$i]['id_srtmasuk']."'";
				$db->update('tm_surat_masuk',$paramInput, $where);
				
				//var_dump($paramInput);
				//echo "$i<br>";
				//var_dump($paramInputDisposisi);
				//echo "<br>";
				unset($paramInputDisposisi);
				unset($paramInput);
				unset($where);
			}
			
			$db->commit(); 
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function detaildisposisiByUserlogin(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$id_srtmasuk = $dataMasukan['id_srtmasuk'];
			$n_teruskanke = $dataMasukan['n_teruskanke'];
			$kd_orgteruskanke = $dataMasukan['kd_orgteruskanke'];
			$sqlProses = "select id_tandaterima
							,id_srtmasuk
							,i_agendasrt
							,c_srtjenis
							,n_srtjenis
							,i_srtpengirim
							,n_srtpengirim
							,d_tglpengirim
							,n_srtperihal
							,n_srtlampiran
							,c_satuanlampiran
							,i_agendalalusrt
							,d_tgldariwaseskab
							,d_tanggalmasuk
							,n_srt_lokasi
							,c_statusdeleteAgendamasuk
							,id_disposisi
							,n_teruskandari
							,kd_jabatanteruskandari
							,n_teruskanke
							,kd_jabatanteruskanke
							,e_disposisi
							,t_disposisi
							,c_statusdeleteDisposisi
							,n_kepada
							,nipKepada
							,kd_orgKepada
							,kd_jabatanKepada
							from vm_disposisi
							where (c_statusdeleteAgendamasuk != 'Y' or c_statusdeleteAgendamasuk is null) 
							and (c_statusdeleteDisposisi != 'Y' or c_statusdeleteDisposisi is null)
							and id_srtmasuk = '$id_srtmasuk'
							and (kd_orgKepada = '$kd_orgteruskanke' or (kd_orgteruskanke = '$kd_orgteruskanke' and n_teruskanke = '$n_teruskanke'))";	
//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);
//var_dump($result);	
			for($i=0; $i<count($result); $i++)
			{
				$globalReferensi = new globalReferensi();
				$nm_orgteruskandari = $globalReferensi->getNamaOrganisasi($result[$i]->n_teruskandari);
				
				$ref_serv = Aplikasi_Referensi_Service::getInstance();
				
				$nip_teruskandari = $globalReferensi->getNipPejabat($result[$i]->n_teruskandari, $result[$i]->kd_jabatanteruskandari);
				// $nama_teruskandari = $globalReferensi->getnamaPegawai($nip_teruskandari);
				// $nm_jabatanteruskandari = $globalReferensi->getNamaJabatan($result->kd_jabatanteruskandari);
				// $namaJabatanLengkapdari = $nm_jabatanteruskandari." ".$globalReferensi->getNamaOrganisasi($result->n_teruskandari);
				
				// $nm_orgteruskanke = $globalReferensi->getNamaOrganisasi($result->n_teruskanke);
				// $nip_teruskanke = $globalReferensi->getNipPejabat($result->n_teruskanke, $result->kd_jabatanteruskanke);
				// $nama_teruskanke = $globalReferensi->getnamaPegawai($nip_teruskanke);
				// $nm_jabatanteruskanke = $globalReferensi->getNamaJabatan($result->kd_jabatanteruskanke);
				// $namaJabatanLengkapke = $nm_jabatanteruskanke." ".$globalReferensi->getNamaOrganisasi($result->n_teruskanke);
				
				// $nm_orgKepada = $globalReferensi->getNamaOrganisasi($result->kd_orgKepada);
				// $nmKepada = $globalReferensi->getnamaPegawai($result->nipKepada);
				$nPenerima = $db->fetchOne("select n_penerima from tm_tandaterima where id = '".$result[$i]->id_tandaterima."'");
				$tTglterima = $db->fetchOne("select d_tglterima from tm_tandaterima where id = '".$result[$i]->id_tandaterima."'");
				$tDisposisi = $db->fetchOne("select t_disposisi from tm_disposisi where id_srtmasuk = '".$result[$i]->id_srtmasuk."'");
				
				
				$hasilAkhir[$i] = array("id_tanggalterima" => (string)$result[$i]->id_tanggalterima,                                                                                
									"id_srtmasuk"	   => (string)$result[$i]->id_srtmasuk,                                                                                
									"i_agendasrt"	   => (string)$result[$i]->i_agendasrt,              
									"c_srtjenis"       => (string)$result[$i]->c_srtjenis,               
									"n_srtjenis"       => (string)$result[$i]->n_srtjenis,               
									"i_srtpengirim"    => (string)$result[$i]->i_srtpengirim,           
									"n_srtpengirim"    => (string)$result[$i]->n_srtpengirim,           
									"d_tglpengirim"    => (string)$result[$i]->d_tglpengirim,           
									"n_srtperihal"     => (string)$result[$i]->n_srtperihal,           
									"n_srtlampiran"    => (string)$result[$i]->n_srtlampiran,    
									"nm_satuanlampiran" => $nm_satuanlampiran,	
									"i_agendalalusrt"  => (string)$result[$i]->i_agendalalusrt,          
									"d_tgldariwaseskab"=> (string)$result[$i]->d_tgldariwaseskab,        
									"d_tanggalmasuk"   => (string)$result[$i]->d_tanggalmasuk,           
									"n_srt_lokasi"     => (string)$result[$i]->n_srt_lokasi,             
									"id_disposisi"      => (string)$result[$i]->id_disposisi,              
									"n_teruskandari"   => (string)$result[$i]->n_teruskandari, 
									"kd_jabatanteruskandari"   => (string)$result[$i]->kd_jabatanteruskandari, 	
									"namaJabatanLengkapdari" => $namaJabatanLengkapdari,
									"nm_orgteruskandari" => $nm_orgteruskandari,
									"nip_teruskandari"	=> $nip_teruskandari,
									"nama_teruskandari" => $nama_teruskandari,
									"n_teruskanke"   => (string)$result[$i]->n_teruskanke, 
									"kd_jabatanteruskanke"   => (string)$result[$i]->kd_jabatanteruskanke, 	
									"namaJabatanLengkapke" => $namaJabatanLengkapke,
									"nm_orgteruskanke" => $nm_orgteruskanke,
									"nip_teruskanke"	=> $nip_teruskanke,
									"nama_teruskanke" => $nama_teruskanke,
									"e_disposisi"      => (string)$result[$i]->e_disposisi,              
									"t_disposisi"      => (string)$result[$i]->t_disposisi,              
									"n_kepada"  	   => (string)$result[$i]->n_kepada,          
									"nipKepada" => (string)$result[$i]->nipKepada,  
									"nmKepada" => $nmKepada,
									"kd_orgKepada"            => (string)$result[$i]->kd_orgKepada,                    
									"kd_jabatanKepada"         => (string)$result[$i]->kd_jabatanKepada,    
									"nm_orgKepada" => $nm_orgKepada,
									"nPenerima" => $nPenerima,
									"tTglterima" => $tTglterima,
									"tDisposisi" => $tDisposisi
									);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function detaildisposisiByNoagenda(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$id_srtmasuk = $dataMasukan['id_srtmasuk'];
			$n_teruskandari = $dataMasukan['n_teruskandari'];
			$sqlProses = "select id_srtmasuk
							,i_agendasrt
							,c_srtjenis
							,n_srtjenis
							,c_srtsifat
							,n_srtsifat
							,i_srtpengirim
							,n_srtpengirim
							,d_tglpengirim
							,n_srtperihal
							,n_srtlampiran
							,c_satuanlampiran
							,nm_satuanlampiran
							,i_agendalalusrt
							,d_tgldariwaseskab
							,d_tanggalmasuk
							,n_srt_lokasi
							,c_statusdeleteAgendamasuk
							,id_disposisi
							,n_teruskandari
							,kd_jabatanteruskandari
							,kd_orgteruskandari
							,n_teruskanke
							,kd_jabatanteruskanke
							,kd_orgteruskanke
							,e_disposisi
							,t_disposisi
							,c_statusdeleteDisposisi
							,n_kepada
							,nipKepada
							,kd_orgKepada
							,kd_jabatanKepada
							from vm_disposisi
							where (c_statusdeleteAgendamasuk != 'Y' or c_statusdeleteAgendamasuk is null) 
							and (c_statusdeleteDisposisi != 'Y' or c_statusdeleteDisposisi is null)
							and vm_disposisi.id_srtmasuk = '$id_srtmasuk' and vm_disposisi.n_teruskandari = '$n_teruskandari'";	
			$result = $db->fetchAll($sqlProses);	
			
			
			//echo $sqlProses;
			$globalReferensi = new globalReferensi();
			$ref_serv = Aplikasi_Referensi_Service::getInstance();
			$format_date = new format_date();
			
			for($i=0; $i<count($result); $i++)
			{
 				$nm_orgteruskandari = $globalReferensi->getNamaOrganisasi($result[$i]->n_teruskandari);
				$nip_teruskandari = $globalReferensi->getNipPejabat($result[$i]->kd_orgteruskandari, $result[$i]->kd_jabatanteruskandari);
				$nama_teruskandari = $globalReferensi->getnamaPegawai($nip_teruskandari);
				$nm_jabatanteruskandari = $globalReferensi->getNamaJabatan($result[$i]->kd_jabatanteruskandari);
				$namaJabatanLengkapdari = $nm_jabatanteruskandari." ".$globalReferensi->getNamaOrganisasi($result[$i]->n_teruskandari);
				
				$nm_orgteruskanke = $globalReferensi->getNamaOrganisasi($result[$i]->kd_orgteruskanke);
				$nip_teruskanke = $result[$i]->n_teruskanke; //$globalReferensi->getNipPejabat($result[$i]->n_teruskanke, $result[$i]->kd_jabatanteruskanke);
				$nama_teruskanke = $globalReferensi->getnamaPegawai($nip_teruskanke);
				$nm_jabatanteruskanke = $ref_serv->getjabatanPegawai($nip_teruskanke);
				//$namaJabatanLengkapke = $nm_jabatanteruskanke." ".$globalReferensi->getNamaOrganisasi($result[$i]->n_teruskanke);
				
				$nm_orgKepada = $globalReferensi->getNamaOrganisasi($result[$i]->kd_orgKepada);
				$nmKepada = $globalReferensi->getnamaPegawai($result[$i]->nipKepada); 
				
				$namaJabatanLengkapKepada	= $ref_serv->getjabatanPegawai($result[$i]->nipKepada);
				
				$d_tanggalmasukformat = $format_date->formatTglLengkap($result[$i]->d_tanggalmasuk);
				$d_tanggalpengirimformat = $format_date->formatTglLengkap($result[$i]->d_tglpengirim);
				$hasilAkhir[$i] = array("id_srtmasuk"		  	   => (string)$result[$i]->id_srtmasuk,                                                                                
									"i_agendasrt"	   => (string)$result[$i]->i_agendasrt,              
									"c_srtjenis"       => (string)$result[$i]->c_srtjenis,               
									"n_srtjenis"       => (string)$result[$i]->n_srtjenis,               
									"c_srtsifat"       => (string)$result[$i]->c_srtsifat,               
									"n_srtsifat"       => (string)$result[$i]->n_srtsifat,               
									"i_srtpengirim"    => (string)$result[$i]->i_srtpengirim,           
									"n_srtpengirim"    => (string)$result[$i]->n_srtpengirim,           
									"d_tglpengirim"    => (string)$result[$i]->d_tglpengirim, 
									"d_tanggalpengirimformat"	=>$d_tanggalpengirimformat,	
									"n_srtperihal"     => (string)$result[$i]->n_srtperihal,           
									"n_srtlampiran"    => (string)$result[$i]->n_srtlampiran, 
									"c_satuanlampiran"    => (string)$result[$i]->c_satuanlampiran, 
									"nm_satuanlampiran"    => (string)$result[$i]->nm_satuanlampiran, 									
									"i_agendalalusrt"  => (string)$result[$i]->i_agendalalusrt,          
									"d_tgldariwaseskab"=> (string)$result[$i]->d_tgldariwaseskab,        
									"d_tanggalmasuk"   => (string)$result[$i]->d_tanggalmasuk,           
									"d_tanggalmasukformat"	=>$d_tanggalmasukformat,	
									"n_srt_lokasi"     => (string)$result[$i]->n_srt_lokasi,             
									"id_disposisi"      => (string)$result[$i]->id_disposisi,              
									"n_teruskandari"   => (string)$result[$i]->n_teruskandari, 
									"kd_jabatanteruskandari"   => (string)$result[$i]->kd_jabatanteruskandari, 	
									"namaJabatanLengkapdari" => $namaJabatanLengkapdari,
									"kd_orgteruskandari"   => (string)$result[$i]->kd_orgteruskandari,
									"nm_orgteruskandari" => $nm_orgteruskandari,
									"nip_teruskandari"	=> $nip_teruskandari,
									"nama_teruskandari" => $nama_teruskandari,
									"n_teruskanke"   => (string)$result[$i]->n_teruskanke, 
									"kd_jabatanteruskanke"   => (string)$result[$i]->kd_jabatanteruskanke, 	
									"namaJabatanLengkapke" => $nm_jabatanteruskanke,
									"kd_orgteruskanke"   => (string)$result[$i]->kd_orgteruskanke,
									"nm_orgteruskanke" => $nm_orgteruskanke,
									"nip_teruskanke"	=> $nip_teruskanke,
									"nama_teruskanke" => $nama_teruskanke,
									"e_disposisi"      => (string)$result[$i]->e_disposisi,              
									"t_disposisi"      => (string)$result[$i]->t_disposisi,              
									"n_kepada"  	   => (string)$result[$i]->n_kepada,          
									"nipKepada" => (string)$result[$i]->nipKepada,  
									"nmKepada" => $nmKepada,
									"kd_orgKepada"            => (string)$result[$i]->kd_orgKepada,                    
									"kd_jabatanKepada"         => (string)$result[$i]->kd_jabatanKepada,    
									"nm_orgKepada" => $nm_orgKepada,
									"namaJabatanLengkapKepada" =>$namaJabatanLengkapKepada
									);
			}
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	//======================================================================
	// Insert Disposisi
	//======================================================================
	public function disposisiUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			//var_dump($dataMasukan);
			
			for($i=0; $i<count($dataMasukan); $i++) {
				$statusProses = $dataMasukan[$i]['statusProses'];
				$statusProsesArr = explode("_", $statusProses);
				$statusProsesData = $statusProsesArr[0];
				$idDisposisiAwal = $statusProsesArr[1];
				//echo "$statusProses statusProsesData = $statusProsesData <br>";
				if($statusProses | $statusProsesData == 'baru')
				{
					$paramInputDisposisi = array("id_srtmasuk" 	=> $dataMasukan[$i]['id_srtmasuk'],
											"n_teruskandari" 	=> $dataMasukan[$i]['n_teruskandari'],
											"kd_jabatanteruskandari" 	=> $dataMasukan[$i]['kd_jabatanteruskandari'],
											"kd_orgteruskandari"	=> $dataMasukan[$i]['kd_orgteruskandari'],
											"n_teruskanke" 	=> $dataMasukan[$i]['n_teruskanke'],
											"kd_jabatanteruskanke" 	=> $dataMasukan[$i]['kd_jabatanteruskanke'],
											"kd_orgteruskanke" 		=> $dataMasukan[$i]['kd_orgteruskanke'],
											"e_disposisi" 	=> $dataMasukan[$i]['e_disposisi'],
											"t_disposisi" 	=> new Zend_Db_Expr('NOW()'),
											"i_entry" 		=> $dataMasukan[$i]['i_entry']
											);
					//var_dump($paramInputDisposisi);						
					$db->insert('tm_disposisi',$paramInputDisposisi);						
				}
				else if($statusProsesData == 'update')
				{
					$paramUpdateDisposisi = array("id_srtmasuk" 	=> $dataMasukan[$i]['id_srtmasuk'],
											"n_teruskandari" 		=> $dataMasukan[$i]['n_teruskandari'],
											"kd_jabatanteruskandari"=> $dataMasukan[$i]['kd_jabatanteruskandari'],
											"kd_orgteruskandari"	=> $dataMasukan[$i]['kd_orgteruskandari'],
											"n_teruskanke" 			=> $dataMasukan[$i]['n_teruskanke'],
											"kd_jabatanteruskanke" 	=> $dataMasukan[$i]['kd_jabatanteruskanke'],
											"kd_orgteruskanke" 		=> $dataMasukan[$i]['kd_orgteruskanke'],
											"e_disposisi" 			=> $dataMasukan[$i]['e_disposisi'],
											"t_disposisi" 			=> new Zend_Db_Expr('NOW()'),
											"i_entry" 				=> $dataMasukan[$i]['i_entry']
											);
					
//echo "<br><br>";
//var_dump($paramUpdateDisposisi);					
					$whereUpdate[] = "id = '".$idDisposisiAwal."'";	
					$db->update('tm_disposisi',$paramUpdateDisposisi, $whereUpdate);	
					
					unset($paramUpdateDisposisi);
					unset($whereUpdate);
				}
				else if ($statusProsesData == 'delete')
				{
					$paramInputDisposisi = array("i_entry" 		=> $dataMasukan[$i]['i_entry'],
											"c_statusdelete" => 'Y'
											);
					$where[] = "id = '".$idDisposisiAwal."'";	
					$db->update('tm_disposisi',$paramInputDisposisi, $where);
 					unset($paramInputDisposisi);
					$paramInputDisposisi = array("id_srtmasuk" 	=> $dataMasukan[$i]['id_srtmasuk'],
											"n_teruskandari" 	=> $dataMasukan[$i]['n_teruskandari'],
											"kd_jabatanteruskandari" 	=> $dataMasukan[$i]['kd_jabatanteruskandari'],
											"n_teruskanke" 	=> $dataMasukan[$i]['n_teruskanke'],
											"kd_jabatanteruskanke" 	=> $dataMasukan[$i]['kd_jabatanteruskanke'],
											"e_disposisi" 	=> $dataMasukan[$i]['e_disposisi'],
											"t_disposisi" 	=> new Zend_Db_Expr('NOW()'),
											"i_entry" 		=> $dataMasukan[$i]['i_entry']
											);
											
					$db->insert('tm_disposisi',$paramInputDisposisi);	 
					
				}
				
				$paramInput2 = array("c_status_bukasurat"	=> 'Y');
				$where2[] = "id = '".$dataMasukan[$i]['id_srtmasuk']."'";
				$db->update('tm_surat_masuk',$paramInput2, $where2);
				
				unset($where);
				unset($paramInputDisposisi);
			}
			if($dataMasukan[0]['hapusbaris'])
			{
				$hapusbarisArr = explode('~',$dataMasukan[0]['hapusbaris']);
				//var_dump($hapusbarisArr);
				//echo "jml = ".count($hapusbarisArr);
				for($j=0; $j<count($hapusbarisArr); $j++)
				{
					$idDisposisiHapus = $hapusbarisArr[$j];
					if($idDisposisiHapus) {
						//echo "$j - $idDisposisiHapus <br>";
						$paramInputDisposisi = array("i_entry" 		=> $dataMasukan[$i]['i_entry'],
												"c_statusdelete" => 'Y'
												);
						$where[] = "id = '".$idDisposisiHapus."'";	
						$db->update('tm_disposisi',$paramInputDisposisi, $where);
					}
					unset($where);
				}
			}
			
			$db->commit(); 
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function disposisiHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));	
								
			$where[] = "id_srtmasuk = '".$dataMasukan['id_srtmasuk']."'";
			$db->update('tm_disposisi',$paramInput, $where);
			
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
//===============================================================================================================================================	
	public function agendaList() {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select distinct(i_agendasrt) i_agendasrt
							from tm_surat_masuk ";	

			$sqlData = $sqlProses;
			$result = $db->fetchCol($sqlData);	
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = $result[$j];
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	

	public function updateTanggalmasuk(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			$suratBaru = $db->fetchOne("select count(*) from tm_surat_masuk where id = '".$dataMasukan['id_agendasrt']." and d_tanggalmasuk is null'");
			
			if($suratBaru == 0){
				$paramInput = array("d_tanggalmasuk"=>date("Y-m-d"),
								   "i_entry"       	=>$dataMasukan['i_entry'],
								   "d_entry"       	=>date("Y-m-d"));

									
				$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
				$db->update('tm_surat_masuk',$paramInput, $where);
				$db->commit();
			}
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function detailagendamasukById($idAgendasrt) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where id = '$idAgendasrt' ";
			$sqlProses = "select id,
								i_agendasrt,
								d_tanggalmasuk,
								c_srtjenis,
								i_srtpengirim,
								d_tglpengirim,
								n_srtperihal,
								n_srtlampiran,
								n_srtpengirim,
								i_agendalalusrt,
								d_tgldariwaseskab,
								n_srt_lokasi
							from tm_surat_masuk ";	

			
			$sqlData = $sqlProses.$where;
			
			
			$result = $db->fetchRow($sqlData);	
			
			$n_srtjenis = $db->fetchOne("select n_srtjenis from tr_jenis_surat where c_srtjenis='".$result->c_srtjenis."'");
			
			$resultKepada = $db->fetchRow("select n_kepada,
													nip,
													kd_jabatan,
													kd_org
											from tm_suratkepada
											where id='".$result->n_srtkepada."'");
			
			$namaJabatan = $db->fetchOne("select nm_jabatan from tr_jabatan where kd_jabatan = '".$resultKepada->kd_jabatan."'");
			$level = $db->fetchOne("select level from tr_struktur_organisasi where kd_struktur_org = '".$resultKepada->kd_org."'");
			$nmLevel = $db->fetchOne("select nm_level from tr_struktur_organisasi where kd_struktur_org = '".$resultKepada->kd_org."'");
			$namaJabatanLengkap = "$namaJabatan $level $nmLevel";
			
			$hasilAkhir = array("id_agendamasuk"		=>(string)$result->id,
									"i_agendasrt"		=>(string)$result->i_agendasrt,	
									"d_tanggalmasuk"	=>(string)$result->d_tanggalmasuk,	
									"n_srtkepadaHidden"	=>(string)$result->n_srtkepada,
									"n_srtkepada"		=>(string)$resultKepada->n_kepada,
									"nip"				=>(string)$resultKepada->nip,									
									"kd_jabatan"		=>(string)$resultKepada->kd_jabatan,
									"kd_org"			=>(string)$resultKepada->kd_org,
									"nm_org"			=>"$level $nmlevel",
									"nm_jabatan_lengkap"=> $namaJabatanLengkap, 
									"c_srtjenis"        =>(string)$result->c_srtjenis,
									"n_srtjenis"        =>$n_srtjenis,										
									"i_srtpengirim"     =>(string)$result->i_srtpengirim,   
									"d_tglpengirim"     =>(string)$result->d_tglpengirim,   
									"n_srtperihal"      =>(string)$result->n_srtperihal,    
									"n_srtlampiran"     =>(string)$result->n_srtlampiran,   
									"n_srtpengirim"     =>(string)$result->n_srtpengirim,   
									"i_agendalalusrt"   =>(string)$result->i_agendalalusrt, 
									"d_tgldariwaseskab" =>(string)$result->d_tgldariwaseskab,
									"n_srt_lokasi" 		=>(string)$result->n_srt_lokasi
									);
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function detailagendamasukById2($idAgendasrt) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where idAgendasrt= '$idAgendasrt' ";
			$sqlProses = "select distinct a.idAgendasrt,
							a.i_agendasrt,
							a.d_tanggalmasuk,
							a.c_srtjenis,
							a.n_srtjenis,
							a.c_srtsifat,
							a.n_srtsifat,
							a.i_srtpengirim,
							a.d_tglpengirim,
							a.n_srtperihal,
							a.n_srtlampiran,
							a.nm_satuanlampiran,
							a.n_srtpengirim,
							a.i_agendalalusrt,
							a.d_tgldariwaseskab,
							a.n_srt_lokasi,
							a.n_kepada,
							a.nip,
							a.kd_org,
							a.kd_jabatan,
							a.nm_jabatan,
							a.i_entry,
							a.e_retro
						from vm_suratmasuk a
						left join tm_disposisi b on (a.id_srtmasuk = b.id_srtmasuk)";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			//echo $sqlData;
			$n_srtjenis = $db->fetchOne("select n_srtjenis from tr_jenis_surat where c_srtjenis='".$result->c_srtjenis."'");
			
			/* $resultKepada = $db->fetchRow("select n_kepada,
													nip,
													kd_jabatan,
													kd_org
											from tm_suratkepada
											where id='".$result->n_srtkepada."'");*/
			
			$namaJabatan = $db->fetchOne("select nm_jabatan from simpeg_internal.tbl_jabatan where kd_jabatan = '".$result->kd_jabatan."'");
			$level = $db->fetchOne("select level from simpeg_internal.tbl_struktur_organisasi where kd_struktur_org = '".$result->kd_org."'");
			$nmLevel = $db->fetchOne("select nm_level from simpeg_internal.tbl_struktur_organisasi where kd_struktur_org = '".$result->kd_org."'");
			
			
			$ref_serv = Aplikasi_Referensi_Service::getInstance();
			$format_date = new format_date();
			
			$namaJabatanLengkap = $ref_serv->getjabatanPegawai($result->nip);
			$d_tanggalmasukformat = $format_date->formatTglLengkap($result->d_tanggalmasuk);
			$d_tanggalpengirimformat = $format_date->formatTglLengkap($result->d_tglpengirim);

			$hasilAkhir = array("id_agendamasuk"		=>(string)$result->idAgendasrt,
									"i_agendasrt"		=>(string)$result->i_agendasrt,	
									"d_tanggalmasuk"	=>(string)$result->d_tanggalmasuk,	
									"d_tanggalmasukformat"	=>$d_tanggalmasukformat,	
									"n_srtkepadaHidden"	=>(string)$result->n_srtkepada,
									"n_srtkepada"		=>(string)$result->n_kepada,
									"nip"				=>(string)$result->nip,									
									"kd_jabatan"		=>(string)$result->kd_jabatan,
									"kd_org"			=>(string)$result->kd_org,
									"nm_org"			=>"$level $nmlevel",
									"nm_jabatan_lengkap"=> $namaJabatanLengkap, 
									"c_srtjenis"        =>(string)$result->c_srtjenis,
									"n_srtjenis"        =>$n_srtjenis,	
									"c_srtsifat"       => (string)$result->c_srtsifat,               
									"n_srtsifat"       => (string)$result->n_srtsifat,               
									"i_srtpengirim"     =>(string)$result->i_srtpengirim,   
									"d_tglpengirim"     =>(string)$result->d_tglpengirim,   
									"d_tanggalpengirimformat" => $d_tanggalpengirimformat,
									"n_srtperihal"      =>(string)$result->n_srtperihal,    
									"n_srtlampiran"     =>(string)$result->n_srtlampiran,   
									"nm_satuanlampiran" =>(string)$result->nm_satuanlampiran,   
									"n_srtpengirim"     =>(string)$result->n_srtpengirim,   
									"i_agendalalusrt"   =>(string)$result->i_agendalalusrt, 
									"d_tgldariwaseskab" =>(string)$result->d_tgldariwaseskab,
									"n_srt_lokasi" 		=>(string)$result->n_srt_lokasi,
									"e_retro" 			=>(string)$result->e_retro
									);
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function agendamasukUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInputSrtKepada = array("n_kepada"    	=> $dataMasukan['n_srtkepada'],  
										"nip"    		=> $dataMasukan['nip'],
										"kd_jabatan"    => $dataMasukan['kd_jabatan'],
										"kd_org"    	=> $dataMasukan['kd_org']);

						
			$whereSrtKepada[] = "id = '".$dataMasukan['nSrtkepadaHidden']."'";
			//var_dump($paramInputSrtKepada);		
			$db->update('tm_suratkepada',$paramInputSrtKepada, $whereSrtKepada);
			
			$paramInput = array("i_agendasrt"	    => $dataMasukan['i_agendasrt'],	                      
								"c_srtjenis"        => $dataMasukan['c_srtjenis'],                          
								"i_srtpengirim"     => $dataMasukan['i_srtpengirim'],  
								"n_srtpengirim"		=> $dataMasukan['n_srtpengirim'],								
								"d_tglpengirim"     => $dataMasukan['d_tglpengirim'],                         
								"n_srtperihal"      => $dataMasukan['n_srtperihal'],                          
								"n_srtlampiran"     => $dataMasukan['n_srtlampiran'],                         
								"n_srtpengirim"     => $dataMasukan['n_srtpengirim'],
								"i_agendalalusrt"   => $dataMasukan['i_agendalalusrt'],
								"d_tgldariwaseskab" => $dataMasukan['d_tgldariwaseskab'],
								"n_srt_lokasi" 		=> $dataMasukan['n_srt_lokasi'],
							    "i_entry"       	=>$dataMasukan['i_entry'],
							    "d_entry"       	=>date("Y-m-d"));
			$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
		    $db->update('tm_surat_masuk', $paramInput, $where); 
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function jmlDispoMasuk($nipuserLogin) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where ((c_statusdeleteAgendamasuk != 'Y' or c_statusdeleteAgendamasuk is null) and 
								(c_statusdeleteDisposisi != 'Y' or c_statusdeleteDisposisi is null)) and
								c_status_bukasuratDisposisi = 'N'";
								//a.c_status_bukasurat='N'";
						
			// perubahan hendar 13-10-2009 jakarta $whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin' OR a.kd_org = '1' OR a.kd_org = '2')";

	//$whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin')";
	$whereByOrg = " and (n_teruskanke='$nipuserLogin')";
	/* if (($kdOrgLogin=='1')||($kdOrgLogin=='2'))	
	{
	
		$whereByOrg = "(".$whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%'))";

	}	 */	
			$where = $whereBase.$whereByOrg;
			
			$sqlProses = "select *
						from vm_disposisi a ";	

			$sqlData = 'select count(*) from ('.$sqlProses.$where.') x';
			//echo $sqlData;
			$result = $db->fetchOne($sqlData);	
			
					
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function ubahStatusBukaSuratDisposisi(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
        
		try {
			$db->beginTransaction();
			
			$suratBaru = $db->fetchOne("select count(*) 
										from tm_disposisi 
										where id_srtmasuk = '".$dataMasukan['id_srtmasuk']."' and 
										n_teruskanke = '".$dataMasukan['nipLogin']."' and c_status_bukasurat =  'Y' ");
			/* echo "select count(*) 
										from tm_disposisi 
										where id_srtmasuk = '".$dataMasukan['id_srtmasuk']."' and 
										n_teruskanke = '".$dataMasukan['nipLogin']."' and c_status_bukasurat =  'Y' "; */
			 
			if($suratBaru == 0){
				$paramInput = array("c_status_bukasurat"	=> 'Y');
				$where[] = "id_srtmasuk = '".$dataMasukan['id_srtmasuk']."'";
				$where[] = "n_teruskanke = '".$dataMasukan['nipLogin']."'";
				//var_dump($where);
				$db->update('tm_disposisi',$paramInput, $where);
			
				$db->commit();
			}
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
}
?>