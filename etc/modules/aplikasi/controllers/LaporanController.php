<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_software_Service.php";
require_once "service/aplikasi/Aplikasi_hardware_Service.php";
require_once "service/aplikasi/Aplikasi_prosedure_Service.php";
require_once "service/aplikasi/Aplikasi_anggaranpengembangan_Service.php";
require_once "service/aplikasi/Aplikasi_kegiatan_Service.php";
require_once "service/aplikasi/Aplikasi_deskinfo_Service.php";
require_once "service/aplikasi/Aplikasi_Sdm_Service.php";
require_once "service/aplikasi/Aplikasi_jaringan_Service.php";
require_once "service/aplikasi/Aplikasi_Responden_Service.php";

class Aplikasi_LaporanController extends Zend_Controller_Action {

public function init() {
    $registry = Zend_Registry::getInstance();
    $this->view->basePath = $registry->get('basepath');
    $this->software_serv = aplikasi_software_Service::getInstance();
    $this->hardware_serv = aplikasi_hardware_Service::getInstance();
    $this->prosedure_serv = aplikasi_prosedure_Service::getInstance();
    $this->pengembangan_serv = aplikasi_anggaranpengembangan_Service::getInstance();
    $this->kegiatan_serv = aplikasi_kegiatan_Service::getInstance();
    $this->view->kegiatan_serv2 = aplikasi_kegiatan_Service::getInstance();
    $this->deskinfo_serv = aplikasi_deskinfo_Service::getInstance();
    $this->sdm_serv   = Aplikasi_Sdm_Service::getInstance();
    $this->jaringan_serv = aplikasi_jaringan_Service::getInstance();
    $this->responden_serv     = Aplikasi_Responden_Service::getInstance();
}

public function indexAction()
{
}
public function laporanAction()
{
//  $this->view->datapengadilan= $this->software_serv->getTmPengadilan($cari);
    $id_kuesioner=$_GET['idKuesioner'];
    $cari=" and id='$id_kuesioner' ";
    $this->view->dataresponden= $this->software_serv->getTmKuesioner($cari);
}

public function listrespondenAction()
{
    $id_pengadilan=$_GET['id_pengadilan'];
    $cari=" and id_pengadilan='$id_pengadilan' ";
    $dataresponden= $this->software_serv->getTmKuesioner($cari);

    $jmldata = count($dataresponden);
if (count($dataresponden)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
            $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
            $this->view->d_bulan_kuesioner=$data[$j]['d_bulan_kuesioner'];
            $this->view->d_tahun_kuesioner=$data[$j]['d_tahun_kuesioner'];
            $this->view->n_responden=$data[$j]['n_responden'];
            $this->view->n_jabatan=$data[$j]['n_jabatan'];
            $this->view->n_pangkat=$data[$j]['n_pangkat'];
            $this->view->n_golongan=$data[$j]['n_golongan'];
            $this->view->no_telepon_responden=$data[$j]['no_telepon_responden'];
            $this->view->no_hp_responden=$data[$j]['no_hp_responden'];
            $this->view->n_organisasi=$data[$j]['n_organisasi'];
            $this->view->n_organisasi_tk1=$data[$j]['n_organisasi_tk1'];

            }
}
}

public function listrespondencetakAction()
{
    $id_kuesioner=$_GET['id'];
    $id_pengadilan=$_GET['id_pengadilan'];
    $d_bulan_kuesioner=$_GET['d_bulan_kuesioner'];
    $d_tahun_kuesioner=$_GET['d_tahun_kuesioner'];

    $cari = " and id ='$id_kuesioner'";

    $dataresponden= $this->software_serv->getTmKuesioner2($cari);
    $jmldata = count($dataresponden);
        if (count($dataresponden)!=0){
                for ($j = 0; $j < $jmldata; $j++) {
                    $this->view->id_kuesioner=$dataresponden[$j]['id_kuesioner'];
                    $this->view->d_bulan_kuesioner=$dataresponden[$j]['d_bulan_kuesioner'];
                    $this->view->d_tahun_kuesioner=$dataresponden[$j]['d_tahun_kuesioner'];
                    $this->view->n_responden=$dataresponden[$j]['n_responden'];
                    $this->view->n_jabatan=$dataresponden[$j]['n_jabatan'];
                    $this->view->n_pangkat=$dataresponden[$j]['n_pangkat'];
                    $this->view->n_golongan=$dataresponden[$j]['n_golongan'];
                    $this->view->no_telepon_responden=$dataresponden[$j]['no_telepon_responden'];
                    $this->view->no_hp_responden=$dataresponden[$j]['no_hp_responden'];
                    $this->view->n_organisasi=$dataresponden[$j]['n_organisasi'];
                    $this->view->no_fax=$dataresponden[$j]['no_fax'];
                    $this->view->n_alamat=$dataresponden[$j]['n_alamat'];
                    $this->view->n_organisasi_tk1=$dataresponden[$j]['n_organisasi_tk1'];

                    }
        }

    $this-> softwareAction($id_kuesioner) ;
    $this-> hardwareAction($id_kuesioner);
    $this->prosedureAction($id_kuesioner);
    $this->anggaranpengembanganAction($id_kuesioner);
    $this->kegiatanAction($id_kuesioner);
    $this->deskinfoAction($id_kuesioner) ;
    $this->sdmolahdataAction($id_kuesioner);
    $this->jaringanAction($id_kuesioner) ;
}
public function softwareAction($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner'";
        $this->view->datasoftware = $this->software_serv->getTmSoftwareAdm($cari);
        $this->view->datasoftwarekendala = $this->software_serv->getTmSoftwareKendala($cari);

        $data = $this->software_serv->getTmSoftware($cari);
        $jmldata = count($data);
if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
            $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
            $this->view->i_xp=$data[$j]['i_xp'];
            $this->view->i_vista=$data[$j]['i_vista'];
            $this->view->i_seven=$data[$j]['i_seven'];
            $this->view->i_linux=$data[$j]['i_linux'];
            $this->view->i_dos=$data[$j]['i_dos'];
            $this->view->i_ext=$data[$j]['i_ext'];
            $this->view->i_browser=$data[$j]['i_browser'];
            $this->view->i_pdfreader=$data[$j]['i_pdfreader'];
            $this->view->i_office=$data[$j]['i_office'];
            $this->view->i_kompresi=$data[$j]['i_kompresi'];
            $this->view->i_virus=$data[$j]['i_virus'];
            $this->view->c_website=$data[$j]['c_website'];
            $this->view->n_website=$data[$j]['n_website'];
            $this->view->c_sw_platform=$data[$j]['c_sw_platform'];
            $this->view->c_sw_database=$data[$j]['c_sw_database'];
            $this->view->c_sk=$data[$j]['c_sk'];
            $this->view->c_struktur=$data[$j]['c_struktur'];
            $this->view->c_nemail=$data[$j]['c_nemail'];
            $this->view->n_sw_email=$data[$j]['n_sw_email'];
        }

}


}
public function hardwareAction($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $this->view->datahardware= $this->hardware_serv->getTmHardwareServer($cari);
        $data = $this->hardware_serv->getTmHardware($cari);
        $jmldata = count($data);
if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
            $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
            $this->view->i_proc_pentium3=$data[$j]['i_proc_pentium3'];
            $this->view->i_proc_pentium4=$data[$j]['i_proc_pentium4'];
            $this->view->i_proc_dualcore=$data[$j]['i_proc_dualcore'];
            $this->view->i_proc_coretwoduo=$data[$j]['i_proc_coretwoduo'];
            $this->view->i_memori_1gb=$data[$j]['i_memori_1gb'];
            $this->view->i_memori_2gb=$data[$j]['i_memori_2gb'];
            $this->view->i_hardisk_40gb=$data[$j]['i_hardisk_40gb'];
            $this->view->i_hardisk_50gb=$data[$j]['i_hardisk_50gb'];
            $this->view->i_monitor_17lcd=$data[$j]['i_monitor_17lcd'];
            $this->view->i_monitor_18lcd=$data[$j]['i_monitor_18lcd'];
            $this->view->i_monitor_17nlcd=$data[$j]['i_monitor_17nlcd'];
            $this->view->i_monitor_18nlcd=$data[$j]['i_monitor_18nlcd'];
            $this->view->i_printer=$data[$j]['i_printer'];
            $this->view->i_scanner=$data[$j]['i_scanner'];
            $this->view->i_ups=$data[$j]['i_ups'];
            $this->view->c_cctv=$data[$j]['c_cctv'];
            $this->view->c_fasilitas_tv=$data[$j]['c_fasilitas_tv'];
            $this->view->c_fasilitas_pc=$data[$j]['c_fasilitas_pc'];
            $this->view->c_fasilitas_liflet=$data[$j]['c_fasilitas_liflet'];
            $this->view->c_fasilitas_no=$data[$j]['c_fasilitas_no'];
            $this->view->i_watt=$data[$j]['i_watt'];
            $this->view->c_problem_watt=$data[$j]['c_problem_watt'];

        }
}

}

public function prosedureAction($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $this->view->dataprosedure = $this->prosedure_serv->getTmProsedure($cari);
}

public function anggaranpengembanganAction($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $this->view->datadipa= $this->pengembangan_serv->getTmPengembanganDipa($cari);
        $this->view->datadonor= $this->pengembangan_serv->getTmPengembanganDonor($cari);
        $this->view->datatahunangaran= $this->pengembangan_serv->getTmPengembanganThnang($cari);

}

public function kegiatanAction($id_kuesioner)
{
    $parkegiatan=$_GET['parkegiatan'];
    if (!$parkegiatan){$parkegiatan='1';}
    $cari = " and id_kuesioner ='$id_kuesioner'";
    $this->view->datakegiatan= $this->kegiatan_serv->getTmKegiatan($cari);
     $this->view->datarefkomponen= $this->kegiatan_serv->getTrKomponen();

}

public function deskinfoAction($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $data = $this->deskinfo_serv->getTmDeskinfo($cari);
        $jmldata = count($data);
    if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
                $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
                $this->view->e_layanan=$data[$j]['e_layanan'];
                $this->view->e_layanan_publik=$data[$j]['e_layanan_publik'];
                $this->view->e_staft_pelaksana=$data[$j]['e_staft_pelaksana'];
                $this->view->e_sk_tugas=$data[$j]['e_sk_tugas'];
                $this->view->e_mohon_info=$data[$j]['e_mohon_info'];
                $this->view->e_status_perkara=$data[$j]['e_status_perkara'];
                $this->view->i_entry=$data[$j]['i_entry'];
                $this->view->d_entry=$data[$j]['d_entry'];}
                $this->view->par='Koreksi';
    }
        $databln = $this->deskinfo_serv->getTmDeskinfobulan($cari);
        $jmldatabln = count($databln);
        if (count($databln)!=0){
            for ($j = 0; $j < $jmldatabln; $j++) {
                    $this->view->v_jan=$databln[$j]['v_jan'];
                $this->view->v_feb=$databln[$j]['v_feb'];
                $this->view->v_mar=$databln[$j]['v_mar'];
                $this->view->v_apr=$databln[$j]['v_apr'];
                $this->view->v_mei=$databln[$j]['v_mei'];
                $this->view->v_jun=$databln[$j]['v_jun'];
                $this->view->v_jul=$databln[$j]['v_jul'];
                $this->view->v_agt=$databln[$j]['v_agt'];
                $this->view->v_sep=$databln[$j]['v_sep'];
                $this->view->v_okt=$databln[$j]['v_okt'];
                $this->view->v_nop=$databln[$j]['v_nop'];
                $this->view->v_des=$databln[$j]['v_des'];
            }
        }

}
    public function sdmolahdataAction($id_kuesioner)
    {

        $data= array("idKuesioner" => $id_kuesioner );
        $this->view->dataSDM = $this->sdm_serv->detailSdmTerakhir($data);
    }

public function jaringanAction($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $data = $this->jaringan_serv->getTmJaringan($cari);
        $jmldata = count($data);
    if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
                $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
                $this->view->c_lan_cable=$data[$j]['c_lan_cable'];
                $this->view->c_lan_wireless=$data[$j]['c_lan_wireless'];
                $this->view->c_lan_securty=$data[$j]['c_lan_securty'];
                $this->view->q_lan=$data[$j]['q_lan'];
                $this->view->q_lan_hub_client=$data[$j]['q_lan_hub_client'];
                $this->view->q_lan_nhub_client=$data[$j]['q_lan_nhub_client'];
                $this->view->c_lan_internet=$data[$j]['c_lan_internet'];
                $this->view->q_lan_hub=$data[$j]['q_lan_hub'];
                $this->view->q_pc_internet=$data[$j]['q_pc_internet'];
                $this->view->c_internet_pasca=$data[$j]['c_internet_pasca'];
                $this->view->q_internet_isp=$data[$j]['q_internet_isp'];
                $this->view->v_internet=$data[$j]['v_internet'];
                $this->view->q_internet_upload=$data[$j]['q_internet_upload'];
                $this->view->q_internet_download=$data[$j]['q_internet_download'];
                $this->view->i_entry=$data[$j]['i_entry'];
                $this->view->d_entry=$data[$j]['d_entry'];}
                $this->view->par='Koreksi';
    }

}
public function laporanjsAction()
{
    header('content-type : text/javascript');
    $this->render('laporanjs');
}
}
?>
