<?php
require_once "share/gen_nosurat.php";
require_once "share/globalReferensi.php";
class srtmemo_popup_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

public function getPejabat($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$result = $db->fetchAll("select a.nip,a.nama,a.kd_golongan,c.nm_golongan,c.pangkat,
									a.unit_kerja,b.kd_jabatan,d.nm_jabatan,b.kd_struktur_org,
									e.level,e.nm_level
									from tm_pegawai a,tm_menjabat b,tr_golongan c,tr_jabatan d,
									tr_struktur_organisasi e
									where a.nip=b.nip and a.kd_golongan=c.kd_golongan and 
									b.kd_jabatan=d.kd_jabatan and b.kd_struktur_org=e.kd_struktur_org
									and b.kd_jabatan='2'
									$cari order by b.kd_jabatan asc");
			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
			$data[$j] = array("nip" =>(string)$result[$j]->nip,
							"nama" =>(string)$result[$j]->nama,
							"kd_golongan" =>(string)$result[$j]->kd_golongan,
							"nm_golongan" =>(string)$result[$j]->nm_golongan,
							"pangkat" =>(string)$result[$j]->pangkat,
							"unit_kerja" =>(string)$result[$j]->unit_kerja,
							"kd_jabatan" =>(string)$result[$j]->kd_jabatan,
							"nm_jabatan" =>(string)$result[$j]->nm_jabatan,
							"kd_struktur_org" =>(string)$result[$j]->kd_struktur_org,
							"level" =>(string)$result[$j]->level,
							"nm_level" =>(string)$result[$j]->nm_level);
			}					
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}

	public function maxnumber($modul,$tahun) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$maxnumber = $db->fetchOne("select max(max_number) from gen_no_surat where c_modul='$modul' and tahun='".$tahun."'");
			$maxnumber = $maxnumber;

		     return $maxnumber;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	}	
	
	public function ubahmaxnumber(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();			
	     $ubahdata = array(	"max_number"=>$data['max_number']);		 
		 $where[] = "c_modul = '".$data['c_modul']."' and tahun='".$data['tahun']."'";
	     $db->update('gen_no_surat',$ubahdata, $where);
		 $db->commit();
	     return 'sukses';
		 
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
}
?>

