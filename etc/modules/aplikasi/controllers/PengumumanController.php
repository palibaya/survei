<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_Pengumuman_Service.php";
require_once "service/sso/Sso_User_Service.php";
require_once "service/adm/logfile.php";
require_once "service/sso/Sso_User_Service.php";


class Aplikasi_PengumumanController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
	   		
		$this->pengumuman_serv = Aplikasi_Pengumuman_Service::getInstance();
		
		$this->sso_serv = Sso_User_Service::getInstance();
		$this->Logfile = new logfile;
	    $ssogroup = new Zend_Session_Namespace('ssogroup');
	   if (!$ssogroup->user_id || !$ssogroup->username || !$ssogroup->ipCurrentLogin){
			//empty ip current login
			//===================
			if (getenv("HTTP_X_FORWARDED_FOR")) 
				$ipCurrentLogin = getenv("HTTP_X_FORWARDED_FOR"); 
			else if(getenv("REMOTE_ADDR")) 
				$ipCurrentLogin = getenv("REMOTE_ADDR"); 
				
			$this->sso_serv->emptyIpCurrentLogin($ipCurrentLogin);
			Zend_Session::destroy(true);
			$opt = array();
			//$this->_redirect('http://sipt',$opt);
			?>
			<script>location.href = 'http://<? echo $_SERVER['SERVER_NAME'];?>';</script>
			<?
		}else
		{
			$this->user_id  = $ssogroup->user_id;
			$this->user  = $ssogroup->username;
			$this->kdOrg  = $ssogroup->kd_struktur_org;
			$this->kdJabatan = $ssogroup->kd_jabatan;
			$this->nip = $ssogroup->nip;
			$this->nama = $ssogroup->nama;
			$this->level = $ssogroup->level; 
			$this->nm_level = $ssogroup->nm_level; 
			$this->nm_jabatan = $ssogroup->nm_jabatan; 
			$this->nm_struktur_org = $ssogroup->nm_struktur_org;
			$this->c_group = $ssogroup->c_group ;
			$this->ipCurrentLogin	= $ssogroup->ipCurrentLogin;
		}
    }
	
    public function indexAction() {
	   
    }
	
	public function pengumumanjsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('pengumumanjs');
    }
	
	//test OPen report
	//----------------------
	public function pengumumanlistAction()
	{
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= 'e_pengumuman';
		$katakunciCari 	= $_POST['cari'];
		$sortBy			= 'e_pengumuman';
		$sort			= 'asc';
		
		$dataMasukanTotal = array("pageNumber" 	=> 0,
								"itemPerPage" 	=> 0,
								"kategoriCari" 	=> $kategoriCari,
								"katakunciCari" => $katakunciCari,
								"sortBy" 		=> $sortBy,
								"sort" 			=> $sortOrder);
		$totalPengumumanList = $this->pengumuman_serv->cariPengumumanList($dataMasukanTotal);
		$this->view->totalData = $totalPengumumanList;
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$this->view->currentPage = $pageNumber;
		$this->view->numToDisplay = $itemPerPage;		
		$this->view->pengumumanList = $this->pengumuman_serv->cariPengumumanList($dataMasukan);
	}
	
	public function pengumumanolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iPengumuman = $_REQUEST['iPengumuman'];
		
		$this->view->detailPengumuman = $this->pengumuman_serv->detailPengumumanById($iPengumuman);
	}
	
	public function pengumumanAction()
	{
		$ePengumuman 	= htmlentities($_POST['ePengumuman']);
		echo unhtmlentities($ePengumuman);
		$cStatusaktif 	= $_POST['cStatusaktif'];
		$iEntry 		= $this->user;
		
		echo $ePengumuman;
		$dataMasukan = array("e_pengumuman" => $ePengumuman,
							"c_statusaktif" => $cStatusaktif,
							"i_entry" => $iEntry);
		
		$this->view->pengumumanInsert = $this->pengumuman_serv->pengumumanInsert($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "Pengumuman";
		$this->view->hasil = $hasil;
		
		$this->Logfile->createLog($this->view->namauser, "Insert Pengumuman", $e_pengumuman." (".$ePengumuman.")");
		
		$this->pengumumanlistAction();
		$this->render('pengumumanlist');
	}
	
	public function pengumumanupdateAction()
	{
		$iPengumuman 		= $_POST['iPengumuman'];
		$ePengumuman 		= htmlentities($_POST['ePengumuman']);
		$cStatusaktif 		= $_POST['cStatusaktif'];
		
		$iEntry 		= $this->user;
		
		$dataMasukan = array("i_pengumuman" => $iPengumuman,
							"e_pengumuman" => $ePengumuman,
							"c_statusaktif" => $cStatusaktif,
							"i_entry" => $iEntry);
		
		$this->view->pengumumanUpdate = $this->pengumuman_serv->pengumumanUpdate($dataMasukan);
		$this->view->proses = "2";	
		$this->view->keterangan = "Pengumuman";
		$this->view->hasil = $hasil;
		
		$this->Logfile->createLog($this->view->namauser, "Update Pengumuman", $e_pengumuman." (".$ePengumuman.")");
		$this->pengumumanlistAction();
		$this->render('pengumumanlist');
	}
	
	public function pengumumanhapusAction()
	{
		$iPengumuman 	= $_REQUEST['iPengumuman'];
		$iEntry 		= $this->user;
		
		$dataMasukan = array("i_pengumuman" => $iPengumuman,
							"i_entry" => $iEntry);
		
		$hasil = $this->pengumuman_serv->pengumumanHapus($dataMasukan);
		$this->view->proses = "3";	
		$this->view->keterangan = "Pengumuman";
		$this->view->hasil = $hasil;
		
		$this->Logfile->createLog($this->view->namauser, "Hapus Pengumuman", $e_pengumuman." (".$ePengumuman.")");
		
		$this->pengumumanlistAction();
		$this->render('pengumumanlist');
	}
}
?>