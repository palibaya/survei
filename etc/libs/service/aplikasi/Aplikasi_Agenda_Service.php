<?php
require_once "share/globalReferensi.php";

class Aplikasi_Agenda_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function cariAgendaList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	   
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort ";
			$sqlProses = "select i_agenda,
								i_user_terima,
								i_peg_telponhp,
								n_agenda_pesan,
								e_agenda_pesan,
								i_user_kirim,
								d_agenda,
								d_agenda_jammulai,
								d_agenda_jamakhir,
								c_agenda_type
							from tm_agenda ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses.$where) a";
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$n_user_terima = $globalReferensi->getnamaPegawai($result[$j]->i_user_terima);
				$hasilAkhir[$j] = array("i_agenda"  	=>(string)$result[$j]->i_agenda,
									   "i_user_terima"  	=>(string)$result[$j]->i_user_terima,
									   "n_user_terima" => $n_user_terima,
									   "i_peg_telponhp" 		=>(string)$result[$j]->i_peg_telponhp,
									   "n_agenda_pesan"      		=>(string)$result[$j]->n_agenda_pesan,
									   "e_agenda_pesan"      		=>(string)$result[$j]->e_agenda_pesan,
									   "i_user_kirim"      		=>(string)$result[$j]->i_user_kirim,
									   "d_agenda"      		=>(string)$result[$j]->d_agenda,
									   "d_agenda_jammulai"      		=>(string)$result[$j]->d_agenda_jammulai,
									   "d_agenda_jamakhir"      		=>(string)$result[$j]->d_agenda_jamakhir,
									   "c_agenda_type"      		=>(string)$result[$j]->c_agenda_type
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function daftarPegawai(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	   
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort ";
			$sqlProses = "select nip,
								nama
							from tm_pegawai ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses.$where) a";
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("nip"  	=>(string)$result[$j]->nip,
									   "nama"  	=>(string)$result[$j]->nama
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function agendaInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
										
			$paramInput = array("i_agenda"	=>$dataMasukan['iAgenda'],
							   "i_user_terima"  =>$dataMasukan['iUserTerima'],
							   "i_peg_telponhp"  =>$dataMasukan['iPegTelponhp'],
							   "n_agenda_pesan"  =>$dataMasukan['nAgendaPesan'],
							   "e_agenda_pesan"  =>$dataMasukan['eAgendaPesan'],
							   "i_user_kirim"  =>$dataMasukan['iUserKirim'],
							   "d_agenda"  =>$dataMasukan['dAgenda'],
							   "d_agenda_jammulai"  =>$dataMasukan['dAgendaJammulai'],
							   "d_agenda_jamakhir"  =>$dataMasukan['dAgendaJamakhir'],
							   "c_agenda_type"  =>$dataMasukan['cAgendaType'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>new Zend_Db_Expr('NOW()'));	

							   //var_dump($paramInput);
			$db->insert('tm_agenda',$paramInput);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function detailAgendaById($iAgenda) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where i_agenda = '$iAgenda' ";
			$sqlProses = "select i_agenda,
								i_user_terima,
								i_peg_telponhp,
								n_agenda_pesan,
								e_agenda_pesan,
								i_user_kirim,
								d_agenda,
								d_agenda_jammulai,
								d_agenda_jamakhir,
								c_agenda_type
							from tm_agenda ";

			$sqlData = $sqlProses.$where;					
			
			$result = $db->fetchRow($sqlData);
			$globalReferensi = new globalReferensi();
			$n_user_terima = $globalReferensi->getnamaPegawai($result->i_user_terima);	
			$n_user_kirim = $globalReferensi->getnamaPegawai($result->i_user_kirim);				
			$hasilAkhir = array("i_agenda"  	=>(string)$result->i_agenda,
							   "i_user_terima"  	=>(string)$result->i_user_terima,
							   "n_user_terima" => $n_user_terima,
							   "i_peg_telponhp" 		=>(string)$result->i_peg_telponhp,
							   "n_agenda_pesan"      		=>(string)$result->n_agenda_pesan,
							   "e_agenda_pesan"      		=>(string)$result->e_agenda_pesan,
							   "i_user_kirim"      		=>(string)$result->i_user_kirim,
							   "n_user_kirim"      		=>$n_user_kirim,
							   "d_agenda"      		=>(string)$result->d_agenda,
							   "d_agenda_jammulai"      		=>(string)$result->d_agenda_jammulai,
							   "d_agenda_jamakhir"      		=>(string)$result->d_agenda_jamakhir,
							   "c_agenda_type"      		=>(string)$result->c_agenda_type
								);
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function agendaUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
										
			$paramInput = array("i_user_terima"  =>$dataMasukan['iUserTerima'],
							   "i_peg_telponhp"  =>$dataMasukan['iPegTelponhp'],
							   "n_agenda_pesan"  =>$dataMasukan['nAgendaPesan'],
							   "e_agenda_pesan"  =>$dataMasukan['eAgendaPesan'],
							   "i_user_kirim"  =>$dataMasukan['iUserKirim'],
							   "d_agenda"  =>$dataMasukan['dAgenda'],
							   "d_agenda_jammulai"  =>$dataMasukan['dAgendaJammulai'],
							   "d_agenda_jamakhir"  =>$dataMasukan['dAgendaJamakhir'],
							   "c_agenda_type"  =>$dataMasukan['cAgendaType'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>new Zend_Db_Expr('NOW()'));	
			$where[] = "i_agenda = '".$dataMasukan['iAgendaHidden']."'";
			
			//var_dump($paramInput);
			//echo "<br>";
			//var_dump($where);
			
			$db->update('tm_agenda',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function agendaHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>new Zend_Db_Expr('NOW()'));	
								
			$where[] = "i_agenda = '".$dataMasukan['i_agenda']."'";
			
			$db->update('tm_agenda',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function tulisEvent($nip) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null";
			
			$sqlProses = "select i_agenda, d_agenda,
								i_user_terima,
								n_agenda_pesan,
								e_agenda_pesan
							from tm_agenda 
							where (c_statusdelete != 'Y' or c_statusdelete is null)
								and i_user_terima = '$nip'";	

			$sqlData = $sqlProses;
			$result = $db->fetchAll($sqlData);	
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			$myFile = $this->baseData."/event.txt";
			if (file_exists($myFile)) {
				unlink($myFile);
			}
			$fh = fopen($myFile, 'w') or die("can't open file");
			//echo "myFile = $myFile";
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$n_user_terima = $globalReferensi->getnamaPegawai($result[$j]->i_user_terima);
				$hasilAkhir[$j] = array("i_agenda"  	=>(string)$result[$j]->i_agenda,
									   "d_agenda"  	=>(string)$result[$j]->d_agenda,
									   "i_user_terima"  	=>(string)$result[$j]->i_user_terima,
									   "n_user_terima" => $n_user_terima,
									   "n_agenda_pesan" 		=>(string)$result[$j]->n_agenda_pesan,
									   "e_agenda_pesan"      		=>(string)$result[$j]->e_agenda_pesan
										);
				$tglarr = explode('-', $result[$j]->d_agenda);
				
				$hari = ($tglarr[2]*1);
				$bulan = ($tglarr[1]*1);
				$tahun = $tglarr[0];
				
				$stringData = "$hari/$bulan/$tahun".";;"."<a href=".'\"#\"'."onClick=".'\"'."popup('".$result[$j]->i_agenda."')".'\"'.">".$result[$j]->e_agenda_pesan."</a>;;\n";
				fwrite($fh, $stringData);
				//echo "$stringData <br>";
				
			}	
			fclose($fh);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

/* public function getCariData($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$result = $db->fetchAll("select a.i_agendasrt,a.n_srtpengirim,b.n_kepada,a.d_tglpengirim,
						a.n_srtperihal,a.c_srtjenis,a.c_srtsifat,a.i_agendalalusrt,a.d_tgldariwaseskab
						from tm_surat_masuk a, tm_suratkepada b
					where a.i_agendasrt=b.id_srtmasuk $cari");
					
			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
			
			
			$data[$j] = array("i_agendasrt" =>(string)$result[$j]->i_agendasrt,
							"n_srtpengirim" =>(string)$result[$j]->n_srtpengirim,
							"n_kepada" =>(string)$result[$j]->n_kepada,
							"d_tglpengirim" =>(string)$result[$j]->d_tglpengirim,
							"n_srtperihal" =>(string)$result[$j]->n_srtperihal,
							"c_srtjenis" =>(string)$result[$j]->c_srtjenis,
							"c_srtsifat" =>(string)$result[$j]->c_srtsifat,
							"i_agendalalusrt" =>(string)$result[$j]->i_agendalalusrt,
							"d_tgldariwaseskab" =>(string)$result[$j]->d_tgldariwaseskab);
			}					
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
public function getCariData2($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$result = $db->fetchAll("select a.i_agendasrt,a.n_srtpengirim,b.n_kepada,a.d_tglpengirim,
						a.n_srtperihal,a.c_srtjenis,a.c_srtsifat,a.i_agendalalusrt,a.d_tgldariwaseskab
						from tm_surat_masuk a, tm_suratkepada b
						where a.i_agendasrt=b.id_srtmasuk $cari");

			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
		
			$data[$j] = array("i_agendasrt" =>(string)$result[$j]->i_agendasrt,
							"n_srtpengirim" =>(string)$result[$j]->n_srtpengirim,
							"n_kepada" =>(string)$result[$j]->n_kepada,
							"d_tglpengirim" =>(string)$result[$j]->d_tglpengirim,
							"n_srtperihal" =>(string)$result[$j]->n_srtperihal);
			}					
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	} */

}
?>