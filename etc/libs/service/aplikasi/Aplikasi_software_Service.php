<?php
class aplikasi_software_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

	public function getTmSoftware($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,i_xp,i_vista,i_seven,i_linux,i_dos,
										i_ext,i_browser,i_pdfreader,i_office,
										i_kompresi,i_virus,c_website,n_website,c_sw_platform,
										c_sw_database,c_sk,c_struktur,c_nemail,n_sw_email
										from tm_software where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"i_xp"=>(string)$result[$j]->i_xp,
									"i_vista"=>(string)$result[$j]->i_vista,
									"i_seven"=>(string)$result[$j]->i_seven,
									"i_linux"=>(string)$result[$j]->i_linux,
									"i_dos"=>(string)$result[$j]->i_dos,
									"i_ext"=>(string)$result[$j]->i_ext,
									"i_browser"=>(string)$result[$j]->i_browser,
									"i_pdfreader"=>(string)$result[$j]->i_pdfreader,
									"i_office"=>(string)$result[$j]->i_office,
									"i_kompresi"=>(string)$result[$j]->i_kompresi,
									"i_virus"=>(string)$result[$j]->i_virus,
									"c_website"=>(string)$result[$j]->c_website,
									"n_website"=>(string)$result[$j]->n_website,
									"c_sw_platform"=>(string)$result[$j]->c_sw_platform,
									"c_sw_database"=>(string)$result[$j]->c_sw_database,
									"c_sk"=>(string)$result[$j]->c_sk,
									"c_struktur"=>(string)$result[$j]->c_struktur,
									"c_nemail"=>(string)$result[$j]->c_nemail,
									"n_sw_email"=>(string)$result[$j]->n_sw_email);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}

	public function getTmSoftwareAdm($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,c_adm,n_aplikasi,c_platform,c_database,c_jns_aplikasi
										from tm_software_adm where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"c_adm"=>(string)$result[$j]->c_adm,
									"n_aplikasi"=>(string)$result[$j]->n_aplikasi,
									"c_platform"=>(string)$result[$j]->c_platform,
									"c_database"=>(string)$result[$j]->c_database,
									"c_jns_aplikasi"=>(string)$result[$j]->c_jns_aplikasi);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}	
	
	public function getTmSoftwareKendala($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,n_aplikasi_kendala,n_kendala
										from tm_software_kendala where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"n_aplikasi_kendala"=>(string)$result[$j]->n_aplikasi_kendala,
									"n_kendala"=>(string)$result[$j]->n_kendala);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}		
	
	public function tambahTmSoftware(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"i_xp"=>$data['i_xp'],
							"i_vista"=>$data['i_vista'],
							"i_seven"=>$data['i_seven'],
							"i_linux"=>$data['i_linux'],
							"i_dos"=>$data['i_dos'],
							"i_ext"=>$data['i_ext'],
							"i_browser"=>$data['i_browser'],
							"i_pdfreader"=>$data['i_pdfreader'],
							"i_office"=>$data['i_office'],
							"i_kompresi"=>$data['i_kompresi'],
							"i_virus"=>$data['i_virus'],
							"c_website"=>$data['c_website'],
							"n_website"=>$data['n_website'],
							"c_sw_platform"=>$data['c_sw_platform'],
							"c_sw_database"=>$data['c_sw_database'],
							"c_sk"=>$data['c_sk'],
							"c_struktur"=>$data['c_struktur'],
							"c_nemail"=>$data['c_nemail'],
							"n_sw_email"=>$data['n_sw_email'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_software',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	

	
	public function ubahTmSoftware(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array("i_xp"=>$data['i_xp'],
							"i_vista"=>$data['i_vista'],
							"i_seven"=>$data['i_seven'],
							"i_linux"=>$data['i_linux'],
							"i_dos"=>$data['i_dos'],
							"i_ext"=>$data['i_ext'],
							"i_browser"=>$data['i_browser'],
							"i_pdfreader"=>$data['i_pdfreader'],
							"i_office"=>$data['i_office'],
							"i_kompresi"=>$data['i_kompresi'],
							"i_virus"=>$data['i_virus'],
							"c_website"=>$data['c_website'],
							"n_website"=>$data['n_website'],
							"c_sw_platform"=>$data['c_sw_platform'],
							"c_sw_database"=>$data['c_sw_database'],
							"c_sk"=>$data['c_sk'],
							"c_struktur"=>$data['c_struktur'],
							"c_nemail"=>$data['c_nemail'],
							"n_sw_email"=>$data['n_sw_email'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));

		$db->update('tm_software',$ubah_data, "id_kuesioner = '".trim($data['id_kuesioner'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}


	public function tambahTmSoftwareAdm(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							/* "id_pengadilan"=>$data['id_pengadilan'],
							"d_bulan_survey"=>$data['d_bulan_survey'],
							"d_tahun_survey"=>$data['d_tahun_survey'], */
							"c_adm"=>$data['c_adm'],
							"n_aplikasi"=>$data['n_aplikasi'],
							"c_platform"=>$data['c_platform'],
							"c_database"=>$data['c_database'],
							"c_jns_aplikasi"=>$data['c_jns_aplikasi'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_software_adm',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	

	
	public function ubahTmSoftwareAdm(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array("n_aplikasi"=>$data['n_aplikasi'],
							"c_platform"=>$data['c_platform'],
							"c_database"=>$data['c_database'],
							"c_jns_aplikasi"=>$data['c_jns_aplikasi'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));
		$db->update('tm_software_adm',$ubah_data, "id_kuesioner= '".trim($data['id_kuesioner'])."'  and c_adm= '".trim($data['c_adm'])."' and n_aplikasi= '".trim($data['n_aplikasi2'])."'");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	public function hapusTmSoftware(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'  ";
		$db->delete('tm_software', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}
	public function hapusTmSoftwareAdm(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."' ";
		$db->delete('tm_software_adm', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}
	public function hapusTmSoftwareAdm2(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'  and c_adm = '".trim($data['c_adm'])."' ";
		$db->delete('tm_software_adm', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
	public function tambahTmSoftwareKendala(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"n_aplikasi_kendala"=>$data['n_aplikasi_kendala'],
							"n_kendala"=>$data['n_kendala'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_software_kendala',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function ubahTmSoftwareKendala(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array("n_kendala"=>$data['n_kendala'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));

		$db->update('tm_software_kendala',$ubah_data, "id_kuesioner= '".trim($data['id_kuesioner'])."'  and n_aplikasi_kendala= '".trim($data['n_aplikasi_kendala'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}

	public function hapusTmSoftwareKendala(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'  ";
		$db->delete('tm_software_kendala', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
}	   

	public function getTmPengadilan($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select i_organisasi ,n_organisasi,i_organisasi_parent,c_kategori_organisasi ,n_alamat ,no_telepon ,no_fax
										from tm_organisasi where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_pengadilan"=>(string)$result[$j]->i_organisasi,
										"n_pengadilan"=>(string)$result[$j]->n_organisasi,
										"id_pengadilan_banding"=>(string)$result[$j]->i_organisasi_parent,
										"c_kategori_pengadilan"=>(string)$result[$j]->c_kategori_organisasi,
										"n_alamat"=>(string)$result[$j]->n_alamat,
										"no_telepon"=>(string)$result[$j]->no_telepon,
										"no_fax"=>(string)$result[$j]->no_fax);}
										


	
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}

	public function getTmKuesioner($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id,id_pengadilan,d_bulan_kuesioner,d_tahun_kuesioner,n_responden,n_jabatan,n_pangkat,
										n_golongan,no_telepon_responden,no_hp_responden
										from tm_kuesioner where 1=1 $cari ");
								
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id"=>(string)$result[$j]->id,
									"id_pengadilan"=>(string)$result[$j]->id_pengadilan,
									"d_bulan_kuesioner"=>(string)$result[$j]->d_bulan_kuesioner,
									"d_tahun_kuesioner"=>(string)$result[$j]->d_tahun_kuesioner,
									"n_responden"=>(string)$result[$j]->n_responden,
									"n_jabatan"=>(string)$result[$j]->n_jabatan,
									"n_pangkat"=>(string)$result[$j]->n_pangkat,
									"n_golongan"=>(string)$result[$j]->n_golongan,
									"no_telepon_responden"=>(string)$result[$j]->no_telepon_responden,
									"no_hp_responden"=>(string)$result[$j]->no_hp_responden);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}

	public function getTmKuesioner2($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id as id_kuesioner,id_pengadilan,d_bulan_kuesioner,d_tahun_kuesioner,n_responden,n_jabatan,n_pangkat,n_golongan,
										no_telepon_responden,no_hp_responden,n_organisasi,i_organisasi_parent,no_fax,n_alamat
										from tm_kuesioner  a, tm_organisasi b where id_pengadilan=i_organisasi $cari ");
					
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{
						$i_organisasi_parent=(string)$result[$j]->i_organisasi_parent;
						if ($i_organisasi_parent)
						{$n_organisasi_tk1 = $db->fetchOne("select n_organisasi from tm_organisasi where i_organisasi='$i_organisasi_parent'");}
					$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"id_pengadilan"=>(string)$result[$j]->id_pengadilan,
									"d_bulan_kuesioner"=>(string)$result[$j]->d_bulan_kuesioner,
									"d_tahun_kuesioner"=>(string)$result[$j]->d_tahun_kuesioner,
									"n_responden"=>(string)$result[$j]->n_responden,
									"n_jabatan"=>(string)$result[$j]->n_jabatan,
									"n_pangkat"=>(string)$result[$j]->n_pangkat,
									"n_golongan"=>(string)$result[$j]->n_golongan,
									"no_fax"=>(string)$result[$j]->no_fax,
									"n_alamat"=>(string)$result[$j]->n_alamat,
									"no_telepon_responden"=>(string)$result[$j]->no_telepon_responden,
									"no_hp_responden"=>(string)$result[$j]->no_hp_responden,
									"n_organisasi"=>(string)$result[$j]->n_organisasi,
									"n_organisasi_tk1"=>$n_organisasi_tk1);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	
}
?>
