<?php
class Aplikasi_Pengumuman_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function cariPengumumanList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	   
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null)";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort ";
			$sqlProses = "select i_pengumuman,
								e_pengumuman,
								c_statusaktif,
								i_entry,
								d_entry
							from tm_pengumuman ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses $where) a";
				//echo $sqlTotal;
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			
			
			
			$jmlResult = count($result);
			
			$hasilAkhir = array();
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("i_pengumuman"  	=>(string)$result[$j]->i_pengumuman,
									   "e_pengumuman"  	=>(string)$result[$j]->e_pengumuman,
									   "c_statusaktif" 		=>(string)$result[$j]->c_statusaktif,
									   "i_entry"      		=>(string)$result[$j]->i_entry,
									   "d_entry"      		=>(string)$result[$j]->d_entry
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function pengumumanInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("e_pengumuman"	=>$dataMasukan['e_pengumuman'],
							   "c_statusaktif"  =>$dataMasukan['c_statusaktif'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));		
			$db->insert('tm_pengumuman',$paramInput);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function detailPengumumanById($iPengumuman) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where i_pengumuman = '$iPengumuman' ";
			$sqlProses = "select i_pengumuman,
								e_pengumuman,
								c_statusaktif,
								i_entry,
								d_entry
							from tm_pengumuman ";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			$hasilAkhir = array("i_pengumuman"  	=>(string)$result->i_pengumuman,
								   "e_pengumuman"  	=>(string)$result->e_pengumuman,
								   "c_statusaktif" 		=>(string)$result->c_statusaktif,
								   "i_entry"      		=>(string)$result->i_entry,
								   "d_entry"      		=>(string)$result->d_entry
									);
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function pengumumanUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("e_pengumuman"	=>$dataMasukan['e_pengumuman'],
							   "c_statusaktif"  =>$dataMasukan['c_statusaktif'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));	
								
			$where[] = "i_pengumuman = '".$dataMasukan['i_pengumuman']."'";
			
			$db->update('tm_pengumuman',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function pengumumanHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));	
								
			$where[] = "i_pengumuman = '".$dataMasukan['i_pengumuman']."'";
			
			$db->update('tm_pengumuman',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
}
?>