<?php
require_once 'share/globalReferensi.php';

class Aplikasi_Referensi_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pejabat
	//======================================================================
	public function getNamaDanJabatanSetkabList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select b.nip,
								b.nama,
								b.kd_jabatan,
								a.kd_struktur_org
							from simpeg_internal.tbl_struktur_organisasi a 
							left join simpeg_internal.v_jab_peg b on (a.kd_struktur_org = b.kd_struktur_org)";	
							
			
			$sqlData = $sqlProses; //.$where;
			
			//echo $sqlData;
			$result = $db->fetchAll($sqlData);				
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$nmJabatan = $globalRef->getNamaJabatan($result[$j]->kd_jabatan);
				$nm_struktur_org = $globalRef->getNamaOrganisasi($result[$j]->kd_struktur_org);
				
				if((strtolower($nmJabatan) == 'deputi') || (strtolower($nmJabatan) == 'wakil sekretaris kabinet') || (!$result[$j]->kd_jabatan)){
					$nm_jabatan_lengkap = $nm_struktur_org;
				} else {
					$nm_jabatan_lengkap = $nmJabatan." ".$nm_struktur_org;
				}
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama"				=> (string)$result[$j]->nama,
										"kd_jabatan"  		=>(string)$result[$j]->kd_jabatan,
									    "kd_struktur_org"  	=>(string)$result[$j]->kd_struktur_org,
										"nm_struktur_org" 	=> $nm_struktur_org,
									    "nmJabatan"			=> $nmJabatan,
									    "nm_jabatan_lengkap"=> $nm_jabatan_lengkap 
										); 
				
				
			}	
			//var_dump($hasilAkhir);	
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	//List Instansi
	//===================
	public function getInstansiList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select a.nm_instansi
							from simpeg_internal.tbl_instansi a";	
							
			
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchAll($sqlData);				
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("nm_instansi"  		=>(string)$result[$j]->nm_instansi
										); 
				//var_dump($hasilAkhir);	
				
			}	
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function isNamaPegawai($nPeg) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select count(*) jml
							from v_pegawai a
							where trim(a.nama) = trim('$nPeg')";	
							
			
			$sqlData = $sqlProses; 
			$jumlah = $db->fetchOne($sqlData);				
			
			return $jumlah;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getnamaPegawai($nip) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select a.nama
							from v_pegawai a
							where nip = '$nip'";	
			
			$sqlData = $sqlProses; 
			$nama = $db->fetchOne($sqlData);				
			
			return $nama;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	public function getjabatanPegawai($nip) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select nm_jabatan, level, nm_level
							from simpeg_internal.v_jab_peg a
							where nip = '$nip' and DATE_FORMAT(tgl_pemberhentian, '%Y-%m-%d') = '0000-00-00'";	
							
			/* echo "select nm_jabatan, level, nm_level
							from simpeg_internal.v_jab_peg a
							where nip = '$nip' and DATE_FORMAT(tgl_pemberhentian, '%Y-%m-%d') = '0000-00-00'"; */
			$sqlData = $sqlProses; 
			$hasil = $db->fetchRow($sqlData);				
			$nm_jabatan = $hasil->nm_jabatan;
			$level = $hasil->level;
			$nm_level = $hasil->nm_level;
			
			if ((strtoupper($nm_jabatan) == 'WAKIL SEKRETARIS KABINET') || (strtoupper($nm_jabatan) == 'DEPUTI')){
				$nama = $level.' '.$nm_level; 
			} else {
				$nama = $nm_jabatan.' '.$level.' '.$nm_level; 
			}
			return $nama;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	public function getSatuanLampiranList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select id, nm_satuanlampiran
							from tr_satuanlampiran
							where c_statusdelete != 'Y'";	
							
			
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchAll($sqlData);				
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("id_satuanlampiran"  		=>(string)$result[$j]->id,
										"nm_satuanlampiran"  		=>(string)$result[$j]->nm_satuanlampiran); 	
			}	
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getAtasan($kdOrg, $kdJabatan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select id_riwayat_jabatan, kd_struktur_org, kd_jabatan, nama, nip, kd_jabatan_induk
						from simpeg_internal.v_r_jabatan_peg
						where id_riwayat_jabatan in 
								(select kd_jabatan_induk from simpeg_internal.v_r_jabatan_peg where kd_jabatan = '$kdJabatan' and kd_struktur_org = '$kdOrg')";	
							
			
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchRow($sqlData);				
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			$hasilAkhir = array("kd_struktur_org_induk" =>(string)$result->kd_struktur_org,
									"kd_jabatan_induk"  =>(string)$result->kd_jabatan,
									"nama"				=>(string)$result->nama,
									"nip"				=>(string)$result->nip); 	
			
			//var_dump($hasilAkhir);
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getNipList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select a.nip
							from v_pegawai a";	
							
			
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchAll($sqlData);				
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("nip"  		=>(string)$result[$j]->nip
										); 
				//var_dump($hasilAkhir);	
				
			}	
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianUnitKerja(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    if ($kategoriCari == 'nm_level'){$kategoriCari = 'a.nm_level';}
			else if ($kategoriCari == 'nama'){$kategoriCari = 'b.nama';}
			
		    $whereBase = "where a.kd_struktur_org_induk <= 10 and (b.kd_jabatan in ('1','2','4','8','9','10') OR b.kd_jabatan is null)";
			$whereOpt = "UPPER($kategoriCari) like '%$kataKunci%' ";
			if ($kategoriCari) {
				$where = $whereBase." and ".$whereOpt;
			} else {
				$where = $whereBase;
			}
			
			$orderBy = 'order by a.kd_struktur_org asc';
			
			$sqlProses = "select distinct a.kd_struktur_org, 
								a.kd_struktur_org_induk, 
								a.level, 
								a.nm_level,
								a.tgl_dibentuk, 
								a.tgl_dibubarkan,
								b.nip,
								b.nama,
								b.kd_jabatan
						from simpeg_internal.tbl_struktur_organisasi a
						left join simpeg_internal.v_r_jabatan_peg b on (a.kd_struktur_org = b.kd_struktur_org)";	
						
			/* SELECT distinct a.kd_struktur_org, a.kd_struktur_org_induk, a.level, a.nm_level, a.tgl_dibentuk,
       a.tgl_dibubarkan,b.kd_jabatan
  FROM simpeg_internal.tbl_struktur_organisasi a
	left join v_r_jabatan_peg b on (a.kd_struktur_org = b.kd_struktur_org)
	where a.kd_struktur_org_induk < 11 and b.kd_jabatan in ('1','2','4','8','9','10')			 */

			$sqlData = $sqlProses.$where.$orderBy;
			
			//echo $sqlData;
			$result = $db->fetchAll($sqlData);	
					
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				
				$hasilAkhir[$j] = array("kd_struktur_org"  		=>(string)$result[$j]->kd_struktur_org,
										"kd_struktur_org_induk" =>(string)$result[$j]->kd_struktur_org_induk,
										"level" 				=>(string)$result[$j]->level,
									    "nm_level"      		=>(string)$result[$j]->nm_level,
									    "tgl_dibentuk"      	=>(string)$result[$j]->tgl_dibentuk,
									    "tgl_dibubarkan"      	=>(string)$result[$j]->tgl_dibubarkan,
									    "nip"			      	=>(string)$result[$j]->nip,
									    "nama"      			=>(string)$result[$j]->nama,
									    "kd_jabatan"      		=>(string)$result[$j]->kd_jabatan
									    //"nm_jabatan"      		=>(string)$result[$j]->nm_jabatan
										);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianDepLpnd(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    $sqlProses = "select a.departemen
							from
							(
							select concat(level ,' ',nm_level) as departemen
							from simpeg_internal.tbl_struktur_organisasi
							union
							select simpeg_internal.tbl_instansi.nm_instansi as departemen
							from simpeg_internal.tbl_instansi
							) a
							where UPPER(a.departemen) like '%$kataKunci%' limit 20 offset 1";	

			$result = $db->fetchAll($sqlProses);	
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("departemen"  		=>(string)$result[$j]->departemen);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianPejabat(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    if ($kategoriCari == 'nm_level'){$kategoriCari = 'a.nm_level';}
			else if ($kategoriCari == 'nama'){$kategoriCari = 'b.nama';}
			
		    //$whereBase = "where a.kd_struktur_org_induk <= 10";
			$whereOpt = "where UPPER($kategoriCari) like '%$kataKunci%' ";
			if ($kategoriCari) {
				$where = $whereOpt;
			} else {
				$where = '';
			}
			
			$sqlProses = "select a.kd_struktur_org, 
								a.kd_struktur_org_induk, 
								a.level, 
								a.nm_level,
								a.tgl_dibentuk, 
								a.tgl_dibubarkan,
								b.nip,
								b.nama,
								b.kd_jabatan,
								c.nm_jabatan
						from simpeg_internal.tbl_struktur_organisasi a
						left join simpeg_internal.v_r_jabatan_peg b on (a.kd_struktur_org = b.kd_struktur_org)
						left join simpeg_internal.tbl_jabatan c on (b.kd_jabatan = c.kd_jabatan)";	

			$sqlData = $sqlProses.$where." limit 20 offset 0";
			//echo $sqlData;
			$result = $db->fetchAll($sqlData);	
					
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				
				$hasilAkhir[$j] = array("kd_struktur_org"  		=>(string)$result[$j]->kd_struktur_org,
										"kd_struktur_org_induk" =>(string)$result[$j]->kd_struktur_org_induk,
										"level" 				=>(string)$result[$j]->level,
									    "nm_level"      		=>(string)$result[$j]->nm_level,
									    "tgl_dibentuk"      	=>(string)$result[$j]->tgl_dibentuk,
									    "tgl_dibubarkan"      	=>(string)$result[$j]->tgl_dibubarkan,
									    "nip"			      	=>(string)$result[$j]->nip,
									    "nama"      			=>(string)$result[$j]->nama,
									    "kd_jabatan"      		=>(string)$result[$j]->kd_jabatan,
									    "nm_jabatan"      		=>(string)$result[$j]->nm_jabatan
										);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getKodeSurat($kdOrg) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    $sqlProses = "select kd_surat
						from tr_kode_surat
						where kd_struktur_org = '$kdOrg'";	

			$kd_surat = $db->fetchOne($sqlProses);	
					
			return $kd_surat;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianPegawai(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		//var_dump($dataMasukan);
		$kategoriCari 		= $dataMasukan['kategoriCari'];
		$katakunciCari 		= strToUpper($dataMasukan['katakunciCari']);
		$kdOrg 				= $dataMasukan['kdOrg'];
		$kdJabatan 			= $dataMasukan['kdJabatan'];
		$level 				= $dataMasukan['level'];
		$idRiwayatJabatan 	= $dataMasukan['idRiwayatJabatan'];
		$kdStrukturOrgInduk = $dataMasukan['kdStrukturOrgInduk'];
		
		//echo "$kdOrg | $kdJabatan | $level";
		if (($kdJabatan == '9') || ($kdJabatan == '8')) {
			$result = $this->daftarDeputi();
			$jmlResult = count($result);
		//var_dump($pegSetkab);
			
		} else if ($kdJabatan == '1'){
			$result = $this->daftarBiro($kdOrg);
			$jmlResult = count($result);
		//var_dump($pegSetkab);
			
		} else if ($kdJabatan == '2'){
		
		//echo "jabatan = $kdJabatan ";
			if ($level == 'Biro'){
				$result = $this->daftarBagian($kdOrg);
				$jmlResult = count($result);
			} else if ($level == 'Bagian'){
				$result = $this->daftarSubbagian($kdOrg);
				$jmlResult = count($result);				
			} else if ($level == 'Subbagian'){
				$result = $this->daftarPelaksana($kdOrg, $idRiwayatJabatan);
				$jmlResult = count($result);
			}  			
		} 
		
		
		for ($j = 0; $j < $jmlResult; $j++) {
			//if ($result[$j]['kd_jabatan'] != $kdJabatan){
				$jabatanPeg = $this->getjabatanPegawai($result[$j]['nip']);
				$hasilAkhir[$j] = array("nip"  				=>$result[$j]['nip'],
										"nama" 				=>$result[$j]['nama'],
										"kd_jabatan" 		=>$result[$j]['kd_jabatan'],
										"kd_struktur_org"   =>$result[$j]['kd_struktur_org'],
										"jabatanPeg"		=> $jabatanPeg
										);				
			//}
		}	
		return $hasilAkhir;
	}
	
	public function daftarDeputi(){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		$kdJabatan 		= strToUpper($dataMasukan['kdJabatan']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		   $sqlProses = "select a.nip, 
								a.nama, 
								a.kd_jabatan, 
								a.kd_struktur_org,
								concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level) as jabatan,
								a.id_riwayat_jabatan,
								a.kd_struktur_org_induk
						from simpeg_internal.v_jab_peg a 
						where a.kd_struktur_org_induk = '0' 
						order by a.kd_struktur_org";

			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);	
			
			$jmlResult = count($result);
			$angkaTerakhir = $jmlResult;
			for ($j = 0; $j < $jmlResult; $j++) {
			
				$jabatanPeg = $this->getjabatanPegawai($result[$j]->nip);
				
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama" 				=>(string)$result[$j]->nama,
										"kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
										"kd_struktur_org"   =>(string)$result[$j]->kd_struktur_org,
										"jabatanPeg"		=> $jabatanPeg,
										"id_riwayat_jabatan"=> (string)$result[$j]->id_riwayat_jabatan,
										"kd_struktur_org_induk"=> (string)$result[$j]->kd_struktur_org_induk
										);
				
												
				if ($result[$j]->kd_struktur_org != '2'){
					$biro = $this->daftarBiro($result[$j]->kd_struktur_org);
					
				    $jmlBiro = count($biro);
					//echo "<br>jmlBiro = $jmlBiro | $angkaTerakhir<br>";
					$b= $angkaTerakhir;
					for ($a=0; $a<$jmlBiro; $a++){
						
						//echo "$b<br>";
						$jabatanPeg = $this->getjabatanPegawai($biro[$a]['nip']);
						$hasilAkhir[$b] = array("nip"  				=>(string)$biro[$a]['nip'],
												"nama" 				=>(string)$biro[$a]['nama'],
												"kd_jabatan" 		=>(string)$biro[$a]['kd_jabatan'],
											    "kd_struktur_org"   =>(string)$biro[$a]['kd_struktur_org'],
												"jabatanPeg"		=> $jabatanPeg,
												"id_riwayat_jabatan"=> (string)$biro[$a]->id_riwayat_jabatan,
												"kd_struktur_org_induk"=> (string)$biro[$a]->kd_struktur_org_induk
												);
						$b++;
					}
					$angkaTerakhir = $angkaTerakhir+$jmlBiro;
					
					unset($biro);
				}
				
			}	
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function daftarBiro($kdOrgDeputi){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		   $sqlProses = "select a.nip, 
								a.nama, 
								a.kd_jabatan, 
								a.kd_struktur_org,
								concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level) as jabatan,
								a.id_riwayat_jabatan,
								a.kd_struktur_org_induk
						from simpeg_internal.v_jab_peg a 
						where a.kd_struktur_org_induk = '$kdOrgDeputi'
						order by  a.kd_jabatan, a.kd_struktur_org";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);	
			
			$jmlResult = count($result);
			$angkaTerakhir = $jmlResult;
			for ($j = 0; $j < $jmlResult; $j++) {
				$jabatanPeg = $this->getjabatanPegawai($result[$j]->nip);
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama" 				=>(string)$result[$j]->nama,
										"kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
									    "kd_struktur_org"   =>(string)$result[$j]->kd_struktur_org,
										"jabatanPeg"		=> $jabatanPeg,
										"id_riwayat_jabatan"=> (string)$result[$j]->id_riwayat_jabatan,
										"kd_struktur_org_induk"=> (string)$result[$j]->kd_struktur_org_induk
										);		

				if ($result[$j]->kd_jabatan == '2') {
					$bagian = $this->daftarBagian($result[$j]->kd_struktur_org);
						
					$jmlBagian = count($bagian);
					//echo "<br>jmlBiro = $jmlBiro | $angkaTerakhir<br>";
					$b= $angkaTerakhir;
					for ($a=0; $a<$jmlBagian; $a++){
						
						//echo "$b<br>";
						$jabatanPeg = $this->getjabatanPegawai($bagian[$a]['nip']);
						$hasilAkhir[$b] = array("nip"  				=>(string)$bagian[$a]['nip'],
												"nama" 				=>(string)$bagian[$a]['nama'],
												"kd_jabatan" 		=>(string)$bagian[$a]['kd_jabatan'],
												"kd_struktur_org"   =>(string)$bagian[$a]['kd_struktur_org'],
												"jabatanPeg"		=> $jabatanPeg,
												"id_riwayat_jabatan"=> (string)$bagian[$a]->id_riwayat_jabatan,
												"kd_struktur_org_induk"=> (string)$bagian[$a]->kd_struktur_org_induk
												);
						$b++;
					}
					$angkaTerakhir = $angkaTerakhir+$jmlBagian;
					
					unset($bagian);
				}
			}	
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function daftarBagian($kdOrgBiro){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		   $sqlProses = "select a.nip, 
								a.nama, 
								a.kd_jabatan, 
								a.kd_struktur_org,
								concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level) as jabatan,
								a.id_riwayat_jabatan,
								a.kd_struktur_org_induk
						from simpeg_internal.v_jab_peg a 
						where a.kd_struktur_org_induk = '$kdOrgBiro'
						order by  a.kd_jabatan, a.kd_struktur_org";	
			//echo $sqlProses;
			
			$result = $db->fetchAll($sqlProses);	
			//var_dump($result);
			$jmlResult = count($result);
			$angkaTerakhir = $jmlResult;
			for ($j = 0; $j < $jmlResult; $j++) {
			//echo "nama = ".$result[$j]->nama;
				$jabatanPeg = $this->getjabatanPegawai($result[$j]->nip);
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama" 				=>(string)$result[$j]->nama,
										"kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
									    "kd_struktur_org"   =>(string)$result[$j]->kd_struktur_org,
										"jabatanPeg"		=> $jabatanPeg,
										"id_riwayat_jabatan"=> (string)$result[$j]->id_riwayat_jabatan,
										"kd_struktur_org_induk"=> (string)$result[$j]->kd_struktur_org_induk
										);		

				 $subbagian = $this->daftarSubbagian($result[$j]->kd_struktur_org);
					
				$jmlSubbagian = count($subbagian);
				//echo "<br>jmlBiro = $jmlBiro | $angkaTerakhir<br>";
				$b= $angkaTerakhir;
				for ($a=0; $a<$jmlSubbagian; $a++){
					
					//echo "$b<br>";
					$jabatanPeg = $this->getjabatanPegawai($subbagian[$a]['nip']);
					$hasilAkhir[$b] = array("nip"  				=>(string)$subbagian[$a]['nip'],
											"nama" 				=>(string)$subbagian[$a]['nama'],
											"kd_jabatan" 		=>(string)$subbagian[$a]['kd_jabatan'],
											"kd_struktur_org"   =>(string)$subbagian[$a]['kd_struktur_org'],
											"jabatanPeg"		=> $jabatanPeg,
											"id_riwayat_jabatan"=> (string)$subbagian[$a]->id_riwayat_jabatan,
											"kd_struktur_org_induk"=> (string)$subbagian[$a]->kd_struktur_org_induk
											);
					$b++;
				}
				$angkaTerakhir = $angkaTerakhir+$jmlSubbagian; 
				
				unset($subbagian); 
			}	
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	

	public function daftarSubbagian($kdOrgBagian){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		   $sqlProses = "select a.nip, 
								a.nama, 
								a.id_riwayat_jabatan,
								a.kd_jabatan, 
								a.kd_struktur_org,
								concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level) as jabatan,
								a.id_riwayat_jabatan,
								a.kd_struktur_org_induk
						from simpeg_internal.v_jab_peg a 
						where a.kd_struktur_org_induk = '$kdOrgBagian'
						order by  a.kd_jabatan, a.kd_struktur_org";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);	
			
			$jmlResult = count($result);
			$angkaTerakhir = $jmlResult;
			for ($j = 0; $j < $jmlResult; $j++) {
				$jabatanPeg = $this->getjabatanPegawai($result[$j]->nip);
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama" 				=>(string)$result[$j]->nama,
										"id_riwayat_jabatan"=>(string)$result[$j]->id_riwayat_jabatan,
									    "kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
									    "kd_struktur_org"   =>(string)$result[$j]->kd_struktur_org,
										"jabatanPeg"		=> $jabatanPeg,
										"id_riwayat_jabatan"=> (string)$result[$j]->id_riwayat_jabatan,
										"kd_struktur_org_induk"=> (string)$result[$j]->kd_struktur_org_induk
										);		

				$pelaksana = $this->daftarPelaksana($result[$j]->kd_struktur_org, $result[$j]->kd_struktur_org_induk);
					
				$jmlPelaksana = count($pelaksana);
				//echo "<br>jmlBiro = $jmlBiro | $angkaTerakhir<br>";
				$b= $angkaTerakhir;
				for ($a=0; $a<$jmlPelaksana; $a++){
					
					//echo "$b<br>";
					$jabatanPeg = $this->getjabatanPegawai($pelaksana[$a]['nip']);
					$hasilAkhir[$b] = array("nip"  				=>(string)$pelaksana[$a]['nip'],
											"nama" 				=>(string)$pelaksana[$a]['nama'],
											"kd_jabatan" 		=>(string)$pelaksana[$a]['kd_jabatan'],
											"kd_struktur_org"   =>(string)$pelaksana[$a]['kd_struktur_org'],
											"jabatanPeg"		=> $jabatanPeg,
											"id_riwayat_jabatan"=> (string)$pelaksana[$a]->id_riwayat_jabatan,
											"kd_struktur_org_induk"=> (string)$pelaksana[$a]->kd_struktur_org_induk
											);
					$b++;
				}
				$angkaTerakhir = $angkaTerakhir+$jmlPelaksana;
				
				unset($pelaksana);  
			}	
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function daftarPelaksana($kdOrgSubbagian, $kdJabatanInduk){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		   $sqlProses = "select a.nip, 
								a.nama, 
								a.kd_jabatan, 
								a.kd_struktur_org,
								concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level) as jabatan
						from simpeg_internal.v_jab_peg a 
						where a.kd_struktur_org = '$kdOrgSubbagian' and a.kd_jabatan_induk = '$kdJabatanInduk'
						order by  a.kd_jabatan, a.kd_struktur_org";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);	
			
			$jmlResult = count($result);
			$angkaTerakhir = $jmlResult;
			for ($j = 0; $j < $jmlResult; $j++) {
				$jabatanPeg = $this->getjabatanPegawai($result[$j]->nip);
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama" 				=>(string)$result[$j]->nama,
										"kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
									    "kd_struktur_org"   =>(string)$result[$j]->kd_struktur_org,
										"jabatanPeg"		=> $jabatanPeg
										);		 
			}	
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianAgendaSrtmasuk(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		$kdOrgLogin 	= strToUpper($dataMasukan['kdOrgLogin']);
		$user 			= strToUpper($dataMasukan['user']);
		$nip 			= strToUpper($dataMasukan['nip']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		   $whereBase = "where (c_statusdeleteAgendamasuk != 'Y' OR c_statusdeleteAgendamasuk is null)";
		   if(($kdOrgLogin!='9') && ($kdOrgLogin!='30') && ($kdOrgLogin!='31') && ($kdOrgLogin!='32') && 
			    ($kdOrgLogin!='170') && ($kdOrgLogin!='174') && ($kdOrgLogin!='178') && ($kdOrgLogin!='182') && ($kdOrgLogin!='186') &&
				($kdOrgLogin!='190') && ($kdOrgLogin!='194') && ($kdOrgLogin!='197') &&
				($kdOrgLogin!='171') && ($kdOrgLogin!='172') && ($kdOrgLogin!='173') &&
				($kdOrgLogin!='175') && ($kdOrgLogin!='176') && ($kdOrgLogin!='177') &&
				($kdOrgLogin!='179') && ($kdOrgLogin!='180') && ($kdOrgLogin!='181') &&
				($kdOrgLogin!='183') && ($kdOrgLogin!='184') && ($kdOrgLogin!='158') &&
				($kdOrgLogin!='187') && ($kdOrgLatogin!='188') && ($kdOrgLogin!='189') &&
				($kdOrgLogin!='191') && ($kdOrgLogin!='192') && ($kdOrgLogin!='193') &&
				($kdOrgLogin!='195') && ($kdOrgLogin!='196') && ($kdOrgLogin!='198') && ($kdOrgLogin!='199'))
			{
				$whereByOrg = " and (kd_orgKepada = '$kdOrgLogin' or 
								(kd_orgteruskanke = '$kdOrgLogin' and n_teruskanke='$nip'))";
			}
			
			if (($kdOrgLogin=='0')||($kdOrgLogin=='1'))	
			{
				//$whereByOrg = $whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%')";
				$whereByOrg = $whereByOrg." or(kd_orgKepada = '0' or kd_orgKepada='1') or
										(upper(n_kepada) = 'PRESIDEN' or UPPER(n_kepada) = 'SESKAB')";
		        //$whereByOrg = '';
			}
		
		   $whereOpt = " AND UPPER($kategoriCari) like '%$kataKunci%' ";
			
		    //$whereBase = "where a.kd_struktur_org_induk <= 10";
			
			
			if ($kategoriCari) {
				$where = $whereBase.$whereByOrg.$whereOpt;
			} else {
				$where = $whereBase.$whereByOrg;
			}
			
			$sqlProses = "SELECT distinct id_srtmasuk, i_agendasrt, i_srtpengirim, n_srtpengirim, n_kepada, n_srtperihal
						  FROM vm_disposisi ";	

			$sqlData = $sqlProses.$where." limit 10 offset 0";
			//echo $sqlData;
			$result = $db->fetchAll($sqlData);	
			
			
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				
				$hasilAkhir[$j] = array("id_srtmasuk"  		=>(string)$result[$j]->id_srtmasuk,
										"i_agendasrt" =>(string)$result[$j]->i_agendasrt,
										"i_srtpengirim" 				=>(string)$result[$j]->i_srtpengirim,
									    "n_srtpengirim"      		=>(string)$result[$j]->n_srtpengirim,
									    "n_kepada"      		=>(string)$result[$j]->n_kepada,
									    "n_srtperihal"      		=>(string)$result[$j]->n_srtperihal
										);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getsrtsifatList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null) ";
			$sqlProses = "select c_srtsifat,
								n_srtsifat
							from tr_sifat ";	
							
			$sqlData = $sqlProses.$whereBase.$order;
			$result = $db->fetchAll($sqlData);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtsifat"  	=>(string)$result[$j]->c_srtsifat,
									   "n_srtsifat"  	=>(string)$result[$j]->n_srtsifat
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getJenisAjuanList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null) ";
			$sqlProses = "select c_srtajuan,
								n_srtajuan
							from tr_jenis_ajuan ";	
							
			$sqlData = $sqlProses.$whereBase.$order;
			$result = $db->fetchAll($sqlData);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtajuan"  	=>(string)$result[$j]->c_srtajuan,
									   "n_srtajuan"  	=>(string)$result[$j]->n_srtajuan
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianPejabatJabatan(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    if ($kategoriCari == 'nip'){$whereOpt = "and $kategoriCari like '%$kataKunci%' ";}
			else if ($kategoriCari == 'nama'){$whereOpt = "and UPPER($kategoriCari) like '%$kataKunci%' ";}
			
		    //$whereBase = "where a.kd_struktur_org_induk <= 10";
			
			
			if ($kategoriCari) {
				$where = $whereOpt;
			} else {
				$where = '';
			}
			
			$sqlProses = "select a.level,
								a.nm_level,
								b.nip, 
								b.nama, 
								b.kd_jabatan, 
								c.nm_jabatan,
								a.kd_struktur_org
							from simpeg_internal.tbl_struktur_organisasi a
							left join simpeg_internal.v_jab_peg b on (a.kd_struktur_org = b.kd_struktur_org)
							left join simpeg_internal.tbl_jabatan c on (b.kd_jabatan = c.kd_jabatan)
							 ";	
			
			//$whereBase = "where b.kd_jabatan = c.kd_jabatan";
			//$where = $whereBase.$where; 
			$sqlData = $sqlProses.$where." order by a.kd_struktur_org";
			$result = $db->fetchAll($sqlData);	
					
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				if($result[$j]->nama)
				{
					$hasilAkhir[$j] = array("level"  		=>(string)$result[$j]->level,
											"nm_level" =>(string)$result[$j]->nm_level,
											"nip" 				=>(string)$result[$j]->nip,
										    "nama"      		=>(string)$result[$j]->nama,
										    "kd_jabatan"      		=>(string)$result[$j]->kd_jabatan,
										    "nm_jabatan"      		=>(string)$result[$j]->nm_jabatan,
										    "kd_struktur_org"      		=>(string)$result[$j]->kd_struktur_org
											);
				}							
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianPegawaiNonPelaksana(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= strToUpper($dataMasukan['kataKunci']);
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    if ($kategoriCari == 'nip'){$whereOpt = "and $kategoriCari like '%$kataKunci%' ";}
			else if ($kategoriCari == 'nama'){$whereOpt = "and UPPER($kategoriCari) like '%$kataKunci%' ";}
			
		    //$whereBase = "where a.kd_struktur_org_induk <= 10";
			
			
			if ($kategoriCari) {
				$where = $whereOpt;
			} else {
				$where = '';
			}
			
			$sqlProses = "select a.level,
								a.nm_level,
								b.nip, 
								b.nama, 
								b.kd_jabatan, 
								c.nm_jabatan,
								a.kd_struktur_org
							from simpeg_internal.tbl_struktur_organisasi a
							left join simpeg_internal.v_jab_peg b on (a.kd_struktur_org = b.kd_struktur_org)
							left join simpeg_internal.tbl_jabatan c on (b.kd_jabatan = c.kd_jabatan)
							 ";	
			
			$whereBase = "where b.kd_jabatan = c.kd_jabatan and c.kd_jabatan in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')";
			$where = $whereBase.$where; 
			$sqlData = $sqlProses.$where." order by a.kd_struktur_org";
			$result = $db->fetchAll($sqlData);	
					
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				if($result[$j]->nama)
				{
					$hasilAkhir[$j] = array("level"  		=>(string)$result[$j]->level,
											"nm_level" =>(string)$result[$j]->nm_level,
											"nip" 				=>(string)$result[$j]->nip,
										    "nama"      		=>(string)$result[$j]->nama,
										    "kd_jabatan"      		=>(string)$result[$j]->kd_jabatan,
										    "nm_jabatan"      		=>(string)$result[$j]->nm_jabatan,
										    "kd_struktur_org"      		=>(string)$result[$j]->kd_struktur_org,
											"nm_jabatan_lengkap" => $this->getNamaOrganisasiPerLevelByNip($result[$j]->nip)
											);
				}							
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getJenisSuratList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
			$sqlProses = "select c_srtjenis,
								 n_srtjenis
							from tr_jenis_surat ";	
							
			$sqlData = $sqlProses.$where.$order;
			$result = $db->fetchAll($sqlData);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtjenis"  	=>(string)$result[$j]->c_srtjenis,
									   "n_srtjenis"  	=>(string)$result[$j]->n_srtjenis
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function pencarianPegawaiAll(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$kategoriCari = $dataMasukan['kategoriCari'];
			$katakunciCari = $dataMasukan['katakunciCari'];
		   
		   if ($kategoriCari == 'jabatan'){
				$whereOpt = "where UPPER(concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level)) like '%$katakunciCari%' ";}
			else if ($kategoriCari == 'nama'){$whereOpt = "where UPPER($kategoriCari) like '%$katakunciCari%' ";}
			
		    //$whereBase = "where a.kd_struktur_org_induk <= 10";
			if ($kategoriCari) {
				$where = $whereOpt;
			} else {
				$where = '';
			}
			
			$sqlProses = "select a.nip, 
								a.nama, 
								a.kd_jabatan, 
								a.kd_struktur_org,
								concat(a.nm_jabatan, ' ', a.level, ' ', a.nm_level) as jabatan
						from simpeg_internal.v_jab_peg a ";	
			
			
			if ($kategoriCari) {
				$sqlData = $sqlProses.$where."order by kd_struktur_org";
			} else {
				$sqlData = $sqlProses.$where." order by a.kd_struktur_org";
			}
			
			//echo $sqlData;
			//echo $sqlData;	
			$result = $db->fetchAll($sqlData);	
				
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				$jabatanPeg = $this->getjabatanPegawai($result[$j]->nip);
				$hasilAkhir[$j] = array("nip"  				=>(string)$result[$j]->nip,
										"nama" 				=>(string)$result[$j]->nama,
										"kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
									    "kd_struktur_org"   =>(string)$result[$j]->kd_struktur_org,
										"jabatanPeg"		=> $jabatanPeg
										);
										
									
			}	
			//var_dump($hasilAkhir);	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getNamaOrganisasiPerLevelByNip($nip){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
				
			$data = $db->fetchRow("SELECT nm_jabatan, level, nm_level
										FROM simpeg_internal.v_jab_peg
										WHERE nip = '$nip'");
			
			$level = $data->level;
			$nmlevel = $data->nm_level;
			$nm_jabatan = $data->nm_jabatan; 
			
			if ((strtoupper($nm_jabatan) == 'SEKRETARIS KABINET') || (strtoupper($nm_jabatan) == 'WAKIL SEKRETARIS KABINET') ||
				 (strtoupper($nm_jabatan) == 'DEPUTI')){
				$nmOrganisasiLengkap = $level." ".$nmlevel;		
			} else {
				$nmOrganisasiLengkap = $nm_jabatan." ".$level." ".$nmlevel;	
			}
					
			return $nmOrganisasiLengkap;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getLevelOrg($kdOrg) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
		    $sqlProses = "select a.level
						from simpeg_internal.tbl_struktur_organisasi a
						where kd_struktur_org = '$kdOrg'";	

			$kd_surat = $db->fetchOne($sqlProses);	
					
			return $kd_surat;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
}
?>