<?php
class Adm_Admsifatpenomoransuratkeluar_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List User
	//======================================================================
	public function cariSifatSuratList(array $dataMasukan, $pageNumber, $itemPerPage) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= strtoupper($dataMasukan['katakunciCari']);
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	   
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = " where (c_statusdelete != 'Y' or c_statusdelete is null) ";
			$whereOpt = " UPPER($kategoriCari) like '%$katakunciCari%' ";
			if($katakunciCari != "") { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = " order by $sortBy $sort ";
			$sqlProses = "select c_srtsifat, n_srtsifat, c_format, c_statusdelete, i_entry, d_entry from tr_sifatpenomoran_suratkeluar";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses"." "."$where) a";
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("c_srtsifat"  		=>(string)$result[$j]->c_srtsifat,
										"n_srtsifat"  	=>(string)$result[$j]->n_srtsifat,
										"c_statusdelete"=>(string)$result[$j]->c_statusdelete,
										"i_entry"      	=>(string)$result[$j]->i_entry,
										"d_entry"      	=>(string)$result[$j]->d_entry
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function sifatPenomoranSuratKeluarInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_srtsifat"  		=>$dataMasukan['c_srtsifat'],
								"n_srtsifat"  	=>$dataMasukan['n_srtsifat'],
								"c_format"  	=>$dataMasukan['c_format'],
								"i_entry"  	=>$dataMasukan['i_entry']);		
			$db->insert('tr_sifatpenomoran_suratkeluar',$paramInput);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function detailSifatSuratById($c_srtsifat) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		
			$where = " where c_srtsifat = '$c_srtsifat' ";
			$sqlProses = "select c_srtsifat, n_srtsifat, c_format, c_statusdelete, d_entry, i_entry from tr_sifatpenomoran_suratkeluar";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			$hasilAkhir = array("c_srtsifat"  		=>(string)$result->c_srtsifat,
								"n_srtsifat"  	=>(string)$result->n_srtsifat,
								"c_format"  	=>(string)$result->c_format,
								"c_statusdelete"=>(string)$result->c_statusdelete,
								"d_entry"      	=>(string)$result->d_entry,
								"i_entry"      	=>(string)$result->i_entry
								);
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function sifatPenomoranSuratKeluarUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_srtsifat" =>$dataMasukan['c_srtsifat'],
								"n_srtsifat" =>$dataMasukan['n_srtsifat'],
								"c_format"   =>$dataMasukan['c_format'],
								"i_entry"  	 =>$dataMasukan['i_entry']);	
								
			//var_dump($paramInput);
			$where[] = " c_srtsifat = '".$dataMasukan['c_srtsifatH']."'";
			
			$db->update('tr_sifatpenomoran_suratkeluar',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function sifatPenomoranSuratKeluarHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_statusdelete"	=> 'Y',
								"i_entry"  	=>$dataMasukan['i_entry']);	
								
			$where[] = " c_srtsifat = '".$dataMasukan['c_srtsifat']."'";
			
			$db->update('tr_sifatpenomoran_suratkeluar',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
		
}
?>
