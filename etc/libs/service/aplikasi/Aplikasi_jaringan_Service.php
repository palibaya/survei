<?php
class Aplikasi_jaringan_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

public function getTmJaringan($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,c_lan_cable,c_lan_wireless,c_lan_securty,q_lan,q_lan_hub_client,
										q_lan_nhub_client,c_lan_internet,q_lan_hub,q_pc_internet,c_internet_pasca,
										q_internet_isp,v_internet,q_internet_upload,q_internet_download
										from tm_jaringan where 1=1 $cari ");
									
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"c_lan_cable"=>(string)$result[$j]->c_lan_cable,
									"c_lan_wireless"=>(string)$result[$j]->c_lan_wireless,
									"c_lan_securty"=>(string)$result[$j]->c_lan_securty,
									"q_lan_hub"=>(string)$result[$j]->q_lan_hub,
									"q_lan_hub_client"=>(string)$result[$j]->q_lan_hub_client,
									"q_lan_nhub_client"=>(string)$result[$j]->q_lan_nhub_client,
									"c_lan_internet"=>(string)$result[$j]->c_lan_internet,
									"q_lan"=>(string)$result[$j]->q_lan,
									"q_pc_internet"=>(string)$result[$j]->q_pc_internet,
									"c_internet_pasca"=>(string)$result[$j]->c_internet_pasca,
									"q_internet_isp"=>(string)$result[$j]->q_internet_isp,
									"v_internet"=>(string)$result[$j]->v_internet,
									"q_internet_upload"=>(string)$result[$j]->q_internet_upload,
									"q_internet_download"=>(string)$result[$j]->q_internet_download);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}	

	public function tambahTmJaringan(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"c_lan_cable"=>$data['c_lan_cable'],
							"c_lan_wireless"=>$data['c_lan_wireless'],
							"c_lan_securty"=>$data['c_lan_securty'],
							"q_lan"=>$data['q_lan'],
							"q_lan_hub_client"=>$data['q_lan_hub_client'],
							"q_lan_nhub_client"=>$data['q_lan_nhub_client'],
							"c_lan_internet"=>$data['c_lan_internet'],
							"q_lan_hub"=>$data['q_lan_hub'],
							"q_pc_internet"=>$data['q_pc_internet'],
							"c_internet_pasca"=>$data['c_internet_pasca'],
							"q_internet_isp"=>$data['q_internet_isp'],
							"v_internet"=>$data['v_internet'],
							"q_internet_upload"=>$data['q_internet_upload'],
							"q_internet_download"=>$data['q_internet_download'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_jaringan',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	

	public function ubahTmJaringan(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $ubah_data = array("c_lan_cable"=>$data['c_lan_cable'],
							"c_lan_wireless"=>$data['c_lan_wireless'],
							"c_lan_securty"=>$data['c_lan_securty'],
							"q_lan"=>$data['q_lan'],
							"q_lan_hub_client"=>$data['q_lan_hub_client'],
							"q_lan_nhub_client"=>$data['q_lan_nhub_client'],
							"c_lan_internet"=>$data['c_lan_internet'],
							"q_lan_hub"=>$data['q_lan_hub'],
							"q_pc_internet"=>$data['q_pc_internet'],
							"c_internet_pasca"=>$data['c_internet_pasca'],
							"q_internet_isp"=>$data['q_internet_isp'],
							"v_internet"=>$data['v_internet'],
							"q_internet_upload"=>$data['q_internet_upload'],
							"q_internet_download"=>$data['q_internet_download'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->update('tm_jaringan',$ubah_data, "id_kuesioner = '".trim($data['id_kuesioner'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function hapusTmJaringan(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."'";
		$db->delete('tm_jaringan', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
	
	public function getJumlahdataJaringan($idKesioner) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$jmlDataJaringan = $db->fetchOne("select count(*) from tm_jaringan where id_kuesioner = '$idKesioner'");
	     	return $jmlDataJaringan;
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
}
?>
