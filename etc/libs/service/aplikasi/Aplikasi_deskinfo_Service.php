<?php
class aplikasi_deskinfo_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

	public function getTmDeskinfo($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,e_layanan,e_layanan_publik,e_staft_pelaksana,e_sk_tugas,e_mohon_info,e_status_perkara,i_entry,d_entry
										from tm_deskinfo where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"e_layanan"=>(string)$result[$j]->e_layanan,
									"e_layanan_publik"=>(string)$result[$j]->e_layanan_publik,
									"e_staft_pelaksana"=>(string)$result[$j]->e_staft_pelaksana,
									"e_sk_tugas"=>(string)$result[$j]->e_sk_tugas,
									"e_mohon_info"=>(string)$result[$j]->e_mohon_info,
									"e_status_perkara"=>(string)$result[$j]->e_status_perkara,
									"i_entry"=>(string)$result[$j]->i_entry,
									"d_entry"=>(string)$result[$j]->d_entry);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}

	public function getTmDeskinfobulan($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,id_kuesioner,v_jan,v_feb,v_mar,v_apr,v_mei,v_jun,v_jul,v_agt,v_sep,v_okt,v_nop,v_des,i_entry,d_entry
										from tm_deskinfo_bln where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"v_jan"=>(string)$result[$j]->v_jan,
									"v_feb"=>(string)$result[$j]->v_feb,
									"v_mar"=>(string)$result[$j]->v_mar,
									"v_apr"=>(string)$result[$j]->v_apr,
									"v_mei"=>(string)$result[$j]->v_mei,
									"v_jun"=>(string)$result[$j]->v_jun,
									"v_jul"=>(string)$result[$j]->v_jul,
									"v_agt"=>(string)$result[$j]->v_agt,
									"v_sep"=>(string)$result[$j]->v_sep,
									"v_okt"=>(string)$result[$j]->v_okt,
									"v_nop"=>(string)$result[$j]->v_nop,
									"v_des"=>(string)$result[$j]->v_des,
									"i_entry"=>(string)$result[$j]->i_entry,
									"d_entry"=>(string)$result[$j]->d_entry);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	
	public function hapusTmDeskinfo(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."' ";
		$db->delete('tm_deskinfo', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}
	
	public function ubahTmDeskinfo(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array(
							"e_layanan"=>$data['e_layanan'],
							"e_layanan_publik"=>$data['e_layanan_publik'],
							"e_staft_pelaksana"=>$data['e_staft_pelaksana'],
							"e_sk_tugas"=>$data['e_sk_tugas'],
							"e_mohon_info"=>$data['e_mohon_info'],
							"e_status_perkara"=>$data['e_status_perkara'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));	
		$db->update('tm_deskinfo',$ubah_data, "id_kuesioner= '".trim($data['id_kuesioner'])."' ");							
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function tambahTmDeskinfo(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"e_layanan"=>$data['e_layanan'],
							"e_layanan_publik"=>$data['e_layanan_publik'],
							"e_staft_pelaksana"=>$data['e_staft_pelaksana'],
							"e_sk_tugas"=>$data['e_sk_tugas'],
							"e_mohon_info"=>$data['e_mohon_info'],
							"e_status_perkara"=>$data['e_status_perkara'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_deskinfo',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	

	public function tambahTmDeskinfobln(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"v_jan"=>$data['v_jan'],
							"v_feb"=>$data['v_feb'],
							"v_mar"=>$data['v_mar'],
							"v_apr"=>$data['v_apr'],
							"v_mei"=>$data['v_mei'],
							"v_jun"=>$data['v_jun'],
							"v_jul"=>$data['v_jul'],
							"v_agt"=>$data['v_agt'],
							"v_sep"=>$data['v_sep'],
							"v_okt"=>$data['v_okt'],
							"v_nop"=>$data['v_nop'],
							"v_des"=>$data['v_des'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_deskinfo_bln',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function ubahTmDeskinfobln(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"v_jan"=>$data['v_jan'],
							"v_feb"=>$data['v_feb'],
							"v_mar"=>$data['v_mar'],
							"v_apr"=>$data['v_apr'],
							"v_mei"=>$data['v_mei'],
							"v_jun"=>$data['v_jun'],
							"v_jul"=>$data['v_jul'],
							"v_agt"=>$data['v_agt'],
							"v_sep"=>$data['v_sep'],
							"v_okt"=>$data['v_okt'],
							"v_nop"=>$data['v_nop'],
							"v_des"=>$data['v_des'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->update('tm_deskinfo_bln',$ubah_data, "id_kuesioner= '".trim($data['id_kuesioner'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	
	public function hapusTmDeskinfobln(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."' ";
		$db->delete('tm_deskinfo_bln', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}
}
?>
