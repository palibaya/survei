<?php
require_once "share/globalReferensi.php";
require_once "share/gen_nosurat.php";
require_once "service/aplikasi/Aplikasi_Referensi_Service.php";

class Aplikasi_Suratmasuktandaterima_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function cariTandaterimaList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		$ref_serv = Aplikasi_Referensi_Service::getInstance();
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari1 		= $dataMasukan['dTglCari1'];
		$dTglCari2	 	= $dataMasukan['dTglCari2'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	   
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null) ";
			
			if ($kategoriCari == 'periode_d_tglterima') {
				$whereOpt = "d_tglterima between '$dTglCari1' and '$dTglCari2' ";
			} else if ($kategoriCari == 'd_tglterima') {
				$whereOpt = "d_tglterima = '$dTglCari' ";
			} else {
				$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			}
			
			if(($kategoriCari) && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "select id,
							i_tandaterima,
							n_penerima,
							d_tglterima,
							n_pengantar,
							e_notelp,
							n_kepada,
							nip,
							kd_org,
							kd_jabatan,
							n_instansipengirim,
							e_perihal
							from tm_tandaterima ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses $where) a";
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				
				$result = $db->fetchAll($sqlData);	
			}
					
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				$nmJabatan	= $ref_serv->getjabatanPegawai($result[$j]->nip);
				
				$hasilAkhir[$j] = array("id_tandaterima"  	=>(string)$result[$j]->id,
										"i_tandaterima"  	=>(string)$result[$j]->i_tandaterima,
									   "n_penerima" 		=>(string)$result[$j]->n_penerima,
									   "d_tglterima"      	=>(string)$result[$j]->d_tglterima,
									   "n_pengantar"      	=>(string)$result[$j]->n_pengantar,
									   "e_notelp"      		=>(string)$result[$j]->e_notelp,
									   "n_kepada"      		=>(string)$result[$j]->n_kepada,
									   "nmJabatan"			=> $nmJabatan,
									   "nip"      			=>(string)$result[$j]->nip,
									   "kd_org"      		=>(string)$result[$j]->kd_org,
									   "kd_jabatan"      	=>(string)$result[$j]->kd_jabatan,
									   "e_perihal"      	=>(string)$result[$j]->e_perihal
										);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function tandaterimaInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			$gen_nosurat = new gen_nosurat();
			$iTandaterima = $gen_nosurat->genNoTandaTerima('TANDA TERIMA', date("Y-m-d"));
			
			$paramInput = array("i_tandaterima"	=>$iTandaterima,
							   "i_srtpengirim"	=>$dataMasukan['i_srtpengirim'],
							   "n_penerima"  =>$dataMasukan['n_penerima'],
							   "d_tglterima"  =>$dataMasukan['d_tglterima'],
							   "n_pengantar"  =>$dataMasukan['n_pengantar'],
							   "e_notelp"  =>$dataMasukan['e_notelp'],
							   "n_kepada"  =>$dataMasukan['n_kepada'],
							   "n_pengirim"  =>$dataMasukan['n_pengirim'],
							   "n_instansipengirim"  =>$dataMasukan['n_instansipengirim'],
							   "e_perihal"  =>$dataMasukan['e_perihal'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>new Zend_Db_Expr('NOW()'));

		   if (($dataMasukan['n_kepada'] != 'Presiden') && ($dataMasukan['n_kepada'] != 'Seskab')){
				if ($dataMasukan['nip']){
					$paramInput['nip'] = $dataMasukan['nip'];
					$paramInput['kd_jabatan'] = $dataMasukan['kd_jabatan'];
					$paramInput['kd_org'] = $dataMasukan['kd_org'];
				}
			}
			 
			
			$db->insert('tm_tandaterima',$paramInput);
			$db->commit();
			
			return 'sukses';
			unset($paramInput);
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function detailTandaterimaById($idTandaterima) {
		$ref_serv = Aplikasi_Referensi_Service::getInstance();
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where id = '$idTandaterima' ";
			$sqlProses = "select id,
							i_tandaterima,
							i_srtpengirim,
							n_penerima,
							d_tglterima,
							n_pengantar,
							e_notelp,
							n_kepada,
							nip,
							kd_jabatan,
							kd_org,
							n_pengirim,
							n_instansipengirim,
							e_perihal
							from tm_tandaterima ";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			$nKepada 	= $result->n_kepada;
			$nip 		= $result->nip;
			$nmJabatan	= $ref_serv->getjabatanPegawai($nip);
			$kdJabatan	= $result->kd_jabatan;
			$kdOrg 		= $result->kd_org;
			
			
			$hasilAkhir = array("id_tandaterima"  	=>(string)$result->id,
								"i_tandaterima"  	=>(string)$result->i_tandaterima,
							   "i_srtpengirim"  	=>(string)$result->i_srtpengirim,
							   "n_penerima" 		=>(string)$result->n_penerima,
							   "d_tglterima"      	=>(string)$result->d_tglterima,
							   "e_notelp"      		=>(string)$result->e_notelp,
							   "n_kepada"      		=>$nKepada,
							   "nip"      			=>$nip,
							   "kd_org"      		=>$kdOrg,
							   "kd_jabatan"      	=>$kdJabatan,
							   "nmJabatan"      	=>$nmJabatan,
							   "n_pengirim"      	=>(string)$result->n_pengirim,
							   "n_instansipengirim" =>(string)$result->n_instansipengirim,
							   "e_perihal"      	=>(string)$result->e_perihal
								);
			
										
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function tandaterimaUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("i_tandaterima"		=>$dataMasukan['i_tandaterima'],
							   "n_penerima"  		=>$dataMasukan['n_penerima'],
							   "d_tglterima"  		=>$dataMasukan['d_tglterima'],
							   "n_pengantar"  		=>$dataMasukan['n_pengantar'],
							   "e_notelp"  			=>$dataMasukan['e_notelp'],
							   "n_kepada"  			=>$dataMasukan['n_kepada'],
							   "nip"  				=>null,
							   "kd_jabatan"  		=>null,
							   "kd_org"  			=>null,
							   "n_pengirim"  		=>$dataMasukan['n_pengirim'],
							   "n_instansipengirim" =>$dataMasukan['n_instansipengirim'],
							   "e_perihal"      	=>$dataMasukan['e_perihal'],
							   "i_entry"       		=>$dataMasukan['i_entry'],
							   "d_entry"       		=>new Zend_Db_Expr('NOW()'));

			if (($dataMasukan['n_kepada'] != 'Presiden') && ($dataMasukan['n_kepada'] != 'Seskab')){
				if ($dataMasukan['nip']){
					$paramInput['nip'] = $dataMasukan['nip'];
					$paramInput['kd_jabatan'] = $dataMasukan['kd_jabatan'];
					$paramInput['kd_org'] = $dataMasukan['kd_org'];
				}
			}
			
			$where[] = "id = '".$dataMasukan['id_tandaterima']."'";
			
			$db->update('tm_tandaterima',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function tandaterimaHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));	
								
			$where[] = "id = '".$dataMasukan['id_tandaterima']."'";
			
			$db->update('tm_tandaterima',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}


	public function getTandaterimaList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select id,
							i_tandaterima,
							i_srtpengirim,
							n_kepada,
							n_pengirim
							from tm_tandaterima ";	

			
			$result = $db->fetchAll($sqlProses);	
					
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("id_tandaterima"  	=>(string)$result[$j]->id,
										"i_tandaterima"  	=>(string)$result[$j]->i_tandaterima,
									   "i_srtpengirim" 		=>(string)$result[$j]->i_srtpengirim,
									   "n_kepada"      		=>(string)$result[$j]->n_kepada,
									   "n_pengirim"      		=>(string)$result[$j]->n_pengirim
										);
			}	
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function detailTandaterimaByNo($iTandaterima) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where i_tandaterima = '$iTandaterima' ";
			$sqlProses = "select id,
							i_tandaterima,
							i_srtpengirim,
							n_penerima,
							n_kepada,
							n_pengirim
							from tm_tandaterima ";	

			
			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			
			
			$hasilAkhir = array("id_tandaterima"  	=>(string)$result->id,
								"i_tandaterima"  	=>(string)$result->i_tandaterima,
							   "i_srtpengirim"  	=>(string)$result->i_srtpengirim,
							   "n_penerima" 		=>(string)$result->n_penerima,
							   "n_kepada"      		=>(string)$result->n_kepada,
							   "n_pengirim"      		=>(string)$result->n_pengirim
								);
										
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function daftarStrukturOrganisasi($levelOrganisasi){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$levelOrganisasi = strtoupper($levelOrganisasi);
			
			
			//echo "levelOrganisasi = $levelOrganisasi";
			if ($levelOrganisasi == 'PRIBADI'){
				$sqlProses = "select kd_struktur_org,
								kd_struktur_org_induk,
								level,
								nm_level,
								tgl_dibentuk,
								tgl_dibubarkan
								from v_struktur_organisasi
							  where UPPER(level) like 'Deputi%'	";	
			}
			else if ($levelOrganisasi == 'SEMUA'){
				$sqlProses = "select kd_struktur_org,
								kd_struktur_org_induk,
								level,
								nm_level,
								tgl_dibentuk,
								tgl_dibubarkan
								from v_struktur_organisasi	";	
			}
			else {
				$sqlProses = "select kd_struktur_org,
								kd_struktur_org_induk,
								level,
								nm_level,
								tgl_dibentuk,
								tgl_dibubarkan
								from v_struktur_organisasi
							  where UPPER(level) like '$levelOrganisasi%'	";	
			}	

			$result = $db->fetchAll($sqlProses);	
			
			for ($j = 0; $j < count($result); $j++) {
				
				$hasilAkhir[$j] = array("kd_struktur_org"  		=>(string)$result[$j]->kd_struktur_org,
								"kd_struktur_org_induk" =>(string)$result[$j]->kd_struktur_org_induk,
							    "level"  				=>(string)$result[$j]->level,
							    "nm_level" 				=>(string)$result[$j]->nm_level,
							    "tgl_dibentuk"      	=>(string)$result[$j]->tgl_dibentuk,
							    "tgl_dibubarkan"      	=>(string)$result[$j]->tgl_dibubarkan
								);				
									
			}	
									
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function pencariantandaterima(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		$global_ref = new globalReferensi();
		$ref_serv = Aplikasi_Referensi_Service::getInstance();
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= $dataMasukan['kataKunci'];
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null) ";
			
			$whereOpt = "$kategoriCari like '%$kataKunci%' ";
			if ($kategoriCari) {
				$where = $whereBase." and ".$whereOpt;
			} else {
				$where = $whereBase;
			}
			
			$order = "order by d_entry desc ";
			$sqlProses = "select id,
							i_tandaterima,
							i_srtpengirim,
							n_penerima,
							d_tglterima,
							n_pengantar,
							e_notelp,
							n_kepada,
							nip,
							kd_org,
							kd_jabatan,
							n_instansipengirim,
							e_perihal
							from tm_tandaterima ";	

			$sqlData = $sqlProses.$where.$order." limit 5 offset 0";
			
			$result = $db->fetchAll($sqlData);	
	
					
			$jmlResult = count($result);
		
			for ($j = 0; $j < $jmlResult; $j++) {
				$nmJabatan	= $ref_serv->getjabatanPegawai($result[$j]->nip);
				$hasilAkhir[$j] = array("id_tandaterima"  	=>(string)$result[$j]->id,
										"i_tandaterima"  	=>(string)$result[$j]->i_tandaterima,
										"i_srtpengirim"  	=>(string)$result[$j]->i_srtpengirim,
									   "n_penerima" 		=>(string)$result[$j]->n_penerima,
									   "d_tglterima"      	=>(string)$result[$j]->d_tglterima,
									   "n_pengantar"      	=>(string)$result[$j]->n_pengantar,
									   "e_notelp"      		=>(string)$result[$j]->e_notelp,
									   "n_kepada"      		=>(string)$result[$j]->n_kepada,
									   "nip"      			=>(string)$result[$j]->nip,
									   "kd_org"      		=>(string)$result[$j]->kd_org,
									   "kd_jabatan"      	=>(string)$result[$j]->kd_jabatan,
									   "nmJabatan"			=> $nmJabatan,
									   "e_perihal"      	=>(string)$result[$j]->e_perihal,
									   "n_instansipengirim" =>(string)$result[$j]->n_instansipengirim
										);
			}	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}





	
}
?>