<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_hardware_Service.php";

class Aplikasi_HardwareController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->hardware_serv = aplikasi_hardware_Service::getInstance();
        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;
}

public function indexAction()
{
}
public function hardware($id_kuesioner)
{

        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $this->view->datahardware= $this->hardware_serv->getTmHardwareServer($cari);
        $data = $this->hardware_serv->getTmHardware($cari);
        $jmldata = count($data);
    if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
            $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
            $this->view->i_proc_pentium3=$data[$j]['i_proc_pentium3'];
            $this->view->i_proc_pentium4=$data[$j]['i_proc_pentium4'];
            $this->view->i_proc_dualcore=$data[$j]['i_proc_dualcore'];
            $this->view->i_proc_coretwoduo=$data[$j]['i_proc_coretwoduo'];
            $this->view->i_memori_1gb=$data[$j]['i_memori_1gb'];
            $this->view->i_memori_2gb=$data[$j]['i_memori_2gb'];
            $this->view->i_hardisk_40gb=$data[$j]['i_hardisk_40gb'];
            $this->view->i_hardisk_50gb=$data[$j]['i_hardisk_50gb'];
            $this->view->i_monitor_17lcd=$data[$j]['i_monitor_17lcd'];
            $this->view->i_monitor_18lcd=$data[$j]['i_monitor_18lcd'];
            $this->view->i_monitor_17nlcd=$data[$j]['i_monitor_17nlcd'];
            $this->view->i_monitor_18nlcd=$data[$j]['i_monitor_18nlcd'];
            $this->view->i_printer=$data[$j]['i_printer'];
            $this->view->i_scanner=$data[$j]['i_scanner'];
            $this->view->i_ups=$data[$j]['i_ups'];
            $this->view->c_cctv=$data[$j]['c_cctv'];
            $this->view->c_fasilitas_tv=$data[$j]['c_fasilitas_tv'];
            $this->view->c_fasilitas_pc=$data[$j]['c_fasilitas_pc'];
            $this->view->c_fasilitas_liflet=$data[$j]['c_fasilitas_liflet'];
            $this->view->c_fasilitas_no=$data[$j]['c_fasilitas_no'];
            $this->view->i_watt=$data[$j]['i_watt'];
            $this->view->c_problem_watt=$data[$j]['c_problem_watt'];

        }
        $this->view->par='Koreksi';
    }
    else
    {
        $this->view->par='Simpan';
        $this->view->id_kuesioner= $_REQUEST['idKuesioner'];
    }
}
public function hardwareAction()
{

        $id_kuesioner = $_REQUEST['idKuesioner'];
        $this->hardware($id_kuesioner);
        $this->view->id_kuesioner= $_REQUEST['idKuesioner'];
}

public function hardwarejsAction()
{
    header('content-type : text/javascript');
    $this->render('hardwarejs');
}

public function maintaindatahardwareAction()
{

            $MaintainData1 = array(
                    "id_kuesioner"=>$_POST['id_kuesioner'],
                    "i_proc_pentium3"=>$_POST['i_proc_pentium3']*1,
                    "i_proc_pentium4"=>$_POST['i_proc_pentium4']*1,
                    "i_proc_dualcore"=>$_POST['i_proc_dualcore']*1,
                    "i_proc_coretwoduo"=>$_POST['i_proc_coretwoduo']*1,
                    "i_memori_1gb"=>$_POST['i_memori_1gb']*1,
                    "i_memori_2gb"=>$_POST['i_memori_2gb']*1,
                    "i_hardisk_40gb"=>$_POST['i_hardisk_40gb']*1,
                    "i_hardisk_50gb"=>$_POST['i_hardisk_50gb']*1,
                    "i_monitor_17lcd"=>$_POST['i_monitor_17lcd']*1,
                    "i_monitor_18lcd"=>$_POST['i_monitor_18lcd']*1,
                    "i_monitor_17nlcd"=>$_POST['i_monitor_17nlcd']*1,
                    "i_monitor_18nlcd"=>$_POST['i_monitor_18nlcd']*1,
                    "i_printer"=>$_POST['i_printer']*1,
                    "i_scanner"=>$_POST['i_scanner']*1,
                    "i_ups"=>$_POST['i_ups']*1,
                    "c_cctv"=>$_POST['c_cctv'],
                    "c_fasilitas_tv"=>$_POST['c_fasilitas_tv'],
                    "c_fasilitas_pc"=>$_POST['c_fasilitas_pc'],
                    "c_fasilitas_liflet"=>$_POST['c_fasilitas_liflet'],
                    "c_fasilitas_no"=>$_POST['c_fasilitas_no'],
                    "i_watt"=>$_POST['i_watt']*1,
                    "c_problem_watt"=>$_POST['c_problem_watt'],
                    "i_entry"=>$_POST['i_entry']);



                    if ($_POST['perintah']=='Koreksi')
                    {$hasil = $this->hardware_serv->ubahTmHardware($MaintainData1); }
                    else
                    {$hasil = $this->hardware_serv->tambahTmHardware($MaintainData1);   }

        if ($hasil =='sukses')
        {
            $counttableserver=$_POST['counttableserver'];
            $con=1;
            for($xi=0;$xi<$counttableserver;$xi++)
            {

                if ($_POST['c_fungsi_sever_'.$con]){

                                     $MaintainData = array(
                                                    "id_kuesioner"=>$_POST['id_kuesioner'],
                                                    "c_fungsi_sever"=>$_POST['c_fungsi_sever_'.$con],
                                                    "n_aplikasi_server"=>$_POST['n_aplikasi_server_'.$con],
                                                    "i_jml_server"=>$_POST['i_jml_server_'.$con],
                                                    "n_spesifikasi"=>$_POST['n_spesifikasi_'.$con],
                                                    "i_entry"=>$data['i_entry'] );


                                    if ($_POST['perintah']=='Koreksi')
                                    {

                                        if ($xi==0){    $hasil = $this->hardware_serv->hapusTmHardwareServer($MaintainData);}
                                        if ($hasil=='sukses'){ $hasil = $this->hardware_serv->tambahTmHardwareServer($MaintainData);}
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";
                                    }
                                    else
                                    {   $hasil = $this->hardware_serv->tambahTmHardwareServer($MaintainData);
                                        $this->view->par="simpan";
                                        $par="Simpan ";
                                    }
                }
                $con++;
            }

        }




        $pesan=$par." data ".$hasil;
        $this->view->pesan = $pesan;
        $this->view->pesancek = $hasil;
        $this->hardware($_POST['id_kuesioner']);
        $this->_helper->viewRenderer('hardware');

}

public function hapusAction()
{
    $id_kuesioner=$_GET['id_kuesioner'];
    $MaintainData1 = array("id_kuesioner"=>$_GET['id_kuesioner']);
    $hasil = $this->hardware_serv->hapusTmHardwareServer($MaintainData1);
    $hasil = $this->hardware_serv->hapusTmHardware($MaintainData1);
    $this->view->par='Simpan';
    $this->_helper->viewRenderer('hardware');

}

}
?>
