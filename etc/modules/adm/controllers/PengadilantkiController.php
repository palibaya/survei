<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
//MA
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";
require_once "service/sso/Sso_User_Service.php";



//MA end


class Adm_PengadilantkiController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
	private $Logfile;	
	private $iduser;
	private $namauser;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->view->jumlahDataPerHalaman = $registry->get('jumlahDataPerHalaman');
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
		
		$ssogroup = new Zend_Session_Namespace('ssogroup');
	   // echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->i_organisasi;
	    $this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		
		$this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();		
    }
	
    public function indexAction() {
	   
    }
	
	public function pengadilantkijsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('pengadilantkijs');
    }
	
	public function pengadilantkijuduliconAction()
	{
		//untuk mengeluarkan judul dan icon Kondisi Sistem TI 
	}
	
	//MA
	//----
	public function daftarpengadilantkiAction()
	{
		$currentPage = $_REQUEST['currentPage']; 
		$this->view->noTitle = $_REQUEST['noTitle'];	
		if((!$currentPage) || ($currentPage == 'undefined'))
		{
			$currentPage = 1;
		} 
		
		$pageNumber = $_REQUEST['currentPage'];
		if(!$pageNumber) {$pageNumber = 1;}
		
		$itemPerPage = $_REQUEST['numToDisplay'];
		if(!$itemPerPage) {$itemPerPage 	= 10;}
		
		$this->view->kategoriList = $this->refPengadilan_serv->getKategoriList();
		
		$kategoriCari 	= $_REQUEST['kategoriCari']; //'username';
		$katakunci 		= $_REQUEST['katakunci'];
		
		$param1 = $_REQUEST['param1'];
		if($param1){$kategoriCari 	= $param1;}
		$param2 = $_REQUEST['param2'];
		if($param2){$katakunci 	= $param2;}
		
		$this->view->kategoriCari = $kategoriCari;
		$this->view->katakunci = $katakunci;
		
		$sortBy			= 'i_organisasi';
		$sort			= 'asc';
		
		$dataMasukan = array("pageNumber" 	=> $pageNumber,
							"itemPerPage" 	=> $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunci" => $katakunci,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$numToDisplay = $itemPerPage;
		$this->view->numToDisplay = $numToDisplay;
		$this->view->currentPage = $currentPage;
		$this->view->pengadilantkiList = $this->refPengadilan_serv->pengadilantkiList($dataMasukan);

		$dataMasukan = array("pageNumber" 	=> 0,
							"itemPerPage" 	=> 0,
							"kategoriCari" => $kategoriCari,
							"katakunci" => $katakunci,
							"sortBy" => $sortBy,
							"sort" => $sort);
		$this->view->totpengadilantkiList = $this->refPengadilan_serv->pengadilantkiList($dataMasukan);;
		
	}
	
	public function pengadilantkiolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$this->view->i_organisasi = $_REQUEST['i_organisasi'];
		$this->view->kategoriList = $this->refPengadilan_serv->getKategoriList();
		$dataMasukan = array("c_kategori_organisasi" => $this->view->kategoriList[0]['id']);
		
		$this->view->dataPengadilanBandingByKategori = $this->refPengadilan_serv->pengadilanBandingListByKategori($dataMasukan);
		
		$dataMasukan = array("i_organisasi" => $this->view->i_organisasi);
		$this->view->detailPengadilanBanding = $this->refPengadilan_serv->getdetailPengadilanBanding($dataMasukan);
		//var_dump($this->view->detailPengadilanBanding);
	}
	
	public function pengadilanbandinglistAction()
	{
		$idKategori = $_REQUEST['idKategori'];
		
		$dataMasukan = array("c_kategori_organisasi" => $idKategori);
		$this->view->dataPengadilanBandingByKategori = $this->refPengadilan_serv->pengadilanBandingListByKategori($dataMasukan);
	}
	///////////////////////////////////////////////////////////////////////////////////////////end TKI
	
	public function pengadilantkiAction()
	{
		//$ = $_POST['i_organisasi'];
		
		$c_kategori_organisasi = $_POST['kategori'];
		$idPengadilanBanding = $_POST['idPengadilanBanding'];
		$n_organisasi 		= $_POST['n_organisasi'];
		$i_entry	= $this->userid;
		
		$dataMasukan = array("i_organisasi_parent" => $idPengadilanBanding,
							 "c_kategori_organisasi" => $c_kategori_organisasi,
							 "n_organisasi" => $n_organisasi,
							 "i_entry" => $i_entry);
							 
		$this->view->pengadilanTkiInsert = $this->refPengadilan_serv->pengadilantkiInsert($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "Pengadilan Tingkat I";
		$this->view->hasil = $this->view->pengadilanTkiInsert; 
		
		$this->daftarpengadilantkiAction();
		$this->render('daftarpengadilantki');
		
		$numToDisplay = 20;
		//$this->view->userList = $this->ref_serv->getNipList();
	}
	
	
	public function pengadilantkihapusAction()
	{
		$i_organisasi 		= $_REQUEST['i_organisasi'];
		//echo $user_id;
		$dataMasukan = array("i_organisasi" => $i_organisasi);
		
		$this->view->pengadilanbandingHapus = $this->refPengadilan_serv->pengadilanbandingHapus($dataMasukan);
		$this->view->proses = "3";	
		$this->view->keterangan = "Pengadilan Tingkat I";
		$this->view->hasil = $this->view->pengadilanbandingHapus;
		
		$this->daftarpengadilantkiAction();
		$this->render('daftarpengadilantki');
		
	}

		public function pengadilantkiupdateAction()
	{
		$i_organisasi = $_POST['i_organisasi'];
		$i_organisasi_parent = $_POST['idPengadilanBanding'];
		$n_organisasi = $_POST['n_organisasi'];
		$c_kategori_organisasi = $_POST['kategori'];
		$i_entry	= $this->userid;
		
		$dataMasukan = array("i_organisasi" => $i_organisasi,
							 "i_organisasi_parent" => $i_organisasi_parent,
							 "n_organisasi" => $n_organisasi,
							 "c_kategori_organisasi" => $c_kategori_organisasi,
							 "i_entry" => $i_entry);
							 
		$this->view->pengadilantkiUpdate = $this->refPengadilan_serv->pengadilantkiUpdate($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "Pengadilan Tingkat I";
		$this->view->hasil = $this->view->pengadilantkiUpdate; 
		
		$this->daftarpengadilantkiAction();
		$this->render('daftarpengadilantki');
		
		//$this->view->userList = $this->ref_serv->getNipList();
	}
	

	//////////////////////////////////////////////////////////////////
	
	public function organisasibandingAction()
	{
		$kategori = $_REQUEST['kategori'];
		$this->view->dataOrganisasiBanding = $this->refPengadilan_serv->organisasiTkIList($kategori);
	}
	
	public function organisasitk2Action(){
		$idOrgTkI = $_REQUEST['idOrgTkI'];
		$dataMasukan = array("idPengadilanBanding" => $idOrgTkI);
		$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
}
?>