<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_Laporanall_Service.php";
require_once "service/aplikasi/Aplikasi_software_Service.php";
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";


class Aplikasi_LaporanallController extends Zend_Controller_Action {

    public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->laporan_serv = aplikasi_laporanall_Service::getInstance();
        $this->software_serv = aplikasi_software_Service::getInstance();
        $this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
    }

    public function indexAction()
    {

    }
    public function laporanallAction()
    {
        $this->view->pengadilanList = $this->refPengadilan_serv->pengadilanListAll();
        //$this->view->datapengadilan= $this->software_serv->getTmPengadilan($_GET);
       $this->view->datalaporan= $this->laporan_serv->getLaporan("");
    }

    public function pengadilantkilistAction(){
        $idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
        $dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
        $this->view->pengadilanList = $this->refPengadilan_serv->pengadilanList($dataMasukan);
    }

    public function laporanalllistAction()
    {

        $id_pengadilan=$_GET['id_pengadilan'];
        $tahun=$_GET['tahun'];
        $cari = '';
        if ($id_pengadilan != ''){
            $cari .= " and b.i_organisasi=$id_pengadilan";
        }
        if ($tahun != ''){
            $cari .= " and a.d_tahun_kuesioner='$tahun' ";
        }
        //echo "xxxx".$cari;
        $this->view->datalaporan= $this->laporan_serv->getLaporan($cari);
        $this->_helper->viewRenderer('laporanalllist');
    }


    public function laporanexcelAction()
    {
        $id_pengadilan=$_GET['id_pengadilan'];
        $bulan=$_GET['bulan'];
        $tahun=$_GET['tahun'];

        if ($id_pengadilan == '-'){
            $cari= " ";
        } else {
            $cari= " and a.id_pengadilan=$id_pengadilan";
        }

        if ($bulan){$cari= $cari." and a.d_bulan_kuesioner='$bulan' ";}
        if ($tahun){$cari= $cari." and a.d_tahun_kuesioner='$tahun' ";}
        $this->view->datalaporan= $this->laporan_serv->getLaporan($cari);
    }

    public function laporanalljsAction()
    {
        header('content-type : text/javascript');
        $this->render('laporanalljs');
    }
}

