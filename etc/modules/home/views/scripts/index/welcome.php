					
				   <div class="box-modul">
				       <div class="hd">Selamat Datang  <span class="user"><?echo $this->username;?></span></div>		   
						<div class="bd">
							<p>Sistem Informasi Kuesioner ini merupakan sarana untuk pengumpulan dan pengolahan data pengembangan 
								Sistem Informasi Manajemen Mahkamah Agung RI.
								Kuesioner ini dibagi dalam 3 (tiga) kelompok besar pertanyaan yaitu mengenai kondisi sistem informasi pengadilan, 
								anggaran pengembangan Sistem TI pada DIPA dan Donor, dan Deskinfo (Meja Informasi).
								<br><br><b> I.   Kondisi Sistem Informasi Pengadilan menyangkut Sumber Daya yang dimiliki pengadilan yaitu : </b>
								<ul class="lsb" >
									<li><span>Sumber Daya Manusia (Human Resource)</span><br>
									<li><span>Perangkat Lunak (Software)</span><br>
									<li><span>Perangkat Keras (Hardware)</span><br>
									<li><span>Perangkat jaringan (Network)</span><br>
									<li><span>Prosedur</span><br>
									<li><span>Agenda</span><br>
								</ul>
							
							&nbsp;<br><br><b> II.  Anggaran Pengembangan Sistem TI </b>
							&nbsp;<br><br><b> III. Deskinfo</b>
							</p>	
							
							<p>Untuk melakukan pengisian data kuesioner, silahkan masuk ke menu <b>'KUESIONER'</b>.</p>
							<p>Selamat bekerja!   
						</div>
						<div class="ft"></div>
					   
			         </div>