<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/adm/Adm_Admuser_Service.php";
require_once "service/adm/Adm_Admgroup_Service.php";
//require_once "service/adm/Adm_Admstatus_Service.php";
require_once "service/aplikasi/Aplikasi_Referensi_Service.php";
require_once "service/adm/logfile.php";
require_once "service/sso/Sso_User_Service.php";

//MA
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";

//MA end


class Adm_UserController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
	private $Logfile;	
	private $iduser;
	private $namauser;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->view->jumlahDataPerHalaman = $registry->get('jumlahDataPerHalaman');
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
		
		$ssogroup = new Zend_Session_Namespace('ssogroup');
	   // echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->i_organisasi;
	    $this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		
		$this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
		
	    $this->user_serv = Adm_Admuser_Service::getInstance();
		$this->group_serv = Adm_Admgroup_Service::getInstance();
//		$this->status_serv = Adm_Admstatus_Service::getInstance();
		$this->ref_serv = Aplikasi_Referensi_Service::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
	    $this->Logfile = new logfile;
		
    }
	
    public function indexAction() {
	   
    }
	
	public function userjsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('userjs');
    }
	
	//MA
	//----
	public function daftaruserAction()
	{
		$currentPage = $_REQUEST['currentPage']; 
			
		if((!$currentPage) || ($currentPage == 'undefined'))
		{
			$currentPage = 1;
		} 
		
		$kategoriCari 	= $_REQUEST['kategoriCari']; //'username';
		$katakunciCari 	= $_POST['carii'];
		$sortBy			= 'i_user';
		$sort			= 'asc';
		
		$dataMasukan = array("kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$numToDisplay = $this->view->jumlahDataPerHalaman;
		$this->view->numToDisplay = $numToDisplay;
		$this->view->currentPage = $currentPage;
		$this->view->totUserList = $this->user_serv->cariUserList($dataMasukan,0,0);
		$this->view->userList = $this->user_serv->cariUserList($dataMasukan,$currentPage, $numToDisplay);		
	}
	
	public function userolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iUser = $_REQUEST['user_id'];
		
		$this->view->detailUser = $this->user_serv->detailUserById($iUser);
		
		//$kelompok = '1'; //pengadilan
		
		$pengadilan = array('1','2','3','4');
		$satker = array('5','6','7','8','9','10');
		
		//echo "id_org = ".$this->detailUser['c_kategori_organisasi'];
		if (strtolower($this->view->jenisForm) == 'insert') {
			$kelompok = '1';
		} else {
			if (in_array($this->view->detailUser['c_kategori_organisasi'], $pengadilan)){
				$kelompok = '1';
			} else if (in_array($this->view->detailUser['c_kategori_organisasi'], $satker)){
				$kelompok = '2';
			} else {
				$kelompok = '3';
			}
		}
		if ($kelompok != '3'){
			$this->view->dataOrganisasiBanding = $this->refPengadilan_serv->organisasiTkIList($kelompok);
		
		
			$dataMasukan = array("idPengadilanBanding" => $this->view->dataOrganisasiBanding[0]['i_organisasi']);
			$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);
		}
		if ($this->view->jenisForm == 'update'){
			$dataMasukan = array("idPengadilanBanding" => $this->view->detailUser['i_organisasi_parent']);
			//var_dump($dataMasukan);
			$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);
		} else {
			// $this->view->detailUser = $this->user_serv->detailUserById($iUser);
			// var_dump($this->view->detailUser);
			$dataMasukan = array("idPengadilanBanding" => $this->view->dataOrganisasiBanding[0]['i_organisasi']);
			$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);
		}
		//$this->view->groupList = $this->group_serv->getGroupListAll();
		
		$numToDisplay = 20;
		//$this->view->userList = $this->ref_serv->getNipList();
	}
	
	public function organisasibandingAction()
	{
		$kategori = $_REQUEST['kategori'];
		$this->view->dataOrganisasiBanding = $this->refPengadilan_serv->organisasiTkIList($kategori);
	}
	
	public function organisasitk2Action(){
		$idOrgTkI = $_REQUEST['idOrgTkI'];
		$dataMasukan = array("idPengadilanBanding" => $idOrgTkI);
		$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);

		
	}
	public function usertk2Action(){
		$idOrgTk2 = $_REQUEST['idOrgTk2'];
		//$this->view->idOrgTk2 = $idOrgTk2;
		$dataMasukan2 = array("i_organisasi" => $idOrgTk2);
		$this->view->dataUserTk2 = $this->user_serv->userByIdOrgList($dataMasukan2);
	   
	}
	public function userAction()
	{
		$i_user			= $_POST['i_user'];      
		$n_user			= $_POST['n_user'];
		$n_password		= $_POST['n_password'];
		$n_password2	= $_POST['n_password'];		
		$kelompok		= $_POST['kelompok'];     
		$id_organisasiTkI = $_POST['id_organisasiTkI'];
		$id_organisasiTk2 = $_POST['id_organisasiTk2'];      
		$i_entry 		= $this->userid;      

		$dataMasukan = array("i_user" 		=>$i_user,
							"n_user" 		=>$n_user,
							"n_password"  	=>$n_password,
							"n_password2" 	=>$n_password2,
							"kelompok" 		=>$kelompok,
							"id_organisasiTkI" =>$id_organisasiTkI,
							"id_organisasiTk2" =>$id_organisasiTk2,
							"i_entry"		=>$i_entry);
			
		$this->view->userInsert = $this->user_serv->userInsert($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->userInsert; 
		
		$this->daftaruserAction();
		$this->render('daftaruser');
	}
	
	public function userupdateAction()
	{
		$i_user			= $_POST['i_user'];      
		$n_user			= $_POST['n_user'];
		$n_password		= $_POST['n_password'];
		$n_password2	= $_POST['n_password'];		
		$kelompok		= $_POST['kelompok'];     
		$id_organisasiTkI = $_POST['id_organisasiTkI'];
		$id_organisasiTk2 = $_POST['id_organisasiTk2'];      
		$i_entry 		= $this->userid;      

		$dataMasukan = array("i_user" 		=>$i_user,
							"n_user" 		=>$n_user,
							"n_password"  	=>$n_password,
							"n_password2" 	=>$n_password2,
							"kelompok" 		=>$kelompok,
							"id_organisasiTkI" =>$id_organisasiTkI,
							"id_organisasiTk2" =>$id_organisasiTk2,
							"i_entry"		=>$i_entry);
		
		$this->view->userUpdate = $this->user_serv->userUpdate($dataMasukan);
		$this->view->proses = "2";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->userUpdate;
		
		$this->daftaruserAction();
		$this->render('daftaruser');
	}
	
	public function userhapusAction()
	{
		$i_user 		= $_REQUEST['i_user'];
		//echo $user_id;
		$dataMasukan = array("i_user" => $i_user);
		
		$this->view->userHapus = $this->user_serv->userHapus($dataMasukan);
		$this->view->proses = "3";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->userHapus;
		
		$this->daftaruserAction();
		$this->render('daftaruser');
	}
	
	public function usergantipwdAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iUser = $_REQUEST['user_id'];
		$this->view->detailUser = $this->user_serv->detailUserById($iUser);
		$this->view->groupList = $this->group_serv->getGroupListAll();
//		$this->view->statusList = $this->status_serv->getStatusListAll();
		
		$numToDisplay = 20;
		$this->view->userList = $this->ref_serv->getNipList();
	}
	
	public function usergantipasswdAction()
	{
		$user_id		= $_POST['user_id'];       
		$username		= $_POST['username'];      
		$nip			= $_POST['nip'];           
		$kd_status		= $_POST['kd_status'];     
		$password		= $_POST['password'];
		$c_group		= $_POST['c_group'];
		
		
		$dataMasukan = array("user_id"  	=>$user_id,
							"username"  	=>$username,
							"nip" 			=>$nip,
							"kd_status" 	=>$kd_status,
							"password"  	=>$password,
							"c_group"  		=>$c_group);
		
		$this->view->userUpdate = $this->user_serv->userUpdate($dataMasukan);
		$this->Logfile->createLog($this->view->namauser, "Ganti Password", $username." (".$nip.")");
		$this->view->proses = "2";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->userUpdate;
		
		//$this->userlistAction();
		//$this->render('userlist');
		$opt = array();
		$this->_redirect('http://'.$_SERVER['SERVER_NAME'].$this->basePath, $opt);
	}
	
	public function ubahpasswordolahdataAction()
	{
		$userid		= $_REQUEST['userid'];       	
		$this->view->detailUser = $this->user_serv->detailUserById($userid);
	}

	public function ubahpasswordAction()
	{
		$user_id = $_POST['i_user'];
		$n_passwordlama = $_POST['n_passwordlama'];
		$n_passwordbaru = $_POST['n_passwordbaru'];
		$n_password2 = $_POST['n_password2'];
		$masukanUbahPassword = array("user_id" => $user_id,
									 "n_passwordlama" =>$n_passwordlama,
									 "n_passwordbaru" =>$n_passwordbaru,
									 "n_password2" =>$n_password2);
		$this->view->hasilUbahPassword = $this->user_serv->ubahPasswd($masukanUbahPassword);
		//echo "hasil = ".$this->view->hasilUbahPassword;
		
		$this->view->proses = "2";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->hasilUbahPassword;
		
		$this->view->detailUser = $this->user_serv->detailUserById($user_id);
		//$this->ubahpasswordolahdataAction();
		$this->render('ubahpasswordolahdata');

	}
	
	public function ubahstatusAction()
	{
		$user_id		= $_GET['userid'];       
		$status		= $_GET['status'];      
		
		//$i_entry 		= $this->user;
		
		
		$dataMasukan = array("kd_status"  	=>$status,
							"user_id" => $user_id);
		$this->view->ubahStatus = $this->user_serv->ubahStatus($dataMasukan);
		$this->Logfile->createLog($this->view->namauser, "Ubah status data user", $user_id." (".$status.")");
		$this->view->proses = "2";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->ubahStatus;
		
		$this->userlistAction();
		$this->render('userlist');
	}
	
	
	public function getnamaPegawaiAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		
		$nip = $_REQUEST['nip'];
		
		$nama = $this->ref_serv->getnamaPegawai($nip);
		if ($nama == ""){
			$nama = "kosong";
		}
		echo "$nama";	
	}
	public function getpasswdpegawaiAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		
		$passwordlm = $_REQUEST['passwordlm'];
		$user_id = $_REQUEST['user_id'];
		$userid = $this->sso_serv->getDataUser2($user_id,$passwordlm);
		
		echo "$userid";	
	}
	
	public function listpegawaiAction()
	{
		$kategoriCari = $_REQUEST['kategoriCari'];
		$kataKunci = $_REQUEST['kataKunci'];
		$this->view->par = $par;
		$this->view->listPegawai = $this->user_serv->getDataPegawai($kategoriCari, $kataKunci);
	}
}
?>