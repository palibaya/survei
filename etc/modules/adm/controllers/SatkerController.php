<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/adm/Adm_Admuser_Service.php";
require_once "service/adm/Adm_Admgroup_Service.php";
//require_once "service/adm/Adm_Admstatus_Service.php";
require_once "service/aplikasi/Aplikasi_Referensi_Service.php";
require_once "service/adm/logfile.php";
require_once "service/sso/Sso_User_Service.php";

//MA
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";


//MA end


class Adm_SatkerController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
	private $Logfile;	
	private $iduser;
	private $namauser;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->view->jumlahDataPerHalaman = $registry->get('jumlahDataPerHalaman');
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
		
		$ssogroup = new Zend_Session_Namespace('ssogroup');
	   // echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->i_organisasi;
	    $this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		
		$this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
		
	    $this->user_serv = Adm_Admuser_Service::getInstance();
		$this->group_serv = Adm_Admgroup_Service::getInstance();
//		$this->status_serv = Adm_Admstatus_Service::getInstance();
		$this->ref_serv = Aplikasi_Referensi_Service::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
	    $this->Logfile = new logfile;
		
    }
	
    public function indexAction() {
	   
    }
	
	public function satkerjsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('satkerjs');
    }
	
	//MA
	//----
	public function daftarsatkerAction()
	{
		$currentPage = $_REQUEST['currentPage']; 
		$this->view->noTitle = $_REQUEST['noTitle'];	
		if((!$currentPage) || ($currentPage == 'undefined'))
		{
			$currentPage = 1;
		} 
		
		$pageNumber = $_REQUEST['currentPage'];
		if(!$pageNumber) {$pageNumber = 1;}
		
		$itemPerPage = $_REQUEST['numToDisplay'];
		if(!$itemPerPage) {$itemPerPage 	= 10;}
		
		$this->view->kategoriList = $this->refPengadilan_serv->getKategoriSatkerList();
		
		$kategoriCari 	= $_REQUEST['kategoriCari']; //'username';
		$katakunci 		= $_REQUEST['katakunci'];
		
		$param1 = $_REQUEST['param1'];
		if($param1){$kategoriCari 	= $param1;}
		$param2 = $_REQUEST['param2'];
		if($param2){$katakunci 	= $param2;}
		
		$this->view->kategoriCari = $kategoriCari;
		$this->view->katakunci = $katakunci;
		$sortBy			= 'i_organisasi';
		$sort			= 'asc';
		
		$dataMasukan = array("pageNumber" 	=> $pageNumber,
							"itemPerPage" 	=> $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunci" => $katakunci,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$numToDisplay = $itemPerPage;
		$this->view->numToDisplay = $numToDisplay;
		$this->view->currentPage = $currentPage;
		$this->view->satkerList = $this->refPengadilan_serv->satkerList($dataMasukan);

		$dataMasukan = array("pageNumber" 	=> 0,
							"itemPerPage" 	=> 0,
							"kategoriCari" => $kategoriCari,
							"katakunci" => $katakunci,
							"sortBy" => $sortBy,
							"sort" => $sort);
		$this->view->totsatkerList = $this->refPengadilan_serv->satkerList($dataMasukan);;
	}
	
	public function satkerolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$this->view->i_organisasi = $_REQUEST['i_organisasi'];
		$this->view->kategoriList = $this->refPengadilan_serv->getKategoriSatkerList();
		$dataMasukan = array("i_organisasi" => $this->view->i_organisasi);
		$this->view->detailPengadilanBanding = $this->refPengadilan_serv->getdetailPengadilanBanding($dataMasukan);
		
		//$this->view->userList = $this->ref_serv->getNipList();
	}
	
	public function satkerAction()
	{
		//$ = $_POST['i_organisasi'];
		$n_organisasi = $_POST['n_organisasi'];
		$c_kategori_organisasi = $_POST['kategori'];
		$i_entry	= $this->userid;
		
		$dataMasukan = array("n_organisasi" => $n_organisasi,
							 "c_kategori_organisasi" => $c_kategori_organisasi,
							 "i_entry" => $i_entry);
							 
		$this->view->satkerInsert = $this->refPengadilan_serv->pengadilanbandingInsert($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "Satker";
		$this->view->hasil = $this->view->satkerInsert; 
		
		$this->daftarsatkerAction();
		$this->render('daftarsatker');
		
		$numToDisplay = 20;
		//$this->view->userList = $this->ref_serv->getNipList();
	}
	
	public function satkerupdateAction()
	{
		$i_organisasi = $_POST['i_organisasi'];
		$n_organisasi = $_POST['n_organisasi'];
		$c_kategori_organisasi = $_POST['kategori'];
		$i_entry	= $this->userid;
		
		$dataMasukan = array("i_organisasi" => $i_organisasi,
							 "n_organisasi" => $n_organisasi,
							 "c_kategori_organisasi" => $c_kategori_organisasi,
							 "i_entry" => $i_entry);
							 
		$this->view->pengadilanbandingUpdate = $this->refPengadilan_serv->pengadilanbandingUpdate($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "Pengadilan Banding";
		$this->view->hasil = $this->view->pengadilanbandingUpdate; 
		
		$this->daftarsatkerAction();
		$this->render('daftarsatker');
		
		//$this->view->userList = $this->ref_serv->getNipList();
	}
	
	
	public function satkerhapusAction()
	{
		$i_organisasi 		= $_REQUEST['i_organisasi'];
		//echo $user_id;
		$dataMasukan = array("i_organisasi" => $i_organisasi);
		
		$this->view->pengadilanbandingHapus = $this->refPengadilan_serv->pengadilanbandingHapus($dataMasukan);
		$this->view->proses = "3";	
		$this->view->keterangan = "satker";
		$this->view->hasil = $this->view->pengadilanbandingHapus;
		
		$this->daftarsatkerAction();
		$this->render('daftarsatker');
		
	}
	
	//////////////////////////////////////////////////////////////////
	
	public function organisasibandingAction()
	{
		$kategori = $_REQUEST['kategori'];
		$this->view->dataOrganisasiBanding = $this->refPengadilan_serv->organisasiTkIList($kategori);
	}
	
	public function organisasitk2Action(){
		$idOrgTkI = $_REQUEST['idOrgTkI'];
		$dataMasukan = array("idPengadilanBanding" => $idOrgTkI);
		$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
}
?>