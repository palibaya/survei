<ul id="adminmenu">
	<li class="wp-menu-separator"><br></li>
	<li class="wp-has-submenu menu-top menu-top-first" id="menu-responden" title="Data Responden">
		<div class="wp-menu-image"><br></div>
		<div style="display: none;" class="wp-menu-toggle"><br></div>
		<a href="#" onClick="open_url_to_right('/aplikasi/responden/respondenolahdata?idKuesioner=10&jenisForm=1','/aplikasi/responden/respondenjs');" class="menu-top" tabindex="1">Data Responden <span id="awaiting-mod" class="count-0"><span class="pending-count">0</span></span></a>
	</li>
	<li class="wp-has-submenu menu-top" id="menu-sistem"  title="Kondisi Sistem TI Pengadilan Tingkat Pertama/Banding">
		<div class="wp-menu-image"><br></div><div class="wp-menu-toggle"><br></div>
			<a href="#" class="wp-has-submenu wp-menu-open menu-top menu-top-first" tabindex="1">Kondisi Sistem TI Pengadilan Tingkat Pertama/Banding</a>
			<div class="wp-submenu"><div class="wp-submenu-head">Kondisi Sistem TI Pengadilan Tingkat Pertama/Banding</div>
			<ul>
				<li class="wp-first-item"><a href="#" class="wp-first-item" tabindex="1" onClick="open_url_to_right('/aplikasi/sdm/sdmOlahData?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/sdm/sdmjs');">Sumber Daya Manusia</a></li>
				<li><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/software/software?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/software/softwarejs');">Software</a></li>
				<li><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/hardware/hardware?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/hardware/hardwarejs');">Hardware</a></li>
				<li><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/jaringan/jaringan?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/jaringan/jaringanjs');">Jaringan</a></li>
				<li><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/prosedure/prosedure?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/prosedure/prosedurejs');">Prosedur</a></li>
			</ul>
		</div>
	</li>
	
	<li class="wp-has-submenu menu-top" id="menu-anggaran" title="Anggaran Pengembangan Sistem TI">
		<div class="wp-menu-image"><br></div>
		<div class="wp-menu-toggle"><br></div>
		<a href="#" class="wp-has-submenu menu-top" tabindex="1">Anggaran Pengembangan Sistem TI</a>
		<div class="wp-submenu"><div class="wp-submenu-head">Anggaran Pengembangan Sistem TI</div>
			<ul>
				<li class="wp-first-item"><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/kegiatan/kegiatan?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/kegiatan/kegiatanjs');">Kegiatan</a></li>
			</ul>
		</div>
	</li>
	<li class="wp-has-submenu menu-top" id="menu-sistem" title="Deskinfo">
		<div class="wp-menu-image"><br></div>
		<div class="wp-menu-toggle"><br></div>
		<a href="#" class="wp-has-submenu menu-top" tabindex="1">Deskinfo</a>
		<div class="wp-submenu"><div class="wp-submenu-head">Deskinfo</div>
			<ul>
				<li class="wp-first-item"><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/deskinfo/deskinfo?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/deskinfo/deskinfojs');">Deskinfo</a></li>
				<li class="wp-first-item"><a href="#" tabindex="1" onClick="open_url_to_right('/aplikasi/chart/chart?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/chart/chartjs');">Chart</a></li>
			</ul>
		</div>
	</li>
	<!--li class="wp-has-submenu menu-top" id="menu-sistem"  title="Deskinfo">
		<div class="wp-menu-image"><br></div>
		<div style="display: none;" class="wp-menu-toggle"><br></div>
		<a href="#" class="wp-has-submenu menu-top" tabindex="1"onClick="open_url_to_right('/aplikasi/deskinfo/deskinfo?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/deskinfo/deskinfojs');">Deskinfo</a></li>
	</li-->	

	<li class="wp-has-submenu menu-top menu-top-last" id="menu-deskinfo" title="Cetak Form Kuesioner">
		<div class="wp-menu-image"><br></div>
		<div style="display: none;" class="wp-menu-toggle"><br></div>
		<a href="#" class="wp-has-submenu menu-top" tabindex="1"onClick="open_url_to_right('/aplikasi/laporan/laporan?idKuesioner=<?php echo $this->detailRespondenTerakhir['id'];?>','/aplikasi/laporan/laporanjs');">Cetak Form Kuesioner</a></li>
	<li class="wp-menu-separator"><br></li>
</ul>

<?/*
			<li><a title="Data Responden" href="javascript:surveyForm('insert','');">Data Responden</a></li>
			</ul>
			<ul>
			<li><a title="Kondisi Sistem TI Pengadilan Tingkat Pertama/ Banding" href="javascript:surveyForm('insert','');">Kondisi Sistem TI Pengadilan Tingkat Pertama/ Banding</a></li>
			<ul>
				<li><a title="Sumber Daya Manusia" href="#" onClick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/sdm/sdmOlahData?idKuesioner=<? echo $idKuesioner;?>','<?php echo $this->basePath; ?>/aplikasi/sdm/sdmjs');">Sumber Daya Manusia</a></li>
				<li><a title="Software" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/software/software','<?php echo $this->basePath; ?>/aplikasi/software/softwarejs');">Software</a></li>
				<li><a title="Hardware" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/hardware/hardware','<?php echo $this->basePath; ?>/aplikasi/hardware/hardwarejs');">Hardware</a></li>
				<li><a title="Jaringan" href="javascript:jaringanForm('insert','');">Jaringan</a></li>
				<li><a title="Prosedure" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/prosedure/prosedure','<?php echo $this->basePath; ?>/aplikasi/prosedure/prosedurejs');">Prosedure</a></li>
			</ul>
			</ul>
			
			<ul>
			<li><a title="Anggaran Pengembangan Sistem TI" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/anggaranpengembangan/anggaranpengembangan','<?php echo $this->basePath; ?>/aplikasi/anggaranpengembangan/anggaranpengembanganjs');Pros();">Anggaran Pengembangan Sistem TI</a></li>
			<ul>
				<li><a title="Kegiatan" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/kegiatan/kegiatan','<?php echo $this->basePath; ?>/aplikasi/kegiatan/kegiatanjs');Pros();">Kegiatan</a></li>
				<!--
				<li><a title="Kegiatan II" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/kegiatan2/kegiatan2','<?php echo $this->basePath; ?>/aplikasi/kegiatan2/kegiatan2js');">Kegiatan II</a></li>
				<li><a title="Kegiatan III" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/kegiatan3/kegiatan3','<?php echo $this->basePath; ?>/aplikasi/kegiatan3/kegiatan3js');">Kegiatan III</a></li>
				<li><a title="Kegiatan IV" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/kegiatan4/kegiatan4','<?php echo $this->basePath; ?>/aplikasi/kegiatan4/kegiatan4js');">Kegiatan IV</a></li>
				<li><a title="Kegiatan V" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/kegiatan5/kegiatan5','<?php echo $this->basePath; ?>/aplikasi/kegiatan5/kegiatan5js');">Kegiatan V</a></li>
				-->
				
			</ul>
			</ul>
			<ul>
			<li><a title="Deskinfo" href="#" onclick="open_url_to_div('<?php echo $this->basePath; ?>/aplikasi/deskinfo/deskinfo','<?php echo $this->basePath; ?>/aplikasi/deskinfo/deskinfojs');Pros();">Deskinfo</a></li>
			</ul>
	</div>
	*/
	?>
