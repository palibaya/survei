<?php

require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_jaringan_Service.php";
class Aplikasi_KontakkamiController extends Zend_Controller_Action {

    public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->jaringan_serv = aplikasi_jaringan_Service::getInstance();
        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;
    }

    public function indexAction()
    {
    }
    public function kontakkamiAction()
    {

    }
    public function kontakkamijuduliconAction()
    {
        //untuk mengeluarkan judul dan icon Kondisi Sistem TI - Software
    }
}
?>
