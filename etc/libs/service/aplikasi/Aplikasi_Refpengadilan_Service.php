<?php
class Aplikasi_Refpengadilan_Service {
    private static $instance;

    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

    //======================================================================
    // List Pengumuman
    //======================================================================
    public function pengadilanbandingList($dataMasukan) {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $pageNumber     = $dataMasukan['pageNumber'];
            $itemPerPage    = $dataMasukan['itemPerPage'];
            $kategoriCari   = $dataMasukan['kategoriCari'];
            $katakunci  = strtoupper($dataMasukan['katakunci']);

            $xLimit=$itemPerPage;
            $xOffset=($pageNumber-1)*$itemPerPage;

            if(trim($kategoriCari) == 'Nama Pengadilan Banding'){
                $where = "and UPPER(n_organisasi) like '%$katakunci%'";
            } else if (($kategoriCari =='1') || ($kategoriCari =='2') ||
                       ($kategoriCari =='3') || ($kategoriCari =='4')) {

                $where = "and c_kategori_organisasi = '$kategoriCari'";
            }
            /* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
            $whereOpt = "$kategoriCari like '%$katakunciCari%' ";
            if($kategoriCari) { $where = $whereBase." and ".$whereOpt;}
            else { $where = $whereBase;}
            $order = "order by $sortBy $sort "; */
            $sqlProses = "select a.i_organisasi,
                                a.n_organisasi,
                                a.c_kategori_organisasi,
                                b.n_kategori
                            from tm_organisasi a
                            left join tr_kategori b on (a.c_kategori_organisasi = b.id)
                            where ((i_organisasi_parent is null) or (i_organisasi_parent = '' ))
                            and c_kategori_organisasi in ('1','2','3','4')
                            $where
                            order by i_organisasi";

            if(($pageNumber==0) && ($itemPerPage==0))
            {
                $sqlTotal = "select count(*) from ($sqlProses) a";

                $hasilAkhir = $db->fetchOne($sqlTotal);
            } else {
                $sqlData = $sqlProses." limit $xLimit offset $xOffset";
            //echo $sqlData;
                $result = $db->fetchAll($sqlData);

                $jmlResult = count($result);

                for ($j = 0; $j < $jmlResult; $j++) {
                    $hasilAkhir[$j] = array("i_organisasi"      =>(string)$result[$j]->i_organisasi,
                                           "n_organisasi"   =>(string)$result[$j]->n_organisasi,
                                           "c_kategori_organisasi"      =>(string)$result[$j]->c_kategori_organisasi,
                                           "n_kategori"     =>(string)$result[$j]->n_kategori
                                            );
                    //var_dump($hasilAkhir);
                }
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function pengadilanbandingList2() {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            /* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
            $whereOpt = "$kategoriCari like '%$katakunciCari%' ";
            if($kategoriCari) { $where = $whereBase." and ".$whereOpt;}
            else { $where = $whereBase;}
            $order = "order by $sortBy $sort "; */
            $sqlProses = "select a.i_organisasi,
                                a.n_organisasi,
                                a.c_kategori_organisasi,
                                b.n_kategori
                            from tm_organisasi a
                            left join tr_kategori b on (a.c_kategori_organisasi = b.id)
                            where ((i_organisasi_parent is null) or (i_organisasi_parent = '' ))
                            order by i_organisasi";

            //echo $sqlProses;
                $result = $db->fetchAll($sqlProses);

                $jmlResult = count($result);

                for ($j = 0; $j < $jmlResult; $j++) {
                    $hasilAkhir[$j] = array("i_organisasi"      =>(string)$result[$j]->i_organisasi,
                                           "n_organisasi"   =>(string)$result[$j]->n_organisasi,
                                           "c_kategori_organisasi"      =>(string)$result[$j]->c_kategori_organisasi,
                                           "n_kategori"     =>(string)$result[$j]->n_kategori
                                            );
                    //var_dump($hasilAkhir);
                }

            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }
    public function pengadilantkiList($dataMasukan) {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $pageNumber     = $dataMasukan['pageNumber'];
            $itemPerPage    = $dataMasukan['itemPerPage'];
            $kategoriCari   = $dataMasukan['kategoriCari'];
            $katakunci  = strtoupper($dataMasukan['katakunci']);

            $xLimit=$itemPerPage;
            $xOffset=($pageNumber-1)*$itemPerPage;

            if(trim($kategoriCari) == 'Nama Pengadilan Tingkat I'){
                $where = "and UPPER(n_organisasi) like '%$katakunci%'";
            } else if (($kategoriCari =='1') || ($kategoriCari =='2') ||
                       ($kategoriCari =='3') || ($kategoriCari =='4')) {
                $where = "and c_kategori_organisasi = '$kategoriCari'";
            }

            /* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
            $whereOpt = "$kategoriCari like '%$katakunciCari%' ";
            if($kategoriCari) { $where = $whereBase." and ".$whereOpt;}
            else { $where = $whereBase;}
            $order = "order by $sortBy $sort "; */
            $sqlProses = "select a.i_organisasi,
                                a.n_organisasi,
                                a.i_organisasi_parent,
                                a.c_kategori_organisasi,
                                b.n_kategori
                            from tm_organisasi a
                            left join tr_kategori b on (a.c_kategori_organisasi = b.id)
                            where a.c_kategori_organisasi in ('1','2','3','4')
                            $where
                            order by i_organisasi";

            if(($pageNumber==0) && ($itemPerPage==0))
            {
                $sqlTotal = "select count(*) from ($sqlProses) a";

                $hasilAkhir = $db->fetchOne($sqlTotal);
            } else {
                $sqlData = $sqlProses." limit $xLimit offset $xOffset";
            //echo $sqlData;
                $result = $db->fetchAll($sqlData);

                $jmlResult = count($result);

                for ($j = 0; $j < $jmlResult; $j++) {
                    if($result[$j]->i_organisasi_parent){
                        $n_organisasi_parent = $db->fetchOne("select n_organisasi
                                                            from tm_organisasi
                                                            where i_organisasi = '".$result[$j]->i_organisasi_parent."'
                                                            ");
                        $n_organisasi =(string)$result[$j]->n_organisasi;
                    } else {
                        $n_organisasi_parent = $db->fetchOne("select n_organisasi
                                                            from tm_organisasi
                                                            where i_organisasi = '".$result[$j]->i_organisasi."'
                                                            ");
                        $n_organisasi = '';
                    }
                    $hasilAkhir[$j] = array("i_organisasi"      =>(string)$result[$j]->i_organisasi,
                                           "n_organisasi"       =>$n_organisasi,
                                           "i_organisasi_parent" =>(string)$result[$j]->i_organisasi_parent,
                                           "n_organisasi_parent" => $n_organisasi_parent,
                                           "c_kategori_organisasi" =>(string)$result[$j]->c_kategori_organisasi,
                                           "n_kategori"         =>(string)$result[$j]->n_kategori
                                            );
                    //var_dump($hasilAkhir);
                }
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function eseloniiList($dataMasukan) {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $pageNumber     = $dataMasukan['pageNumber'];
            $itemPerPage    = $dataMasukan['itemPerPage'];
            $kategoriCari   = $dataMasukan['kategoriCari'];
            $katakunci  = strtoupper($dataMasukan['katakunci']);

            $xLimit=$itemPerPage;
            $xOffset=($pageNumber-1)*$itemPerPage;

            if(trim($kategoriCari) == 'Nama Eselon II'){
                $where = "and UPPER(n_organisasi) like '%$katakunci%'";
            } else if (($kategoriCari =='5') || ($kategoriCari =='6') ||
                       ($kategoriCari =='7') || ($kategoriCari =='8') ||
                       ($kategoriCari =='9') || ($kategoriCari =='10')) {
                $where = "and c_kategori_organisasi = '$kategoriCari'";
            }

            /* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
            $whereOpt = "$kategoriCari like '%$katakunciCari%' ";
            if($kategoriCari) { $where = $whereBase." and ".$whereOpt;}
            else { $where = $whereBase;}
            $order = "order by $sortBy $sort "; */
            $sqlProses = "select a.i_organisasi,
                                a.n_organisasi,
                                a.i_organisasi_parent,
                                a.c_kategori_organisasi,
                                b.n_kategori
                            from tm_organisasi a
                            left join tr_kategori b on (a.c_kategori_organisasi = b.id)
                            where a.c_kategori_organisasi in ('5','6','7','8','9','10')
                            $where
                            order by i_organisasi";

            if(($pageNumber==0) && ($itemPerPage==0))
            {
                $sqlTotal = "select count(*) from ($sqlProses) a";

                $hasilAkhir = $db->fetchOne($sqlTotal);
            } else {
                $sqlData = $sqlProses." limit $xLimit offset $xOffset";
            //echo $sqlData;
                $result = $db->fetchAll($sqlData);

                $jmlResult = count($result);

                for ($j = 0; $j < $jmlResult; $j++) {
                    if($result[$j]->i_organisasi_parent){
                        $n_organisasi_parent = $db->fetchOne("select n_organisasi
                                                            from tm_organisasi
                                                            where i_organisasi = '".$result[$j]->i_organisasi_parent."'
                                                            ");
                        $n_organisasi =(string)$result[$j]->n_organisasi;
                    } else {
                        $n_organisasi_parent = $db->fetchOne("select n_organisasi
                                                            from tm_organisasi
                                                            where i_organisasi = '".$result[$j]->i_organisasi."'
                                                            ");
                        $n_organisasi = '';
                    }
                    $hasilAkhir[$j] = array("i_organisasi"      =>(string)$result[$j]->i_organisasi,
                                           "n_organisasi"       =>$n_organisasi,
                                           "i_organisasi_parent" =>(string)$result[$j]->i_organisasi_parent,
                                           "n_organisasi_parent" => $n_organisasi_parent,
                                           "c_kategori_organisasi" =>(string)$result[$j]->c_kategori_organisasi,
                                           "n_kategori"         =>(string)$result[$j]->n_kategori
                                            );
                    //var_dump($hasilAkhir);
                }
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function organisasiTkIList($kategori) {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            /* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
            $whereOpt = "$kategoriCari like '%$katakunciCari%' ";
            if($kategoriCari) { $where = $whereBase." and ".$whereOpt;}
            else { $where = $whereBase;}
            $order = "order by $sortBy $sort "; */
            if ($kategori == '1'){
                $where = "c_kategori_organisasi in ('1', '2', '3', '4')";
            } else if ($kategori == '2') {
                $where = "c_kategori_organisasi in ('5', '6', '7', '8', '9', '10')";
            } else if ($kategori == 'all') {
                $where = "c_kategori_organisasi in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')";
            }
            $sqlProses = "select i_organisasi,
                                n_organisasi
                            from tm_organisasi
                            where ((i_organisasi_parent is null) or (i_organisasi_parent = '' )) AND
                                    $where
                            order by i_organisasi";

            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("i_organisasi"      =>(string)$result[$j]->i_organisasi,
                                       "n_organisasi"   =>(string)$result[$j]->n_organisasi
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }


    public function pengadilanList($dataMasukan) {

        $idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select i_organisasi,
                                n_organisasi
                            from tm_organisasi
                            where i_organisasi_parent = '$idPengadilanBanding'
                            order by i_organisasi";
            //echo $sqlProses;
            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("i_organisasi"  =>(string)$result[$j]->i_organisasi,
                                       "n_organisasi"   =>(string)$result[$j]->n_organisasi
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }


    public function pengadilanListAll() {


        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select i_organisasi,
                                n_organisasi
                            from tm_organisasi
                            order by i_organisasi";
            //echo $sqlProses;
            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("i_organisasi"  =>(string)$result[$j]->i_organisasi,
                                       "n_organisasi"   =>(string)$result[$j]->n_organisasi
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }




    public function getalamatpengadilan($dataMasukan) {

        $idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select n_alamat
                            from tm_organisasi
                            where i_organisasi = '$idPengadilanBanding'";
            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("n_alamat" =>(string)$result[$j]->n_alamat
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function getnotelppengadilan($dataMasukan) {

        $idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select no_telepon
                            from tm_organisasi
                            where i_organisasi = '$idPengadilanBanding'";
            //echo $sqlProses;
            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("no_telepon" =>(string)$result[$j]->no_telepon
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function getnofaxpengadilan($dataMasukan) {

        $idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select no_fax
                            from tm_organisasi
                            where i_organisasi = '$idPengadilanBanding'";
            //echo $sqlProses;
            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("no_fax" =>(string)$result[$j]->no_fax
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function getkategoriList() {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select id, n_kategori from tr_kategori where id in ('1','2','3','4')";

            $result = $db->fetchall($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("id"    =>(string)$result[$j]->id,
                                        "n_kategori"    =>(string)$result[$j]->n_kategori
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function getkategoriAllList() {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select id, n_kategori from tr_kategori";

            $result = $db->fetchall($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("id"    =>(string)$result[$j]->id,
                                        "n_kategori"    =>(string)$result[$j]->n_kategori
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function getkategoriSatkerList() {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select id, n_kategori from tr_kategori where id not in ('1','2','3','4')";

            $result = $db->fetchall($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("id"    =>(string)$result[$j]->id,
                                        "n_kategori"    =>(string)$result[$j]->n_kategori
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function pengadilanbandingInsert(array $dataMasukan) {
        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        try {
            $db->beginTransaction();
            $maxidPengadilan = $db->fetchOne("select max(i_organisasi) from tm_organisasi");
            $idPengadilan = $maxidPengadilan+1;

            $paramInput = array("i_organisasi"          =>$idPengadilan,
                                "n_organisasi"          =>$dataMasukan['n_organisasi'],
                                "c_kategori_organisasi" =>$dataMasukan['c_kategori_organisasi'],
                                "i_entry"       =>$dataMasukan['i_entry'],
                                "d_entry"       =>date('Y-m-d'));

            //var_dump($paramInput);
            $db->insert('tm_organisasi',$paramInput);
            $db->commit();

            return 'sukses';
        } catch (Exception $e) {
            $db->rollBack();
            $errmsgArr = explode(":",$e->getMessage());

            $errMsg = $errmsgArr[0];

            if($errMsg == "SQLSTATE[23000]")
            {
                return "gagal.Data Sudah Ada.";
            }
            else
            {
                return $e->getMessage();
            }
       }
    }

    public function pengadilanbandingUpdate(array $dataMasukan) {
        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        try {
            $db->beginTransaction();

            $paramInput = array("n_organisasi"          =>$dataMasukan['n_organisasi'],
                                "c_kategori_organisasi" =>$dataMasukan['c_kategori_organisasi'],
                                "i_entry"       =>$dataMasukan['i_entry'],
                                "d_entry"       =>date('Y-m-d'));

            $where[] = " i_organisasi = '".$dataMasukan['i_organisasi']."'";

            $db->update('tm_organisasi',$paramInput, $where);
            $db->commit();

            return 'sukses';
        } catch (Exception $e) {
            $db->rollBack();
            $errmsgArr = explode(":",$e->getMessage());

            $errMsg = $errmsgArr[0];

            if($errMsg == "SQLSTATE[23000]")
            {
                return "gagal.Data Sudah Ada.";
            }
            else
            {
                return $e->getMessage();
            }
       }
    }

    public function pengadilantkiUpdate(array $dataMasukan) {
        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        try {
            $db->beginTransaction();

            $paramInput = array("n_organisasi"          =>$dataMasukan['n_organisasi'],
                                "i_organisasi_parent"   =>$dataMasukan['i_organisasi_parent'],
                                "c_kategori_organisasi" =>$dataMasukan['c_kategori_organisasi'],
                                "i_entry"       =>$dataMasukan['i_entry'],
                                "d_entry"       =>date('Y-m-d'));

            $where[] = " i_organisasi = '".$dataMasukan['i_organisasi']."'";

            $db->update('tm_organisasi',$paramInput, $where);
            $db->commit();

            return 'sukses';
        } catch (Exception $e) {
            $db->rollBack();
            $errmsgArr = explode(":",$e->getMessage());

            $errMsg = $errmsgArr[0];

            if($errMsg == "SQLSTATE[23000]")
            {
                return "gagal.Data Sudah Ada.";
            }
            else
            {
                return $e->getMessage();
            }
       }
    }

    public function pengadilanbandingHapus(array $dataMasukan) {
        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        try {
            $db->beginTransaction();

            $where[] = " i_organisasi = '".$dataMasukan['i_organisasi']."'";
            $db->delete('tm_organisasi',$where);
            $db->commit();

            return 'sukses';
        } catch (Exception $e) {
            $db->rollBack();
            $errmsgArr = explode(":",$e->getMessage());

            $errMsg = $errmsgArr[0];

            if($errMsg == "SQLSTATE[23000]")
            {
                return "gagal.Data Sudah Ada.";
            }
            else
            {
                return $e->getMessage();
            }
       }
    }

    public function getdetailPengadilanBanding($dataMasukan) {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $i_organisasi = $dataMasukan['i_organisasi'];
            $sqlProses = "select a.i_organisasi, a.c_kategori_organisasi, a.n_organisasi, a. i_organisasi_parent
                            from tm_organisasi a
                            where i_organisasi = '$i_organisasi'";

            $sqlData = $sqlProses;
            $result = $db->fetchRow($sqlData);

            $hasilAkhir = array("i_organisasi"          =>(string)$result->i_organisasi,
                                "i_organisasi_parent"       =>(string)$result->i_organisasi_parent,
                                "n_organisasi"      =>(string)$result->n_organisasi,
                                "c_kategori_organisasi"     =>(string)$result->c_kategori_organisasi
                                );
            //var_dump($hasilAkhir);
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function getnamapengadilan($dataMasukan) {

        $iOrganisasi = $dataMasukan['iOrganisasi'];

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select n_organisasi
                            from tm_organisasi
                            where i_organisasi = '$iOrganisasi'";
            $result = $db->fetchOne($sqlProses);
            if (strtoupper($iOrganisasi) == 'MA'){
                $result = "Mahkamah Agung";
            }
            return $result;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function pengadilanBandingListByKategori($dataMasukan) {

        $c_kategori_organisasi = $dataMasukan['c_kategori_organisasi'];

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $sqlProses = "select i_organisasi,
                                n_organisasi
                            from tm_organisasi
                            where c_kategori_organisasi = '$c_kategori_organisasi' and
                                ((i_organisasi_parent is null) OR (i_organisasi_parent = ''))
                            order by i_organisasi";
            //echo $sqlProses;
            $result = $db->fetchAll($sqlProses);

            $jmlResult = count($result);

            for ($j = 0; $j < $jmlResult; $j++) {
                $hasilAkhir[$j] = array("i_organisasi"  =>(string)$result[$j]->i_organisasi,
                                       "n_organisasi"   =>(string)$result[$j]->n_organisasi
                                        );
                //var_dump($hasilAkhir);
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }

    public function pengadilantkiInsert(array $dataMasukan) {
        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        try {
            $db->beginTransaction();
            $maxidPengadilan = $db->fetchOne("select max(i_organisasi) from tm_organisasi");
            $idPengadilan = $maxidPengadilan+1;

            $paramInput = array("i_organisasi"          =>$idPengadilan,
                                "n_organisasi"          =>$dataMasukan['n_organisasi'],
                                "c_kategori_organisasi" =>$dataMasukan['c_kategori_organisasi'],
                                "i_organisasi_parent"           =>$dataMasukan['i_organisasi_parent'],
                                "i_entry"       =>$dataMasukan['i_entry'],
                                "d_entry"       =>date('Y-m-d'));

            //var_dump($paramInput);
            $db->insert('tm_organisasi',$paramInput);
            $db->commit();

            return 'sukses';
        } catch (Exception $e) {
            $db->rollBack();
            $errmsgArr = explode(":",$e->getMessage());

            $errMsg = $errmsgArr[0];

            if($errMsg == "SQLSTATE[23000]")
            {
                return "gagal.Data Sudah Ada.";
            }
            else
            {
                return $e->getMessage();
            }
       }
    }

    public function satkerList($dataMasukan) {

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');

        try {
            $db->setFetchMode(Zend_Db::FETCH_OBJ);

            $pageNumber     = $dataMasukan['pageNumber'];
            $itemPerPage    = $dataMasukan['itemPerPage'];
            $kategoriCari   = $dataMasukan['kategoriCari'];
            $katakunci  = strtoupper($dataMasukan['katakunci']);

            $xLimit=$itemPerPage;
            $xOffset=($pageNumber-1)*$itemPerPage;

            if((trim($kategoriCari) == 'Nama Satker') || (!$kategoriCari)){
                $where = "and UPPER(n_organisasi) like '%$katakunci%'";
            } else if (($kategoriCari =='5') || ($kategoriCari =='6') ||
                       ($kategoriCari =='7') || ($kategoriCari =='8')||
                       ($kategoriCari =='9') || ($kategoriCari =='10')) {

                $where = "and c_kategori_organisasi = '$kategoriCari'";
            }
            /* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
            $whereOpt = "$kategoriCari like '%$katakunciCari%' ";
            if($kategoriCari) { $where = $whereBase." and ".$whereOpt;}
            else { $where = $whereBase;}
            $order = "order by $sortBy $sort "; */
            $sqlProses = "select a.i_organisasi,
                                a.n_organisasi,
                                a.c_kategori_organisasi,
                                b.n_kategori
                            from tm_organisasi a
                            left join tr_kategori b on (a.c_kategori_organisasi = b.id)
                            where ((i_organisasi_parent is null) or (i_organisasi_parent = '' ))
                            and c_kategori_organisasi in ('5','6','7','8','9','10')
                            $where
                            order by i_organisasi";

            if(($pageNumber==0) && ($itemPerPage==0))
            {
                $sqlTotal = "select count(*) from ($sqlProses) a";

                $hasilAkhir = $db->fetchOne($sqlTotal);
            } else {
                $sqlData = $sqlProses." limit $xLimit offset $xOffset";
            //echo $sqlData;
                $result = $db->fetchAll($sqlData);

                $jmlResult = count($result);

                for ($j = 0; $j < $jmlResult; $j++) {
                    $hasilAkhir[$j] = array("i_organisasi"      =>(string)$result[$j]->i_organisasi,
                                           "n_organisasi"   =>(string)$result[$j]->n_organisasi,
                                           "c_kategori_organisasi"      =>(string)$result[$j]->c_kategori_organisasi,
                                           "n_kategori"     =>(string)$result[$j]->n_kategori
                                            );
                    //var_dump($hasilAkhir);
                }
            }
            return $hasilAkhir;

       } catch (Exception $e) {
         echo $e->getMessage().'<br>';
         return 'gagal <br>';
       }
    }
    //////////////////////////////////////// end MA
}
?>
