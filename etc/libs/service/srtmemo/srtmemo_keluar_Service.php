<?php
require_once "share/gen_nosurat.php";
require_once "share/globalReferensi.php";
require_once "service/aplikasi/Aplikasi_Referensi_Service.php";

class srtmemo_keluar_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

	//public function getSuratKeluarList($cari,$pageNumber,$itemPerPage) {
	public function getSuratKeluarList(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$pageNumber 	= $dataMasukan['pageNumber'];
			$itemPerPage 	= $dataMasukan['itemPerPage'];
			$kategoriCari 	= $dataMasukan['kategoriCari'];
			$katakunciCari 	= $dataMasukan['katakunciCari'];
			$dTglCari1 		= $dataMasukan['dTglCari1'];
			$dTglCari2	 	= $dataMasukan['dTglCari2'];
			$dTglCari	 	= $dataMasukan['dTglCari'];
			$sortBy			= $dataMasukan['sortBy'];
			$sort			= $dataMasukan['sort'];
			$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
			$nipLogin		= $dataMasukan['nipLogin'];
			
			$whereBase = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV and tm_memo_keluar.n_dari = '$kdOrgLogin' 
								 and tm_memo_keluar.kd_struktur_orgdari = '$nipLogin'";
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tanggalmasuk') {
					$whereOpt = "tm_memo_keluar.d_memo between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tanggalmasuk') {
					$whereOpt = "tm_memo_keluar.d_memo like '$dTglCari%' ";
				} else {
					$whereOpt = "tm_memo_keluar.$kategoriCari like '%$katakunciCari%' ";
				}
			}
			
			//$whereByOrg = "(a.n_dari = '$kdOrgLogin')";
			if(($kategoriCari) && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			
			//$where = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV ";
			$order = "order by tm_memo_keluar.d_entry desc";
			$sqlProses = "select tm_memo_keluar.id, 
											tm_memo_keluar.id_srtmasuk, 
											a.i_memo, 
											a.rev, 
											tm_memo_keluar.d_memo,
											tm_memo_keluar.n_dari,
											tm_memo_keluar.kd_jabatan_dari,
											c.n_kepada, 
											c.kd_jabatan_kepada, 
											tm_memo_keluar.n_perihal, 
											tm_memo_keluar.n_lampiran, 
											tm_memo_keluar.q_lampiran, 
											tm_memo_keluar.e_isi,
											tm_memo_keluar.c_statusmemo,
											tm_memo_keluar.n_memo_lokasi,
											tm_memo_keluar.c_status_bukasurat
										from
										(select vm_memo_keluar_terakhir.i_memo, vm_memo_keluar_terakhir.REV 
											from vm_memo_keluar_terakhir) a, tm_memo_keluar
										left join tm_memo_keluarkepada c on(tm_memo_keluar.i_memo = c.i_memo)	";
			
			//echo $sqlProses.$where;	
			$sql = $sqlProses.$where." group by tm_memo_keluar.id_srtmasuk, a.i_memo, a.rev ";	
			if(($pageNumber==0) && ($itemPerPage==0))
			{
				$data = $db->fetchOne("select count(*)
									from 
									($sql) b");

			}
			else		
			{
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;	
				$result = $db->fetchAll("$sql $order limit $xLimit offset $xOffset");
							
				//echo "$sql $order limit $xLimit offset $xOffset";  
											
				$jmlResult = count($result);
				for ($j = 0; $j < $jmlResult; $j++) {
					$i_srtmasuk = $db->fetchone("select i_agendasrt from tm_surat_masuk where id = '".$result[$j]->id_srtmasuk."'");
					
					$n_dari=$result[$j]->n_dari;
					$kd_jabatan_dari=$result[$j]->kd_jabatan_dari;
					
					$n_kepada=$result[$j]->n_kepada;
					$kd_jabatan_kepada=$result[$j]->kd_jabatan_kepada;
					
					$globalReferensi = new globalReferensi();
					$nmjabatandari = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_dari'");
					$dari = $nmjabatandari." ".$globalReferensi->getNamaOrganisasi($n_dari);
					
					$nmjabatankepada = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_kepada'");
					$kepada = $nmjabatankepada." ".$globalReferensi->getNamaOrganisasi($n_kepada);										

			//		
					$n_lampiran = $result[$j]->n_lampiran;
					$n_lampiran = $db->fetchOne("select nm_satuanlampiran from tr_satuanlampiran where id='$n_lampiran'");
					$data[$j] = array("id_memo" =>(string)$result[$j]->id,
									"id_srtmasuk" => (string)$result[$j]->id_srtmasuk,
									"i_srtmasuk" => $i_srtmasuk,
									"i_memo" =>(string)$result[$j]->i_memo,
									"c_memorev" =>(string)$result[$j]->rev,
									"d_memo" =>(string)$result[$j]->d_memo,
									"n_dari" =>(string)$result[$j]->n_dari,
									"dari" =>$dari,
									"kd_jabatan_dari" => $kd_jabatan_dari,
									"n_kepada" =>(string)$result[$j]->n_kepada,
									"kepada" =>$kepada,
									"kd_jabatan_kepada" => $kd_jabatan_kepada,
									"n_perihal" =>(string)$result[$j]->n_perihal,
									"n_lampiran" =>$n_lampiran,
									"q_lampiran" =>(string)$result[$j]->q_lampiran,								
									"c_statusmemo" =>(string)$result[$j]->c_statusmemo,
									"n_memo_lokasi" => (string)$result[$j]->n_memo_lokasi,
									"kd_jabatan_kepada" =>$kd_jabatan_kepada,
									"c_status_bukasurat" =>(string)$result[$j]->c_status_bukasurat);
				}	
				
			} 
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function pencarianNoMemo($dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$kataKunci 		= $dataMasukan['kataKunci'];
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$whereBase = "where ";
			
			$whereOpt = "$kategoriCari like '%$kataKunci%' ";
			if ($kategoriCari) {
				$where = "where ".$whereOpt;
			} else {
				$where = '';
			}
			
			$order = "order by d_entry desc ";
			
			$sqlProses = "select distinct  
						  a.i_memo, a.n_perihal
					      from tm_memo_keluar a ";
			
			$sqlData = $sqlProses.$where.$order." limit 5 offset 0";
			
			$result = $db->fetchAll("$sqlData");
								
			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
				$data[$j] = array("i_memo" =>(string)$result[$j]->i_memo,
								  "n_perihal" =>(string)$result[$j]->n_perihal);
				}	
				
			
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function getSuratMasukList(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$pageNumber 	= $dataMasukan['pageNumber'];
			$itemPerPage 	= $dataMasukan['itemPerPage'];
			$kategoriCari 	= $dataMasukan['kategoriCari'];
			$katakunciCari 	= $dataMasukan['katakunciCari'];
			$dTglCari1 		= $dataMasukan['dTglCari1'];
			$dTglCari2	 	= $dataMasukan['dTglCari2'];
			$dTglCari	 	= $dataMasukan['dTglCari'];
			$sortBy			= $dataMasukan['sortBy'];
			$sort			= $dataMasukan['sort'];
			$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
			$nipLogin		= $dataMasukan['nipLogin'];
			
			$whereBase = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV and 
								((tm_memo_keluar.n_teruskan = '$kdOrgLogin' and c.kd_struktur_orgkepada = '$nipLogin') or
								(tm_memo_keluar.n_dari = '$kdOrgLogin' and tm_memo_keluar.kd_struktur_orgdari = '$nipLogin' and tm_memo_keluar.c_statusmemo = 'kembali'))";
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tanggalmasuk') {
					$whereOpt = "tm_memo_keluar.d_memo between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tanggalmasuk') {
					$whereOpt = "tm_memo_keluar.d_memo like '$dTglCari%' ";
				} else {
					$whereOpt = "tm_memo_keluar.$kategoriCari like '%$katakunciCari%' ";
				}
			}
			
			//$whereByOrg = "(a.n_dari = '$kdOrgLogin')";
			if(($kategoriCari) && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			
			//$where = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV ";
			$order = "order by tm_memo_keluar.d_entry desc";
			$sqlProses = "select tm_memo_keluar.id, 
											tm_memo_keluar.id_srtmasuk, 
											a.i_memo, 
											a.rev, 
											tm_memo_keluar.n_dari,
											tm_memo_keluar.kd_jabatan_dari,
											c.n_kepada, 
											c.kd_jabatan_kepada, 
											c.kd_struktur_orgkepada,
											tm_memo_keluar.n_perihal, 
											tm_memo_keluar.n_lampiran, 
											tm_memo_keluar.q_lampiran, 
											tm_memo_keluar.e_isi,
											tm_memo_keluar.c_statusmemo,
											tm_memo_keluar.t_terimaberkas,
											tm_memo_keluar.c_status_bukasurat,
											tm_memo_keluar.n_teruskan,
											tm_memo_keluar.kd_jabatan_teruskan
										from
										(select vm_memo_keluar_terakhir.i_memo, vm_memo_keluar_terakhir.REV 
											from vm_memo_keluar_terakhir) a, tm_memo_keluar
										left join tm_memo_keluarkepada c on(tm_memo_keluar.i_memo = c.i_memo)	";
			
			//echo $sqlProses.$where;	
			$sql = $sqlProses.$where." group by tm_memo_keluar.id_srtmasuk, a.i_memo, a.rev ";	
			if(($pageNumber==0) && ($itemPerPage==0))
			{
				$data = $db->fetchOne("select count(*)
									from 
									($sql) b");

			}
			else		
			{
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;	
				$result = $db->fetchAll("$sql $order limit $xLimit offset $xOffset");
								
				//echo "$sql";  
											
				$jmlResult = count($result);
				for ($j = 0; $j < $jmlResult; $j++) {
					$i_srtmasuk = $db->fetchone("select i_agendasrt from tm_surat_masuk where id = '".$result[$j]->id_srtmasuk."'");
					
					$n_dari=$result[$j]->n_dari;
					$kd_jabatan_dari=$result[$j]->kd_jabatan_dari;
					
					$n_kepada=$result[$j]->n_kepada;
					$kd_jabatan_kepada=$result[$j]->kd_jabatan_kepada;
					
					$globalReferensi = new globalReferensi();
					$nmjabatandari = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_dari'");
					$dari = $nmjabatandari." ".$globalReferensi->getNamaOrganisasi($n_dari);
					
					$nmjabatankepada = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_kepada'");
					$kepada = $nmjabatankepada." ".$globalReferensi->getNamaOrganisasi($n_kepada);										

					$nLampiran= $db->fetchOne("select nm_satuanlampiran from tr_satuanlampiran where id = '".$result[$j]->n_lampiran."'");
					$data[$j] = array("id_memo" =>(string)$result[$j]->id,
									"id_srtmasuk" => (string)$result[$j]->id_srtmasuk,
									"i_srtmasuk" => $i_srtmasuk,
									"i_memo" =>(string)$result[$j]->i_memo,
									"c_memorev" =>(string)$result[$j]->rev,
									"n_dari" =>(string)$result[$j]->n_dari,
									"dari" =>$dari,
									"kd_jabatan_dari" => $kd_jabatan_dari,
									"n_kepada" =>(string)$result[$j]->n_kepada,
									"kepada" =>$kepada,
									"kd_jabatan_kepada" => $kd_jabatan_kepada,
									"n_perihal" =>(string)$result[$j]->n_perihal,
									"n_lampiran" =>$nLampiran,
									"q_lampiran" =>(string)$result[$j]->q_lampiran,								
									"c_statusmemo" =>(string)$result[$j]->c_statusmemo,
									"d_status_baca" =>(string) $result[$j]->t_terimaberkas,
									"kd_jabatan_kepada" =>$kd_jabatan_kepada,
									"c_status_bukasurat" =>(string) $result[$j]->c_status_bukasurat);
				}	
				
			}
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function getDataSuratKeluar($cari,$pageNumber,$itemPerPage) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$where = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV ";
			$order = "order by tm_memo_keluar.d_entry desc";
			$sqlProses = "select distinct tm_memo_keluar.id, 
											tm_memo_keluar.id_srtmasuk, 
											a.i_memo, 
											a.rev, 
											tm_memo_keluar.n_dari,
											tm_memo_keluar.kd_jabatan_dari,
											tm_memo_keluar.n_kepada, 
											tm_memo_keluar.kd_jabatan_kepada, 
											tm_memo_keluar.n_perihal, 
											tm_memo_keluar.n_lampiran, 
											tm_memo_keluar.q_lampiran, 
											tm_memo_keluar.e_isi 
										from
										(select vm_memo_keluar_terakhir.i_memo, vm_memo_keluar_terakhir.REV 
											from vm_memo_keluar_terakhir) a, tm_memo_keluar ";
			
			$sql = $sqlProses.$where.$cari;	
			
			if(($pageNumber==0) && ($itemPerPage==0))
			{
				$data = $db->fetchOne("select count(*)
									from ($sql) a ");

			}
			else		
			{
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;	
				$result = $db->fetchAll("$sql $order limit $xLimit offset $xOffset");
								
				 //echo "$sql $order"; 
											
				$jmlResult = count($result);
				for ($j = 0; $j < $jmlResult; $j++) {
					$i_srtmasuk = $db->fetchone("select i_agendasrt from tm_surat_masuk where id = '".$result[$j]->id_srtmasuk."'");
					$n_dari=$result[$j]->n_dari;
					$kd_jabatan_dari=$result[$j]->kd_jabatan_dari;
					$n_kepada=$result[$j]->n_kepada;
					$kd_jabatan_kepada=$result[$j]->kd_jabatan_kepada;
					$n_tandatangan=$result[$j]->n_tandatangan;
					$kd_jabatan_ttd=$result[$j]->kd_jabatan_ttd;


					$globalReferensi = new globalReferensi();
					$nmjabatandari = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_dari'");
					$dari = $nmjabatandari." ".$globalReferensi->getNamaOrganisasi($n_dari);										
					//$dari=$nmjabatandari." ".$leveldari." ".$nmleveldari;

					$nmjabatankepada = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_kepada'");
					$kepada = $nmjabatankepada." ".$globalReferensi->getNamaOrganisasi($n_kepada);										

					$nmjabatanttd = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_tembusan'");
					$tandatangan = $nmjabatanttd." ".$globalReferensi->getNamaOrganisasi($n_tandatangan);										

					
					$namapejabatttd = $db->fetchOne("select nama from tm_pegawai a,simpeg_internal.v_jab_peg b 
													where a.nip=b.nip and kd_jabatan='$kd_jabatan_tembusan'");
					$nipttd = $db->fetchOne("select a.nip from tm_pegawai a,simpeg_internal.v_jab_peg b 
													where a.nip=b.nip and kd_jabatan='$kd_jabatan_tembusan'");												
					
										
					
					$data[$j] = array("id_memo" =>(string)$result[$j]->id,
									"id_srtmasuk" => (string)$result[$j]->id_srtmasuk,
									"i_srtmasuk" => $i_srtmasuk,
									"i_memo" =>(string)$result[$j]->i_memo,
									"c_memorev" =>(string)$result[$j]->rev,
									"d_memo" =>(string)$result[$j]->d_memo,
									"n_dari" =>(string)$result[$j]->n_dari,
									"dari" =>$dari,
									"n_kepada" =>(string)$result[$j]->n_kepada,
									"kepada" =>$kepada,
									"n_perihal" =>(string)$result[$j]->n_perihal,
									"n_lampiran" =>(string)$result[$j]->n_lampiran,
									"q_lampiran" =>(string)$result[$j]->q_lampiran,								
									"n_tembusan" =>(string)$result[$j]->n_tembusan,
									"tandatangan" =>$tandatangan,
									"e_isi" =>(string)$result[$j]->e_isi,
									"c_status_memo" =>(string)$result[$j]->c_status_memo,
									"d_status_memo" =>(string)$result[$j]->d_status_memo,								
									"kd_jabatan_dari" =>$kd_jabatan_dari,
									"kd_jabatan_tembusan" =>$kd_jabatan_tembusan,
									"kd_jabatan_kepada" =>$kd_jabatan_kepada,
									"namapejabatttd" =>$namapejabatttd,
									"nipttd" =>$nipttd,
									"i_entry" =>(string)$result[$j]->i_entry,
									"d_entry" =>(string)$result[$j]->d_entry);
				}	
				
			}
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function getDetailSuratKeluar($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$where = "where a.i_memo = b.i_memo and 
							a.c_memorev = b.REV AND 
							$cari";
			$sqlProses = "select a.id, a.i_memo, a.c_memorev, a.id_srtmasuk, a.n_dari, a.kd_jabatan_dari, a.kd_struktur_orgdari, a.n_perihal, a.n_lampiran, 
								a.q_lampiran, a.e_isi, a.t_terimaberkas, time(a.t_terimaberkas) as timeTerimaBerkas, a.c_statusmemo, 
								a.n_lampiran, a.c_sifatmemo, a.c_jenisajuan
								from tm_memo_keluar a , vm_memo_keluar_terakhir b  ";
			
			$sql = $sqlProses.$where;	
			
			$result = $db->fetchAll("$sql");
							
//echo "$sql";			
			//$data = array();
			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
				$i_srtmasuk = $db->fetchone("select i_agendasrt from tm_surat_masuk where id = '".$result[$j]->id_srtmasuk."'");
				$n_srtperihal = $db->fetchone("select n_srtperihal from tm_surat_masuk where id = '".$result[$j]->id_srtmasuk."'");
				$id_memo = $result[$j]->id;
				$n_dari=$result[$j]->n_dari;
				$kd_jabatan_dari=$result[$j]->kd_jabatan_dari;
				$n_kepada=$result[$j]->n_kepada;
				$kd_jabatan_kepada=$result[$j]->kd_jabatan_kepada;
				$n_tembusan=$result[$j]->n_tembusan;
				$kd_jabatan_tembusan=$result[$j]->kd_jabatan_tembusan;


				$globalReferensi = new globalReferensi();
				$nmjabatandari = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_dari'");
				$dari = $nmjabatandari." ".$globalReferensi->getNamaOrganisasi($n_dari);										
				//$dari=$nmjabatandari." ".$leveldari." ".$nmleveldari;

				$nmjabatantembusan = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_tembusan'");
				$tembusan = $nmjabatanttd." ".$globalReferensi->getNamaOrganisasi($n_tembusan);										

				
				$namapejabattembusan = $db->fetchOne("select a.nama from tm_pegawai a,simpeg_internal.v_jab_peg b 
												where a.nip=b.nip and kd_jabatan='$kd_jabatan_tembusan'");
				$niptembusan = $db->fetchOne("select a.nip from tm_pegawai a,simpeg_internal.v_jab_peg b 
												where a.nip=b.nip and kd_jabatan='$kd_jabatan_tembusan'");												
				
				$hasilkepada = $db->fetchAll("select n_kepada, kd_jabatan_kepada, kd_struktur_orgkepada 
											from tm_memo_keluarkepada 
											where i_memo = '".$result[$j]->i_memo."' AND
												  id_memo = '$id_memo' AND
											      c_statuskepada = 'K'");
				
				for ($c = 0; $c < count($hasilkepada); $c++) {	
					$nmjabatankepada = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='".$hasilkepada[$c]->kd_jabatan_kepada."'");
					if ($hasilkepada[$c]->n_kepada =='0'){ $kepada = "Presiden";}
					else {
						$kepada = $nmjabatankepada." ".$globalReferensi->getNamaOrganisasi($hasilkepada[$c]->n_kepada);											
					}
					$dataKepada[$c] = array("n_kepada" =>$hasilkepada[$c]->n_kepada,
										"kd_jabatan_kepada" =>$hasilkepada[$c]->kd_jabatan_kepada,
										"kd_struktur_orgkepada" => $hasilkepada[$c]->kd_struktur_orgkepada,
										"nmjabatankepada" => $nmjabatankepada,
										"kepada" => $kepada);
				}
								
				$hasilTembusan = $db->fetchAll("select n_kepada, kd_jabatan_kepada, kd_struktur_orgkepada  
											from tm_memo_keluarkepada 
											where i_memo = '".$result[$j]->i_memo."' AND
											      id_memo = '$id_memo' AND
												  c_statuskepada = 'T'");								  
				
				for ($c = 0; $c < count($hasilTembusan); $c++) {	
					$nmjabatanTembusan = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='".$hasilTembusan[$c]->kd_jabatan_kepada."'");
					$tembusan = $nmjabatanTembusan." ".$globalReferensi->getNamaOrganisasi($hasilTembusan[$c]->n_kepada);											
					$dataTembusan[$c] = array("n_tembusan" =>$hasilTembusan[$c]->n_kepada,
										"kd_jabatan_tembusan" =>$hasilTembusan[$c]->kd_jabatan_kepada,
										"kd_struktur_orgtembusan" => $hasilkepada[$c]->kd_struktur_orgkepada,
										"nmjabatantembusan" => $nmjabatanTembusan,
										"tembusan" => $tembusan);
				}
				
				$data[$j] = array("id_memo" =>(string)$result[$j]->id,
								"id_srtmasuk" => (string)$result[$j]->id_srtmasuk,
								"i_srtmasuk" => $i_srtmasuk,
								"n_srtperihal" =>$n_srtperihal, 
								"i_memo" =>(string)$result[$j]->i_memo,
								"c_memorev" =>(string)$result[$j]->c_memorev,
								"d_memo" =>(string)$result[$j]->d_memo,
								"n_dari" =>(string)$result[$j]->n_dari,
								"dari" =>$dari,
								"dataKepada" =>$dataKepada,
								"n_perihal" =>(string)$result[$j]->n_perihal,
								"n_lampiran" =>(string)$result[$j]->n_lampiran,
								"q_lampiran" =>(string)$result[$j]->q_lampiran,	
								"n_lampiran" =>(string)$result[$j]->n_lampiran,									
								"dataTembusan" =>$dataTembusan,
								"e_isi" =>(string)$result[$j]->e_isi,
								"kd_jabatan_dari" =>$kd_jabatan_dari,
								"niptembusan" =>$niptembusan,
								"i_entry" =>(string)$result[$j]->i_entry,
								"d_entry" =>(string)$result[$j]->d_entry,
								"t_terimaberkas" => (string)$result[$j]->t_terimaberkas,
								"time_terimaberkas" => (string)$result[$j]->timeTerimaBerkas,
								"c_statusmemo" => (string)$result[$j]->c_statusmemo,
								"c_sifatmemo" => (string)$result[$j]->c_sifatmemo,
								"c_jenisajuan" => (string)$result[$j]->c_jenisajuan,
								"jmlTembusan" => $jmlResult);
			}	
//var_dump($data);			
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function TambahDataMemo(array $data) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
		$db->beginTransaction();

		// $maxidmemo = $db->fetchOne("select MAX(id)+1 from tm_memo_keluar");
		// $maxidmemoSebelumnya = $db->fetchOne("select MAX(id) from tm_memo_keluar");
		// if(!$maxidmemo){$maxidmemo=1;}
		
		/* if($data['param'] == "diteruskan"){
			$id_memo_trs = $data['id_memo_trs'];
			$dTerimaberkas_trs = $data['dTerimaberkas_trs'];
			$timeTerimaberkas_trs = $data['timeTerimaberkas_trs'];
			$e_isi_trs = $data['e_isi_trs'];
			$c_statusmemo_trs = $data['c_statusmemo_trs'];
			$i_entry_trs = $data['i_entry_trs'];
			
			$tTerimaBerkas_trs = "'".$dTerimaberkas_trs." ".$timeTerimaberkas_trs."'";
			$ubah_data = array("e_isi" =>$e_isi_trs,
							"c_statusmemo" =>$c_statusmemo_trs,
							"t_terimaberkas" => new Zend_Db_Expr($tTerimaBerkas_trs),
							"i_entry" =>$i_entry_trs,
							"d_entry"=>date("Y-m-d H:i:s")
							);

			$where[] = "id = '".$id_memo_trs."'";
			$db->update('tm_memo_keluar',$ubah_data, $where);  					
		} */
		//var_dump($ubah_data);
		
		if(count($data['dataKepada'])){
			$i_memo_seskab = $db->fetchOne("select i_memo_seskab from tm_memo_keluar where id = '$maxidmemoSebelumnya' ");
			$d_memo_seskab = $db->fetchOne("select d_memo_seskab from tm_memo_keluar where id = '$maxidmemoSebelumnya' ");
			$d_memo_seskab2 = substr($d_memo_seskab,6,4).'-'.substr($d_memo_seskab,3,2).'-'.substr($d_memo_seskab,0,2);
			$time_memo_seskab = substr($d_memo_seskab,11,5);
			
			for($i=0; $i<count($data['dataKepada']); $i++){
				if ($i == 0){
					$tambah_data = array("id_srtmasuk" => $data['id_srtmasuk'],
										//"id"	=> $maxidmemo,
										"i_memo" =>$data['i_memo'],
										"c_memorev" =>$data['c_memorev'],
										"d_memo" =>date("Y-m-d H:i:s"),
										"n_dari" =>$data['n_dari'], //diisi kd struktur organisasi
										"kd_struktur_orgdari" =>$data['nip_dari'], //diisi nip
										"kd_jabatan_dari" =>$data['kd_jabatan_dari'],
										"n_perihal" =>$data['n_perihal'],
										"n_lampiran" =>$data['n_lampiran'],
										"q_lampiran" =>$data['q_lampiran'],
										"e_isi" =>$data['e_isi'],
										"n_teruskan" =>$data['dataKepada'][$i]['n_srt_kepada'], //diisi struktur organisasi kepada
										"kd_struktur_orgteruskan" =>$data['dataKepada'][$i]['nip_kepada'],  //diisi nip kepada
										"kd_jabatan_teruskan" =>$data['dataKepada'][$i]['kd_jabatan_kepada'],
										"c_sifatmemo" =>$data['c_sifatmemo'],
										"c_jenisajuan" =>$data['c_jenisajuan'],
										"n_memo_lokasi" =>$data['n_memo_lokasi'],
										"i_memo_seskab" => $i_memo_seskab, 
										//"d_memo_seskab" => new Zend_Db_Expr($d_memo_seskab), 
										"i_entry" =>$data['i_entry'],
										"d_entry"=>date("Y-m-d H:i:s")
									);	
					//echo "$d_memo_seskab $d_memo_seskab2 $time_memo_seskab";					
					// var_dump($tambah_data);
					// echo "<br>";
					$db->insert('tm_memo_keluar',$tambah_data);
				}
			}
		}
		//var_dump($tambah_data);
		//if(($data['param'] != 'revisi')){
			//echo "parameter = ".$data['param'];
			
			//echo "dataKepada = ".count($data['dataKepada']);
			//echo "<br>dataTembusan = ".count($data['dataTembusan']);
		$curIdmemo = $db->fetchOne("select id  
								   from tm_memo_keluar 
								   where id_srtmasuk = '".$data['id_srtmasuk']."' and
								         i_memo = '".$data['i_memo']."' and
										 c_memorev = '".$data['c_memorev']."'");
		
			if(count($data['dataKepada'])){
				for($i=0; $i<count($data['dataKepada']); $i++){
					$tambahDatakepada = array("id_memo"	=> $curIdmemo,
											  "i_memo" => $data['i_memo'],
											  "n_kepada" => $data['dataKepada'][$i]['n_srt_kepada'],
											  "kd_struktur_orgkepada" =>$data['dataKepada'][$i]['nip_kepada'],
											  "kd_jabatan_kepada" => $data['dataKepada'][$i]['kd_jabatan_kepada'],
											  "c_statuskepada" => 'K',
											  "i_entry" =>$data['i_entry']);
											  
					// var_dump($tambahDatakepada);
					// echo "<br>";
					$db->insert('tm_memo_keluarkepada',$tambahDatakepada);	
					unset($tambahDatakepada);	
				}
			}
			//echo "<br><br>";
			if(count($data['dataTembusan'])){
				for($i=0; $i<count($data['dataTembusan']); $i++){
					$tambahDataTembusan = array("id_memo"	=> $curIdmemo,
											  "i_memo" => $data['i_memo'],
											  "n_kepada" => $data['dataTembusan'][$i]['n_srt_tembusan'],
											  "kd_struktur_orgkepada" =>$data['dataTembusan'][$i]['nip_tembusan'],
											  "kd_jabatan_kepada" => $data['dataTembusan'][$i]['kd_jabatan_tembusan'],
											  "c_statuskepada" => 'T',
											  "i_entry" =>$data['i_entry']);
											  
					// var_dump($tambahDataTembusan);
					// echo "<br>";
					$db->insert('tm_memo_keluarkepada',$tambahDataTembusan);
					unset($tambahDataTembusan);						
				}
			}	 					  
		//}

		$db->commit();
		return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			echo $e->getMessage().'<br>';
			return 'gagal';
		}
	}

	public function EditDataMemo(array $data) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
		$db->beginTransaction();
		
		$edit_data = array("d_memo" =>date("Y-m-d H:i:s"),
						   "n_perihal" =>$data['n_perihal'],
						   "n_lampiran" =>$data['n_lampiran'],
					       "q_lampiran" =>$data['q_lampiran'],
						   "e_isi" =>$data['e_isi'],
						   //"n_memo_lokasi" =>$data['n_memo_lokasi'],
						   "i_entry" =>$data['i_entry'],
						   "d_entry"=>date("Y-m-d H:i:s")
							);	
		$whereEdit[] = "id = '".$data['id_memo']."'";					
		$db->update('tm_memo_keluar',$edit_data, $whereEdit); 
		
		//var_dump($whereEdit);
		//echo "<br>";
		
		if(count($data['dataKepada'])){
			//delete Data Kepada
			//================
			$whereKepada[] = "c_statuskepada = 'K' and i_memo = '".$data['i_memo']."'";
			//$whereKepada[] = "i_memo = '".$data['i_memo']."'";
			$db->delete('tm_memo_keluarkepada',$whereKepada); 
			
		
			for($i=0; $i<count($data['dataKepada']); $i++){
				$tambahDatakepada = array("id_memo" => $data['id_memo'],
										  "i_memo" => $data['i_memo'],
										  "n_kepada" => $data['dataKepada'][$i]['n_srt_kepada'],
										  "kd_struktur_orgkepada" =>$data['dataKepada'][$i]['nip_kepada'],
										  "kd_jabatan_kepada" => $data['dataKepada'][$i]['kd_jabatan_kepada'],
										  "c_statuskepada" => 'K',
										  "i_entry" =>$data['i_entry']);
				
				//var_dump($tambahDatakepada);
				//echo "<br>";
				$db->insert('tm_memo_keluarkepada',$tambahDatakepada);	
				unset($tambahDatakepada);	
			}
		}
		//echo "<br><br>";
		if(count($data['dataTembusan'])){
			//delete Data Tembusan
			//===================
			$whereTembusan[] = "c_statuskepada = 'T' and i_memo = '".$data['i_memo']."'";
			//$whereTembusan[] = "i_memo = '".$dataMasukan['i_memo']."'";
			$db->delete('tm_memo_keluarkepada',$whereTembusan);
			
			for($i=0; $i<count($data['dataTembusan']); $i++){
				$tambahDataTembusan = array("id_memo" => $data['id_memo'],
										  "i_memo" => $data['i_memo'],
										  "n_kepada" => $data['dataTembusan'][$i]['n_srt_tembusan'],
										  "kd_struktur_orgkepada" =>$data['dataTembusan'][$i]['nip_tembusan'],
										  "kd_jabatan_kepada" => $data['dataTembusan'][$i]['kd_jabatan_tembusan'],
										  "c_statuskepada" => 'T',
										  "i_entry" =>$data['i_entry']);
										  
				//var_dump($tambahDataTembusan);
				$db->insert('tm_memo_keluarkepada',$tambahDataTembusan);
				unset($tambahDataTembusan);						
			}
		}	 		 		  

		$db->commit();
		return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			echo $e->getMessage().'<br>';
			return 'gagal';
		}
	}	

	public function ubahLokasiFileMemo(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
					
			$paramInput = array("n_memo_lokasi"	=>$dataMasukan['n_memo_lokasi'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));
//var_dump($paramInput);
									
			$where[] = "i_memo = '".$dataMasukan['i_memo']."'";
			$db->update('tm_memo_keluar',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	
	public function ubahStatusBukasurat($iMemo) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
					
			$paramInput = array("c_status_bukasurat"	=>"Y");
//var_dump($paramInput);
									
			$where[] = "id= '".$iMemo."'";
			
			// var_dump($paramInput);
			// echo "<br>";
			// var_dump($where);
			$db->update('tm_memo_keluar',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function setMemoRev($idSrtmasuk, $iMemo) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$memoRev = $db->fetchOne("select (max(c_memorev))
									from tm_memo_keluar 
									where id_srtmasuk = '$idSrtmasuk' and i_memo= '$iMemo'");
								
		     return $memoRev;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	}

public function UbahDataMemoKembali(array $data) {
	$registry = Zend_Registry::getInstance();
	$db = $registry->get('db');
	try {
	 $db->beginTransaction();
	 $tTerimaBerkas = "'".$data['dTerimaberkas']." ".$data['timeTerimaberkas']."'";
	 $ubah_data = array("e_isi" =>$data['e_isi'],
						"c_statusmemo" =>$data['c_statusmemo'],
						"c_status_bukasurat" => 'N',
						"t_terimaberkas" => new Zend_Db_Expr($tTerimaBerkas),
						"i_entry" =>$data['i_entry'],
						"d_entry"=>date("Y-m-d H:i:s")
						);

     	$where[] = "id = '".$data['id_memo']."'";
		$db->update('tm_memo_keluar',$ubah_data, $where);  
		$db->commit();
		return 'sukses';
	} catch (Exception $e) {
		$db->rollBack();
		echo $e->getMessage().'<br>';
		return 'gagal';
	}
}
	
public function UbahDataMemo(array $data) {
	$registry = Zend_Registry::getInstance();
	$db = $registry->get('db');
	try {
	 $db->beginTransaction();
	 $ubah_data = array("d_memo" =>$data['d_memo'],
						"i_konsep" =>$data['i_konsep'],
						"n_dari" =>$data['n_dari'],
						"kd_jabatan_dari" =>$data['kd_jabatan_dari'],
						"n_kepada" =>$data['n_kepada'],
						"kd_jabatan_kepada" =>$data['kd_jabatan_kepada'],
						"n_perihal" =>$data['n_perihal'],
						"n_lampiran" =>$data['n_lampiran'],
						"q_lampiran" =>$data['q_lampiran'],
						"n_tandatangan" =>$data['n_tandatangan'],
						"kd_jabatan_ttd" =>$data['kd_jabatan_ttd'],
						"a_alamatlkp" =>$data['a_alamatlkp'],
						"e_isi" =>$data['e_isi'],
						"c_status_memo" =>$data['c_status_memo'],
						"i_entry" =>$data['i_entry'],
						"d_entry"=>date("Y-m-d H:i:s")
						);

		$where[] = "id = '".$data['id']."'";
		$db->update('tm_memo_keluar',$ubah_data, $where);  
		$db->commit();
		return 'sukses';
	} catch (Exception $e) {
		$db->rollBack();
		echo $e->getMessage().'<br>';
		return 'gagal';
	}
}

public function HapusDataMemo(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
		 $delData = array("c_statusdelete" => 'Y');
		 $where[] = "id = '".$data['id']."'";
	     $db->delete('tm_memo_keluar',$where);  
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}


public function UbahDataStatusMemo(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $ubah_data = array("c_status_memo" =>$data['c_status_memo'],
							"d_status_memo" =>$data['d_status_memo']);
		 $where[] = "id = '".$data['id']."' and n_kepada = '".$data['n_kepada']."'";	
//echo  "id = '".$data['id']."' and n_kepada = '".$data['n_kepada']."'";	;		 
	     //$db->update('tm_memo_keluar',$ubah_data, $where);  
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}

	public function CekStatus($id,$kepada) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$maxnumber = $db->fetchOne("select d_status_memo from tm_memo_keluar where id='$id' and n_kepada='$kepada' and c_status_memo='Y'");
			$maxnumber = $maxnumber;

		     return $maxnumber;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	}		   




///tambahan hendar 13-10-2009 jakarta

public function getPejabat($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$result = $db->fetchAll("select a.nip,a.nama,a.kd_golongan,c.nm_golongan,c.pangkat,
									a.unit_kerja,b.kd_jabatan,d.nm_jabatan,b.kd_struktur_org,
									e.level,e.nm_level
									from tm_pegawai a,simpeg_internal.v_jab_peg b,simpeg_internal.tbl_golongan c,simpeg_internal.tbl_jabatan d,
									simpeg_internal.tbl_struktur_organisasi e
									where a.nip=b.nip and a.kd_golongan=c.kd_golongan and 
									b.kd_jabatan=d.kd_jabatan and b.kd_struktur_org=e.kd_struktur_org
									and b.kd_jabatan='2'
									$cari order by b.kd_jabatan asc");
			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
			$data[$j] = array("nip" =>(string)$result[$j]->nip,
							"nama" =>(string)$result[$j]->nama,
							"kd_golongan" =>(string)$result[$j]->kd_golongan,
							"nm_golongan" =>(string)$result[$j]->nm_golongan,
							"pangkat" =>(string)$result[$j]->pangkat,
							"unit_kerja" =>(string)$result[$j]->unit_kerja,
							"kd_jabatan" =>(string)$result[$j]->kd_jabatan,
							"nm_jabatan" =>(string)$result[$j]->nm_jabatan,
							"kd_struktur_org" =>(string)$result[$j]->kd_struktur_org,
							"level" =>(string)$result[$j]->level,
							"nm_level" =>(string)$result[$j]->nm_level);
			}					
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}

	public function jmlmemomasuk($kdOrgLogin, $userLogin) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$whereBase = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV and c.n_kepada = '$kdOrgLogin' 
								and tm_memo_keluar.c_status_bukasurat != 'Y'";
			$where = $whereBase;
			
			$sqlProses = "select distinct (a.i_memo), tm_memo_keluar.id_srtmasuk,  a.rev, 
						c.n_kepada, c.kd_jabatan_kepada
						from
						(select vm_memo_keluar_terakhir.i_memo, vm_memo_keluar_terakhir.REV 
							from vm_memo_keluar_terakhir) a, tm_memo_keluar
						left join tm_memo_keluarkepada c on(tm_memo_keluar.i_memo = c.i_memo)	";

			
			$sql = 'select count(*) from ('.$sqlProses.$where.') a';	
			//echo $sql;	
			$result = $db->fetchOne("$sql");
			 
		     return $result;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	
	public function detailMemoById($idMemo, $cStatusMemo, $e_isi, $dTerimaberkas, $timeTerimaberkas, $i_entry) {
	    $ref = new srtmemo_keluar_Service();
		$ref_serv = Aplikasi_Referensi_Service::getInstance();
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
		$db->beginTransaction();
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$sql = "select b.id,a.id_srtmasuk,a.i_memo,a.c_memorev,a.d_memo,a.n_perihal,a.n_lampiran,a.q_lampiran,a.e_isi, 
					b.n_kepada, b.kd_struktur_orgkepada, b.kd_jabatan_kepada, a.n_memo_lokasi, a.c_sifatmemo, a.c_jenisajuan
					from tm_memo_keluar a, tm_memo_keluarkepada b
					where a.i_memo = b.i_memo and a.id ='$idMemo'
					and b.c_statuskepada = 'K' and
					b.id in (select max(id) from tm_memo_keluarkepada where tm_memo_keluarkepada.i_memo = a.i_memo)";
			//echo $sql;
			$result = $db->fetchAll("$sql");
			
			$qLampiran = $result[0]->q_lampiran;
			if(!$qLampiran){ $qLampiran=null;}
			$jmlResult = count($result);
			
			// insert kepada
			//============
			if($cStatusMemo == 'diteruskankeseskab'){
				$n_kepada = '1';
			} else {
				$atasan = $ref_serv->getAtasan($result[0]->n_kepada, $result[0]->kd_jabatan_kepada);
				$n_kepada = $atasan['kd_struktur_org_induk'];
				$kd_struktur_orgteruskan = $atasan['nip'];
				$kd_jabatan_teruskan = $atasan['kd_jabatan_induk'];
			//var_dump($atasan);
				//echo "$n_kepada |".$atasan['kd_struktur_org_induk'];
				if(!$n_kepada){ 
					$n_kepada = '2';
				}
			}
			
			
			$maxidmemo = $db->fetchOne("select MAX(id)+1 from tm_memo_keluar");
		
			if(!$maxidmemo){$maxidmemo=1;}
			
			$tTerimaBerkas = "'".$dTerimaberkas." ".$timeTerimaberkas."'";
	 
						
			
			$data= array("id_srtmasuk" => (string)$result[0]->id_srtmasuk,
						"id" => $maxidmemo,
						"i_memo" =>(string)$result[0]->i_memo,
						"c_memorev" =>(string)$result[0]->c_memorev,
						"d_memo" =>date("Y-m-d H:i:s"),
						"n_dari" =>(string)$result[0]->n_kepada,
						"kd_struktur_orgdari" => (string)$result[0]->kd_struktur_orgkepada,
						"kd_jabatan_dari" => (string)$result[0]->kd_jabatan_kepada,
						"n_perihal" =>(string)$result[0]->n_perihal,
						"n_lampiran" =>(string)$result[0]->n_lampiran,
						"q_lampiran" =>$qLampiran,								
						"e_isi" =>$e_isi,
						"n_teruskan" =>$n_kepada,
						"kd_struktur_orgteruskan" =>$kd_struktur_orgteruskan, 
						"kd_jabatan_teruskan" =>$kd_jabatan_teruskan,
						"n_memo_lokasi" => (string)$result[0]->n_memo_lokasi,
						"t_terimaberkas" => new Zend_Db_Expr($tTerimaBerkas),
						"c_sifatmemo" =>(string)$result[0]->c_sifatmemo,
						"c_jenisajuan" =>(string)$result[0]->c_jenisajuan,
						"i_entry" =>$i_entry,
						"d_entry" =>date('Y-m-d'));
			
			$db->insert('tm_memo_keluar',$data); 
			//var_dump($data); 
			
			$dataKepada = array("c_statuskepada" => 'K',
								"id_memo" => $maxidmemo,
								"i_memo" => $result[0]->i_memo,
								"n_kepada" => $n_kepada,
								"kd_struktur_orgkepada" =>$kd_struktur_orgteruskan,
								"kd_jabatan_kepada" => $kd_jabatan_teruskan,
								"i_entry" =>$i_entry,
								"d_entry" =>date('Y-m-d'));
			
			$db->insert('tm_memo_keluarkepada',$dataKepada); 	
			
			//insert tembusan
			//==============
			
			$hasilTembusan = "select b.i_memo, b.kd_jabatan_kepada, b.n_kepada, kd_struktur_orgkepada, b.c_statuskepada
								from tm_memo_keluarkepada b
								where b.i_memo = '".$result[0]->i_memo."'
								and b.c_statuskepada = 'T'";
			$hasiltembusan = $db->fetchAll($hasilTembusan);
			if(count($hasiltembusan)){
				for ($x=0; $x<count($hasiltembusan); $x++){
				  $dataTembusan = array("id_memo" => $maxidmemo,
										"i_memo" => $hasilTembusan[$x]->i_memo,
										"kd_jabatan_kepada" => $hasilTembusan[$x]->kd_jabatan_kepada,
										"n_kepada" => $hasilTembusan[$x]->n_kepada,
										"kd_struktur_orgkepada" => $hasilTembusan[$x]->kd_struktur_orgkepada,
										"c_statuskepada" => $hasilTembusan[$x]->c_statuskepada,
										"i_entry" =>$i_entry,
										"d_entry" =>date('Y-m-d'));
										
				  $db->insert('tm_memo_keluarkepada',$dataTembusan);
				  unset($dataTembusan);	
				}
			}
			 
			$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function memoteruskan($dataMasukanTeruskan) {
	    $ref = new srtmemo_keluar_Service();
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
		$db->beginTransaction();
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			/* Update Status memo sekarang menjadi 'diteruskan' atau 'diteruskankeseskab atau diteruskankepresiden' 
			    isi tanggal terima berkas                                                                             
			    ==========================================================================================*/

			$id_memo = $dataMasukanTeruskan['id_memo'];
			$dTerimaberkas = $dataMasukanTeruskan['dTerimaberkas'];
			$timeTerimaberkas = $dataMasukanTeruskan['timeTerimaberkas'];
			$e_isi = $dataMasukanTeruskan['e_isi'];
			$e_disposisi_seskab = $dataMasukanTeruskan['e_disposisi_seskab'];
			$e_disposisi_presiden = $dataMasukanTeruskan['e_disposisi_presiden'];
			$c_statusmemo = $dataMasukanTeruskan['c_statusmemo'];
			$n_dari = $dataMasukanTeruskan['n_dari'];
			$kd_jabatandari = $dataMasukanTeruskan['kd_jabatandari'];
			$i_memo_seskab = $dataMasukanTeruskan['i_memo_seskab'];
			$i_entry = $dataMasukanTeruskan['i_entry'];
			
			$tTerimaBerkas = "'".$dTerimaberkas." ".$timeTerimaberkas."'";
			$ubah_data = array("e_isi" =>$e_isi,
							"e_disposisi_seskab" => $e_disposisi_seskab,
							"e_disposisi_presiden" => $e_disposisi_presiden,
							"c_statusmemo" =>$c_statusmemo,
							"t_terimaberkas" => new Zend_Db_Expr($tTerimaBerkas),
							"i_entry" =>$i_entry,
							"d_entry"=>date("Y-m-d H:i:s")
							);

			$where[] = "id = '".$id_memo."'";
			
			$db->update('tm_memo_keluar',$ubah_data, $where);  
			
			/* insert memo dari user login kepada atasan langsung atau seskab (untuk level deputi) 
			    status = null ;  tgl memo = curdate                                                                             
			    =======================================================================*/
			
			$sql = "select   a.id,a.id_srtmasuk,a.i_memo,a.c_memorev,a.d_memo,a.n_perihal,a.n_lampiran,a.q_lampiran,a.e_isi, 
					a.n_teruskan, a.kd_jabatan_teruskan, a.n_memo_lokasi, a.c_sifatmemo, a.c_jenisajuan
					from tm_memo_keluar a, tm_memo_keluarkepada b
					where a.i_memo = b.i_memo and a.id ='$id_memo'
					and b.c_statuskepada = 'K' 
					and a.id = b.id_memo"; //in (select max(id) from tm_memo_keluarkepada where tm_memo_keluarkepada.i_memo = a.i_memo)";
			//echo $sql;
			$result = $db->fetchAll("$sql");
			
			$qLampiran = $result[0]->q_lampiran;
			if(!$qLampiran){ $qLampiran=null;}
			$jmlResult = count($result);
			
			// insert kepada
			//============
			//echo "$c_statusmemo<br>";
			if($c_statusmemo == 'diteruskankeseskab'){
				$n_kepada = '1';
			} else if($c_statusmemo == 'diteruskankepresiden'){
				$n_kepada = '0';
				
			} 
			else {
				$atasan = $ref->getAtasan($result[0]->n_teruskan, $result[0]->kd_jabatan_teruskan);
				$n_kepada = $atasan['kd_struktur_org_induk'];
			
				//echo "$n_kepada |".$atasan['kd_struktur_org_induk'];
				if(!$n_kepada){ 
					$n_kepada = '2';
				}
			}
			
			$maxidmemo = $db->fetchOne("select MAX(id)+1 from tm_memo_keluar");
		
			if(!$maxidmemo){$maxidmemo=1;}
			
			$data= array("id_srtmasuk" => (string)$result[0]->id_srtmasuk,
						"id" => $maxidmemo,
						"i_memo" => (string)$result[0]->i_memo,
						"c_memorev" =>(string)$result[0]->c_memorev,
						"d_memo" =>date("Y-m-d H:i:s"),
						"n_dari" =>(string)$result[0]->n_teruskan,
						"kd_jabatan_dari" => (string)$result[0]->kd_jabatan_teruskan,
						"n_perihal" =>(string)$result[0]->n_perihal,
						"n_lampiran" =>(string)$result[0]->n_lampiran,
						"q_lampiran" =>$qLampiran,								
						"e_isi" =>(string)$result[0]->e_isi,
						"n_teruskan" =>$n_kepada,
						"kd_jabatan_teruskan" =>$atasan['kd_jabatan_induk'],
						"i_memo_seskab" =>$i_memo_seskab,
						"n_memo_lokasi" => (string)$result[0]->n_memo_lokasi,
						"t_terimaberkas" => new Zend_Db_Expr($tTerimaBerkas),
						"c_sifatmemo" =>(string)$result[0]->c_sifatmemo,
						"c_jenisajuan" =>(string)$result[0]->c_jenisajuan,
						"i_entry" =>$i_entry,
						"d_entry" =>date('Y-m-d'));
						
			if ($c_statusmemo == 'diteruskankepresiden'){			
				$data['d_memo_seskab']=  new Zend_Db_Expr($tTerimaBerkas);
			}
			
			//var_dump($data);
			$db->insert('tm_memo_keluar',$data); 
			
			/* $id_memobaru = $db->fetchOne("select id from tm_memo_keluar where i_memo = '".$result[0]->i_memo."'
										AND n_dari = '".$result[0]->n_teruskan."' 
										AND n_teruskan = '$n_kepada'"); */
										
			//echo "id_memobaru = $id_memobaru";							
			// insert Kepada ke table tm_memo_keluarkepada
			//=========================================
			$dataKepada = array("c_statuskepada" => 'K',
						"id_memo" => $maxidmemo,
						"i_memo" => $result[0]->i_memo,
						"n_kepada" => $n_kepada,
						"kd_jabatan_kepada" => $atasan['kd_jabatan_induk']);
			
			$db->insert('tm_memo_keluarkepada',$dataKepada); 	
			
			//insert tembusan ke table tm_memo_keluarkepada
			//=========================================
						
			$hasilTembusan = "select b.i_memo, b.kd_jabatan_kepada, b.n_kepada, b.c_statuskepada
								from tm_memo_keluarkepada b
								where b.i_memo = '".$result[0]->i_memo."'
									and b.id_memo = '$id_memo'
								and b.c_statuskepada = 'T'";
			$hasiltembusan = $db->fetchAll($hasilTembusan);
			if(count($hasiltembusan)){
				for ($x=0; $x<count($hasiltembusan); $x++){
				  $dataTembusan = array("id_memo" => $maxidmemo,
										"i_memo" => $hasilTembusan[$x]->i_memo,
										"kd_jabatan_kepada" => $hasilTembusan[$x]->kd_jabatan_kepada,
										"n_kepada" => $hasilTembusan[$x]->n_kepada,
										"c_statuskepada" => $hasilTembusan[$x]->c_statuskepada);
										
				  $db->insert('tm_memo_keluarkepada',$dataTembusan);
				  unset($dataTembusan);	
				}
			}
			  
			$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function buatsuratkeluar($dataMasukanTeruskan) {
	    $ref = new srtmemo_keluar_Service();
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
		$db->beginTransaction();
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			/* Update Status memo sekarang menjadi 'diteruskan' atau 'diteruskankeseskan' 
			    isi tanggal terima berkas                                                                             
			    ===================================================================*/

			$id_memo = $dataMasukanTeruskan['id_memo'];
			$dTerimaberkas = $dataMasukanTeruskan['dTerimaberkas'];
			$timeTerimaberkas = $dataMasukanTeruskan['timeTerimaberkas'];
			$e_isi = $dataMasukanTeruskan['e_isi'];
			$e_disposisi_seskab = $dataMasukanTeruskan['e_disposisi_seskab'];
			$e_disposisi_presiden = $dataMasukanTeruskan['e_disposisi_presiden'];
			$c_statusmemo = $dataMasukanTeruskan['c_statusmemo'];
			$n_dari = $dataMasukanTeruskan['n_dari'];
			$kd_jabatandari = $dataMasukanTeruskan['kd_jabatandari'];
			$i_entry = $dataMasukanTeruskan['i_entry'];
			
			$tTerimaBerkas = "'".$dTerimaberkas." ".$timeTerimaberkas."'";
			$ubah_data = array("e_isi" =>$e_isi,
							"e_disposisi_seskab" => $e_disposisi_seskab,
							"e_disposisi_presiden" => $e_disposisi_presiden,
							"c_statusmemo" =>$c_statusmemo,
							"t_terimaberkas" => new Zend_Db_Expr($tTerimaBerkas),
							"i_entry" =>$i_entry,
							"d_entry"=>date("Y-m-d H:i:s")
							);

			$where[] = "id = '".$id_memo."'";
			$db->update('tm_memo_keluar',$ubah_data, $where);  
			  
			$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}

	function getAtasan($kdOrg, $kdJabatan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select id_riwayat_jabatan, kd_struktur_org, kd_jabatan, nama, nip, kd_jabatan_induk
						from simpeg_internal.v_r_jabatan_peg
						where id_riwayat_jabatan in 
								(select kd_jabatan_induk from simpeg_internal.v_r_jabatan_peg where kd_jabatan = '$kdJabatan' and kd_struktur_org = '$kdOrg')";	
							
			//echo $sqlProses;
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchRow($sqlData);				
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			$hasilAkhir = array("kd_struktur_org_induk"  		=>(string)$result->kd_struktur_org,
									"kd_jabatan_induk"  		=>(string)$result->kd_jabatan); 	
			
			//var_dump($hasilAkhir);
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getMemokeSeskabList(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ);
			
			$pageNumber 	= $dataMasukan['pageNumber'];
			$itemPerPage 	= $dataMasukan['itemPerPage'];
			$kategoriCari 	= $dataMasukan['kategoriCari'];
			$katakunciCari 	= $dataMasukan['katakunciCari'];
			$dTglCari1 		= $dataMasukan['dTglCari1'];
			$dTglCari2	 	= $dataMasukan['dTglCari2'];
			$dTglCari	 	= $dataMasukan['dTglCari'];
			$sortBy			= $dataMasukan['sortBy'];
			$sort			= $dataMasukan['sort'];
			$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
			$teruskanKe		= $dataMasukan['teruskanKe'];
			
			if (!$kdOrgLogin) {
				$whereBase = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV and 
									tm_memo_keluar.n_teruskan = '$teruskanKe' and
									c.id_memo = (select max(id) from tm_memo_keluar x where x.i_memo = tm_memo_keluar.i_memo)";
			} else {
				$whereBase = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV and 
									tm_memo_keluar.n_teruskan = '$teruskanKe' and
									c.id_memo = (select max(id) from tm_memo_keluar x where x.i_memo = tm_memo_keluar.i_memo) and
									n_dari = '$kdOrgLogin'";
			}
			
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tanggalmasuk') {
					$whereOpt = "tm_memo_keluar.d_memo between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tanggalmasuk') {
					$whereOpt = "tm_memo_keluar.d_memo like '$dTglCari%' ";
				} else {
					$whereOpt = "tm_memo_keluar.$kategoriCari like '%$katakunciCari%' ";
				}
			}
			
			//$whereByOrg = "(a.n_dari = '$kdOrgLogin')";
			if(($kategoriCari) && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			
			//$where = "where tm_memo_keluar.i_memo = a.i_memo and tm_memo_keluar.c_memorev = a.REV ";
			$order = "order by tm_memo_keluar.d_entry desc";
			$sqlProses = "select tm_memo_keluar.id, 
											tm_memo_keluar.id_srtmasuk, 
											a.i_memo, 
											a.rev, 
											tm_memo_keluar.n_dari,
											tm_memo_keluar.kd_jabatan_dari,
											c.n_kepada, 
											c.kd_jabatan_kepada, 
											tm_memo_keluar.n_perihal, 
											tm_memo_keluar.n_lampiran, 
											tm_memo_keluar.q_lampiran, 
											tm_memo_keluar.e_isi,
											tm_memo_keluar.c_statusmemo,
											tm_memo_keluar.t_terimaberkas,
											tm_memo_keluar.c_statusbaca,
											tm_memo_keluar.n_teruskan,
											tm_memo_keluar.kd_jabatan_teruskan,  
											tm_memo_keluar.c_jenisajuan,
											tm_memo_keluar.d_memo,
											tm_memo_keluar.d_memo_seskab,
											tm_memo_keluar.i_memo_seskab,
											tm_memo_keluar.e_disposisi_seskab
										from
										(select vm_memo_keluar_terakhir.i_memo, vm_memo_keluar_terakhir.REV 
											from vm_memo_keluar_terakhir) a, tm_memo_keluar
										left join tm_memo_keluarkepada c on(tm_memo_keluar.i_memo = c.i_memo)	";
			
			//echo $sqlProses.$where;	
			$sql = $sqlProses.$where." group by tm_memo_keluar.id_srtmasuk, a.i_memo, a.rev ";	
			if(($pageNumber==0) && ($itemPerPage==0))
			{
				$data = $db->fetchOne("select count(*)
									from 
									($sql) b");

			}
			else		
			{
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;	
				$result = $db->fetchAll("$sql $order limit $xLimit offset $xOffset");
				/* var_dump($result);				
				echo "$sql";  */ 
											
				$jmlResult = count($result);
				for ($j = 0; $j < $jmlResult; $j++) {
					$i_srtmasuk = $db->fetchone("select i_agendasrt from tm_surat_masuk where id = '".$result[$j]->id_srtmasuk."'");
					
					$n_dari=$result[$j]->n_dari;
					$kd_jabatan_dari=$result[$j]->kd_jabatan_dari;
					
					$n_kepada=$result[$j]->n_kepada;
					$kd_jabatan_kepada=$result[$j]->kd_jabatan_kepada;
					
					$globalReferensi = new globalReferensi();
					$nmjabatandari = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_dari'");
					$dari = $nmjabatandari." ".$globalReferensi->getNamaOrganisasi($n_dari);
					
					$nmjabatankepada = $db->fetchOne("select nm_jabatan from simpeg_internal.v_jab_peg where kd_jabatan='$kd_jabatan_kepada'");
					$kepada = $nmjabatankepada." ".$globalReferensi->getNamaOrganisasi($n_kepada);										

					$nLampiran= $db->fetchOne("select nm_satuanlampiran from tr_satuanlampiran where id = '".$result[$j]->n_lampiran."'");
					$nJenisAjuan = $db->fetchOne("select n_srtajuan from tr_jenis_ajuan where c_srtajuan = '".$result[$j]->c_jenisajuan."'");
					
					$data[$j] = array("id_memo" =>(string)$result[$j]->id,
									"id_srtmasuk" => (string)$result[$j]->id_srtmasuk,
									"i_srtmasuk" => $i_srtmasuk,
									"i_memo" =>(string)$result[$j]->i_memo,
									"c_memorev" =>(string)$result[$j]->rev,
									"n_dari" =>(string)$result[$j]->n_dari,
									"dari" =>$dari,
									"kd_jabatan_dari" => $kd_jabatan_dari,
									"n_kepada" =>(string)$result[$j]->n_kepada,
									"kepada" =>$kepada,
									"kd_jabatan_kepada" => $kd_jabatan_kepada,
									"n_perihal" =>(string)$result[$j]->n_perihal,
									"n_lampiran" =>$nLampiran,
									"q_lampiran" =>(string)$result[$j]->q_lampiran,								
									"c_statusmemo" =>(string)$result[$j]->c_statusmemo,
									"d_status_baca" =>(string) $result[$j]->t_terimaberkas,
									"c_statusbaca" =>(string) $result[$j]->c_statusbaca,
									"kd_jabatan_kepada" =>$kd_jabatan_kepada,
									"c_jenisajuan" => (string) $result[$j]->c_jenisajuan,
									"nJenisAjuan" => $nJenisAjuan,
									"d_memo" => (string) $result[$j]->d_memo,
									"t_terimaberkas" => (string) $result[$j]->t_terimaberkas,
									"d_memo_seskab" => (string) $result[$j]->d_memo_seskab,
									"i_memo_seskab" => (string) $result[$j]->i_memo_seskab,
									"e_disposisi_seskab" => (string) $result[$j]->e_disposisi_seskab);
				}	
				
			} 
			//var_dump($data);
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
/////
}


?>