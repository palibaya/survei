<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "share/format_date.php";
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";
require_once "service/aplikasi/Aplikasi_Responden_Service.php";



class Aplikasi_RespondenController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
		
		$this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
		$this->responden_serv	  = Aplikasi_Responden_Service::getInstance();
	    $ssogroup = new Zend_Session_Namespace('ssogroup');
	   // echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->i_organisasi;
	    $this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		$this->c_kategori_organisasi  = $ssogroup->c_kategori_organisasi;			
    }
	
    public function indexAction() {
	   
    }
	
	public function respondenjsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('respondenjs');
    }
	
	//MA
	//----------------------
	public function respondenolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		//if (!$_REQUEST['jenisForm']) {$this->view->jenisForm = 'insert';}
		$iAgenda = $_REQUEST['iAgenda'];
		$this->view->userid = $this->userid;
		$this->view->i_organisasi = $this->i_organisasi;
		$idPengadilan = $this->i_organisasi;
		$this->view->idPengadilan = $idPengadilan;
		
		
		$kategoriPengadilan = array('1', '2', '3', '4');
		$kategoriSatker = array('5', '6', '7', '8', '9', '10');
		if(in_array($this->c_kategori_organisasi, $kategoriPengadilan)){
			$kategoriUser = 'pengadilan';
		} else if(in_array($this->c_kategori_organisasi, $kategoriSatker)){
			$kategoriUser = 'satker';
		} else {
			$kategoriUser = 'ma';
		}
		$this->view->kategoriUser = $kategoriUser;
		//echo "organisasi = ".$idPengadilan." kategoriUser = $kategoriUser";
		
		if ($kategoriUser == 'ma'){	
			//list data pengadilan banding dan data satker level badan
			//-----------------------------------------------------------------------
			$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList2();
			
			//list data pengadilan TK I / biro, diambil dari data pengadilan banding yg ke 1
			//-------------------------------------------------------------------------------------------------
			$dataMasukan1 = array("idPengadilanBanding" => $this->view->dataPengadilanBanding[0]['i_organisasi']);
			$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan1);
			
			//select tahun terakhir data
			//---------------------------------
			$dataMasukan2 = array("i_organisasi" => $this->view->dataPengadilan[0]['i_organisasi']);
			$this->view->tahunTerakhir = $this->responden_serv->tahunTerakhirData($dataMasukan2); 
			//echo "tahundata = ".$this->view->tahunTerakhir;
			if($this->view->tahunTerakhir != date('Y')){
				//get detail data untuk pengadilan banding ke 1 dan pengadilan tk1. ke 1
				//----------------------------------------------------------------------------------------
				$dataMasukan3 = array("i_organisasi" => $this->view->dataPengadilan[0]['i_organisasi'],
									  "d_tahun_kuesioner" => $this->view->tahunTerakhir,
									  "i_entry" => $this->view->userid);
				$this->view->hasilDuplikatData = $this->responden_serv->duplikatData($dataMasukan3);
		
				$dataMasukan4 = array("idPengadilan" => $this->view->dataPengadilan[0]['i_organisasi']);
				$this->view->detailRespondenTerakhir = $this->responden_serv->detailRespondenTerakhir($dataMasukan4);
			
			} else {
				//get detail data untuk pengadilan banding ke 1 dan pengadilan tk1. ke 1
				//----------------------------------------------------------------------------------------
				$dataMasukan5 = array("idPengadilan" => $this->view->dataPengadilan[0]['i_organisasi']);
				$this->view->detailRespondenTerakhir = $this->responden_serv->detailRespondenTerakhir($dataMasukan5);
				
			}
		} else {
			$dataMasukan9 = array("i_organisasi" => $idPengadilan);
			$this->view->tahunTerakhir = $this->responden_serv->tahunTerakhirData($dataMasukan9); 
			if($this->view->tahunTerakhir != date('Y')){
				//list data pengadilan banding dan data satker level badan
				//-----------------------------------------------------------------------
				$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList2();
				$dataMasukan3 = array("idPengadilan" =>$idPengadilan);
				$this->view->detailRespondenTerakhir = $this->responden_serv->getIdOrganisasi($dataMasukan3);
				
				$dataMasukan4 = array("idPengadilanBanding" => $this->view->detailRespondenTerakhir['id_pengadilan_banding']);
				$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan4);
				//var_dump($this->view->detailRespondenTerakhir);
			} else {
				//list data pengadilan banding dan data satker level badan
				//-----------------------------------------------------------------------
				$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList2();
				$dataMasukan3 = array("idPengadilan" =>$idPengadilan);
				$this->view->detailRespondenTerakhir = $this->responden_serv->getIdOrganisasi($dataMasukan3);
			
				//list data pengadilan banding dan data satker level badan
				//------------------------------------------------------------
				$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList2();
				
				//select data terakhir 
				//---------------------
				$dataMasukan7 = array("idPengadilan" =>$idPengadilan);
				$this->view->detailRespondenTerakhir = $this->responden_serv->detailRespondenTerakhir($dataMasukan7);
				
				$dataMasukan8 = array("idPengadilanBanding" => $this->view->detailRespondenTerakhir['id_pengadilan_banding']);
				$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan8);
			}	
		}
	}
	
	public function duplikatdataAction()
	{
		$this->view->userid = $this->userid;
		$this->view->i_organisasi = $this->i_organisasi;
		
		$dataMasukan = array("i_organisasi" => $this->view->i_organisasi);
		$this->view->tahunTerakhir = $this->responden_serv->tahunTerakhirData($dataMasukan);
		
		if($this->view->tahunTerakhir){
			$dataMasukan2 = array("i_organisasi" => $this->view->i_organisasi,
								"d_tahun_kuesioner" => $this->view->tahunTerakhir,
								"i_entry" => $this->view->userid);
								
			$this->view->hasilDuplikatData = $this->responden_serv->duplikatData($dataMasukan2);
		} else {
			$dataMasukan2 = array("i_organisasi" => $this->view->i_organisasi,
								"d_tahun_kuesioner" => date('Y'),
								"i_entry" => $this->view->userid);
			$this->view->hasilDuplikatData = $this->responden_serv->databaruresponden($dataMasukan2);
		}
	}
	
	public function databaruAction()
	{
		$this->view->userid = $this->userid;
		$this->view->i_organisasi = $this->i_organisasi;
		
		$dataMasukan = array("i_organisasi" => $this->view->i_organisasi);
		$this->view->tahunTerakhir = $this->responden_serv->tahunTerakhirData($dataMasukan);
		
		$dataMasukan2 = array("i_organisasi" => $this->view->i_organisasi,
							"d_tahun_kuesioner" => $this->view->tahunTerakhir,
							"i_entry" => $this->view->userid);
		$this->view->databaruresponden = $this->responden_serv->databaruresponden($dataMasukan2);
	}
	
	public function gettahunterakhirdataAction()
	{
		//$this->_helper->viewRenderer->setNoRender(true);
		$iOrganisasi = $this->i_organisasi;
		
		$kategoriPengadilan = array('1', '2', '3', '4');
		$kategoriSatker = array('5', '6', '7', '8', '9', '10');
		if(in_array($this->c_kategori_organisasi, $kategoriPengadilan)){
			$kategoriUser = 'pengadilan';
		} else if(in_array($this->c_kategori_organisasi, $kategoriSatker)){
			$kategoriUser = 'satker';
		} else {
			$kategoriUser = 'ma';
		}
		
		$dataMasukan = array("i_organisasi" => $iOrganisasi);
		$this->view->tahunTerakhir = $this->responden_serv->tahunTerakhirData($dataMasukan);
	}
	
	public function pengadilantkilistAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	public function setalamatpengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->alamatPengadilan = $this->refPengadilan_serv->getalamatpengadilan($dataMasukan);
	}
	
	public function setnotelppengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->noTelpPengadilan = $this->refPengadilan_serv->getnotelppengadilan($dataMasukan);
	}
	
	public function setnofaxpengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->noFaxPengadilan = $this->refPengadilan_serv->getnofaxpengadilan($dataMasukan);
	}
	
	public function sdmolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iAgenda = $_REQUEST['iAgenda'];
		$this->view->nipAgendaDari = $this->nip;
		$this->view->namaAgendaDari = $this->nama;
		$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList();
		
		$dataMasukan = array("idPengadilanBanding" => $this->view->dataPengadilanBanding[0]['id_pengadilan']);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	public function jaringanolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iAgenda = $_REQUEST['iAgenda'];
		$this->view->nipAgendaDari = $this->nip;
		$this->view->namaAgendaDari = $this->nama;
		$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList();
		
		$dataMasukan = array("idPengadilanBanding" => $this->view->dataPengadilanBanding[0]['id_pengadilan']);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	//end of MA
	
	public function respondenAction()
	{
		$format_date = new format_date();
		$id 					= $_POST['idKuesioner'];
		$id_pengadilanbanding	= $_POST['id_pengadilanbanding'];
		if(!$id_pengadilanbanding){ $id_pengadilanbanding = $_POST['id_pengadilanbandingH'];}
		$id_pengadilanTkI 		= $_POST['id_pengadilanTkI'];
		if(!$id_pengadilanTkI){ $id_pengadilanTkI = $_POST['id_pengadilanTkIH'];}
		
		$d_bulan_kuesioner = date('m');
		$d_tahun_kuesioner = date('Y');
		
		$nAlamat 				= $_POST['nAlamat'];
		$noTelp 				= $_POST['noTelp'];
		$noFax 					= $_POST['noFax'];
		$n_responden 			= $_POST['n_responden'];
		$n_jabatan 				= $_POST['n_jabatan'];
		$n_pangkat 				= $_POST['n_pangkat'];
		$n_golongan 			= $_POST['n_golongan'];
		$no_telepon_responden 	= $_POST['no_telepon_responden'];
		$no_hp_responden 		= $_POST['no_hp_responden'];
		$i_entry				= $this->userid;
		
		$dataMasukan = array("id" => $id,
							"id_pengadilanTkI" => $id_pengadilanTkI,
							"id_pengadilanbanding" => $id_pengadilanbanding,
							"n_alamat" => $nAlamat,
							"no_telepon" => $noTelp,
							"no_fax" => $noFax,
							"d_bulan_kuesioner" => $d_bulan_kuesioner,
							"d_tahun_kuesioner" => $d_tahun_kuesioner,
							"n_responden" => $n_responden,
							"n_jabatan" => $n_jabatan,
							"n_pangkat" => $n_pangkat,
							"n_golongan" => $n_golongan,
							"no_telepon_responden" => $no_telepon_responden,
							"no_hp_responden" => $no_hp_responden,
							"i_entry" => $i_entry);
		
		//var_dump($dataMasukan);
		$hasil = $this->responden_serv->insertResponden($dataMasukan);
		
		$this->view->proses = "1";	
		$this->view->keterangan = "Responden";
		$this->view->hasil = $hasil;
		//echo "hasil = $hasil";
		$this->respondenolahdataAction();
		$this->render('respondenolahdata');
	}
}
?>