<?php

require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_jaringan_Service.php";
class Aplikasi_JaringanController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->jaringan_serv = aplikasi_jaringan_Service::getInstance();
        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;
    }

public function indexAction()
{
}
public function jaringan($id_kuesioner)
{
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $data = $this->jaringan_serv->getTmJaringan($cari);
        $jmldata = count($data);
    if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
                $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
                $this->view->c_lan_cable=$data[$j]['c_lan_cable'];
                $this->view->c_lan_wireless=$data[$j]['c_lan_wireless'];
                $this->view->c_lan_securty=$data[$j]['c_lan_securty'];
                $this->view->q_lan=$data[$j]['q_lan'];
                $this->view->q_lan_hub_client=$data[$j]['q_lan_hub_client'];
                $this->view->q_lan_nhub_client=$data[$j]['q_lan_nhub_client'];
                $this->view->c_lan_internet=$data[$j]['c_lan_internet'];
                $this->view->q_lan_hub=$data[$j]['q_lan_hub'];
                $this->view->q_pc_internet=$data[$j]['q_pc_internet'];
                $this->view->c_internet_pasca=$data[$j]['c_internet_pasca'];
                $this->view->q_internet_isp=$data[$j]['q_internet_isp'];
                $this->view->v_internet=$data[$j]['v_internet'];
                $this->view->q_internet_upload=$data[$j]['q_internet_upload'];
                $this->view->q_internet_download=$data[$j]['q_internet_download'];
                $this->view->i_entry=$data[$j]['i_entry'];
                $this->view->d_entry=$data[$j]['d_entry'];}
                $this->view->par='Koreksi';
    }
    else{$this->view->par='Simpan';}

}
public function jaringanAction()
{
    $id_kuesioner = $_REQUEST['idKuesioner'];
    $this->jaringan($id_kuesioner);
    $this->view->id_kuesioner=$id_kuesioner;
}


public function jaringanjsAction()
{
    header('content-type : text/javascript');
    $this->render('jaringanjs');
}

public function maintaindatajaringanAction()
{
    $q_lan = $_POST['q_lan'];
    if(!$q_lan) { $q_lan = 0;}
    $q_lan_hub_client = $_POST['q_lan_hub_client'];
    if(!$q_lan_hub_client) { $q_lan_hub_client = 0;}
    $q_lan_nhub_client = $_POST['q_lan_nhub_client'];
    if(!$q_lan_nhub_client) { $q_lan_nhub_client = 0;}
    $q_lan_hub = $_POST['q_lan_hub'];
    if(!$q_lan_hub) { $q_lan_hub = 0;}
    $q_pc_internet = $_POST['q_pc_internet'];
    if(!$q_pc_internet) { $q_pc_internet = 0;}

    $v_internet = $_POST['v_internet'];
    if(!$v_internet) { $v_internet = 0;}
    $q_internet_upload = $_POST['q_internet_upload'];
    if(!$q_internet_upload) { $q_internet_upload = 0;}
    $q_internet_download = $_POST['q_internet_download'];
    if(!$q_internet_download) { $q_internet_download = 0;}

    $MaintainData1 = array("id_kuesioner"=>$_POST['id_kuesioner'],
                        "c_lan_cable"=>$_POST['c_lan_cable'],
                        "c_lan_wireless"=>$_POST['c_lan_wireless'],
                        "c_lan_securty"=>$_POST['c_lan_securty'],
                        "q_lan"=>$q_lan,
                        "q_lan_hub_client"=>$q_lan_hub_client,
                        "q_lan_nhub_client"=>$q_lan_nhub_client,
                        "c_lan_internet"=>$_POST['c_lan_internet'],
                        "q_lan_hub"=>$q_lan_hub,
                        "q_pc_internet"=>$q_pc_internet,
                        "c_internet_pasca"=>$_POST['c_internet_pasca'],
                        "q_internet_isp"=>$_POST['q_internet_isp'],
                        "v_internet"=>$v_internet,
                        "q_internet_upload"=>$q_internet_upload,
                        "q_internet_download"=>$q_internet_download,
                        "i_entry"=>$_POST['i_entry']);

    $jmldataJaringan =  $this->jaringan_serv->getJumlahdataJaringan($_POST['id_kuesioner']);
    if ($jmldataJaringan>0)
    {
        $hasil = $this->jaringan_serv->ubahTmJaringan($MaintainData1);
        $this->view->par="Koreksi";
        $par="Simpan ";}
    else
    {   $hasil = $this->jaringan_serv->tambahTmJaringan($MaintainData1);
        $this->view->par="simpan";
        $par="Simpan ";}

    $pesan=$par." data ".$hasil;
    $this->view->pesan = $pesan;
    $this->view->pesancek = $hasil;
    $this->view->id_kuesioner=$id_kuesioner;
    $this->jaringan($_POST['id_kuesioner']) ;
    $this->_helper->viewRenderer('jaringan');

}

public function hapusAction()
{
    $id_kuesioner=$_GET['id_kuesioner'];
    $MaintainData1 = array("id_kuesioner"=>$_GET['id_kuesioner']);
    $hasil = $this->jaringan_serv->hapusTmJaringan($MaintainData1);
    $this->view->par='Simpan';
    $this->_helper->viewRenderer('jaringan');

}
    public function jaringanjuduliconAction()
    {
        //untuk mengeluarkan judul dan icon Kondisi Sistem TI - Software
    }
}
?>
