<?php
class aplikasi_kegiatan_Service {
    private static $instance;
  
    private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

public function getTmKegiatan($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,c_kegiatan,n_kegiatan,e_kegiatan,v_anggaran,
										e_alamat_prshn,e_mekanisme,i_lama,i_waktu_mulai,i_waktu_akhir,c_jaminan,e_cara,e_pedoman
										from tm_kegiatan where 1=1 $cari ");
										
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"c_kegiatan"=>(string)$result[$j]->c_kegiatan,
									"n_kegiatan"=>(string)$result[$j]->n_kegiatan,
									"e_kegiatan"=>(string)$result[$j]->e_kegiatan,
									"v_anggaran"=>(string)$result[$j]->v_anggaran,
									"e_alamat_prshn"=>(string)$result[$j]->e_alamat_prshn,
									"e_mekanisme"=>(string)$result[$j]->e_mekanisme,
									"i_lama"=>(string)$result[$j]->i_lama,
									"i_waktu_mulai"=>(string)$result[$j]->i_waktu_mulai,
									"i_waktu_akhir"=>(string)$result[$j]->i_waktu_akhir,
									"c_jaminan"=>(string)$result[$j]->c_jaminan,
									"e_cara"=>(string)$result[$j]->e_cara,
									"e_pedoman"=>(string)$result[$j]->e_pedoman);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}	
	
public function getTrKomponen() 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select c_komponen,n_komponen
										from tr_kegiatan_komponen order by c_komponen asc");		

										
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("c_komponen"=>(string)$result[$j]->c_komponen,
									"n_komponen"=>(string)$result[$j]->n_komponen);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	
	public function tambahTmKegiatan(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"c_kegiatan"=>$data['c_kegiatan'],
							"n_kegiatan"=>$data['n_kegiatan'],
							"e_kegiatan"=>$data['e_kegiatan'],
							"v_anggaran"=>$data['v_anggaran'],
							"e_alamat_prshn"=>$data['e_alamat_prshn'],
							"e_mekanisme"=>$data['e_mekanisme'],
							"i_lama"=>$data['i_lama'],
							"i_waktu_mulai"=>$data['i_waktu_mulai'],
							"i_waktu_akhir"=>$data['i_waktu_akhir'],
							"c_jaminan"=>$data['c_jaminan'],
							"e_cara"=>$data['e_cara'],
							"e_pedoman"=>$data['e_pedoman'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_kegiatan',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	
	
	public function ubahTmKegiatan(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $ubah_data = array(	"id_kuesioner"=>$data['id_kuesioner'],
							"n_kegiatan"=>$data['n_kegiatan'],
							"e_kegiatan"=>$data['e_kegiatan'],
							"v_anggaran"=>$data['v_anggaran'],
							"e_alamat_prshn"=>$data['e_alamat_prshn'],
							"e_mekanisme"=>$data['e_mekanisme'],
							"i_lama"=>$data['i_lama'],
							"i_waktu_mulai"=>$data['i_waktu_mulai'],
							"i_waktu_akhir"=>$data['i_waktu_akhir'],
							"c_jaminan"=>$data['c_jaminan'],
							"e_cara"=>$data['e_cara'],
							"e_pedoman"=>$data['e_pedoman'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     
		$db->update('tm_kegiatan',$ubah_data, "id_kuesioner = '".trim($data['id_kuesioner'])."'  and   c_kegiatan = '".trim($data['c_kegiatan'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	

public function getTmKegiatanKomponen($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,c_kegiatan,b.c_komponen,n_komponen,e_spesipikasi,i_jml_komponen,e_keterangan
										from tm_kegiatan_komponen a, tr_kegiatan_komponen b where a.c_komponen=b.c_komponen  $cari  order by a.c_komponen asc ");	
								
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"c_kegiatan"=>(string)$result[$j]->c_kegiatan,
									"c_komponen"=>(string)$result[$j]->c_komponen,
									"n_komponen"=>(string)$result[$j]->n_komponen,
									"e_spesipikasi"=>(string)$result[$j]->e_spesipikasi,
									"i_jml_komponen"=>(string)$result[$j]->i_jml_komponen,
									"e_keterangan"=>(string)$result[$j]->e_keterangan);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	
	public function tambahTmKegiatanKomponen(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"c_kegiatan"=>$data['c_kegiatan'],
							"c_komponen"=>$data['c_komponen'],
							"e_spesipikasi"=>$data['e_spesipikasi'],
							"i_jml_komponen"=>$data['i_jml_komponen'],
							"e_keterangan"=>$data['e_keterangan'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_kegiatan_komponen',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}
	
	public function ubahTmKegiatanKomponen(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();

	     $ubah_data = array("e_spesipikasi"=>$data['e_spesipikasi'],
						"i_jml_komponen"=>$data['i_jml_komponen'],
						"e_keterangan"=>$data['e_keterangan'],
						"i_entry"=>$data['i_entry'],
						"d_entry"=>date("Y-m-d"));	
		$db->update('tm_kegiatan_komponen',$ubah_data, "id_kuesioner = '".trim($data['id_kuesioner'])."'  and   c_kegiatan = '".trim($data['c_kegiatan'])."' and   c_komponen = '".trim($data['c_komponen'])."' ");
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}	

}
?>
