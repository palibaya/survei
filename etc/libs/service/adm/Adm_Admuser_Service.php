<?php
class Adm_Admuser_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List User
	//======================================================================
	public function cariUserList(array $dataMasukan, $pageNumber, $itemPerPage) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		//$pageNumber 	= $dataMasukan['pageNumber'];
		//$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= strtoupper($dataMasukan['katakunciCari']);
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
	   
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			if(!$kategoriCari){ $kategoriCari='i_user';}
			$where = "where UPPER($kategoriCari) like '%$katakunciCari%' ";
			$order = " order by $sortBy $sort ";
			$sqlProses = "select a.i_user, a.n_user, a.i_organisasi, b.n_organisasi
						  from tm_user a
						  left join tm_organisasi b on(a.i_organisasi = b.i_organisasi) ";	

						  if(($pageNumber==0) && ($itemPerPage==0))
			{	
				$sqlTotal = "select count(*) from ($sqlProses"." "."$where) a";
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$xLimit=$itemPerPage;
				$xOffset=($pageNumber-1)*$itemPerPage;
			
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				//echo $sqlData;
				$result = $db->fetchAll($sqlData);	
			}
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("i_user"  		=>(string)$result[$j]->i_user,
										"n_user"  	    =>(string)$result[$j]->n_user,
										"i_organisasi"  =>(string)$result[$j]->i_organisasi,
										"n_organisasi" 	=>(string)$result[$j]->n_organisasi
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	public function userByIdOrgList($dataMasukan) {
	
		$i_organisasi = $dataMasukan['i_organisasi'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select a.i_user as i_user, a.n_user as n_user, a.i_organisasi as i_organisasi, b.n_organisasi as n_organisasi from tm_user a left join tm_organisasi b on(a.i_organisasi = b.i_organisasi) where a.i_organisasi = '$i_organisasi'";
				
			
			//echo $sqlProses;				
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("i_user"  		=>(string)$result[$j]->i_user,
										"n_user"  	    =>(string)$result[$j]->n_user,
										"i_organisasi"  =>(string)$result[$j]->i_organisasi,
										"n_organisasi" 	=>(string)$result[$j]->n_organisasi
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	public function userInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			if($dataMasukan['kelompok'] == '3'){
				$i_organisasi = 'MA';
			} else {
				if ($dataMasukan['id_organisasiTk2'] == '-'){
					$i_organisasi = $dataMasukan['id_organisasiTkI'];
				} else {
					$i_organisasi = $dataMasukan['id_organisasiTk2'];
				}
			}
			
			$jmluser = $db->fetchOne("select count(*) from tm_user where i_organisasi = '$i_organisasi'");
			$nOrganisasi = $db->fetchOne("select n_organisasi from tm_organisasi where i_organisasi = '$i_organisasi'");
			
			if($dataMasukan['kelompok'] != '3'){
				if($jmluser > 0){
					return "User untuk <b>$nOrganisasi</b> sudah ada";
				} else {
					$paramInput = array("i_user"  	=>$dataMasukan['i_user'],
										"n_user" 			=>$dataMasukan['n_user'],
										"n_password" 			=>$dataMasukan['n_password'],
										"i_organisasi" 	=>$i_organisasi,
										"i_entry"		=>$dataMasukan['i_entry'],
										"d_entry"		=>date('Y-m-d'));	
				
					$db->insert('tm_user',$paramInput);
				}
			} else {
				$paramInput = array("i_user"  	=>$dataMasukan['i_user'],
										"n_user" 			=>$dataMasukan['n_user'],
										"n_password" 			=>$dataMasukan['n_password'],
										"i_organisasi" 	=>$i_organisasi,
										"i_entry"		=>$dataMasukan['i_entry'],
										"d_entry"		=>date('Y-m-d'));	
				
				$db->insert('tm_user',$paramInput);
			}
			$db->commit();
			
			return 'sukses';

		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return $e->getMessage();
			}
	   }
	}
	
	public function detailUserById($user_id) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select a.i_user, a.n_user, a.i_organisasi, b.i_organisasi_parent,
	b.c_kategori_organisasi
							from tm_user a
							left join tm_organisasi b on (a.i_organisasi = b.i_organisasi)
							where i_user = '$user_id'";	

			$sqlData = $sqlProses;
			$result = $db->fetchRow($sqlData);	
			
			$hasilAkhir = array("i_user"  		=>(string)$result->i_user,
								"n_user"  	=>(string)$result->n_user,
								"i_organisasi" 	=>(string)$result->i_organisasi,
								"i_organisasi_parent" 	=>(string)$result->i_organisasi_parent,
								"c_kategori_organisasi" =>(string)$result->c_kategori_organisasi
								);
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function userUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("n_user" 			=>$dataMasukan['n_user'],
								"i_entry"		=>$dataMasukan['i_entry'],
								"d_entry"		=>date('Y-m-d'));	
			
			if ($dataMasukan['n_password']){
				$paramInput["n_password"] = $dataMasukan['n_password'];
								
			}
			//var_dump($paramInput);
			$where[] = " i_user = '".$dataMasukan['i_user']."'";
			
			$db->update('tm_user',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function userHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
								
			$where[] = " i_user = '".$dataMasukan['i_user']."'";
			
			$db->delete('tm_user', $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "sukses";
			}
	   }
	}
	
	public function getUserListAll() {
	
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
		$db->setFetchMode(Zend_Db::FETCH_OBJ);
		$result = $db->fetchAll("SELECT * FROM tm_pegawai where c_statusdelete != 'Y'");
				
		 
         $jmlResult = count($result);
		 return $result;
	    } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
	}

	

	

	
	
	public function ubahStatus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInput = array("kd_status" 	=>$dataMasukan['kd_status']);
				
			//var_dump($paramInput);
			$where[] = " user_id = '".$dataMasukan['user_id']."'";
			
			$db->update('tm_pegawai',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	
	
	public function ubahPasswd(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			$passwordLama = $dataMasukan['n_passwordlama'];
			$CheckPasswordLama = $db->fetchOne("select n_password from tm_user
												where i_user = '".$dataMasukan['user_id']."'");
			//echo "$passwordLama == $CheckPasswordLama";									
			if($passwordLama == $CheckPasswordLama){
				$paramInput = array("n_password"	=> $dataMasukan['n_passwordbaru']);	
									
				$where[] = " i_user = '".$dataMasukan['user_id']."'";
				
				$db->update('tm_user',$paramInput, $where);
				$db->commit();
				return 'sukses';
			} else {
				return 'Gagal. User Lama salah.';
			}	
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			return "gagalbbbb.";
	   }
	}
	
	public function getDataPegawai($kategori, $katakunci) {
		
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
		$db->setFetchMode(Zend_Db::FETCH_OBJ);
		
		if (!$kategori){
			$kategori = "nip";
		}
		$result = $db->fetchAll("SELECT nm_jabatan, jenis_jabatan, nip, id_riwayat_jabatan, kd_jabatan,
								       kd_jabatan_induk, nama, kd_struktur_org, kd_struktur_org_induk, level,
								       nm_level, sk_pemberhentian, sk_pengangkatan, tgl_pemberhentian,
								       tgl_pengangkatan, tmt, eselon_jabatan
								  FROM simpeg_internal.v_jab_peg
								  WHERE $kategori like '%$katakunci%' 
								  ORDER BY kd_struktur_org");
								  
		 for ($j = 0; $j < count($result); $j++) {
				$hasilAkhir[$j] = array("nm_jabatan"  		=>(string)$result[$j]->nm_jabatan,
										"jenis_jabatan"  	=>(string)$result[$j]->jenis_jabatan,
										"nip"  				=>(string)$result[$j]->nip,
										"id_riwayat_jabatan"=>(string)$result[$j]->id_riwayat_jabatan,
										"kd_jabatan" 		=>(string)$result[$j]->kd_jabatan,
										"kd_jabatan_induk"  =>(string)$result[$j]->kd_jabatan_induk,
										"nama"  			=>(string)$result[$j]->nama,
										"kd_struktur_org"	=>(string)$result[$j]->kd_struktur_org,
										"kd_struktur_org_induk"	=>(string)$result[$j]->kd_struktur_org_induk,
										"level"      		=>(string)$result[$j]->level,
										"nm_level"      	=>(string)$result[$j]->nm_level,
										"sk_pemberhentian"  =>(string)$result[$j]->sk_pemberhentian,
										"sk_pengangkatan"   =>(string)$result[$j]->sk_pengangkatan,
										"tgl_pemberhentian" =>(string)$result[$j]->tgl_pemberhentian,
										"tgl_pengangkatan"  =>(string)$result[$j]->tgl_pengangkatan,
										"tmt"      			=>(string)$result[$j]->tmt,
										"eselon_jabatan"    =>(string)$result[$j]->eselon_jabatan
										);
				//var_dump($hasilAkhir);				
		}	
		return $hasilAkhir;	
		} catch (Exception $e) {
		 echo $e->getMessage().'<br>';
		 return 'gagal';
		}
	}
		
}
?>
