<?php
require_once 'share/globalReferensi.php';

class Aplikasi_Laporan_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
   /*  private function __construct() {
       //echo 'I am constructed';
    } */

    // The singleton method
    /* public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }
 */
	//======================================================================
	// List Pejabat
	//======================================================================
	public function rekapSuratMasukPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$nKepada 			= $dataMasukan['nKepada'];
			$kdOrgTeruskandari 	= $dataMasukan['kdOrgTeruskandari'];
			$kdOrgTeruskanke 	= $dataMasukan['kdOrgTeruskanke'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			$jenisSrt		 	= trim($dataMasukan['jenisSrt']);
			
			if (strtoupper($nKepada) == 'PRIBADI'){
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
									where UPPER(a.n_kepada) not in ('PRESIDEN', 'SESKAB')
									AND a.kd_orgteruskandari not in ('0', '1')
									AND a.kd_orgteruskandari = a.kd_orgKepada";
			} else if ((strtoupper($nKepada) == 'PRESIDEN') || (strtoupper($nKepada) == 'SESKAB')) {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '1' ";
			} else {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '$kdOrgTeruskandari' 
								AND a.kd_orgteruskandari = a.kd_orgKepada";
			}
			
			if (($kdOrgTeruskanke == '3') || ($kdOrgTeruskanke == '6')){
				$sqlProses = "select count(*) as jumlah
								from
								(".$sql1."
								AND ( a.kd_orgteruskanke = '3' OR a.kd_orgteruskanke = '6')
								AND MONTH(a.t_disposisi) = '$bulan'
								AND YEAR(a.t_disposisi) = '$tahun'
								AND a.c_srtjenis like '$jenisSrt') x";	
			} else if ($kdOrgTeruskanke == '00'){
				$sqlProses = "select count(*) as jumlah
									from
									(".$sql1."
									AND a.kd_orgteruskanke not in ('1','2','3','4','5','7','8','9','10')
									AND MONTH(a.t_disposisi) = '$bulan'
									AND YEAR(a.t_disposisi) = '$tahun'
									AND a.c_srtjenis like '$jenisSrt') x";	
			} else {
				$sqlProses = "select count(*) as jumlah
									from
									(".$sql1."
									AND a.kd_orgteruskanke = '$kdOrgTeruskanke'
									AND MONTH(a.t_disposisi) = '$bulan'
									AND YEAR(a.t_disposisi) = '$tahun'
									AND a.c_srtjenis like '$jenisSrt') x";	
			}					
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				
			
			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapSuratMasukSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$nKepada 			= $dataMasukan['nKepada'];
			$kdOrgTeruskandari 	= $dataMasukan['kdOrgTeruskandari'];
			$kdOrgTeruskanke 	= $dataMasukan['kdOrgTeruskanke'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			$jenisSrt		 	= $dataMasukan['jenisSrt'];
			
			if (strtoupper($nKepada) == 'PRIBADI'){
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
									where UPPER(a.n_kepada) not in ('PRESIDEN', 'SESKAB')
									AND a.kd_orgteruskandari not in ('0', '1')
									AND a.kd_orgteruskandari = a.kd_orgKepada";
			} else if ((strtoupper($nKepada) == 'PRESIDEN') || (strtoupper($nKepada) == 'SESKAB')) {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '1' ";
			} else {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '$kdOrgTeruskandari' 
								AND a.kd_orgteruskandari = a.kd_orgKepada";
			}
			
			if (($kdOrgTeruskanke == '3') || ($kdOrgTeruskanke == '6')){
				$sqlProses = "select count(*) as jumlah
								from
								(".$sql1."
								AND (a.kd_orgteruskanke = '3' OR a.kd_orgteruskanke = '6')
								AND ((MONTH(a.t_disposisi) >= 1) AND (MONTH(a.t_disposisi) <= $bulan))
								AND YEAR(a.t_disposisi) = '$tahun'
								AND a.c_srtjenis = '$jenisSrt') x";	
			} else if ($kdOrgTeruskanke == '00'){
				$sqlProses = "select count(*) as jumlah
								from
								(".$sql1."
								AND a.kd_orgteruskanke not in ('1','2','3','4','5','7','8','9','10')
								AND ((MONTH(a.t_disposisi) >= 1) AND (MONTH(a.t_disposisi) <= $bulan))
								AND YEAR(a.t_disposisi) = '$tahun'
								AND a.c_srtjenis = '$jenisSrt') x";	
			} 
			else {
				$sqlProses = "select count(*) as jumlah
								from
								(".$sql1." 
								AND a.kd_orgteruskanke = '$kdOrgTeruskanke'
								AND ((MONTH(a.t_disposisi) >= 1) AND (MONTH(a.t_disposisi) <= $bulan))
								AND YEAR(a.t_disposisi) = $tahun
								AND a.c_srtjenis = '$jenisSrt') x";	
			}				
			
			$result = $db->fetchOne($sqlProses);				
			//echo $sqlProses;
			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
 
	public function jumlahRekapSuratMasukPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$nKepada 			= $dataMasukan['nKepada'];
			$kdOrgTeruskandari 	= $dataMasukan['kdOrgTeruskandari'];
			$kdOrgTeruskanke 	= $dataMasukan['kdOrgTeruskanke'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			$jenisSrt		 	= $dataMasukan['jenisSrt'];
			
			//echo "xxxxxxx = ".strtoupper($nKepada);
			if (strtoupper($nKepada) == 'PRIBADI'){
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
									where UPPER(a.n_kepada) not in ('PRESIDEN', 'SESKAB')
									AND a.kd_orgteruskandari not in ('0', '1')
									AND a.kd_orgteruskandari = a.kd_orgKepada";
			} else if ((strtoupper($nKepada) == 'PRESIDEN') || (strtoupper($nKepada) == 'SESKAB')) {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '1' ";
			} else {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '$kdOrgTeruskandari' 
								AND a.kd_orgteruskandari = a.kd_orgKepada";
			}
			
			if (strtoupper($nKepada) == 'PRIBADI'){
				$sqlProses = "select count(*) as jumlah
									from
									(".$sql1."
									AND a.kd_orgteruskanke not in ('1','2','3','4','5','7','8','9','10')
									AND MONTH(a.t_disposisi) = '$bulan'
									AND YEAR(a.t_disposisi) = '$tahun'
									AND a.c_srtjenis = '$jenisSrt') x";	
			} else {
				$sqlProses = "select count(*) as jumlah
									from
									(".$sql1."
									AND a.kd_orgteruskanke in $kdOrgTeruskanke
									AND MONTH(a.t_disposisi) = '$bulan'
									AND YEAR(a.t_disposisi) = '$tahun'
									AND a.c_srtjenis = '$jenisSrt') x";	
			}
								
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				
			
			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function jumlahRekapSuratMasukSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$nKepada 			= $dataMasukan['nKepada'];
			$kdOrgTeruskandari 	= $dataMasukan['kdOrgTeruskandari'];
			$kdOrgTeruskanke 	= $dataMasukan['kdOrgTeruskanke'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			$jenisSrt		 	= $dataMasukan['jenisSrt'];
			
			if (strtoupper($nKepada) == 'PRIBADI'){
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
									where UPPER(a.n_kepada) not in ('PRESIDEN', 'SESKAB')
									AND a.kd_orgteruskandari not in ('0', '1')
									AND a.kd_orgteruskandari = a.kd_orgKepada";
			} else if ((strtoupper($nKepada) == 'PRESIDEN') || (strtoupper($nKepada) == 'SESKAB')) {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '1' ";
			} else {
				$sql1 = "select a.id_srtmasuk, a.i_agendasrt, a.n_kepada, a.n_teruskanke, a.kd_orgteruskanke, a.n_teruskandari, 
									a.kd_orgteruskandari, MONTH(a.t_disposisi),YEAR(a.t_disposisi), a.c_srtjenis
								from vm_disposisi a
								where UPPER(a.n_kepada) like '%$nKepada%'
								AND a.kd_orgteruskandari = '$kdOrgTeruskandari' 
								AND a.kd_orgteruskandari = a.kd_orgKepada";
			}
			
			if (strtoupper($nKepada) == 'PRIBADI'){
				$sqlProses = "select count(*) as jumlah
									from
									(".$sql1."
									AND a.kd_orgteruskanke not in ('1','2','3','4','5','7','8','9','10')
									AND ((MONTH(a.t_disposisi) >= 1) AND (MONTH(a.t_disposisi) <= $bulan))
									AND YEAR(a.t_disposisi) = '$tahun'
									AND a.c_srtjenis = '$jenisSrt') x";	
			} else {
				$sqlProses = "select count(*) as jumlah
									from
									(".$sql1."
									AND a.kd_orgteruskanke in $kdOrgTeruskanke
									AND ((MONTH(a.t_disposisi) >= 1) AND (MONTH(a.t_disposisi) <= $bulan))
									AND YEAR(a.t_disposisi) = '$tahun'
									AND a.c_srtjenis = '$jenisSrt') x";	
			}
			// $sqlProses = "select count(*) as jumlah
								// from
								// (".$sql1."
								// AND a.kd_orgteruskanke in $kdOrgTeruskanke
								// AND ((MONTH(a.t_disposisi) >= 1) AND (MONTH(a.t_disposisi) <= $bulan))
								// AND YEAR(a.t_disposisi) = $tahun
								// AND a.c_srtjenis = '$jenisSrt') x";	
								
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				
			
			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	//List Instansi
	//===================
	public function getNamaUnitKerjaSingkat($kd_org) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select level, nm_level
						  from simpeg_internal.tbl_struktur_organisasi
						  where kd_struktur_org = '$kd_org'";	
							
			
			$result = $db->fetchRow($sqlProses);				
			
			$level = (string)$result->level;
			$nm_level = (string)$result->nm_level;
			
			$nama = $level.' '.$nm_level;
			return $nama;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPenomoranSuratPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$sifatPenomoranSurat 	= $dataMasukan['sifatPenomoranSurat'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			//echo "$sifatPenomoranSurat<br>";
			if ($kdOrgDari == 'ESELON I'){
				$kdOrgDari = "(select kd_struktur_org 
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('DEPUTI', 'STAF AHLI SEKRETARIS KABINET', 'STAF KHUSUS PRESIDEN', 'STAF KHUSUS SEKRETARIS KABINET')
								AND kd_struktur_org_induk = '0')";
			} else if ($kdOrgDari == 'ESELON II'){
				$kdOrgDari = "(select kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('BIRO'))";
			} else if ($kdOrgDari == 'JUMLAH') {
				$kdOrgDari = "(select kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where (UPPER(level) IN ('DEPUTI', 'STAF AHLI SEKRETARIS KABINET', 'STAF KHUSUS PRESIDEN', 'STAF KHUSUS SEKRETARIS KABINET')
								AND kd_struktur_org_induk = '0')
								OR UPPER(level) IN ('BIRO')
								OR kd_struktur_org = '0'
								OR kd_struktur_org = '1'
								OR kd_struktur_org = '2')";
			} else {
				$kdOrgDari = "(select kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where kd_struktur_org = '$kdOrgDari')";
			}
			
			
			if ($sifatPenomoranSurat == 'SL'){
			    $sifatPenomoranSurat = "('KEP', 'SPT', 'R', 'B')";
				$sqlProses = "select count(*) as jumlah
								from
								(SELECT a.i_srtajuan, a.n_dari, a.kd_org_dari, a.d_tglajuan, a.c_srtsifat
								  FROM tm_surat_ajuan a
									WHERE a.kd_org_dari in $kdOrgDari
										AND UPPER(a.c_srtsifat) not in $sifatPenomoranSurat
										AND MONTH(a.d_tglajuan) = '$bulan'
										AND YEAR(a.d_tglajuan) = '$tahun'

								) x";	

			} else if ($sifatPenomoranSurat == 'JML'){
				$sqlProses = "select count(*) as jumlah
								from
								(SELECT a.i_srtajuan, a.n_dari, a.kd_org_dari, a.d_tglajuan, a.c_srtsifat
								  FROM tm_surat_ajuan a
									WHERE a.kd_org_dari in $kdOrgDari
										AND UPPER(a.c_srtsifat) in (select c_srtsifat from tr_sifatpenomoran_suratkeluar)
										AND MONTH(a.d_tglajuan) = '$bulan'
										AND YEAR(a.d_tglajuan) = '$tahun'

								) x";	
			}else {
				$sqlProses = "select count(*) as jumlah
								from
								(SELECT a.i_srtajuan, a.n_dari, a.kd_org_dari, a.d_tglajuan, a.c_srtsifat
								  FROM tm_surat_ajuan a
									WHERE a.kd_org_dari in $kdOrgDari
										AND UPPER(a.c_srtsifat) = '$sifatPenomoranSurat'
										AND MONTH(a.d_tglajuan) = '$bulan'
										AND YEAR(a.d_tglajuan) = '$tahun'

								) x";	
			}
			
			$result = $db->fetchOne($sqlProses);				
			
			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPenomoranSuratSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$sifatPenomoranSurat 	= $dataMasukan['sifatPenomoranSurat'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			if ($kdOrgDari == 'ESELON I'){
				$kdOrgDari = "(select kd_struktur_org 
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('DEPUTI', 'STAF AHLI SEKRETARIS KABINET', 'STAF KHUSUS PRESIDEN', 'STAF KHUSUS SEKRETARIS KABINET')
								AND kd_struktur_org_induk = '0')";
			} else if ($kdOrgDari == 'ESELON II'){
				$kdOrgDari = "(select kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('BIRO'))";
			} else if ($kdOrgDari == 'JUMLAH') {
				$kdOrgDari = "(select kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where (UPPER(level) IN ('DEPUTI', 'STAF AHLI SEKRETARIS KABINET', 'STAF KHUSUS PRESIDEN', 'STAF KHUSUS SEKRETARIS KABINET')
								AND kd_struktur_org_induk = '0')
								OR UPPER(level) IN ('BIRO')
								OR kd_struktur_org = '0'
								OR kd_struktur_org = '1'
								OR kd_struktur_org = '2')";
			} else {
				$kdOrgDari = "(select kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where kd_struktur_org = '$kdOrgDari')";
			}
			
			if ($sifatPenomoranSurat == 'SL'){
				$sifatPenomoranSurat = "('KEP', 'SPT', 'R', 'B')";
				$sqlProses = "select count(*) as jumlah
								from
								(SELECT a.i_srtajuan, a.n_dari, a.kd_org_dari, a.d_tglajuan, a.c_srtsifat
								  FROM tm_surat_ajuan a
									WHERE a.kd_org_dari in $kdOrgDari
										AND UPPER(a.c_srtsifat) not in $sifatPenomoranSurat
										AND ((MONTH(a.d_tglajuan) >= 1) AND (MONTH(a.d_tglajuan) <= $bulan))
										AND YEAR(a.d_tglajuan) = '$tahun'

								) x";	

			} else if ($sifatPenomoranSurat == 'JML'){
				$sqlProses = "select count(*) as jumlah
								from
								(SELECT a.i_srtajuan, a.n_dari, a.kd_org_dari, a.d_tglajuan, a.c_srtsifat
								  FROM tm_surat_ajuan a
									WHERE a.kd_org_dari in $kdOrgDari
										AND UPPER(a.c_srtsifat) in (select c_srtsifat from tr_sifatpenomoran_suratkeluar)
										AND ((MONTH(a.d_tglajuan) >= 1) AND (MONTH(a.d_tglajuan) <= $bulan))
										AND YEAR(a.d_tglajuan) = '$tahun'

								) x";	
			}else {
				$sqlProses = "select count(*) as jumlah
								from
								(SELECT a.i_srtajuan, a.n_dari, a.kd_org_dari, a.d_tglajuan, a.c_srtsifat
								  FROM tm_surat_ajuan a
									WHERE a.kd_org_dari in $kdOrgDari
										AND UPPER(a.c_srtsifat) = '$sifatPenomoranSurat'
										AND ((MONTH(a.d_tglajuan) >= 1) AND (MONTH(a.d_tglajuan) <= $bulan))
										AND YEAR(a.d_tglajuan) = '$tahun'

								) x";	
			}
			
			
			$result = $db->fetchOne($sqlProses);				
			
			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function listUnitkerja($eselon) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			if (strtoupper($eselon) == 'ESELON I'){
				$sqlProses = "select level, nm_level, kd_struktur_org 
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('DEPUTI', 'STAF AHLI SEKRETARIS KABINET', 'STAF KHUSUS PRESIDEN', 'STAF KHUSUS SEKRETARIS KABINET')
								AND kd_struktur_org_induk = '0'";
			} else {
				$sqlProses = "select level, nm_level, kd_struktur_org
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('BIRO')";
			}
				
							
			
			$result = $db->fetchAll($sqlProses);				
			
			for($i=0; $i<count($result); $i++){
				$level = (string)$result[$i]->level;
				$nm_level = (string)$result[$i]->nm_level;
				$kd_struktur_org = (string)$result[$i]->kd_struktur_org;
				$nama = $level.' '.$nm_level;
				$hasil[$i] = array("kd_struktur_org" => $kd_struktur_org,
									"nama" => $nama);
			}

			return $hasil;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function listJenisAjuan($kdJenisAjuan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select c_srtajuan, n_srtajuan
							from tr_jenis_ajuan
							where c_srtajuan = '$kdJenisAjuan'";	
							
			
			$result = $db->fetchRow($sqlProses);				
			
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			$hasilAkhir = array("c_srtajuan"  		=>(string)$result->c_srtajuan,
										"n_srtajuan"  		=>(string)$result->n_srtajuan); 	
				
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function listUnitkerja2() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select level, nm_level, kd_struktur_org 
						  from simpeg_internal.tbl_struktur_organisasi
						  where kd_struktur_org_induk = '0'";
			
			$result = $db->fetchAll($sqlProses);				
			
			for($i=0; $i<count($result); $i++){
				$level = (string)$result[$i]->level;
				$nm_level = (string)$result[$i]->nm_level;
				$kd_struktur_org = (string)$result[$i]->kd_struktur_org;
				$nama = $level.' '.$nm_level;
				$hasil[$i] = array("kd_struktur_org" => $kd_struktur_org,
									"nama" => $nama);
			}

			return $hasil;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function listLevelBiro($kdOrgInduk) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select level, nm_level, kd_struktur_org 
						  from simpeg_internal.tbl_struktur_organisasi
						  where UPPER(level) IN ('BIRO') AND kd_struktur_org_induk = '$kdOrgInduk'";
			
			$result = $db->fetchAll($sqlProses);				
			
			for($i=0; $i<count($result); $i++){
				$level = (string)$result[$i]->level;
				$nm_level = (string)$result[$i]->nm_level;
				$kd_struktur_org = (string)$result[$i]->kd_struktur_org;
				$nama = $level.' '.$nm_level;
				$hasil[$i] = array("kd_struktur_org" => $kd_struktur_org,
									"nama" => $nama);
			}

			return $hasil;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPengirimanSuratKeluarPosPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*) as jumlah 
						  from tm_surat_ajuan
						  where c_srt_saranakirim = 'pos' AND
								kd_org_dari = '$kdOrgDari' AND
								MONTH(d_tglkirim) = '$bulan' AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPengirimanSuratKeluarLangsungPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*) as jumlah 
						  from tm_surat_ajuan
						  where c_srt_saranakirim = 'langsung' AND
								kd_org_dari = '$kdOrgDari' AND
								MONTH(d_tglkirim) = '$bulan' AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPengirimanSuratKeluarPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*) as jumlah 
						  from tm_surat_ajuan
						  where kd_org_dari = '$kdOrgDari' AND
								MONTH(d_tglkirim) = '$bulan' AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapBiayaPengirimanSuratKeluarPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select sum(v_biayakirim) biayaKirim
						  from tm_surat_ajuan
						  where kd_org_dari = '$kdOrgDari' AND
								MONTH(d_tglkirim) = '$bulan' AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	//////////////////////
	public function rekapPengirimanSuratKeluarPosSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*) as jumlah 
						  from tm_surat_ajuan
						  where c_srt_saranakirim = 'pos' AND
								kd_org_dari = '$kdOrgDari' AND
								((MONTH(d_tglkirim) >= 1) AND (MONTH(d_tglkirim) <= $bulan)) AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPengirimanSuratKeluarLangsungSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*) as jumlah 
						  from tm_surat_ajuan
						  where c_srt_saranakirim = 'langsung' AND
								kd_org_dari = '$kdOrgDari' AND
								((MONTH(d_tglkirim) >= 1) AND (MONTH(d_tglkirim) <= $bulan)) AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapPengirimanSuratKeluarSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*) as jumlah 
						  from tm_surat_ajuan
						  where kd_org_dari = '$kdOrgDari' AND
								((MONTH(d_tglkirim) >= 1) AND (MONTH(d_tglkirim) <= $bulan)) AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapBiayaPengirimanSuratKeluarSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$kdOrgDari 	= $dataMasukan['kdOrgDari'];
			$bulan			 	= $dataMasukan['bulan'];
			$tahun			 	= $dataMasukan['tahun'];
			
			$sqlProses = "select sum(v_biayakirim) biayaKirim
						  from tm_surat_ajuan
						  where kd_org_dari = '$kdOrgDari' AND
								((MONTH(d_tglkirim) >= 1) AND (MONTH(d_tglkirim) <= $bulan)) AND
								YEAR(d_tglkirim) = '$tahun'";
			
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapMemoSeskabSisaBulanLalu($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$ajukanKe	= $dataMasukan['ajukanKe'];
			$bulan		= $dataMasukan['bulan']-1;
			$tahun		= $dataMasukan['tahun'];
			
			if(($ajukanKe != 'S')){
				$sqlProses = "select count(*)  as jumlah 
							  from tm_memo_keluar a
							  where a.c_jenisajuan = '$jenisAjuan' and ";
							  
				if (($ajukanKe == '0') ||($ajukanKe == '1')) {			  
					$sqlProses .= "a.n_teruskan = '$ajukanKe' AND ";
				} else {
					$sqlProses .= "a.n_teruskan not in ('0', '1') AND ";
				}
				if ($ajukanKe == '0'){				
					$sqlProses .= "MONTH(d_memo_seskab) = '$bulan' AND
								YEAR(d_memo_seskab) = '$tahun'";
				}
				else if (($ajukanKe == '1') || ($ajukanKe == 'L')) {				
					$sqlProses .= "MONTH(t_terimaberkas) = '$bulan' AND
								YEAR(t_terimaberkas) = '$tahun'";
				}
			} else {
				$sqlProses = "select count(*)  as jumlah 
							  from tm_memo_keluar a
							  where a.c_jenisajuan = '$jenisAjuan' and 
									a.c_statusmemo = 'buatsuratkeluar' AND
									MONTH(t_terimaberkas) = '$bulan' AND
									YEAR(t_terimaberkas) = '$tahun'";
				//echo $sqlProses;
			}
			
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapMemoSeskabPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$jenisProses	= $dataMasukan['jenisProses'];
			$bulan		= $dataMasukan['bulan'];
			$tahun		= $dataMasukan['tahun'];
			
			if($jenisProses == 'M'){
				$sqlProses = "select count(*)
								from
								(
								select max(a.id) as id, a.i_memo
								from tm_memo_keluar a 
								where a.c_jenisajuan = '$jenisAjuan'  
								and MONTH(t_terimaberkas) = '$bulan' AND YEAR(t_terimaberkas) = '$tahun'
								group by a.i_memo
								) x, tm_memo_keluar b
								where x.id = b.id 
								and (i_memo_seskab is not null or i_memo_seskab != '')
								and b.c_statusmemo != 'buatsuratkeluar'";
			}
			else if($jenisProses == 'S'){
				$sqlProses = "select count(*)
								from
								(
								select max(a.id) as id, a.i_memo
								from tm_memo_keluar a 
								where a.c_jenisajuan = '$jenisAjuan'  
								and MONTH(t_terimaberkas) = '$bulan' AND YEAR(t_terimaberkas) = '$tahun'
								group by a.i_memo
								) x, tm_memo_keluar b
								where x.id = b.id 
								and (i_memo_seskab is not null or i_memo_seskab != '')
								and b.c_statusmemo = 'buatsuratkeluar'";
			}
			//echo $sqlProses."<br>";
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapMemoSeskabTerprosesPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$ajuanKe	= $dataMasukan['ajuanKe'];
			$bulan		= $dataMasukan['bulan'];
			$tahun		= $dataMasukan['tahun'];
			
			if ($ajuanKe != 'L') {
				$sqlProses = "select count(*)
									from
									(
									select max(a.id) as id, a.i_memo
									from tm_memo_keluar a 
									where a.c_jenisajuan = '$jenisAjuan'  
									group by a.i_memo
									) x, tm_memo_keluar b
									where x.id = b.id 
										and MONTH(b.t_terimaberkas) = '$bulan' AND YEAR(b.t_terimaberkas) = '$tahun'
										and b.n_teruskan = '$ajuanKe'
										and (b.c_statusmemo is null or b.c_statusmemo = '')";
			} else {
				$sqlProses = "select count(*)
									from
									(
									select max(a.id) as id, a.i_memo
									from tm_memo_keluar a 
									where a.c_jenisajuan = '$jenisAjuan'  
									group by a.i_memo
									) x, tm_memo_keluar b
									where x.id = b.id 
										and MONTH(b.t_terimaberkas) = '$bulan' AND YEAR(b.t_terimaberkas) = '$tahun'
										and b.n_teruskan not in ('0', '1')
										and (b.c_statusmemo is null or b.c_statusmemo = '')";
			}
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapMemoSeskabTerprosesSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$ajuanKe	= $dataMasukan['ajuanKe'];
			$bulan		= $dataMasukan['bulan'];
			$tahun		= $dataMasukan['tahun'];
			
			if ($ajuanKe != 'L') {
				$sqlProses = "select count(*)
									from
									(
									select max(a.id) as id, a.i_memo
									from tm_memo_keluar a 
									where a.c_jenisajuan = '$jenisAjuan'  
									group by a.i_memo
									) x, tm_memo_keluar b
									where x.id = b.id 
										and ((MONTH(b.t_terimaberkas) >= 1) AND (MONTH(b.t_terimaberkas) <= $bulan)) 
										AND YEAR(b.t_terimaberkas) = '$tahun'
										and b.n_teruskan = '$ajuanKe'
										and (b.c_statusmemo is null or b.c_statusmemo = '')";
			} else {
				$sqlProses = "select count(*)
									from
									(
									select max(a.id) as id, a.i_memo
									from tm_memo_keluar a 
									where a.c_jenisajuan = '$jenisAjuan'  
									group by a.i_memo
									) x, tm_memo_keluar b
									where x.id = b.id 
										and ((MONTH(b.t_terimaberkas) >= 1) AND (MONTH(b.t_terimaberkas) <= $bulan))
										AND YEAR(b.t_terimaberkas) = '$tahun'
										and b.n_teruskan not in ('0', '1')
										and (b.c_statusmemo is null or b.c_statusmemo = '')";
			}
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapMemoSeskabSelesaiPerBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$ajuanKe	= $dataMasukan['ajuanKe'];
			$bulan		= $dataMasukan['bulan'];
			$tahun		= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*)
								from
								(
								select max(a.id) as id, a.i_memo
								from tm_memo_keluar a 
								where a.c_jenisajuan = '$jenisAjuan'  
								group by a.i_memo
								) x, tm_memo_keluar b
								where x.id = b.id 
									and MONTH(b.t_terimaberkas) = '$bulan' AND YEAR(b.t_terimaberkas) = '$tahun'
									and (b.c_statusmemo is not null or b.c_statusmemo != '')";
			
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function rekapMemoSeskabSelesaiSampaiBulan($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$ajuanKe	= $dataMasukan['ajuanKe'];
			$bulan		= $dataMasukan['bulan'];
			$tahun		= $dataMasukan['tahun'];
			
			$sqlProses = "select count(*)
								from
								(
								select max(a.id) as id, a.i_memo
								from tm_memo_keluar a 
								where a.c_jenisajuan = '$jenisAjuan'  
								group by a.i_memo
								) x, tm_memo_keluar b
								where x.id = b.id 
									AND ((MONTH(b.t_terimaberkas) >= 1) AND (MONTH(b.t_terimaberkas) <= $bulan)) 
									AND YEAR(b.t_terimaberkas) = '$tahun'
									and (b.c_statusmemo is not null or b.c_statusmemo != '')";
			
			//echo $sqlProses;
			$result = $db->fetchOne($sqlProses);				

			return $result;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function monitoringSuratKeluarList($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$jenisAjuan = $dataMasukan['jenisAjuan'];
			$ajuanKe	= $dataMasukan['ajuanKe'];
			$bulan		= $dataMasukan['bulan'];
			$tahun		= $dataMasukan['tahun'];
			
			$sqlProses = "SELECT max(id) maxId, id_srtmasuk, i_memo, d_memo, c_memorev, n_dari,
									n_perihal, c_jenisajuan, t_terimaberkas, c_statusmemo
						  FROM tm_memo_keluar
						  WHERE n_teruskan = '1'
						  GROUP BY id_srtmasuk, i_memo, d_memo,  n_dari, n_perihal, c_jenisajuan, c_statusmemo";
			
			$result = $db->fetchAll($sqlProses);
			for($x=0; $x<count($result); $x++){
				$n_jenisajuan = $db->fetchOne("select n_srtajuan from tr_jenis_ajuan where c_srtajuan='".$result[$x]->c_jenisajuan."'");
				$d_memo_kepresiden = $db->fetchOne("select d_memo from tm_memo_keluar 
													where i_memo = '".$result[$x]->i_memo."' AND
														  c_memorev = '".$result[$x]->c_memorev."' AND
														  n_teruskan = '0'");
														  
				$i_memo_seskab = $db->fetchOne("select i_memo_seskab from tm_memo_keluar 
													where i_memo = '".$result[$x]->i_memo."' AND
														  c_memorev = '".$result[$x]->c_memorev."' AND
														  n_teruskan = '0'");										  

				$hasil[$x] = array("id_srtmasuk"	=> (string)$result[$x]->id_srtmasuk,
									"i_memo"		=> (string)$result[$x]->i_memo,
									"c_memorev"		=> (string)$result[$x]->c_memorev,
									"n_dari"		=> (string)$result[$x]->n_dari,
									"n_perihal"		=> (string)$result[$x]->n_perihal,
									"c_jenisajuan"	=> (string)$result[$x]->c_jenisajuan,
									"n_jenisajuan"	=> $n_jenisajuan,
									"d_memo"		=> (string)$result[$x]->d_memo,
									"t_terimaberkas"=> (string)$result[$x]->t_terimaberkas,
									"c_statusmemo"	=> (string)$result[$x]->c_statusmemo,
									"d_memo_kepresiden" => $d_memo_kepresiden,
									"i_memo_seskab" => $i_memo_seskab
								  );
			}
			
			return $hasil;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
}
?>