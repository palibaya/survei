<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "share/format_date.php";
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";


class Aplikasi_SurveyController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
		
		$this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
	   // $ssogroup = new Zend_Session_Namespace('ssogroup');
	   //echo "TEST ".$ssogroup->n_user_grp." ".$ssogroup->i_user." ".$ssogroup->i_peg_nip;
	   $this->user  = 'cdr';
	   // $this->username  = 'Yuliah';
	   //$this->nip  = strtoupper($this->id['nip']);
	   // $this->usernip  = '060046350';
	   // $this->kdorg = 'SK1201';
	 // $this->modul    = '5';
	 // $this->category = 'A';
		
    }
	
    public function indexAction() {
	   
    }
	
	public function surveyjsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('surveyjs');
    }
	
	//MA
	//----------------------
	public function surveylistAction()
	{
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= 'id_pengadilan';
		$katakunciCari 	= $_POST['cari'];
		$sortBy			= 'id_pengadilan';
		$sort			= 'desc';
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$this->view->currentPage = $pageNumber;
		$this->view->numToDisplay = $itemPerPage;		
		//$this->view->surveyList = $this->survey_serv->carisurveyList($dataMasukan);
		
	}
	
	public function surveyolahdataAction()
	{

	}
	
	
	public function pengadilantkilistAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	public function setalamatpengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->alamatPengadilan = $this->refPengadilan_serv->getalamatpengadilan($dataMasukan);
	}
	
	public function setnotelppengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->noTelpPengadilan = $this->refPengadilan_serv->getnotelppengadilan($dataMasukan);
	}
	
	public function sdmolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iAgenda = $_REQUEST['iAgenda'];
		$this->view->nipAgendaDari = $this->nip;
		$this->view->namaAgendaDari = $this->nama;
		$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList();
		
		$dataMasukan = array("idPengadilanBanding" => $this->view->dataPengadilanBanding[0]['id_pengadilan']);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	//end of MA
	
	
	
	public function daftarpegawaiAction()
	{
		$this->view->par = $_REQUEST['par'];
		
		$kategoriCari = $_REQUEST['kategoriCari'];
		$kataKunci = $_REQUEST['kataKunci'];
		
		$dataMasukan = array("kategoriCari" => $kategoriCari,
							 "kataKunci" => $kataKunci);
		$this->view->pejabatList = $this->ref_serv->pencarianPegawai($dataMasukan);	
	}
	
	public function agendaAction()
	{
		$format_date = new format_date();
		//$iAgenda 	= $_POST['iAgenda'];
		$iUserTerima 	= $_POST['iUserTerima'];
		$iPegTelponhp 	= $_POST['iPegTelponhp'];
		$nAgendaPesan 	= $_POST['nAgendaPesan'];
		$eAgendaPesan 	= $_POST['eAgendaPesan'];
		$iUserKirim 	= $_POST['iUserKirim'];
		$dAgenda 		= $_POST['dAgenda'];
		//$dAgenda = null;
		if($dAgenda){ $dAgenda = $format_date->convertTglHumanToMachine($dAgenda);}
		else {$dAgenda = null;}
		
		$dAgendaJammulai 	= $_POST['dAgendaJammulai'];
		if(!$dAgendaJammulai) { $dAgendaJammulai = null;}
		$dAgendaJamakhir 	= $_POST['dAgendaJamakhir'];
		if(!$dAgendaJamakhir) { $dAgendaJamakhir = null;}
		$cAgendaType 	= $_POST['cAgendaType'];

		
		$iEntry 		= $this->user;
		
		$dataMasukan = array("iAgenda" => $iAgenda,
							"iUserTerima" => $iUserTerima,
							"iPegTelponhp" => $iPegTelponhp,
							"nAgendaPesan" => $nAgendaPesan,
							"eAgendaPesan" => $eAgendaPesan,
							"iUserKirim" => $iUserKirim,
							"dAgenda" => $dAgenda,
							"dAgendaJammulai" => $dAgendaJammulai,
							"dAgendaJamakhir" => $dAgendaJamakhir,
							"cAgendaType" => $cAgendaType,
							"iEntry" => $iEntry);
		
		
		
		$hasil = $this->agenda_serv->agendaInsert($dataMasukan);
		$this->view->proses = "1";	
		$this->view->keterangan = "Agenda";
		$this->view->hasil = $hasil;
		//echo "hasil = $hasil";
		$this->agendalistAction();
		$this->render('agendalist');
	}
	
	
	
	
	public function agendaupdateAction()
	{
		$format_date = new format_date();
		$iAgenda 	= $_POST['iAgenda'];
		$iAgendaHidden 	= $_POST['iAgendaHidden'];
		$iUserTerima 	= $_POST['iUserTerima'];
		$iPegTelponhp 	= $_POST['iPegTelponhp'];
		$nAgendaPesan 	= $_POST['nAgendaPesan'];
		$eAgendaPesan 	= $_POST['eAgendaPesan'];
		$iUserKirim 	= $_POST['iUserKirim'];
		
		$dAgenda 		= $_POST['dAgenda'];
		
		if($dAgenda){ $dAgenda = $format_date->convertTglHumanToMachine($dAgenda);}
		else {$dAgenda = null;}
		
		$dAgendaJammulai 	= $_POST['dAgendaJammulai'];
		$dAgendaJamakhir 	= $_POST['dAgendaJamakhir'];
		$cAgendaType 	= $_POST['cAgendaType'];

		
		$iEntry 		= $this->user;
		
		$dataMasukan = array("iAgenda" => $iAgenda,
							"iAgendaHidden" => $iAgendaHidden,
							"iUserTerima" => $iUserTerima,
							"iPegTelponhp" => $iPegTelponhp,
							"nAgendaPesan" => $nAgendaPesan,
							"eAgendaPesan" => $eAgendaPesan,
							"iUserKirim" => $iUserKirim,
							"dAgenda" => $dAgenda,
							"dAgendaJammulai" => $dAgendaJammulai,
							"dAgendaJamakhir" => $dAgendaJamakhir,
							"cAgendaType" => $cAgendaType,
							"iEntry" => $iEntry);
		//var_dump($dataMasukan);
		$hasil = $this->agenda_serv->agendaUpdate($dataMasukan);
		$this->view->proses = "2";	
		$this->view->keterangan = "Agenda";
		$this->view->hasil = $hasil;
		$this->agendalistAction();
		$this->render('agendalist');
	}
	
	public function agendahapusAction()
	{
		$iAgenda 		= $_REQUEST['iAgenda'];
		
		$dataMasukan = array("i_agenda" => $iAgenda);
		
		$hasil = $this->agenda_serv->agendaHapus($dataMasukan);
		$this->view->proses = "3";	
		$this->view->keterangan = "Agenda";
		$this->view->hasil = $hasil;
		
		$this->agendalistAction();
		$this->render('agendalist');
	}
	
	public function detailagendaAction()
	{
		$idAgenda = $_REQUEST['idAgenda'];
		
		$this->view->detailAgenda = $this->agenda_serv->detailAgendaById($idAgenda);
	}
}
?>