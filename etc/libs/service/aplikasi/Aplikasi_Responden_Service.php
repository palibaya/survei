<?php
class Aplikasi_Responden_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//MA
	//======
	public function detailRespondenTerakhir($data){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$idPengadilan = $data['idPengadilan'];
			$sqlProses = "SELECT a.id, a.id_pengadilan, a.d_bulan_kuesioner, a.d_tahun_kuesioner, a.n_responden,
							a.n_jabatan, a.n_pangkat, a.n_golongan, a.no_telepon_responden, a.no_hp_responden, 
							a.i_entry, a.d_entry, b.n_alamat, b.no_telepon, b.no_fax, b.i_organisasi_parent
						FROM tm_kuesioner a, tm_organisasi b
						WHERE a.id_pengadilan = b.i_organisasi AND
							a.id_pengadilan='$idPengadilan' AND
							a.id = (select max(c.id) from tm_kuesioner c where c.id_pengadilan = '$idPengadilan')";	
			
//echo $sqlProses;			
			$result = $db->fetchRow($sqlProses);				
			
			$jmlResult = count($result);
			
			$hasilAkhir = array("id"  	=>(string)$result->id,
								"id_pengadilan"  	=>(string)$result->id_pengadilan,
								"id_pengadilan_banding"  	=>(string)$result->i_organisasi_parent,
								"n_pengadilan"  	=>(string)$result->n_pengadilan,
								"n_alamat"  	=>(string)$result->n_alamat,
								"no_telepon"  	=>(string)$result->no_telepon,
								"no_fax"  	=>(string)$result->no_fax,
								"d_bulan_kuesioner"  	=>(string)$result->d_bulan_kuesioner,
								"d_tahun_kuesioner"  	=>(string)$result->d_tahun_kuesioner,
								"n_responden"  	=>(string)$result->n_responden,
								"n_jabatan"  	=>(string)$result->n_jabatan,
								"n_pangkat"  	=>(string)$result->n_pangkat,
								"n_golongan"  	=>(string)$result->n_golongan,
								"no_telepon_responden"  	=>(string)$result->no_telepon_responden,
								"no_hp_responden"  	=>(string)$result->no_hp_responden,
								"i_entry"  	=>(string)$result->i_entry,
								"d_updateResponden"  	=>(string)$result->d_entry
								);
				//var_dump($hasilAkhir);				
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function insertResponden(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
			$db->beginTransaction();
			
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			// $idKuesioner = $db->fetchOne('select max(id)  from tm_kuesioner');
			// if(!$idKuesioner){
				// $idKuesioner = 1;
			// } else {
				// $idKuesioner = $idKuesioner+1;
			// }
			
			//update tm_organisasi
			//==================
			$id_pengadilanTkI= $data['id_pengadilanTkI'];
			$id_pengadilanbanding= $data['id_pengadilanbanding'];
			if ($id_pengadilanTkI) {
				$idPengadilan = $id_pengadilanTkI;
				$idPengadilanBanding = $id_pengadilanbanding;
			} else {
				$idPengadilan = $id_pengadilanbanding;
				$idPengadilanBanding = null;
			}
			
			$id= $data['id'];
			$n_alamat= $data['n_alamat'];
			$no_telepon= $data['no_telepon'];
			$no_fax= $data['no_fax'];
			
			$paramUpdatePengadilan = array("n_alamat" => $n_alamat,
											"no_telepon" => $no_telepon,
											"no_fax" => $no_fax
										   );
			$where1[] = "i_organisasi = ".$idPengadilan;
			//$where1[] = "i_organisasi_parent = ".$idPengadilanBanding;
			
			/* var_dump($paramUpdatePengadilan);
			echo "<br>";
			var_dump($where1); */
			$db->update('tm_organisasi', $paramUpdatePengadilan, $where1);			
			
			//pengisian data responden
			//=====================
			$d_bulan_kuesioner= $data['d_bulan_kuesioner'];
			$d_tahun_kuesioner= $data['d_tahun_kuesioner'];
			$n_responden= $data['n_responden'];
			$n_jabatan= $data['n_jabatan'];
			$n_pangkat= $data['n_pangkat'];
			$n_golongan= $data['n_golongan'];
			$no_telepon_responden= $data['no_telepon_responden'];
			$no_hp_responden= $data['no_hp_responden'];
			$i_entry= $data['i_entry'];;
	    // echo "id = ".$data['id'];
		
			$data1 = $db->fetchOne("select count(*) from tm_kuesioner where id = '$id'");
			if($data1 == 0){
				$paramInsertKuesioner = array("id" => $id,
									"id_pengadilan" => $idPengadilan,
									"d_bulan_kuesioner" => $d_bulan_kuesioner,
									"d_tahun_kuesioner" => $d_tahun_kuesioner,
									"n_responden" => $n_responden,
									"n_jabatan" => $n_jabatan,
									"n_pangkat" => $n_jabatan,
									"n_golongan" => $n_golongan,
									"no_telepon_responden" => $no_telepon_responden,
									"no_hp_responden" => $no_hp_responden,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);
				$db->insert('tm_kuesioner', $paramInsertKuesioner);
			} else {
				$paramUpdateKuesioner = array("n_responden" => $n_responden,
									"n_jabatan" => $n_jabatan,
									"n_pangkat" => $n_jabatan,
									"n_golongan" => $n_golongan,
									"no_telepon_responden" => $no_telepon_responden,
									"no_hp_responden" => $no_hp_responden,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);
				$whereUpdateKuesioner[] = "id = '".$data['id']."'";
				$db->update('tm_kuesioner', $paramUpdateKuesioner, $whereUpdateKuesioner);
			}
		 $db->commit();
		 unset($paramInsert);
	     return 'sukses';
	   } catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
			return "gagal.Data Sudah Ada.";
			}
			else
			{
			return "gagal.";
			}
	   }
	}
	
	public function pengadilanbandingList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			/* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort "; */
			$sqlProses = "select id_pengadilan,
								n_pengadilan
							from tm_pengadilan
							where (id_pengadilan_banding is null) or (id_pengadilan_banding = '' )
							order by id_pengadilan";	
							
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("id_pengadilan"  	=>(string)$result[$j]->id_pengadilan,
									   "n_pengadilan"  	=>(string)$result[$j]->n_pengadilan
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function pengadilanList($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select id_pengadilan,
								n_pengadilan
							from tm_pengadilan
							where id_pengadilan_banding = '$idPengadilanBanding' 
							order by id_pengadilan";	
							
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("id_pengadilan"  	=>(string)$result[$j]->id_pengadilan,
									   "n_pengadilan"  	=>(string)$result[$j]->n_pengadilan
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getalamatpengadilan($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select n_alamat
							from tm_pengadilan
							where id_pengadilan = '$idPengadilanBanding'";	
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("n_alamat" =>(string)$result[$j]->n_alamat
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getnotelppengadilan($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select no_telepon
							from tm_pengadilan
							where id_pengadilan = '$idPengadilanBanding'";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("no_telepon" =>(string)$result[$j]->no_telepon
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function getnofaxpengadilan($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select no_fax
							from tm_pengadilan
							where id_pengadilan = '$idPengadilanBanding'";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("no_fax" =>(string)$result[$j]->no_fax
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function tahunTerakhirData($dataMasukan) {
	
		$i_organisasi = $dataMasukan['i_organisasi'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select a.d_tahun_kuesioner
							from tm_kuesioner a
							where a.id_pengadilan = '$i_organisasi' and
							a.id = (select max(id) from tm_kuesioner where id_pengadilan = '$i_organisasi')";	
			$tahunTerakhir = $db->fetchOne($sqlProses);				
			//echo "tahunterakhir = $sqlProses";
			return $tahunTerakhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function duplikatData($dataMasukan) {
	
		$i_organisasi = $dataMasukan['i_organisasi'];
		$d_tahun_kuesioner = $dataMasukan['d_tahun_kuesioner'];
		$i_entry = $dataMasukan['i_entry'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->beginTransaction();
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			//select data kuesioner terakhir
			//--------------------------------
			$sqlProsesKuesioner = "SELECT id, id_pengadilan, d_bulan_kuesioner, d_tahun_kuesioner, n_responden,
								       n_jabatan, n_pangkat, n_golongan, no_telepon_responden, no_hp_responden, i_entry, d_entry
								  FROM tm_kuesioner
								  WHERE id_pengadilan = '$i_organisasi' AND
										d_tahun_kuesioner = '$d_tahun_kuesioner'
								";	
			$dataKuesioner = $db->fetchRow($sqlProsesKuesioner);
			$idKuesionerLama = $dataKuesioner->id;
			
			//id kuesioner baru
			//===============
			$idKuesionerBaru = $db->fetchOne('select max(id)  from tm_kuesioner');
			if(!$idKuesionerBaru){
				$idKuesionerBaru = 1;
			} else {
				$idKuesionerBaru = $idKuesionerBaru+1;
			}
			
			//insert kuesioner baru tahun sekarang
			//================================
			if($dataKuesioner->n_responden){
				$nResponden = $dataKuesioner->n_responden;
			} else {
				$nResponden = $i_entry;
			}
			
			$paramInsertKuesioner = array("id" => $idKuesionerBaru,
									"id_pengadilan" => $i_organisasi,
									"d_bulan_kuesioner" => date('m'),
									"d_tahun_kuesioner" => date('Y'),
									"n_responden" => $nResponden,
									"n_jabatan" => $dataKuesioner->n_jabatan,
									"n_pangkat" => $dataKuesioner->n_pangkat,
									"n_golongan" => $dataKuesioner->n_golongan,
									"no_telepon_responden" => $dataKuesioner->no_telepon_responden,
									"no_hp_responden" => $dataKuesioner->no_hp_responden,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);
			$db->insert('tm_kuesioner', $paramInsertKuesioner);
			
			//select data sdm
			//----------------
			$sqlProsesSDM = "SELECT id_kuesioner, i_jumlahhakim, i_jumlahpns, i_jumlahpp, i_jumlahhonorer,
								   i_jumlahpeneliti,i_jumlahwidyaiswara, i_jumlahpranatakomputer, i_jumlahauditor,
							       i_stafit_sertifikasi_software, i_stafit_sertifikasi_hardware,
							       i_stafit_sertifikasi_network, i_user_siap, i_user_sdm, i_user_sai,
							       i_user_rkakl, i_user_sabmn, i_user_website, i_hakim_pc, i_pp_pc,
							       i_pns_pc, i_honorer_pc, i_entry, d_entry
							  FROM tm_sdm
							  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataSDM = $db->fetchRow($sqlProsesSDM);
			
			//insert data SDM baru
			//-----------------------
			$paramInsertSdm = array("id_kuesioner" => $idKuesionerBaru,
									"i_jumlahhakim" => $dataSDM->i_jumlahhakim,
									"i_jumlahpns" => $dataSDM->i_jumlahpns,
									"i_jumlahpp" => $dataSDM->i_jumlahpp,
									"i_jumlahhonorer" => $dataSDM->i_jumlahhonorer,
									"i_jumlahpeneliti" => $dataSDM->i_jumlahpeneliti,
									"i_jumlahwidyaiswara" => $dataSDM->i_jumlahwidyaiswara,
									"i_jumlahpranatakomputer" => $dataSDM->i_jumlahpranatakomputer,
									"i_jumlahauditor" => $dataSDM->i_jumlahauditor,
									"i_stafit_sertifikasi_software" => $dataSDM->i_stafit_sertifikasi_software,
									"i_stafit_sertifikasi_hardware" => $dataSDM->i_stafit_sertifikasi_hardware,
									"i_stafit_sertifikasi_network" => $dataSDM->i_stafit_sertifikasi_network,
									"i_user_siap" => $dataSDM->i_user_siap,
									"i_user_sdm" => $dataSDM->i_user_sdm,
									"i_user_sai" => $dataSDM->i_user_sai,
									"i_user_rkakl" => $dataSDM->i_user_rkakl,
									"i_user_sabmn" => $dataSDM->i_user_sabmn,
									"i_user_website" => $dataSDM->i_user_website,
									"i_hakim_pc" => $dataSDM->i_hakim_pc,
									"i_pp_pc" => $dataSDM->i_pp_pc,
									"i_pns_pc" => $dataSDM->i_pns_pc,
									"i_honorer_pc" => $dataSDM->i_honorer_pc,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);

			$db->insert('tm_sdm', $paramInsertSdm);
			
			//select data software
			//---------------------
			
			$sqlProsesSofetware = "SELECT id_kuesioner, i_xp, i_vista, i_seven, i_linux, i_dos, i_ext, i_browser,
							       i_pdfreader, i_office, i_kompresi, i_virus, c_website, n_website,
							       c_sw_platform, c_sw_database, c_sk, c_struktur, c_nemail, n_sw_email,
							       i_entry, d_entry
							  FROM tm_software
							  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataSoftware = $db->fetchRow($sqlProsesSofetware);
			
			//insert  data software baru
			//-----------------------------
			$tambah_data = array("id_kuesioner"=>$idKuesionerBaru,
							"i_xp"=>$dataSoftware->i_xp,
							"i_vista"=>$dataSoftware->i_vista,
							"i_seven"=>$dataSoftware->i_seven,
							"i_linux"=>$dataSoftware->i_linux,
							"i_dos"=>$dataSoftware->i_dos,
							"i_ext"=>$dataSoftware->i_ext,
							"i_browser"=>$dataSoftware->i_browser,
							"i_pdfreader"=>$dataSoftware->i_pdfreader,
							"i_office"=>$dataSoftware->i_office,
							"i_kompresi"=>$dataSoftware->i_kompresi,
							"i_virus"=>$dataSoftware->i_virus,
							"c_website"=>$dataSoftware->c_website,
							"n_website"=>$dataSoftware->n_website,
							"c_sw_platform"=>$dataSoftware->c_sw_platform,
							"c_sw_database"=>$dataSoftware->c_sw_database,
							"c_sk"=>$dataSoftware->c_sk,
							"c_struktur"=>$dataSoftware->c_struktur,
							"c_nemail"=>$dataSoftware->c_nemail,
							"n_sw_email"=>$dataSoftware->n_sw_email,
							"i_entry"=>$i_entry,
							"d_entry"=>date("Y-m-d"));									
			$db->insert('tm_software',$tambah_data);
			
			//select tm_software_adm
			//-------------------------
			$sqlProsesSofetwareAdm = "SELECT id_kuesioner, c_adm, n_aplikasi, c_platform, c_database, c_jns_aplikasi, i_entry, d_entry
									  FROM tm_software_adm
									  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataSoftwareAdm = $db->fetchAll($sqlProsesSofetwareAdm);
			//insert tm_software_adm
			//-------------------------
			if(count($dataSoftwareAdm)){
				for($x=0; $x<count($dataSoftwareAdm); $x++){
					$tambah_data = array("id_kuesioner"=>$idKuesionerBaru,
										"c_adm"=>$dataSoftwareAdm[$x]->c_adm,
										"n_aplikasi"=>$dataSoftwareAdm[$x]->n_aplikasi,
										"c_platform"=>$dataSoftwareAdm[$x]->c_platform,
										"c_database"=>$dataSoftwareAdm[$x]->c_database,
										"c_jns_aplikasi"=>$dataSoftwareAdm[$x]->c_jns_aplikasi,
										"i_entry"=>$i_entry,
										"d_entry"=>date("Y-m-d"));									
				     $db->insert('tm_software_adm',$tambah_data);
				}
			}
			
			//select tm_software_kendala
			//------------------------------
			$sqlProsesSofetwareKendala = "SELECT id_kuesioner, n_aplikasi_kendala, n_kendala, i_entry, d_entry
									  FROM tm_software_kendala
									  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataSoftwareKendala = $db->fetchAll($sqlProsesSofetwareKendala);
			//insert tm_software_kendala
			//-------------------------
			//if(count($dataSoftwareKendala)){
				for($x=0; $x<count($dataSoftwareKendala); $x++){
					$paramInsertareKendala = array("id_kuesioner"=>$idKuesionerBaru,
										"n_aplikasi_kendala"=>$dataSoftwareKendala[$x]->n_aplikasi_kendala,
										"n_kendala"=>$dataSoftwareKendala[$x]->n_kendala,
										"i_entry"=>$i_entry,
										"d_entry"=>date("Y-m-d"));									
				     $db->insert('tm_software_kendala',$paramInsertareKendala);
				}
			/* } else {
				$paramInsertareKendala = array("id_kuesioner"=>$idKuesionerBaru,
										"i_entry"=>$i_entry,
										"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_software_kendala',$paramInsertareKendala);
			} */
	
			//select data hardware
			//---------------------
			
			$sqlProsesHw = "SELECT id_kuesioner, i_proc_pentium3, i_proc_pentium4, i_proc_dualcore,
							       i_proc_coretwoduo, i_memori_1gb, i_memori_2gb, i_hardisk_40gb,
							       i_hardisk_50gb, i_monitor_17lcd, i_monitor_18lcd, i_monitor_17nlcd,
							       i_monitor_18nlcd, i_printer, i_scanner, i_ups, c_cctv, c_fasilitas_tv,
							       c_fasilitas_pc, c_fasilitas_liflet, c_fasilitas_no, i_watt,
							       c_problem_watt, i_entry, d_entry
							  FROM ma.tm_hardware
							  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataHw = $db->fetchRow($sqlProsesHw);
			
			//insert  data hardware baru
			//-----------------------------
			$paramtHwInsert = array("id_kuesioner"=>$idKuesionerBaru,
							"i_proc_pentium3"=>$dataHw->i_proc_pentium3,
							"i_proc_pentium4"=>$dataHw->i_proc_pentium4,
							"i_proc_dualcore"=>$dataHw->i_proc_dualcore,
							"i_proc_coretwoduo"=>$dataHw->i_proc_coretwoduo,
							"i_memori_1gb"=>$dataHw->i_memori_1gb,
							"i_memori_2gb"=>$dataHw->i_memori_2gb,
							"i_hardisk_40gb"=>$dataHw->i_hardisk_40gb,
							"i_hardisk_50gb"=>$dataHw->i_hardisk_50gb,
							"i_monitor_17lcd"=>$dataHw->i_monitor_17lcd,
							"i_monitor_18lcd"=>$dataHw->i_monitor_18lcd,
							"i_monitor_17nlcd"=>$dataHw->i_monitor_17nlcd,
							"i_monitor_18nlcd"=>$dataHw->i_monitor_18nlcd,
							"i_printer"=>$dataHw->i_printer,
							"i_scanner"=>$dataHw->i_scanner,
							"i_ups"=>$dataHw->i_ups,
							"c_cctv"=>$dataHw->c_cctv,
							"c_fasilitas_tv"=>$dataHw->c_fasilitas_tv,
							"c_fasilitas_pc"=>$dataHw->c_fasilitas_pc,
							"c_fasilitas_liflet"=>$dataSoftware->c_fasilitas_liflet,
							"c_fasilitas_no"=>$dataSoftware->c_fasilitas_no,
							"i_watt"=>$dataSoftware->i_watt,
							"c_problem_watt"=>$dataSoftware->c_fasilitas_liflet,
							"i_entry"=>$i_entry,
							"d_entry"=>date("Y-m-d"));									
			$db->insert('tm_hardware',$paramtHwInsert);
	
			//select data hardware server
			//-----------------------------
			
			$sqlProsesHwServer = "SELECT id_kuesioner, c_fungsi_sever, n_aplikasi_server, i_jml_server,
							       n_spesifikasi, i_entry, d_entry
							  FROM tm_hardware_server
							  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataHwServer = $db->fetchAll($sqlProsesHwServer);
			//insert  data hardware server baru
			//------------------------------------
			//if(count($dataHwServer)) {
				for($x=0; $x<count($dataHwServer); $x++){
					$paramtHwServerInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"c_fungsi_sever"=>$dataHwServer[$x]->c_fungsi_sever,
									"n_aplikasi_server"=>$dataHwServer[$x]->n_aplikasi_server,
									"i_jml_server"=>$dataHwServer[$x]->i_jml_server,
									"n_spesifikasi"=>$dataHwServer[$x]->n_spesifikasi,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_hardware_server',$paramtHwServerInsert);
					unset($paramtHwServerInsert);
				}
			/* } else {
				$paramtHwServerInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_hardware_server',$paramtHwServerInsert);
			} */
			
			//select data jaringan
			//-----------------------
			
			$sqlProsesJaringan = "SELECT id_kuesioner, c_lan_cable, c_lan_wireless, c_lan_securty, q_lan,
								       q_lan_hub_client, q_lan_nhub_client, c_lan_internet, q_lan_hub,
								       q_pc_internet, c_internet_pasca, q_internet_isp, v_internet,
								       q_internet_upload, q_internet_download, i_entry, d_entry
								  FROM ma.tm_jaringan
							      WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataJaringan = $db->fetchRow($sqlProsesJaringan);
			//insert  data jaringan baru
			//-----------------------------
			$paramtJaringanInsert = array("id_kuesioner"=>$idKuesionerBaru,
							"c_lan_cable"=>$dataJaringan->c_lan_cable,
							"c_lan_wireless"=>$dataJaringan->c_lan_wireless,
							"c_lan_securty"=>$dataJaringan->c_lan_securty,
							"q_lan"=>$dataJaringan->q_lan,
							"q_lan_hub_client"=>$dataJaringan->q_lan_hub_client,
							"q_lan_nhub_client"=>$dataJaringan->q_lan_nhub_client,
							"c_lan_internet"=>$dataJaringan->c_lan_internet,
							"q_lan_hub"=>$dataJaringan->q_lan_hub,
							"q_pc_internet"=>$dataJaringan->q_pc_internet,
							"c_internet_pasca"=>$dataJaringan->c_internet_pasca,
							"q_internet_isp"=>$dataJaringan->q_internet_isp,
							"v_internet"=>$dataJaringan->v_internet,
							"q_internet_download"=>$dataJaringan->q_internet_download,
							"q_internet_download"=>$dataJaringan->q_internet_download,
							"i_entry"=>$i_entry,
							"d_entry"=>date("Y-m-d"));									
			$db->insert('tm_jaringan',$paramtJaringanInsert);
		
			//select data prosedure
			//-----------------------
			
			$sqlProsesProsedure = "SELECT id_kuesioner, n_prosedure, e_desc, i_entry, d_entry
								   FROM tm_prosedure
							      WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataProsedure = $db->fetchAll($sqlProsesProsedure);
			//insert  data prosedure baru
			//-----------------------------
			//if (count($dataProsedure)) {
				for($x=0; $x<count($dataProsedure); $x++){
					$paramtProsedureInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"n_prosedure"=>$dataProsedure[$x]->n_prosedure,
									"e_desc"=>$dataProsedure[$x]->e_desc,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_prosedure',$paramtProsedureInsert);
					unset($paramtProsedureInsert);
				}
			/* } else {
				$paramtProsedureInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_prosedure',$paramtProsedureInsert);
			} */
			
			
			//select data anggaran DIPA
			//----------------------------
			
			$sqlProsesDIPA = "SELECT id_kuesioner, i_tahun_dipa, v_dipa, i_entry, d_entry
							  FROM tm_pengembangan_dipa
							  WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataDIPA = $db->fetchAll($sqlProsesDIPA);
			//insert  data software baru
			//-----------------------------
			//if (count($dataDIPA)) {
				for($x=0; $x<count($dataDIPA); $x++){
					$paramtDIPAInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_tahun_dipa"=>$dataDIPA[$x]->i_tahun_dipa,
									"v_dipa"=>$dataDIPA[$x]->v_dipa,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_pengembangan_dipa',$paramtDIPAInsert);
					unset($paramtProsedureInsert);
				}
			/* } else {
				$paramtDIPAInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_pengembangan_dipa',$paramtDIPAInsert);
			} */
			
			//select data anggaran Donor
			//----------------------------
			
			$sqlProsesDonor = "SELECT id_kuesioner, e_donor, i_tahun_donor, i_entry, d_entry
							   FROM tm_pengembangan_donor
							   WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataDonor = $db->fetchAll($sqlProsesDonor);
			//insert  data anggaran Donor baru
			//-----------------------------------
			//if(count($dataDonor)){
				for($x=0; $x<count($dataDonor); $x++){
					$paramtDonorInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"e_donor"=>$dataDonor[$x]->e_donor,
									"i_tahun_donor"=>$dataDonor[$x]->i_tahun_donor,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_pengembangan_donor',$paramtDonorInsert);
					unset($paramtDonorInsert);
				}
			/* } else {
				$paramtDonorInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_pengembangan_donor',$paramtDonorInsert);
			} */
			
			//select data thn anggaran
			//----------------------------
			
			$sqlProsesThnAngg = "SELECT id_kuesioner, i_tahun_ang, e_kegiatan, i_entry, d_entry
								 FROM tm_pengembangan_thnang
							     WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataThnangg = $db->fetchAll($sqlProsesThnAngg);
			//insert  data thn anggaran baru
			//-----------------------------------
			//if(count($dataThnangg)){
				for($x=0; $x<count($dataThnangg); $x++){
					$paramtThnangInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_tahun_ang"=>$dataThnangg[$x]->i_tahun_ang,
									"e_kegiatan"=>$dataThnangg[$x]->e_kegiatan,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_pengembangan_thnang',$paramtThnangInsert);
					unset($paramtThnangInsert);
				}
			/* } else {
				$paramtThnangInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_pengembangan_thnang',$paramtThnangInsert);
			} */
			
			//select data kegiatan
			//----------------------------
			
			$sqlProsesKegiatan = "SELECT id_kuesioner, c_kegiatan, n_kegiatan, e_kegiatan, v_anggaran,
								       e_alamat_prshn, e_mekanisme, i_lama, i_waktu_mulai, i_waktu_akhir,
								       c_jaminan, e_cara, e_pedoman, i_entry, d_entry
								  FROM tm_kegiatan
							     WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataKegiatan = $db->fetchAll($sqlProsesKegiatan);
			//insert  data thn anggaran baru
			//-----------------------------------
			//if(count($dataKegiatan)){
				for($x=0; $x<count($dataKegiatan); $x++){
					$paramtKegiatanInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"c_kegiatan"=>$dataKegiatan[$x]->c_kegiatan,
									"n_kegiatan"=>$dataKegiatan[$x]->n_kegiatan,
									"e_kegiatan"=>$dataKegiatan[$x]->e_kegiatan,
									"v_anggaran"=>$dataKegiatan[$x]->v_anggaran,
									"e_alamat_prshn"=>$dataKegiatan[$x]->e_alamat_prshn,
									"e_mekanisme"=>$dataKegiatan[$x]->e_mekanisme,
									"i_lama"=>$dataKegiatan[$x]->i_lama,
									"i_waktu_mulai"=>$dataKegiatan[$x]->i_waktu_mulai,
									"i_waktu_akhir"=>$dataKegiatan[$x]->i_waktu_akhir,
									"c_jaminan"=>$dataKegiatan[$x]->c_jaminan,
									"e_cara"=>$dataKegiatan[$x]->e_cara,
									"e_pedoman"=>$dataKegiatan[$x]->e_pedoman,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_kegiatan',$paramtKegiatanInsert);
					unset($paramtKegiatanInsert);
				}
			/* } else {
				$paramtKegiatanInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_kegiatan',$paramtKegiatanInsert);
			} */
			
			//select data deskinfo
			//----------------------------
			
			$sqlProsesDeskinfo = "SELECT id_kuesioner, e_layanan, e_layanan_publik, e_staft_pelaksana,
								       e_sk_tugas, e_mohon_info, e_status_perkara, i_entry, d_entry
								  FROM tm_deskinfo
							     WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataDeskinfo = $db->fetchAll($sqlProsesDeskinfo);
			//insert  data thn anggaran baru
			//-----------------------------------
			//if(count($dataDeskinfo)){
				for($x=0; $x<count($dataDeskinfo); $x++){
					$paramtDeskinfoInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"e_layanan"=>$dataDeskinfo[$x]->e_layanan,
									"e_layanan_publik"=>$dataDeskinfo[$x]->e_layanan_publik,
									"e_staft_pelaksana"=>$dataDeskinfo[$x]->e_staft_pelaksana,
									"e_sk_tugas"=>$dataDeskinfo[$x]->e_sk_tugas,
									"e_mohon_info"=>$dataDeskinfo[$x]->e_mohon_info,
									"e_status_perkara"=>$dataDeskinfo[$x]->e_status_perkara,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
					$db->insert('tm_deskinfo',$paramtDeskinfoInsert);
					unset($paramtDeskinfoInsert);
				}
			/* } else {
				$paramtDeskinfoInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_deskinfo',$paramtDeskinfoInsert);
			}  */
			
			//select data deskinfo bulanan
			//------------------------------------
			
			$sqlProsesDeskinfoBulanan = "SELECT id_kuesioner, v_jan, v_feb, v_mar, v_apr, v_mei, v_jun, v_jul, v_agt,
								       v_sep, v_okt, v_nop, v_des, i_entry, d_entry
								  FROM ma.tm_deskinfo_bln
							     WHERE id_kuesioner = '$idKuesionerLama'";	
							  
			$dataDeskinfoBulanan = $db->fetchRow($sqlProsesDeskinfoBulanan);
			//insert  data deskinfo bulanan
			//--------------------------------------
			//if(count($dataDeskinfo)){
			$paramtDeskinfoBulananInsert = array("id_kuesioner"=>$idKuesionerBaru,
							"v_jan"=>$dataDeskinfoBulanan->v_jan,
							"v_feb"=>$dataDeskinfoBulanan->v_feb,
							"v_mar"=>$dataDeskinfoBulanan->v_mar,
							"v_apr"=>$dataDeskinfoBulanan->v_apr,
							"v_mei"=>$dataDeskinfoBulanan->v_mei,
							"v_jun"=>$dataDeskinfoBulanan->v_jun,
							"v_jul"=>$dataDeskinfoBulanan->v_jul,
							"v_agt"=>$dataDeskinfoBulanan->v_agt,
							"v_sep"=>$dataDeskinfoBulanan->v_sep,
							"v_okt"=>$dataDeskinfoBulanan->v_okt,
							"v_nop"=>$dataDeskinfoBulanan->v_nop,
							"v_des"=>$dataDeskinfoBulanan->v_des,
							"i_entry"=>$i_entry,
							"d_entry"=>date("Y-m-d"));									
			$db->insert('tm_deskinfo_bln',$paramtDeskinfoBulananInsert);
			unset($paramtDeskinfoInsert);
			/* } else {
				$paramtDeskinfoInsert = array("id_kuesioner"=>$idKuesionerBaru,
									"i_entry"=>$i_entry,
									"d_entry"=>date("Y-m-d"));									
				$db->insert('tm_deskinfo',$paramtDeskinfoInsert);
			}  */
			
			
			$db->commit();
			return 'sukses';						  
			
	   } catch (Exception $e) {
			$db->rollBack();
			echo $e->getMessage().'<br>';
			return 'gagal <br>';
	   }
	}
	
	public function getIdOrganisasi($dataMasukan) {
	
		$idPengadilan = $dataMasukan['idPengadilan'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select i_organisasi, i_organisasi_parent, n_alamat, no_telepon, no_fax
							from tm_organisasi
							where i_organisasi = '$idPengadilan'";	
			$result = $db->fetchRow($sqlProses);				
			
			$jmlResult = count($result);
			
			//id kuesioner baru
			//===============
			$idKuesionerBaru = $db->fetchOne('select max(id)  from tm_kuesioner');
			if(!$idKuesionerBaru){
				$idKuesionerBaru = 1;
			} else {
				$idKuesionerBaru = $idKuesionerBaru+1;
			}
			
			$hasilAkhir = array("id" => $idKuesionerBaru,
								"id_pengadilan" =>(string)$result->i_organisasi,
								"id_pengadilan_banding" =>(string)$result->i_organisasi_parent,
								"n_alamat" =>(string)$result->n_alamat,
								"no_telepon" =>(string)$result->no_telepon,
								"no_fax" =>(string)$result->no_fax
								);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function databaruresponden($dataMasukan) {
	
		$i_organisasi = $dataMasukan['i_organisasi'];
		$d_tahun_kuesioner = $dataMasukan['d_tahun_kuesioner'];
		$i_entry = $dataMasukan['i_entry'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->beginTransaction();
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			//select data kuesioner terakhir
			//--------------------------------
			$sqlProsesKuesioner = "SELECT id, id_pengadilan, d_bulan_kuesioner, d_tahun_kuesioner, n_responden,
								       n_jabatan, n_pangkat, n_golongan, no_telepon_responden, no_hp_responden, i_entry, d_entry
								  FROM tm_kuesioner
								  WHERE id_pengadilan = '$i_organisasi' AND
										d_tahun_kuesioner = '$d_tahun_kuesioner'
								";	
			$dataKuesioner = $db->fetchRow($sqlProsesKuesioner);
			$idKuesionerLama = $dataKuesioner->id;
			
			//id kuesioner baru
			//===============
			$idKuesionerBaru = $db->fetchOne('select max(id)  from tm_kuesioner');
			if(!$idKuesionerBaru){
				$idKuesionerBaru = 1;
			} else {
				$idKuesionerBaru = $idKuesionerBaru+1;
			} 
			
			//insert kuesioner baru tahun sekarang
			//================================
			if($dataKuesioner->n_responden){
				$nResponden = $dataKuesioner->n_responden;
			} else {
				$nResponden = $i_entry;
			}
			
			$paramInsertKuesioner = array("id" => $idKuesionerBaru,
									"id_pengadilan" => $i_organisasi,
									"d_bulan_kuesioner" => date('m'),
									"d_tahun_kuesioner" => date('Y'),
									"n_responden" => $nResponden,
									"n_jabatan" => $dataKuesioner->n_jabatan,
									"n_pangkat" => $dataKuesioner->n_pangkat,
									"n_golongan" => $dataKuesioner->n_golongan,
									"no_telepon_responden" => $dataKuesioner->no_telepon_responden,
									"no_hp_responden" => $dataKuesioner->no_hp_responden,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);
			$db->insert('tm_kuesioner', $paramInsertKuesioner);
			
			$db->commit();
			return 'sukses';						  
			
	   } catch (Exception $e) {
			$db->rollBack();
			echo $e->getMessage().'<br>';
			return 'gagal <br>';
	   }
	}
	////////////////////////////////////////
}
?>