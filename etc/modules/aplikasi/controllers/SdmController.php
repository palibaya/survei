<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "share/format_date.php";
require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";
require_once "service/aplikasi/Aplikasi_Responden_Service.php";
require_once "service/aplikasi/Aplikasi_Sdm_Service.php";



class Aplikasi_SdmController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
		
		$this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
		$this->responden_serv	  = Aplikasi_Responden_Service::getInstance();
		$this->sdm_serv	  = Aplikasi_Sdm_Service::getInstance();
	    $ssogroup = new Zend_Session_Namespace('ssogroup');
	   // echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->i_organisasi;
	    $this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		$this->c_kategori_organisasi  = $ssogroup->c_kategori_organisasi;	
    }
	
    public function indexAction() {
	   
    }
	
	public function sdmjsAction() 
    {
		 header('content-type : text/javascript');
		 $this->render('sdmjs');
    }
	
	//MA
	//----------------------
	public function sdmjuduliconAction()
	{
		//untuk mengeluarkan judul dan icon Kondisi Sistem TI 
	}
	
	public function sdmolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$idKuesioner = $_REQUEST['idKuesioner'];
		$this->view->idKuesioner = $idKuesioner;
		$this->view->nipAgendaDari = $this->nip;
		$this->view->namaAgendaDari = $this->nama;
		
		$data= array("idKuesioner" => $idKuesioner );
		$this->view->dataSDM = $this->sdm_serv->detailSdmTerakhir($data);
		$this->view->c_kategori_organisasi = $this->c_kategori_organisasi;
	}
	
	public function sdmAction()
	{
		$format_date = new format_date();
		$idKuesioner 	= $_POST['idKuesioner'];
		$iJumlahhakim 	= $_POST['i_jumlahhakim'];
		if(!$iJumlahhakim){ $iJumlahhakim = 0;}
		$iJumlahpp		= $_POST['i_jumlahpp'];
		if(!$iJumlahpp){ $iJumlahpp = 0;}
		$iJumlahpns 	= $_POST['i_jumlahpns'];
		if(!$iJumlahpns){ $iJumlahpns = 0;}
		$iJumlahhonorer = $_POST['i_jumlahhonorer'];
		if(!$iJumlahhonorer){ $iJumlahhonorer = 0;}
		$iJumlahpeneliti= $_POST['i_jumlahpeneliti'];
		if(!$iJumlahpeneliti){ $iJumlahpeneliti = 0;}
		$iJumlahwidyaiswara		= $_POST['i_jumlahwidyaiswara'];
		if(!$iJumlahwidyaiswara){ $iJumlahwidyaiswara = 0;}
		$iJumlahpranatakomputer = $_POST['i_jumlahpranatakomputer'];
		if(!$iJumlahpranatakomputer){ $iJumlahpranatakomputer = 0;}
		$iJumlahauditor = $_POST['i_jumlahauditor'];
		if(!$iJumlahauditor){ $iJumlahauditor = 0;}
		
		$iStafitSertifikasiSoftware = $_POST['i_stafit_sertifikasi_software'];
		if(!$iStafitSertifikasiSoftware){ $iStafitSertifikasiSoftware = 0;}
		$iStafitSertifikasiHardware = $_POST['i_stafit_sertifikasi_hardware'];
		if(!$iStafitSertifikasiHardware){ $iStafitSertifikasiHardware = 0;}
		$iStafitSertifikasiNetwork 	= $_POST['i_stafit_sertifikasi_network'];
		if(!$iStafitSertifikasiNetwork){ $iStafitSertifikasiNetwork = 0;}
		$iUserSiap 	= $_POST['i_user_siap'];
		if(!$iUserSiap){ $iUserSiap = 0;}
		$iUserSdm 	= $_POST['i_user_sdm'];
		if(!$iUserSdm){ $iUserSdm = 0;}
		$iUserSai 	= $_POST['i_user_sai'];
		if(!$iUserSai){ $iUserSai = 0;}
		$iUserRkakl = $_POST['i_user_rkakl'];
		if(!$iUserRkakl){ $iUserRkakl = 0;}
		$iUserSabmn	= $_POST['i_user_sabmn'];
		if(!$iUserSabmn){ $iUserSabmn = 0;}
		$iUserWebsite = $_POST['i_user_website'];
		if(!$iUserWebsite){ $iUserWebsite = 0;}
		$iHakimPc 	= $_POST['i_hakim_pc'];
		if(!$iHakimPc){ $iHakimPc = 0;}
		$iPpPc 		= $_POST['i_pp_pc'];
		if(!$iPpPc){ $iPpPc = 0;}
		$iPnsPc 	= $_POST['i_pns_pc'];
		if(!$iPnsPc){ $iPnsPc = 0;}
		$iHonorerPc 	= $_POST['i_honorer_pc'];
		if(!$iHonorerPc){ $iHonorerPc = 0;}
		$i_entry	= $this->userid;
		
		$dataMasukan = array("idKuesioner" => $idKuesioner,
							"iJumlahhakim" => $iJumlahhakim,
							"iJumlahpp" => $iJumlahpp,
							"iJumlahpns" => $iJumlahpns,
							"iJumlahhonorer" => $iJumlahhonorer,
							"iJumlahpeneliti" => $iJumlahpeneliti,
							"iJumlahwidyaiswara" => $iJumlahwidyaiswara,
							"iJumlahpranatakomputer" => $iJumlahpranatakomputer,
							"iJumlahauditor" => $iJumlahauditor,
							"iStafitSertifikasiSoftware" => $iStafitSertifikasiSoftware,
							"iStafitSertifikasiHardware" => $iStafitSertifikasiHardware,
							"iStafitSertifikasiNetwork" => $iStafitSertifikasiNetwork,
							"iUserSiap" => $iUserSiap,
							"iUserSdm" => $iUserSdm,
							"iUserSai" => $iUserSai,
							"iUserRkakl" => $iUserRkakl,
							"iUserSabmn" => $iUserSabmn,
							"iUserWebsite" => $iUserWebsite,
							"iHakimPc" => $iHakimPc,
							"iPpPc" => $iPpPc,
							"iPnsPc" => $iPnsPc,
							"iHonorerPc" => $iHonorerPc,
							"i_entry" => $i_entry);
		
		$hasil = $this->sdm_serv->insertSdm($dataMasukan);
		
		$this->view->proses = "2";	
		$this->view->keterangan = "SDM";
		$this->view->hasil = $hasil;
		//echo "hasil = $hasil";
		$this->sdmolahdataAction();
		$this->render('sdmolahdata');
	}
	
/* 	public function respondenlistAction()
	{
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= 'id_pengadilan';
		$katakunciCari 	= $_POST['cari'];
		$sortBy			= 'id_pengadilan';
		$sort			= 'desc';
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$this->view->currentPage = $pageNumber;
		$this->view->numToDisplay = $itemPerPage;		
		//$this->view->surveyList = $this->survey_serv->carisurveyList($dataMasukan);
		
	}
	
	public function respondenolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		if (!$_REQUEST['jenisForm']) {$this->view->jenisForm = 'insert';}
		$iAgenda = $_REQUEST['iAgenda'];
		$this->view->nipAgendaDari = $this->nip;
		$this->view->namaAgendaDari = $this->nama;
		$idPengadilan = $this->id_pengadilan;
		$this->view->idPengadilan = $idPengadilan;
		
		$dataMasukan1 = array("idPengadilan" =>$idPengadilan);
		$this->view->detailRespondenTerakhir = $this->responden_serv->detailRespondenTerakhir($dataMasukan1);
		$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList();
		
		$dataMasukan = array("idPengadilanBanding" => $this->view->dataPengadilanBanding[0]['id_pengadilan']);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	
	public function pengadilantkilistAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	public function setalamatpengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->alamatPengadilan = $this->refPengadilan_serv->getalamatpengadilan($dataMasukan);
	}
	
	public function setnotelppengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->noTelpPengadilan = $this->refPengadilan_serv->getnotelppengadilan($dataMasukan);
	}
	
	public function setnofaxpengadilanAction()
	{
		$idPengadilanBanding = $_REQUEST['idPengadilanBanding'];
		
		$dataMasukan = array("idPengadilanBanding" => $idPengadilanBanding);
		$this->view->noFaxPengadilan = $this->refPengadilan_serv->getnofaxpengadilan($dataMasukan);
	} */
	
	
	
	public function jaringanolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$iAgenda = $_REQUEST['iAgenda'];
		$this->view->nipAgendaDari = $this->nip;
		$this->view->namaAgendaDari = $this->nama;
		$this->view->dataPengadilanBanding = $this->refPengadilan_serv->pengadilanbandingList();
		
		$dataMasukan = array("idPengadilanBanding" => $this->view->dataPengadilanBanding[0]['id_pengadilan']);
		$this->view->dataPengadilan = $this->refPengadilan_serv->pengadilanList($dataMasukan);
	}
	
	//end of MA
	
	
}
?>