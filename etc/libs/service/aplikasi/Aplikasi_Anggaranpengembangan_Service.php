<?php
class aplikasi_anggaranpengembangan_Service {
    private static $instance;
  
private function __construct() {
    }

    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
    }

public function getTmPengembanganDipa($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,i_tahun_dipa,v_dipa
										from tm_pengembangan_dipa where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"i_tahun_dipa"=>(string)$result[$j]->i_tahun_dipa,
									"v_dipa"=>(string)$result[$j]->v_dipa);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	
	public function hapusTmPengembanganDipa(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."' ";
		$db->delete('tm_pengembangan_dipa', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}
	
public function tambahTmPengembanganDipa(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"i_tahun_dipa"=>$data['i_tahun_dipa'],
							"v_dipa"=>$data['v_dipa'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_pengembangan_dipa',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
}

public function getTmPengembanganDonor($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,e_donor,i_tahun_donor
										from tm_pengembangan_donor where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"e_donor"=>(string)$result[$j]->e_donor,
									"i_tahun_donor"=>(string)$result[$j]->i_tahun_donor);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}
	
public function hapusTmPengembanganDonor(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."' ";
		$db->delete('tm_pengembangan_donor', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
public function tambahTmPengembanganDonor(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"e_donor"=>$data['e_donor'],
							"i_tahun_donor"=>$data['i_tahun_donor'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_pengembangan_donor',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
}	

public function getTmPengembanganThnang($cari) 
	{
			$registry = Zend_Registry::getInstance();
			$db = $registry->get('db');
			try 
			{
				$db->setFetchMode(Zend_Db::FETCH_OBJ);			
					$result = $db->fetchAll("select id_kuesioner,i_tahun_ang,e_kegiatan
										from tm_pengembangan_thnang where 1=1 $cari ");
							
					$jmlResult = count($result);
					for ($j = 0; $j < $jmlResult; $j++) 
					{$data[$j] = array("id_kuesioner"=>(string)$result[$j]->id_kuesioner,
									"i_tahun_ang"=>(string)$result[$j]->i_tahun_ang,
									"e_kegiatan"=>(string)$result[$j]->e_kegiatan);}
					return $data;
			} catch (Exception $e) 
			{
		         	echo $e->getMessage().'<br>';
			     	return 'Data tidak ada <br>';
			}
	}

public function hapusTmPengembanganThnang(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try 
	   {
	     	$db->beginTransaction();
		$where[] = "id_kuesioner= '".trim($data['id_kuesioner'])."' ";
		$db->delete('tm_pengembangan_thnang', $where);
		$db->commit();
	     	return 'sukses';
	   } 
	   catch (Exception $e) 
	   {
         	$db->rollBack();
         	echo $e->getMessage().'<br>';
	     	return 'gagal';
	   }
	}	
public function tambahTmPengembanganThnang(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
	     $db->beginTransaction();
	     $tambah_data = array("id_kuesioner"=>$data['id_kuesioner'],
							"i_tahun_ang"=>$data['i_tahun_ang'],
							"e_kegiatan"=>$data['e_kegiatan'],
							"i_entry"=>$data['i_entry'],
							"d_entry"=>date("Y-m-d"));									
	     $db->insert('tm_pengembangan_thnang',$tambah_data);
		$db->commit();
	     return 'sukses';
	   } catch (Exception $e) {
         $db->rollBack();
         echo $e->getMessage().'<br>';
	     return 'gagal';
	   }
}
}
?>
