<?php

require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_anggaranpengembangan_Service.php";
class Aplikasi_AnggaranpengembanganController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->pengembangan_serv = aplikasi_anggaranpengembangan_Service::getInstance();

    }

public function indexAction()
{
}
public function anggaranpengembanganAction()
{
        $cari = " and id_kuesioner =1 ";
        $this->view->datadipa= $this->pengembangan_serv->getTmPengembanganDipa($cari);
        $this->view->datadonor= $this->pengembangan_serv->getTmPengembanganDonor($cari);
        $this->view->datatahunangaran= $this->pengembangan_serv->getTmPengembanganThnang($cari);

}
public function anggaranpengembanganjsAction()
{
    header('content-type : text/javascript');
    $this->render('anggaranpengembanganjs');
}
public function maintaindatapengembanganAction()
{

            $counttablepengembangan1=$_POST['counttablepengembangan1'];
            $con=1;
            for($xi=0;$xi<$counttablepengembangan1;$xi++)
            {
                 $MaintainData = array(
                                    "id_kuesioner"=>'1',
                                    "i_tahun_dipa"=>$_POST['i_tahun_dipa_'.$con],
                                    "v_dipa"=>$_POST['v_dipa_'.$con],
                                    "i_entry"=>$data['i_entry'] );


                                    if ($_POST['perintah']=='Koreksi')
                                    {
                                        if ($xi==0){
                                        $hasil = $this->pengembangan_serv->hapusTmPengembanganDipa($MaintainData);}
                                        if ($hasil=='sukses'){ $hasil = $this->pengembangan_serv->tambahTmPengembanganDipa($MaintainData);}
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";}
                                    else
                                    {   $hasil = $this->pengembangan_serv->tambahTmPengembanganDipa($MaintainData);
                                        $this->view->par="simpan";
                                        $par="Simpan ";}

                $con++;
            }


        if ($hasil =='sukses')
        {
            $counttablepengembangan2=$_POST['counttablepengembangan2'];
            $con=1;
            for($xi=0;$xi<$counttablepengembangan2;$xi++)
            {
                 $MaintainData = array(
                                    "id_kuesioner"=>'1',
                                    "e_donor"=>$_POST['e_donor_'.$con],
                                    "i_tahun_donor"=>$_POST['i_tahun_donor_'.$con],
                                    "i_entry"=>$data['i_entry'] );


                                    if ($_POST['perintah']=='Koreksi')
                                    {
                                        if ($xi==0){
                                        $hasil = $this->pengembangan_serv->hapusTmPengembanganDonor($MaintainData);}
                                        if ($hasil=='sukses'){ $hasil = $this->pengembangan_serv->tambahTmPengembanganDonor($MaintainData);}
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";}
                                    else
                                    {   $hasil = $this->pengembangan_serv->tambahTmPengembanganDonor($MaintainData);
                                        $this->view->par="simpan";
                                        $par="Simpan ";}

                $con++;
            }

        }
        if ($hasil =='sukses')
        {
            $counttablepengembangan3=$_POST['counttablepengembangan3'];
            $con=1;
            for($xi=0;$xi<$counttablepengembangan3;$xi++)
            {
                 $MaintainData = array(
                                    "id_kuesioner"=>'1',
                                    "i_tahun_ang"=>$_POST['i_tahun_ang_'.$con],
                                    "e_kegiatan"=>$_POST['e_kegiatan_'.$con],
                                    "i_entry"=>$data['i_entry'] );

                                    if ($_POST['perintah']=='Koreksi')
                                    {
                                        if ($xi==0){
                                        $hasil = $this->pengembangan_serv->hapusTmPengembanganThnang($MaintainData);}
                                        if ($hasil=='sukses'){ $hasil = $this->pengembangan_serv->tambahTmPengembanganThnang($MaintainData);}
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";}
                                    else
                                    {   $hasil = $this->pengembangan_serv->tambahTmPengembanganThnang($MaintainData);
                                        $this->view->par="simpan";
                                        $par="Simpan ";}

                $con++;
            }

        }
            $this->view->par="simpan";
            $par="Simpan ";

        $pesan=$par." data ".$hasil;
        $this->view->pesan = $pesan;
        $this->view->pesancek = $hasil;
        $this->anggaranpengembanganAction();
        $this->_helper->viewRenderer('anggaranpengembangan');

    }



}
?>
