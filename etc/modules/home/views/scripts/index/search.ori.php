<?
require_once 'Zend/View.php';
require_once 'share/format_date.php'; 
require_once 'share/format_page.php';
require_once "share/share_message.php";
$referensi = Aplikasi_Referensi_Service::getInstance();

$ctrlFrm = new Zend_View();
$pesan = new share_message();
$halaman = new format_page();
		
$currentPage = $this->currentPage;
$numToDisplay = $this->numToDisplay;

?>
<script type="text/javascript" src="<? echo $this->basePath; ?>/aplikasi/suratmasukagenda/agendamasukjs.phtml"></script>
<script type="text/javascript" src="<? echo $this->basePath;?>/srtmemo/suratmasuk/suratmasukjs.phtml"></script>
<script type="text/javascript" src="<? echo $this->basePath;?>/srtmemo/suratkeluar/suratkeluarjs.phtml"></script>

<script type="text/javascript">

function cariAgendamasuk()
{
	var kategoriCari =  document.getElementById('kategoriCari').value;
	var katakunciCari = document.getElementById('katakunciCari').value;
	var param = {kategoriCari : kategoriCari, katakunciCari : katakunciCari};

	var url = '<?php echo $this->basePath; ?>/aplikasi/suratmasukagenda/agendamasuklist';
	jQuery.get(url, param, function(data) {
		jQuery("div#tableview").html(data);
		jQuery("#frmtambahagendamasuk").ajaxForm(tambahAgendamasuk_opt);
		jQuery("#frmubahagendamasuk").ajaxForm(ubahAgendamasuk_opt);
		var scriptAction = '<?php echo $this->basePath; ?>/aplikasi/global/globaljs';
			jQuery.getScript(scriptAction,function (data) {
			});
	});
}
function cariData(proses)
{
	if(proses == 'suratMasuk'){
	  cariAgendamasuk();   
	}
	else {
	       if(proses == 'memoKeluar'){
	          open_url_to_div('<?php echo $this->basePath; ?>/srtmemo/suratkeluar/suratkeluar','<?php echo $this->basePath; ?>/srtmemo/suratkeluar/suratkeluarjs');
	       }
	       else{
	          open_url_to_div('<?php echo $this->basePath; ?>/srtmemo/suratmasuk/suratmasuk','<?php echo $this->basePath; ?>/srtmemo/suratmasuk/suratmasukjs');
	       }
	}
		
}
</script>
<div class="bar-search">
<form method="post" id="frmCariData" name="frmCariData">
				<?
				$prosesAttrib = array("suratMasuk" => "Surat Masuk",
									"memoKeluar" => "Memo Keluar",
									"memoMasuk" => "Memo Masuk");
										
				echo $ctrlFrm->formSelect('proses', null, null, $prosesAttrib);
				echo $ctrlFrm->formHidden('kategoriCari', 'semua', null);
				
				?>
				<input type="text" name="katakunciCari" size="10" id="katakunciCari">
				 <input type="Button" name="Submit" id="Submit" value="cari" onclick="javascript:cariData(document.frmCariData.proses.options[document.frmCariData.proses.selectedIndex].value);">
				
</form>
</div>
