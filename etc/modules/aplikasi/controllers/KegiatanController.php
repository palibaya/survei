<?php

require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_kegiatan_Service.php";

class Aplikasi_KegiatanController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->kegiatan_serv = aplikasi_kegiatan_Service::getInstance();
        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;

    }

public function indexAction()
{
}
public function kegiatanAction()
{
    $this->kegiatan($_REQUEST['idKuesioner'],$_REQUEST['parkegiatan']);

}

public function kegiatan($id_kuesioner,$parkegiatan)
{

    $cari = " and id_kuesioner ='$id_kuesioner' ";
    if (!$parkegiatan){$parkegiatan='1';}
    $cari = " and id_kuesioner ='$id_kuesioner'  and c_kegiatan='$parkegiatan' ";
    //echo $cari;
    $datakegiatan= $this->kegiatan_serv->getTmKegiatan($cari);
    $jmldata = count($datakegiatan);
    if (count($datakegiatan)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
                $this->view->id_kuesioner=$datakegiatan[$j]['id_kuesioner'];
                $this->view->c_kegiatan=$datakegiatan[$j]['c_kegiatan'];
                $this->view->n_kegiatan=$datakegiatan[$j]['n_kegiatan'];
                $this->view->e_kegiatan=$datakegiatan[$j]['e_kegiatan'];
                $this->view->v_anggaran=$datakegiatan[$j]['v_anggaran'];
                $this->view->e_alamat_prshn=$datakegiatan[$j]['e_alamat_prshn'];
                $this->view->e_mekanisme=$datakegiatan[$j]['e_mekanisme'];
                $this->view->i_lama=$datakegiatan[$j]['i_lama'];
                $this->view->i_waktu_mulai=$datakegiatan[$j]['i_waktu_mulai'];
                $this->view->i_waktu_akhir=$datakegiatan[$j]['i_waktu_akhir'];
                $this->view->c_jaminan=$datakegiatan[$j]['c_jaminan'];
                $this->view->e_cara=$datakegiatan[$j]['e_cara'];
                $this->view->e_pedoman=$datakegiatan[$j]['e_pedoman'];
                $parkegiatan=$datakegiatan[$j]['c_kegiatan'];
        }
        $this->view->par='Koreksi';
    }
else
    {
        $this->view->par='Simpan';
        $this->view->id_kuesioner= $_REQUEST['idKuesioner'];
    }

    $carix = " and id_kuesioner ='$id_kuesioner ' ";
    $jmlkegiatan= $this->kegiatan_serv->getTmKegiatan($carix);
    $this->view->jmlkegiatan=count($jmlkegiatan);
    $parkegiatan=$parkegiatan*1+1;
    $this->view->datakomponen= $this->kegiatan_serv->getTmKegiatanKomponen($cari);
    if (count($this->view->datakomponen)==0){
        $this->view->datarefkomponen= $this->kegiatan_serv->getTrKomponen();
    }
    $this->view->parkegiatan=$parkegiatan;
    $this->view->jmldata=$jmldata;
    $this->view->id_kuesioner=$id_kuesioner;

}

public function kegiatanjsAction()
{
    header('content-type : text/javascript');
    $this->render('kegiatanjs');
}

public function maintaindatakegiatanAction()
{
            $MaintainData1 = array("id_kuesioner"=>$_POST['id_kuesioner'],
                            "c_kegiatan"=>$_POST['c_kegiatan'],
                            "n_kegiatan"=>$_POST['n_kegiatan'],
                            "e_kegiatan"=>$_POST['e_kegiatan'],
                            "v_anggaran"=>$_POST['v_anggaran']*1,
                            "e_alamat_prshn"=>$_POST['e_alamat_prshn'],
                            "e_mekanisme"=>$_POST['e_mekanisme'],
                            "i_lama"=>$_POST['i_lama'],
                            "i_waktu_mulai"=>$_POST['i_waktu_mulai'],
                            "i_waktu_akhir"=>$_POST['i_waktu_akhir'],
                            "c_jaminan"=>$_POST['c_jaminan'],
                            "e_cara"=>$_POST['e_cara'],
                            "e_pedoman"=>$_POST['e_pedoman'],
                            "i_entry"=>$_POST['i_entry']);
                if ($_POST['perintah']=='Koreksi')
                    {
                        $hasil = $this->kegiatan_serv->ubahTmKegiatan($MaintainData1);
                    }
                else{
                        $hasil = $this->kegiatan_serv->tambahTmKegiatan($MaintainData1);
                    }

        if ($hasil =='sukses')
        {
            $con=1;
            for($xi=0;$xi<12;$xi++)
            {
                 $MaintainData = array("id_kuesioner"=>$_POST['id_kuesioner'],
                                    "c_kegiatan"=>$_POST['c_kegiatan'],
                                    "e_keterangan"=>$_POST['e_keterangan_'.$con],
                                    "e_spesipikasi"=>$_POST['e_spesipikasi_'.$con],
                                    "i_jml_komponen"=>$_POST['i_jml_komponen_'.$con]*1,
                                    "c_komponen"=>$_POST['n_komponen_'.$con],
                                    "i_entry"=>$data['i_entry'] );
                if ($_POST['perintah']=='Koreksi')
                    {

                        $id_kuesioner=$_POST['id_kuesioner'];
                        $parkegiatan=$_POST['c_kegiatan'];
                        $cari = " and id_kuesioner ='$id_kuesioner'  and c_kegiatan='$parkegiatan' ";

                        $datakomponen= $this->kegiatan_serv->getTmKegiatanKomponen($cari);
                        if (count($datakomponen)==0){
                            $hasil = $this->kegiatan_serv->tambahTmKegiatanKomponen($MaintainData);
                        }
                        else{
                        $hasil = $this->kegiatan_serv->ubahTmKegiatanKomponen($MaintainData);
                        }
                        $this->view->par="koreksi";
                        $par="Koreksi ";
                    }
                else{
                        $hasil = $this->kegiatan_serv->tambahTmKegiatanKomponen($MaintainData);
                        $this->view->par="simpan";
                        $par="Simpan ";
                    }
                $con++;
            }
        }
    $this->view->parkegiatan=$_POST['c_kegiatan'];
    $pesan=$par." data ".$hasil;
    $this->view->pesan = $pesan;
    $this->view->pesancek = $hasil;
    //$this->view->datarefkomponen= $this->kegiatan_serv->getTrKomponen();
    $this->kegiatan($_POST['id_kuesioner'],$_POST['c_kegiatan']);
    $this->_helper->viewRenderer('kegiatan');

}

}
?>
