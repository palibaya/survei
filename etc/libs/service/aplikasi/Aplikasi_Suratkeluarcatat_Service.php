<?php
require_once "share/globalReferensi.php";
require_once "share/gen_nosurat.php";
require_once 'Zend/Date.php';
require_once 'Zend/Locale.php';
require_once 'Zend/Date/Cities.php';

class Aplikasi_Suratkeluarcatat_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function cariCatatSuratKeluarList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari1 		= $dataMasukan['dTglCari1'];
		$dTglCari2	 	= $dataMasukan['dTglCari2'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null)";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tglajuan') {
					$whereOpt = "d_tglajuan between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tglajuan') {
					$whereOpt = "d_tglajuan like '$dTglCari%' ";
				} else {
					$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
				}
			}
			
			if($kategoriCari && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "select id,
								i_srtajuan,
								id_memo,
								n_dari,
								nip_dari
								kd_jabatan_dari,
								kd_org_dari,
								d_tglajuan,
								n_perihal,
								n_srtlampiran,
								n_penandatangan,
								nip_penandatangan,
								kd_jabatan_penandatangan,
								kd_org_penandatangan,
								c_srtkualifikasi,
								c_srtsifat,
								n_pengirim,
								d_tglkirim,
								n_penerima,
								d_tglterima,
								c_srt_saranakirim,
								n_srt_lokasi
						from tm_surat_ajuan ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			//echo $sqlProses.$where.$order;
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				/* $namaJabatanLengkap = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
				if(!$result[$j]->nip)
					$n_tujuansrt = $n_kepada;
				else
					$n_tujuansrt = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
								 */
								 
				$nKepada = $db->fetchRow("select * from tm_surat_ajuankepada where id_suratajuan = '".$result[$j]->id."'");
				
				$hasilAkhir[$j] = array("id_srtajuan"     	=>(string)$result[$j]->id,  
										"i_srtajuan"     	=>(string)$result[$j]->i_srtajuan,    
										"i_srtverbal"     	=>(string)$result[$j]->i_srtverbal,    
										"n_dari"         	=>(string)$result[$j]->n_dari,        
										"nip_dari"     	=>(string)$result[$j]->nip_dari,    
										"kd_jabatan_dari"      	=>(string)$result[$j]->kd_jabatan_dari,     
										"kd_org_dari"        	=>(string)$result[$j]->kd_org_dari,       
										"d_tglajuan" 	=>(string)$result[$j]->d_tglajuan,
										"n_perihal"   	=>(string)$result[$j]->n_perihal,  
										"n_srtlampiran"   	=>(string)$result[$j]->n_srtlampiran,  
										"n_penandatangan"    	=>(string)$result[$j]->n_penandatangan,   
										"nip_penandatangan"	=>(string)$result[$j]->nip_penandatangan,
										"kd_jabatan_penandatangan"      	=>(string)$result[$j]->kd_jabatan_penandatangan,     
										"kd_org_penandatangan"   	=>(string)$result[$j]->kd_org_penandatangan,  
										"c_srtkualifikasi"   	=>(string)$result[$j]->c_srtkualifikasi,  
										"c_srtsifat"   	=>(string)$result[$j]->c_srtsifat,  
										"n_pengirim"	=>(string)$result[$j]->n_pengirim,
										"d_tglkirim"	=>(string)$result[$j]->d_tglkirim,
										"n_penerima"     	=>(string)$result[$j]->n_penerima,    
										"d_tglterima"	=>(string)$result[$j]->d_tglterima,
										"c_srt_saranakirim"  	=>(string)$result[$j]->c_srt_saranakirim,
										"n_srt_lokasi"  	=>(string)$result[$j]->n_srt_lokasi,
										"n_kepada" => $nKepada->n_kepada
										);
			}	
			//var_dump($hasilAkhir);
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function cariCatatSuratKeluarResiList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari1 		= $dataMasukan['dTglCari1'];
		$dTglCari2	 	= $dataMasukan['dTglCari2'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null) ";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tglajuan') {
					$whereOpt = "d_tglajuan between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tglajuan') {
					$whereOpt = "d_tglajuan like '$dTglCari%' ";
				} else {
					$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
				}
			}
			
			if($kategoriCari && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "select id,
								i_srtajuan,
								id_memo,
								n_dari,
								nip_dari
								kd_jabatan_dari,
								kd_org_dari,
								d_tglajuan,
								n_perihal,
								n_srtlampiran,
								n_penandatangan,
								nip_penandatangan,
								kd_jabatan_penandatangan,
								kd_org_penandatangan,
								c_srtkualifikasi,
								c_srtsifat,
								n_pengirim,
								d_tglkirim,
								n_penerima,
								d_tglterima,
								c_srt_saranakirim,
								v_biayakirim
								from tm_surat_ajuan ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			//echo $sqlProses.$where.$order;
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				/* $namaJabatanLengkap = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
				if(!$result[$j]->nip)
					$n_tujuansrt = $n_kepada;
				else
					$n_tujuansrt = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
								 */
								 
				$nKepada = $db->fetchRow("select * from tm_surat_ajuankepada where id_suratajuan = '".$result[$j]->id."'");
				
				$hasilAkhir[$j] = array("id_srtajuan"     	=>(string)$result[$j]->id,  
										"i_srtajuan"     	=>(string)$result[$j]->i_srtajuan,    
										"i_srtverbal"     	=>(string)$result[$j]->i_srtverbal,    
										"n_dari"         	=>(string)$result[$j]->n_dari,        
										"nip_dari"     	=>(string)$result[$j]->nip_dari,    
										"kd_jabatan_dari"      	=>(string)$result[$j]->kd_jabatan_dari,     
										"kd_org_dari"        	=>(string)$result[$j]->kd_org_dari,       
										"d_tglajuan" 	=>(string)$result[$j]->d_tglajuan,
										"n_perihal"   	=>(string)$result[$j]->n_perihal,  
										"n_srtlampiran"   	=>(string)$result[$j]->n_srtlampiran,  
										"n_penandatangan"    	=>(string)$result[$j]->n_penandatangan,   
										"nip_penandatangan"	=>(string)$result[$j]->nip_penandatangan,
										"kd_jabatan_penandatangan"      	=>(string)$result[$j]->kd_jabatan_penandatangan,     
										"kd_org_penandatangan"   	=>(string)$result[$j]->kd_org_penandatangan,  
										"c_srtkualifikasi"   	=>(string)$result[$j]->c_srtkualifikasi,  
										"c_srtsifat"   	=>(string)$result[$j]->c_srtsifat,  
										"n_pengirim"	=>(string)$result[$j]->n_pengirim,
										"d_tglkirim"	=>(string)$result[$j]->d_tglkirim,
										"n_penerima"     	=>(string)$result[$j]->n_penerima,    
										"d_tglterima"	=>(string)$result[$j]->d_tglterima,
										"c_srt_saranakirim"  	=>(string)$result[$j]->c_srt_saranakirim,
										"v_biayakirim"  	=>(string)$result[$j]->v_biayakirim,
										"n_kepada" => $nKepada->n_kepada
										);
			}	
			//var_dump($hasilAkhir);
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getIdSrtmasuk($data) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select id
							from tm_surat_masuk ";	
			$where = "where i_agendasrt = '".$data['iSrtmasuk']."'";
			
			$sqlData = $sqlProses.$where;
			
			$result = $db->fetchOne($sqlData);	
			return trim($result);						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function agendaList() {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select distinct(i_agendasrt) i_agendasrt
							from tm_surat_masuk ";	

			$sqlData = $sqlProses;
			$result = $db->fetchCol($sqlData);	
			//echo $sqlData;
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = $result[$j];
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function buatNoSuratKeluar(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			$idSuratajuan = $dataMasukan['id_suratajuan'];
			$kodeSurat = $dataMasukan['kodeSurat'];
			$kodeSifatSurat = $dataMasukan['kodeSifatSurat'];
			$levelOrg = $dataMasukan['levelOrg'];
			$iEntry = $dataMasukan['i_entry'];
			
			$genNosurat = new gen_nosurat();
			
			//tentukan jenis surat
			if ($levelOrg == 'SEKRETARIS KABINET'){
				if (($kodeSifatSurat == 'B') || ($kodeSifatSurat == 'J') || ($kodeSifatSurat == 'SE') || 
				    ($kodeSifatSurat == 'KET') || ($kodeSifatSurat == 'SPRIN') || ($kodeSifatSurat == 'CT') || 
				    ($kodeSifatSurat == 'UND')){
					$jenisSurat = 'BIASA';
				} else {
					$jenisSurat = $kodeSifatSurat;
				}
				$ketLevelOrg = strtoupper($levelOrg);
			} else if ($levelOrg == 'WAKIL SEKRETARIS KABINET'){
				if (($kodeSifatSurat == 'B') || ($kodeSifatSurat == 'J') || ($kodeSifatSurat == 'SE') || 
				    ($kodeSifatSurat == 'KET') || ($kodeSifatSurat == 'SPRIN') || ($kodeSifatSurat == 'CT') || 
				    ($kodeSifatSurat == 'UND')){
					$jenisSurat = 'BIASA';
				} else {
					$jenisSurat = $kodeSifatSurat;
				}
				$ketLevelOrg = strtoupper($levelOrg);
			} else if (($levelOrg == 'DEPUTI') || (substr($levelOrg, 0,11) == 'STAF KHUSUS') || 
			           (substr($levelOrg, 0,9) == 'STAF AHLI')){
				if (($kodeSifatSurat == 'B') || ($kodeSifatSurat == 'J') || ($kodeSifatSurat == 'SE') || 
				    ($kodeSifatSurat == 'KET') || ($kodeSifatSurat == 'SPRIN') || ($kodeSifatSurat == 'CT') || 
				    ($kodeSifatSurat == 'UND')){
					$jenisSurat = 'BIASA';
				} else {
					$jenisSurat = $kodeSifatSurat;
				}
				$levelOrg = 'DEPUTI';
				$ketLevelOrg = 'ESELON I';
			} else if ($levelOrg == 'BIRO'){
				if (($kodeSifatSurat == 'B') || ($kodeSifatSurat == 'J') || ($kodeSifatSurat == 'SE') || 
				    ($kodeSifatSurat == 'KET') || ($kodeSifatSurat == 'SPRIN') || ($kodeSifatSurat == 'CT') || 
				    ($kodeSifatSurat == 'UND') || ($kodeSifatSurat == 'R') || ($kodeSifatSurat == 'KEP') || 
					($kodeSifatSurat == 'PER')   ){
					$jenisSurat = 'BIASA';
				} 
				$levelOrg = 'BIRO';
				$ketLevelOrg = 'ESELON II';

			}
			
			$nomorAwal = $db->fetchOne("select i_nomorawal from tr_nomorawal_suratkeluar
			                            where UPPER(c_org_level) = '$levelOrg' AND c_srtsifat = '$kodeSifatSurat'");
												
			$keteranganJenisSurat = 'SURAT KELUAR '.$ketLevelOrg.' '.$jenisSurat;
			
			//echo "xxxxx= $nomorAwal | $keteranganJenisSurat";
			
			$iCatatSuratKeluar = $genNosurat->genNoSuratKeluar($keteranganJenisSurat, date("Y-m-d"), $kodeSurat, $kodeSifatSurat, $nomorAwal);
			
			/* $paramInput = array("i_srtajuan"	    => $iCatatSuratKeluar,
									"i_entry"       	=>$iEntry,
									"d_entry"       	=>date("Y-m-d"));
			
			$where[] = "id = '".$idSuratajuan."'";
		    $db->update('tm_surat_ajuan',$paramInput, $where);  */
			//var_dump($paramInput);
			//echo "<br>";
			//var_dump($where);
			$db->commit();
			
			return "$iCatatSuratKeluar";
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal";
			}
	   }
	}
	
	public function simpanNoSuratKeluar(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			$idSuratajuan = $dataMasukan['id_suratajuan'];
			$noSuratKeluar = $dataMasukan['noSuratKeluar'];
			$kodeSurat = $dataMasukan['kodeSurat'];
			$iEntry = $dataMasukan['i_entry'];
			
			//$genNosurat = new gen_nosurat();
			//$iCatatSuratKeluar = $genNosurat->genNoSuratKeluar('SURAT KELUAR', date("Y-m-d"), $kodeSurat);
			
			$paramInput = array("i_srtajuan"	    => $noSuratKeluar,
								"i_entry"       	=>$iEntry,
								"d_entry"       	=>date("Y-m-d"));
			
			$where[] = "id = '".$idSuratajuan."'";
		    $db->update('tm_surat_ajuan',$paramInput, $where); 
			//var_dump($paramInput);
			//echo "<br>";
			//var_dump($where);
			$db->commit();
			
			return "sukses";
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal";
			}
	   }
	}
	public function catatsuratkeluarInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			//$genNosurat = new gen_nosurat();
			//$iCatatSuratKeluar = $genNosurat->genNoSuratKeluar('SURAT KELUAR', date("Y-m-d"));
			//echo "jumlah data kepada = ".count($dataMasukan['dataKepada']);
			$paramInput = array("n_dari"        => $dataMasukan['n_dari'],     
								"kd_org_dari"	=> $dataMasukan['kd_org_dari'], 
								"id_memo"        => $dataMasukan['id_memo'],   
								"d_tglajuan"        => $dataMasukan['d_tglajuan'],                          					
								"n_perihal"        => $dataMasukan['n_perihal'],                          
								"n_srtlampiran"        => $dataMasukan['n_lampiran'], 
								"c_satuanlampiran"        => $dataMasukan['c_satuanlampiran'], 
								"n_penandatangan"        => $dataMasukan['n_penandatangan'], 
								"nip_penandatangan"        => $dataMasukan['nip_penandatangan'], 
								"kd_jabatan_penandatangan"        => $dataMasukan['kd_jabatan_penandatangan'], 
								"kd_org_penandatangan"        => $dataMasukan['kd_org_penandatangan'], 								
								"c_srtkualifikasi"        => $dataMasukan['c_srtkualifikasi'],                          
								"c_srtsifat"     => $dataMasukan['c_srtsifat'],  
								"n_alamatkirim"     => $dataMasukan['n_alamatkirim'],  
								//"n_pengirim"		=> $dataMasukan['n_pengirim'],								
								//"d_tglkirim"     => $dataMasukan['d_tglkirim'],                         
								//"n_penerima"      => $dataMasukan['n_penerima'],                          
								//"d_tglterima"     => $dataMasukan['d_tglterima'],                         
								//"c_srt_saranakirim"     => $dataMasukan['c_srt_saranakirim'],
								"n_srt_lokasi"   => "", 
								"i_entry"       	=>$dataMasukan['i_entry'],
								"d_entry"       	=>date("Y-m-d"));
			
			//var_dump($paramInput);
		    $db->insert('tm_surat_ajuan',$paramInput); 
			
			//$idSuratajuan = $db->fetchOne("select max(id) from tm_surat_ajuan where i_srtajuan = '$iCatatSuratKeluar'");
			$idSuratajuan = $db->fetchOne("select max(id) from tm_surat_ajuan");
			
			//echo "jumlah data kepada = ".count($dataMasukan['dataKepada']);
			//memasukkan data ajuan kepada
			//===========================
			for($i=0; $i< count($dataMasukan['dataKepada']); $i++)
			{
				
				$n_kepada = trim($dataMasukan['dataKepada'][$i]['nKepada']);
				
				if($n_kepada){
					$paramInputSrtKepada = array("id_suratajuan"	    => $idSuratajuan,	
												"n_kepada"    	=> $n_kepada,  
												"c_statuskepada"	=> "K", 
												"i_entry"       	=>$dataMasukan['i_entry'],
												"d_entry"       	=>date("Y-m-d"));
					
					$db->insert('tm_surat_ajuankepada',$paramInputSrtKepada);	
				}	
				unset($paramInputSrtKepada);
			}
			//echo "jumlah data kepada = ".count($dataMasukan['dataTembusan']);
			// memasukkan data ajuan tembusan
			//=============================
			for($i=0; $i< count($dataMasukan['dataTembusan']); $i++)
			{
				$n_tembusan = trim($dataMasukan['dataTembusan'][$i]['nTembusan']);
				$nip_tembusan = $dataMasukan['dataTembusan'][$i]['nipnTembusan'];
				$kd_jabatan_tembusan = $dataMasukan['dataTembusan'][$i]['kdJabatannTembusan'];
				$kd_org_tembusan = $dataMasukan['dataTembusan'][$i]['kdOrgnTembusan'];
				
				//var_dump($paramInputSrtKepada);		
				if($n_tembusan){
					$paramInputSrtTembusan = array("id_suratajuan"	    => $idSuratajuan,	
											"n_kepada"    	=> $n_tembusan,  
											"c_statuskepada"	=> "T", 
											"i_entry"       	=>$dataMasukan['i_entry'],
											"d_entry"       	=>date("Y-m-d"));
					
					$db->insert('tm_surat_ajuankepada',$paramInputSrtTembusan);
				}	
				unset($paramInputSrtTembusan);
			} 
																			
			$db->commit();
			
			return "$idSuratajuan~sukses";
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "$errMsg";
			}
	   }
	}
	
	public function catatsuratkeluarUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			//$genNosurat = new gen_nosurat();
			//$iCatatSuratKeluar = $genNosurat->genNoSuratKeluar('SURAT KELUAR', date("Y-m-d"));
			//echo "jumlah data kepada = ".count($dataMasukan['dataKepada']);
			$paramInput = array("n_dari"        => $dataMasukan['n_dari'],     
								"kd_org_dari"	=> $dataMasukan['kd_org_dari'], 
								"id_memo"        => $dataMasukan['id_memo'],   
								"d_tglajuan"        => $dataMasukan['d_tglajuan'],                          					
								"n_perihal"        => $dataMasukan['n_perihal'],                          
								"n_srtlampiran"        => $dataMasukan['n_lampiran'], 
								"c_satuanlampiran"        => $dataMasukan['c_satuanlampiran'], 
								"n_penandatangan"        => $dataMasukan['n_penandatangan'], 
								"nip_penandatangan"        => $dataMasukan['nip_penandatangan'], 
								"kd_jabatan_penandatangan"        => $dataMasukan['kd_jabatan_penandatangan'], 
								"kd_org_penandatangan"        => $dataMasukan['kd_org_penandatangan'], 								
								"c_srtkualifikasi"        => $dataMasukan['c_srtkualifikasi'],                          
								"c_srtsifat"     => $dataMasukan['c_srtsifat'],  
								"n_alamatkirim"     => $dataMasukan['n_alamatkirim'],  
								//"n_pengirim"		=> $dataMasukan['n_pengirim'],								
								//"d_tglkirim"     => $dataMasukan['d_tglkirim'],                         
								//"n_penerima"      => $dataMasukan['n_penerima'],                          
								//"d_tglterima"     => $dataMasukan['d_tglterima'],                         
								//"c_srt_saranakirim"     => $dataMasukan['c_srt_saranakirim'],
								"n_srt_lokasi"   => "", 
								"i_entry"       	=>$dataMasukan['i_entry'],
								"d_entry"       	=>date("Y-m-d"));
			
			//var_dump($paramInput);
			$where[] = "id = '".$dataMasukan['id_srtkeluar']."'";
			//var_dump($where);
		    $db->update('tm_surat_ajuan',$paramInput, $where); 
			
			//delete data ajuan kepada
			//======================
			$whereKepada[] = "id_suratajuan = '".$dataMasukan['id_srtkeluar']."'";
			$whereKepada[] = "c_statuskepada = 'K'";
			$db->delete('tm_surat_ajuankepada',$whereKepada); 
			
			//memasukkan data ajuan kepada
			//===========================
			for($i=0; $i< count($dataMasukan['dataKepada']); $i++)
			{
				
				$n_kepada = trim($dataMasukan['dataKepada'][$i]['nKepada']);
				
				if($n_kepada){
					$paramInputSrtKepada = array("id_suratajuan"	    => $dataMasukan['id_srtkeluar'],	
												"n_kepada"    	=> $n_kepada,  
												"c_statuskepada"	=> "K", 
												"i_entry"       	=>$dataMasukan['i_entry'],
												"d_entry"       	=>date("Y-m-d"));
					
					$db->insert('tm_surat_ajuankepada',$paramInputSrtKepada);	
				}	
				unset($paramInputSrtKepada);
			}
			
			//delete data ajuan kepada
			//======================
			$whereTembusan[] = "id_suratajuan = '".$dataMasukan['id_srtkeluar']."'";
			$whereTembusan[] = "c_statuskepada = 'T'";
			$db->delete('tm_surat_ajuankepada',$whereTembusan); 
			
			// memasukkan data ajuan tembusan
			//=============================
			for($i=0; $i< count($dataMasukan['dataTembusan']); $i++)
			{
				$n_tembusan = trim($dataMasukan['dataTembusan'][$i]['nTembusan']);
				$nip_tembusan = $dataMasukan['dataTembusan'][$i]['nipnTembusan'];
				$kd_jabatan_tembusan = $dataMasukan['dataTembusan'][$i]['kdJabatannTembusan'];
				$kd_org_tembusan = $dataMasukan['dataTembusan'][$i]['kdOrgnTembusan'];
				
				//var_dump($paramInputSrtKepada);		
				if($n_tembusan){
					$paramInputSrtTembusan = array("id_suratajuan"	    => $dataMasukan['id_srtkeluar'],	
											"n_kepada"    	=> $n_tembusan,  
											"c_statuskepada"	=> "T", 
											"i_entry"       	=>$dataMasukan['i_entry'],
											"d_entry"       	=>date("Y-m-d"));
					
					$db->insert('tm_surat_ajuankepada',$paramInputSrtTembusan);
				}	
				unset($paramInputSrtTembusan);
			}  
					 													
			$db->commit();
			
			return $dataMasukan['id_srtkeluar']."~sukses";
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "$errMsg";
			}
	   }
	}

	public function catatsuratkeluarIdsys($i_srtajuan)
	{
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select id	
							from tm_surat_ajuan 
							where (c_statusdelete != 'Y' or c_statusdelete is null) 
								and i_srtajuan= '$i_srtajuan' ";	

			$result = $db->fetchOne($sqlProses);	
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function updateLokasiFile(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
					
			$paramInput = array("n_srt_lokasi"	=>$dataMasukan['n_srt_lokasi'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));

								
			$where[] = "id = '".$dataMasukan['id_suratajuan']."'";
			//var_dump($where);	
			$db->update('tm_surat_ajuan',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function updateTanggalmasuk(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
        
		try {
			$db->beginTransaction();
			
			$suratBaru = $db->fetchOne("select count(*) from vm_suratmasuk where idAgendasrt = '".$dataMasukan['id_agendasrt']."' and d_tanggalmasuk is not null");
			/* echo "select count(*) from vm_suratmasuk where idAgendasrt = '".$dataMasukan['id_agendasrt']."' and d_tanggalmasuk is not null ||| 
			$suratBaru";
			 */
			if($suratBaru == 0){
				$paramInput = array("d_tanggalmasuk"=>new Zend_Db_Expr('NOW()'),
								   "i_entry"       	=>$dataMasukan['i_entry'],
								   "d_entry"       	=>date("Y-m-d"));
				$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
				$db->update('tm_surat_masuk',$paramInput, $where);
			
				$db->commit();
			}
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function detailsuratkeluarById($idSrtajuan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where id = '$idSrtajuan' ";
			$sqlProses = "SELECT id, i_srtajuan, id_memo, n_dari, nip_dari, kd_jabatan_dari, kd_org_dari, 
								d_tglajuan, n_perihal, n_srtlampiran, c_satuanlampiran,
								n_penandatangan, nip_penandatangan, kd_jabatan_penandatangan,
								kd_org_penandatangan, c_srtkualifikasi, c_srtsifat, n_pengirim, n_alamatkirim,
								d_tglkirim, n_penerima, d_tglterima, c_srt_saranakirim, n_srt_lokasi,
								v_biayakirim, c_srtjenis, d_tglkepresiden, d_tgldaripresiden,
								d_majuseskab, d_dariseskab, d_tglkirimke, d_terimadari, c_statusdelete,
								i_entry, d_entry
						  FROM tm_surat_ajuan ";	

			$sqlData = $sqlProses.$where;
			$result = $db->fetchRow($sqlData);	
			
			$globalReferensi = new globalReferensi();
			$namaJabatanLengkap = $result->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result->kd_org);
			if(!$result->nip)
				$n_tujuansrt = $n_kepada;
			else
				$n_tujuansrt = $result->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result->kd_org);
			
			$nm_org = $globalReferensi->getNamaOrganisasi($result->kd_org);
			
			$sqlKepada = $db->fetchCol("select n_kepada 
										from tm_surat_ajuankepada 
										where c_statuskepada = 'K'
										AND id_suratajuan = '".$result->id."'");
			
			for($k=0; $k<count($sqlKepada); $k++) {
				$kepada[$k] = array($sqlKepada[$k]);
			}
			
			$sqlTembusan = $db->fetchCol("select n_kepada 
										from tm_surat_ajuankepada 
										where c_statuskepada = 'T'
										AND id_suratajuan = '".$result->id."'");
			
			for($t=0; $k<count($sqlKepada); $t++) {
				$tembusan[$t] = array($sqlTembusan[$t]);
			}
			
			$hasilAkhir = array("id_srtkeluar"		=>(string)$result->id,
								"i_srtajuan"		=>(string)$result->i_srtajuan,	
								"id_memo"	=>(string)$result->id_memo,	
								"n_dari"		=>(string)$result->n_dari,
								"nip_dari"				=>(string)$result->nip_dari,									
								"kd_jabatan_dari"		=>(string)$result->kd_jabatan_dari,
								"kd_org_dari"		=>(string)$result->kd_org_dari,
								"d_tglajuan"			=>(string)$result->d_tglajuan,
								"n_perihal"			=>(string)$result->n_perihal,
								"n_srtlampiran"        =>(string)$result->n_srtlampiran,
								"c_satuanlampiran"        =>(string)$result->c_satuanlampiran,										
								"n_penandatangan"        =>(string)$result->n_penandatangan,
								"nip_penandatangan"        =>(string)$result->nip_penandatangan,
								"kd_jabatan_penandatangan"     =>(string)$result->kd_jabatan_penandatangan,   
								"kd_org_penandatangan"     =>(string)$result->kd_org_penandatangan,   
								"c_srtkualifikasi"      =>(string)$result->c_srtkualifikasi,    
								"c_srtsifat"     =>(string)$result->c_srtsifat,   
								"n_pengirim"     =>(string)$result->n_pengirim,   
								"d_tglkirim"   =>(string)$result->d_tglkirim, 
								"n_alamatkirim"		=> (string)$result->n_alamatkirim,
								"v_biayakirim"   =>(string)$result->v_biayakirim, 
								"c_srtjenis" =>(string)$result->c_srtjenis,
								"n_srt_lokasi" 		=>(string)$result->n_srt_lokasi,
								"c_srt_saranakirim"	=> (string)$result->c_srt_saranakirim,
								"kepada" 			=> $sqlKepada,
								"tembusan"			=> $sqlTembusan
								);
			
			return $hasilAkhir;						  
			 
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getkodesifatsurat($sifatsurat) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$sqlProses = "SELECT c_format from tr_sifatpenomoran_suratkeluar where c_srtsifat = '$sifatsurat'";		
			$result = $db->fetchOne($sqlProses);	
			
			return $result;						  
			 
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function agendamasukUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInputSrtKepada = array("n_kepada"    	=> $dataMasukan['n_srtkepada'],  
										"nip"    		=> $dataMasukan['nip'],
										"kd_jabatan"    => $dataMasukan['kd_jabatan'],
										"kd_org"    	=> $dataMasukan['kd_org']);

						
			$whereSrtKepada[] = "id_srtmasuk = '".$dataMasukan['id_agendasrt']."'";
			//var_dump($paramInputSrtKepada);		
			$db->update('tm_suratkepada',$paramInputSrtKepada, $whereSrtKepada);
			
			if(!$dataMasukan['n_srt_lokasi'])
			{
				$paramInput = array("i_agendasrt"	    => $dataMasukan['i_agendasrt'],	                      
								"c_srtjenis"        => $dataMasukan['c_srtjenis'],                          
								"c_srtsifat"        => $dataMasukan['c_srtsifat'],                          
								"i_srtpengirim"     => $dataMasukan['i_srtpengirim'],  
								"n_srtpengirim"		=> $dataMasukan['n_srtpengirim'],								
								"d_tglpengirim"     => $dataMasukan['d_tglpengirim'],                         
								"n_srtperihal"      => $dataMasukan['n_srtperihal'],                          
								"n_srtlampiran"     => $dataMasukan['n_srtlampiran'],                         
								"n_srtpengirim"     => $dataMasukan['n_srtpengirim'],
								"i_agendalalusrt"   => $dataMasukan['i_agendalalusrt'],
								"d_tgldariwaseskab" => $dataMasukan['d_tgldariwaseskab'],
								"i_entry"       	=>$dataMasukan['i_entry'],
							    "d_entry"       	=>date("Y-m-d"));
			}	
			else {
				$paramInput["n_srt_lokasi"] = $dataMasukan['n_srt_lokasi'];
			}

			$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
		    $db->update('tm_surat_masuk', $paramInput, $where); 
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function suratKeluarHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));	
								
			$where[] = "id = '".$dataMasukan['id']."'";
			
			$db->update('tm_surat_ajuan',$paramInput, $where);
			
			$whereKepada[] = "id = $n_srtkepada";
			$db->update('tm_surat_ajuankepada',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function catatresi(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			$id_srtkeluar = $dataMasukan['id_srtkeluar'];
			$n_alamatkirim = $dataMasukan['nAlamatKirim'];
			$n_pengirim = $dataMasukan['nPengirim'];
			$d_tglkirim = $dataMasukan['dTglkirim'];
			$c_srt_saranakirim = $dataMasukan['cSrtSaranakirim'];
			$vBiaya = $dataMasukan['vBiaya'];
			
			$paramInput = array("n_alamatkirim"		=> $n_alamatkirim,
								"n_pengirim"		=> $n_pengirim,
								"d_tglkirim"		=> $d_tglkirim,
								"c_srt_saranakirim"	=> $c_srt_saranakirim,
								"v_biayakirim" 		=> $vBiaya,
								"i_entry"       	=>$iEntry,
								"d_entry"       	=>date("Y-m-d"));
			
			$where[] = "id = '".$id_srtkeluar."'";
		    $db->update('tm_surat_ajuan',$paramInput, $where); 
			/* var_dump($paramInput);
			echo "<br>";
			var_dump($where); */
			$db->commit();
			
			return "sukses";
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal";
			}
	   }
	}
	
	public function pencarianSuratkeluar(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		if (!$kategoriCari) { $kategoriCari = 'i_srtajuan'; }
		$katakunciCari 	= $dataMasukan['katakunci'];
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (c_statusdelete != 'Y' or c_statusdelete is null)";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			
			$where = $whereBase." and ".$whereOpt;
			
			$sqlProses = "select id,
								i_srtajuan,
								id_memo,
								n_dari,
								nip_dari
								kd_jabatan_dari,
								kd_org_dari,
								d_tglajuan,
								n_perihal
						from tm_surat_ajuan ";	

			$sqlData = $sqlProses.$where;
			$result = $db->fetchAll($sqlData);	
			
			//echo $sqlData;
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				/* $namaJabatanLengkap = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
				if(!$result[$j]->nip)
					$n_tujuansrt = $n_kepada;
				else
					$n_tujuansrt = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
								 */
								 
				$nKepada = $db->fetchRow("select * from tm_surat_ajuankepada where id_suratajuan = '".$result[$j]->id."'");
				
				$hasilAkhir[$j] = array("id_srtajuan"     	=>(string)$result[$j]->id,  
										"i_srtajuan"     	=>(string)$result[$j]->i_srtajuan,
										"i_memo"     		=>(string)$result[$j]->id_memo,										
										"n_dari"         	=>(string)$result[$j]->n_dari,        
										"nip_dari"     	=>(string)$result[$j]->nip_dari,    
										"kd_jabatan_dari"      	=>(string)$result[$j]->kd_jabatan_dari,     
										"kd_org_dari"        	=>(string)$result[$j]->kd_org_dari,       
										"d_tglajuan" 	=>(string)$result[$j]->d_tglajuan,
										"n_perihal"   	=>(string)$result[$j]->n_perihal,  
										);
			}	
			//var_dump($hasilAkhir);
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getSuratKeluarHarianPerLevel(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$level			= strtoupper($dataMasukan['level']);
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$where = "where (a.c_statusdelete != 'Y' or a.c_statusdelete is null) and
							a.kd_org_dari = b.kd_struktur_org and
							a.d_tglajuan like '%$dTglCari%' and
							UPPER(b.level) = '$level' ";
			
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "SELECT a.id, a.c_srtsifat, a.i_srtajuan, a.d_tglajuan, a.n_perihal, a.n_penerima, a.n_pengirim, a.kd_org_dari
						  FROM persuratan.tm_surat_ajuan a, simpeg_internal.tbl_struktur_organisasi b ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			//echo $sqlProses.$where.$order;
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$nKepada = $db->fetchRow("select * from tm_surat_ajuankepada where id_suratajuan = '".$result[$j]->id."'");
				$nOrgDari = $db->fetchOne("select concat(level ,' ',nm_level) as nOrg
											from simpeg_internal.tbl_struktur_organisasi where kd_struktur_org = '".$result[$j]->kd_org_dari."'");
				$hasilAkhir[$j] = array("id_srtajuan"     	=>(string)$result[$j]->id,  
										"i_srtajuan"     	=>(string)$result[$j]->i_srtajuan,    
										"i_srtverbal"     	=>(string)$result[$j]->i_srtverbal,    
										"n_dari"         	=>(string)$result[$j]->n_dari,  
										"nOrgDari"         	=>$nOrgDari,        
										"nip_dari"     	=>(string)$result[$j]->nip_dari,    
										"kd_jabatan_dari"      	=>(string)$result[$j]->kd_jabatan_dari,     
										"kd_org_dari"        	=>(string)$result[$j]->kd_org_dari,       
										"d_tglajuan" 	=>(string)$result[$j]->d_tglajuan,
										"n_perihal"   	=>(string)$result[$j]->n_perihal,  
										"n_srtlampiran"   	=>(string)$result[$j]->n_srtlampiran,  
										"n_penandatangan"    	=>(string)$result[$j]->n_penandatangan,   
										"nip_penandatangan"	=>(string)$result[$j]->nip_penandatangan,
										"kd_jabatan_penandatangan"      	=>(string)$result[$j]->kd_jabatan_penandatangan,     
										"kd_org_penandatangan"   	=>(string)$result[$j]->kd_org_penandatangan,  
										"c_srtkualifikasi"   	=>(string)$result[$j]->c_srtkualifikasi,  
										"c_srtsifat"   	=>(string)$result[$j]->c_srtsifat,  
										"n_pengirim"	=>(string)$result[$j]->n_pengirim,
										"d_tglkirim"	=>(string)$result[$j]->d_tglkirim,
										"n_penerima"     	=>(string)$result[$j]->n_penerima,    
										"d_tglterima"	=>(string)$result[$j]->d_tglterima,
										"c_srt_saranakirim"  	=>(string)$result[$j]->c_srt_saranakirim,
										"n_srt_lokasi"  	=>(string)$result[$j]->n_srt_lokasi,
										"n_kepada" => $nKepada->n_kepada
										);
			}	
			//var_dump($hasilAkhir);
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getPengirimanSurat(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$level			= strtoupper($dataMasukan['level']);
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$where = "where (a.c_statusdelete != 'Y' or a.c_statusdelete is null) and
							a.id = b.id_suratajuan and
							a.d_tglajuan like '%$dTglCari%'";
			
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "SELECT a.id, a.i_srtajuan, a.d_tglajuan, a.n_perihal, a.kd_org_dari, a.n_dari, 
								 a.nip_dari, a.kd_jabatan_dari, b.n_kepada, a.n_penerima, 
								 a.d_tglterima, a.c_srt_saranakirim, a.d_tglkirim
						  FROM tm_surat_ajuan a, tm_surat_ajuankepada b ";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where.$order." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			//echo $sqlProses.$where.$order;
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$nOrgDari = $db->fetchOne("select concat(level ,' ',nm_level) as nOrg
											from simpeg_internal.tbl_struktur_organisasi where kd_struktur_org = '".$result[$j]->kd_org_dari."'");
				$hasilAkhir[$j] = array("id_srtajuan"     	=>(string)$result[$j]->id,  
										"i_srtajuan"     	=>(string)$result[$j]->i_srtajuan,    
										"d_tglajuan" 	=>(string)$result[$j]->d_tglajuan,
										"n_perihal"   	=>(string)$result[$j]->n_perihal,  
										"n_dari"         	=>(string)$result[$j]->n_dari,  
										"kd_org_dari"        	=>(string)$result[$j]->kd_org_dari,       
										"nOrgDari"         	=>$nOrgDari,        
										"nip_dari"     	=>(string)$result[$j]->nip_dari,    
										"kd_jabatan_dari"      	=>(string)$result[$j]->kd_jabatan_dari,     
										"n_kepada"		=> (string)$result[$j]->n_kepada, 
										"n_penerima"     	=>(string)$result[$j]->n_penerima,    
										"d_tglterima"	=>(string)$result[$j]->d_tglterima,
										"c_srt_saranakirim"  	=>(string)$result[$j]->c_srt_saranakirim,
										"d_tglkirim"	=>(string)$result[$j]->d_tglkirim
										);
			}	
			//var_dump($hasilAkhir);
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
}
?>