<?php
require_once "share/globalReferensi.php";
require_once "share/gen_nosurat.php";
require_once 'Zend/Date.php';
require_once 'Zend/Locale.php';
require_once 'Zend/Date/Cities.php';
require_once 'service/aplikasi/Aplikasi_Referensi_Service.php';
require_once 'share/format_date.php'; 


class Aplikasi_Suratmasukagenda_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function cariAgendamasukList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari1 		= $dataMasukan['dTglCari1'];
		$dTglCari2	 	= $dataMasukan['dTglCari2'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
		$userLogin		= $dataMasukan['userLogin'];
		$nipuserLogin	= $dataMasukan['nipuserLogin'];
		$cetak			= $dataMasukan['cetak'];
		//echo $userLogin;
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (a.c_statusdeleteAgendamasuk != 'Y' or a.c_statusdeleteAgendamasuk is null) and 
								(a.c_statusdeleteKepada != 'Y' or a.c_statusdeleteKepada is null) ";
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tanggalmasuk') {
					$whereOpt = "a.d_tglpengirim between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tanggalmasuk') {
					$whereOpt = "a.d_tanggalmasuk like '$dTglCari%' ";
				} else {
					$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
				}
			}
			//echo $whereOpt;
			
			// perubahan hendar 13-10-2009 jakarta $whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin' OR a.kd_org = '1' OR a.kd_org = '2')";

			//untuk user Tata Usaha  persuratan 
			if(($kdOrgLogin!='9') && ($kdOrgLogin!='30') && ($kdOrgLogin!='31') && ($kdOrgLogin!='32') && 
			    ($kdOrgLogin!='170') && ($kdOrgLogin!='174') && ($kdOrgLogin!='178') && ($kdOrgLogin!='182') && ($kdOrgLogin!='186') &&
				($kdOrgLogin!='190') && ($kdOrgLogin!='194') && ($kdOrgLogin!='197') &&
				($kdOrgLogin!='171') && ($kdOrgLogin!='172') && ($kdOrgLogin!='173') &&
				($kdOrgLogin!='175') && ($kdOrgLogin!='176') && ($kdOrgLogin!='177') &&
				($kdOrgLogin!='179') && ($kdOrgLogin!='180') && ($kdOrgLogin!='181') &&
				($kdOrgLogin!='183') && ($kdOrgLogin!='184') && ($kdOrgLogin!='158') &&
				($kdOrgLogin!='187') && ($kdOrgLatogin!='188') && ($kdOrgLogin!='189') &&
				($kdOrgLogin!='191') && ($kdOrgLogin!='192') && ($kdOrgLogin!='193') &&
				($kdOrgLogin!='195') && ($kdOrgLogin!='196') && ($kdOrgLogin!='198') && ($kdOrgLogin!='199'))
			{
				$whereByOrg = " and (a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or 
								(b.kd_orgteruskanke = '$kdOrgLogin' and b.n_teruskanke='$nipuserLogin'))";
			}
			
			if (($kdOrgLogin=='0')||($kdOrgLogin=='1'))	
			{
				//$whereByOrg = $whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%')";
				$whereByOrg = $whereByOrg."or(kd_org = '0' or kd_org='1') or
										(upper(a.n_kepada) = 'PRESIDEN' or UPPER(a.n_kepada) = 'SESKAB')";
		        //$whereByOrg = '';
			}
		
			if(($kategoriCari) && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt.$whereByOrg;} 
			else { $where = $whereBase.$whereByOrg;}

			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "select distinct a.idAgendasrt,
							a.i_agendasrt,
							a.d_tanggalmasuk,
							a.c_srtjenis,
							a.n_srtjenis,
							a.c_srtsifat,
							a.n_srtsifat,
							a.i_srtpengirim,
							a.d_tglpengirim,
							a.n_srtperihal,
							a.n_srtlampiran,
							a.c_satuanlampiran,
							a.n_srtpengirim,
							a.i_agendalalusrt,
							a.d_tgldariwaseskab,
							a.n_srt_lokasi,
							a.n_kepada,
							a.nip,
							a.kd_org,
							a.kd_jabatan,
							a.nm_jabatan,
							a.i_entry,
							a.c_status_bukasurat,
							b.c_status_bukasurat as c_status_bukasuratDisposisi
						from vm_suratmasuk a
						left join tm_disposisi b on (a.id_srtmasuk = b.id_srtmasuk)";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where group by a.i_agendasrt) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				if ($cetak == 'pdf') {
					$sqlData = $sqlProses.$where." group by a.i_agendasrt $order";
				} else {
					$sqlData = $sqlProses.$where." group by a.i_agendasrt $order"." limit $xLimit offset $xOffset";
				}
				//echo $sqlData;
				$result = $db->fetchAll($sqlData);	
			}
				
			//var_dump($result);
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$namaJabatanLengkap = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
				$nm_org = $globalReferensi->getNamaOrganisasi($result[$j]->kd_org); 
				$nm_satuanlampiran = $db->fetchOne("select nm_satuanlampiran from tr_satuanlampiran where id='".$result[$j]->c_satuanlampiran."'");
				/*if(!$result[$j]->nip)
					$n_tujuansrt = $n_kepada;
				else
					$n_tujuansrt = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);*/
				$hasilAkhir[$j] = array("id_srtmasuk"		=>(string)$result[$j]->idAgendasrt,	
										"i_agendasrt"		=>(string)$result[$j]->i_agendasrt,	
										"d_tanggalmasuk"	=>(string)$result[$j]->d_tanggalmasuk,	
										//"n_tujuansrt"		=>$n_tujuansrt,
										"n_kepada"			=>(string)$result[$j]->n_kepada,
										"nip"				=>(string)$result[$j]->nip,
										"kd_jabatan"		=>(string)$result[$j]->kd_jabatan,
										"namaJabatan"		=>(string)$result[$j]->nm_jabatan,
										"kd_org"			=>(string)$result[$j]->kd_org,
										"namaJabatanLengkap"=>$namaJabatanLengkap,
										"nm_org"			=> $nm_org,
										"c_srtjenis"        =>(string)$result[$j]->c_srtjenis,
										"n_srtjenis"        =>(string)$result[$j]->n_srtjenis,										
										"c_srtsifat"        =>(string)$result[$j]->c_srtsifat,
										"n_srtsifat"        =>(string)$result[$j]->n_srtsifat,										
										"i_srtpengirim"     =>(string)$result[$j]->i_srtpengirim,   
										"d_tglpengirim"     =>(string)$result[$j]->d_tglpengirim,   
										"n_srtperihal"      =>(string)$result[$j]->n_srtperihal,    
										"n_srtlampiran"     =>(string)$result[$j]->n_srtlampiran,   
										"nm_satuanlampiran" =>$nm_satuanlampiran,
										"n_srtpengirim"     =>(string)$result[$j]->n_srtpengirim,   
										"i_agendalalusrt"   =>(string)$result[$j]->i_agendalalusrt, 
										"d_tgldariwaseskab" =>(string)$result[$j]->d_tgldariwaseskab,
										"n_srt_lokasi" 		=>(string)$result[$j]->n_srt_lokasi,
										"i_entry" 			=>(string)$result[$j]->i_entry,
										"c_status_bukasurat"=>(string)$result[$j]->c_status_bukasurat,
										"c_status_bukasuratDisposisi"=>(string)$result[$j]->c_status_bukasuratDisposisi
										);
			}	
			
			
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
		public function cariAgendamasukList2(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari1 		= $dataMasukan['dTglCari1'];
		$dTglCari2	 	= $dataMasukan['dTglCari2'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
		$userLogin		= $dataMasukan['userLogin'];
		
		//echo $kategoriCari;
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (a.c_statusdeleteAgendamasuk != 'Y' or a.c_statusdeleteAgendamasuk is null) and 
								(a.c_statusdeleteKepada != 'Y' or a.c_statusdeleteKepada is null) ";
			//if(trim($kategoriCari) == "semua"){
				$whereOpt = "a.i_agendasrt like '%$katakunciCari%' or a.n_srtjenis like '%$katakunciCari%' or a.n_srtpengirim like '%$katakunciCari%' or a.n_srtperihal like '%$katakunciCari%'  ";
			//}
			
			
			// perubahan hendar 13-10-2009 jakarta $whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin' OR a.kd_org = '1' OR a.kd_org = '2')";

$whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin')";
if (($kdOrgLogin=='1')||($kdOrgLogin=='2'))	
{
	
	$whereByOrg = $whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%')";

}		
			 $where = $whereBase." and ".$whereOpt." and ".$whereByOrg;
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "select distinct a.idAgendasrt,
							a.i_agendasrt,
							a.d_tanggalmasuk,
							a.c_srtjenis,
							a.n_srtjenis,
							a.i_srtpengirim,
							a.d_tglpengirim,
							a.n_srtperihal,
							a.n_srtlampiran,
							a.n_srtpengirim,
							a.i_agendalalusrt,
							a.d_tgldariwaseskab,
							a.n_srt_lokasi,
							a.n_kepada,
							a.nip,
							a.kd_org,
							a.kd_jabatan,
							a.nm_jabatan,
							a.i_entry
						from vm_suratmasuk a
						left join tm_disposisi b on (a.id_srtmasuk = b.id_srtmasuk)";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where group by a.i_agendasrt) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where." group by a.i_agendasrt $order"." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			//echo $sqlData;		
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$namaJabatanLengkap = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
				$nm_org = $globalReferensi->getNamaOrganisasi($result[$j]->kd_org); 
				/*if(!$result[$j]->nip)
					$n_tujuansrt = $n_kepada;
				else
					$n_tujuansrt = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);*/
				$hasilAkhir[$j] = array("id_srtmasuk"		=>(string)$result[$j]->idAgendasrt,	
										"i_agendasrt"		=>(string)$result[$j]->i_agendasrt,	
										"d_tanggalmasuk"	=>(string)$result[$j]->d_tanggalmasuk,	
										//"n_tujuansrt"		=>$n_tujuansrt,
										"n_kepada"			=>(string)$result[$j]->n_kepada,
										"nip"				=>(string)$result[$j]->nip,
										"kd_jabatan"		=>(string)$result[$j]->kd_jabatan,
										"namaJabatan"		=>(string)$result[$j]->nm_jabatan,
										"kd_org"			=>(string)$result[$j]->kd_org,
										"namaJabatanLengkap"=>$namaJabatanLengkap,
										"nm_org"			=> $nm_org,
										"c_srtjenis"        =>(string)$result[$j]->c_srtjenis,
										"n_srtjenis"        =>(string)$result[$j]->n_srtjenis,										
										"i_srtpengirim"     =>(string)$result[$j]->i_srtpengirim,   
										"d_tglpengirim"     =>(string)$result[$j]->d_tglpengirim,   
										"n_srtperihal"      =>(string)$result[$j]->n_srtperihal,    
										"n_srtlampiran"     =>(string)$result[$j]->n_srtlampiran,   
										"n_srtpengirim"     =>(string)$result[$j]->n_srtpengirim,   
										"i_agendalalusrt"   =>(string)$result[$j]->i_agendalalusrt, 
										"d_tgldariwaseskab" =>(string)$result[$j]->d_tgldariwaseskab,
										"n_srt_lokasi" 		=>(string)$result[$j]->n_srt_lokasi,
										"i_entry" 		=>(string)$result[$j]->i_entry
										);
			}	
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	
	public function agendaList() {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select distinct(i_agendasrt) i_agendasrt, n_srtperihal
							from tm_surat_masuk 
							where (c_statusdelete != 'Y' or c_statusdelete is null)";

			$sqlData = $sqlProses;
			$hasilAkhir = $db->fetchCol($sqlData);	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function agendamasukInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			$gen_nosurat = new gen_nosurat();
			$iAgendamasuk = $gen_nosurat->genNoAgendamasuk('AGENDA SURAT MASUK', date("Y-m-d"));
			$paramInput = array("i_agendasrt"	    => $iAgendamasuk,	
								"id_tandaterima"	=> $dataMasukan['id_tandaterima'],
								"c_srtjenis"        => $dataMasukan['c_srtjenis'],                          
								"c_srtsifat"        => $dataMasukan['c_srtsifat'],                          
								"i_srtpengirim"     => $dataMasukan['i_srtpengirim'],  
								"n_srtpengirim"		=> $dataMasukan['n_srtpengirim'],								
								"d_tglpengirim"     => $dataMasukan['d_tglpengirim'],                         
								"n_srtperihal"      => $dataMasukan['n_srtperihal'],                          
								"n_srtlampiran"     => $dataMasukan['n_srtlampiran'],                         
								"c_satuanlampiran"  => $dataMasukan['c_satuanlampiran'],                         
								"i_agendalalusrt"   => $dataMasukan['i_agendalalusrt'],
								"d_tgldariwaseskab" => $dataMasukan['d_tgldariwaseskab'],
								"n_srt_lokasi" 		=> "",
								"i_entry"       	=> $dataMasukan['i_entry'],
								"d_entry"			=> date('Y-m-d'),
								"d_tanggalmasuk"	=> $dataMasukan['d_tanggalmasuk'],
								"e_retro"			=> $dataMasukan['e_retro']);
								
					
			$db->insert('tm_surat_masuk',$paramInput); 
			$idSrtmasuk = $db->fetchOne("select max(id) from tm_surat_masuk where i_agendasrt = '$iAgendamasuk'");
			
			$paramInputSrtKepada = array("id_srtmasuk"	=> $idSrtmasuk,	
										"n_kepada"    	=> $dataMasukan['n_kepada'], //$dataMasukan['n_kepada'],  
										"c_srtjenis"	=> $dataMasukan['c_srtjenis'],
										"i_entry"       =>$dataMasukan['i_entry'],
										"kd_org"        =>$dataMasukan['kd_org']);
			
			 //if (($dataMasukan['n_kepada'] != 'Presiden') && ($dataMasukan['n_kepada'] != 'Seskab')){
				if ($dataMasukan['nip']){
					$paramInputSrtKepada['nip'] = $dataMasukan['nip'];
					$paramInputSrtKepada['kd_jabatan'] = $dataMasukan['kd_jabatan'];
					$paramInputSrtKepada['kd_org'] = $dataMasukan['kd_org'];
				}
			//}
			$db->insert('tm_suratkepada',$paramInputSrtKepada); 
			$db->commit();
			
			return "$iAgendamasuk~sukses";
			unset($paramInput);
			unset($paramInputSrtKepada);
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal";
				//return $e->getMessage();
			}
	   }
	}

	public function agendamasukIdsys($iAgendasrt)
	{
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select id	
							from tm_surat_masuk 
							where (c_statusdelete != 'Y' or c_statusdelete is null) 
								and i_agendasrt= '$iAgendasrt' ";	

			$result = $db->fetchOne($sqlProses);	
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function agendamasukPerihal($iSrtmasuk)
	{
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select n_srtperihal	
							from tm_surat_masuk 
							where (c_statusdelete != 'Y' or c_statusdelete is null) 
								and i_agendasrt= '$iSrtmasuk' ";	

			$result = $db->fetchOne($sqlProses);	
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function updateLokasiFile(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
					
			$paramInput = array("n_srt_lokasi"	=>$dataMasukan['n_srt_lokasi'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));

									
			$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
			$db->update('tm_surat_masuk',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function ubahStatusBukaSurat(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
        
		try {
			$db->beginTransaction();
			
			$suratBaru = $db->fetchOne("select count(*) from vm_suratmasuk where idAgendasrt = '".$dataMasukan['id_agendasrt']."' and c_status_bukasurat =  'Y' ");
			//echo "select count(*) from vm_suratmasuk where idAgendasrt = '".$dataMasukan['id_agendasrt']."' and c_status_bukasurat =  'Y' ||| 
			//s$suratBaru";
			 
			if($suratBaru == 0){
				$paramInput = array("c_status_bukasurat"	=> 'Y');
				$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
				$db->update('tm_surat_masuk',$paramInput, $where);
			
				$db->commit();
			}
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	
	public function detailagendamasukById($idAgendasrt) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where idAgendasrt = '$idAgendasrt' ";
			$sqlProses = "select idAgendasrt,
							i_agendasrt,
							id_tandaterima,
							i_tandaterima,
							d_tanggalmasuk,
							c_srtjenis,
							n_srtjenis,
							c_srtsifat,
							n_srtsifat,
							i_srtpengirim,
							d_tglpengirim,
							n_srtperihal,
							n_srtlampiran,
							c_satuanlampiran,
							nm_satuanlampiran,
							n_srtpengirim,
							i_agendalalusrt,
							d_tgldariwaseskab,
							n_srt_lokasi,
							d_tanggalmasuk,
							n_kepada,
							nip,
							kd_org,
							kd_jabatan,
							nm_jabatan,
							e_retro
						from vm_suratmasuk ";	

			$sqlData = $sqlProses.$where;
 			$result = $db->fetchRow($sqlData);	
			
			$globalReferensi = new globalReferensi();
			$ref_serv = Aplikasi_Referensi_Service::getInstance();
			$format_date = new format_date();

			//$namaJabatanLengkap = $result->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result->kd_org);
			
			$namaJabatanLengkap	= $ref_serv->getjabatanPegawai($result->nip);
			
			if(!$result->nip)
				$n_tujuansrt = $n_kepada;
			else
				$n_tujuansrt = $result->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result->kd_org);
			
			$nm_org = $globalReferensi->getNamaOrganisasi($result->kd_org);
						
			$d_tanggalmasukformat = $format_date->formatTglLengkap($result->d_tanggalmasuk);
			$d_tanggalpengirimformat = $format_date->formatTglLengkap($result->d_tglpengirim);
			$hasilAkhir = array("id_srtmasuk"		=>(string)$result->idAgendasrt,
								"i_agendasrt"		=>(string)$result->i_agendasrt,	
								"id_tandaterima"	=>(string)$result->id_tandaterima,	 
								"i_tandaterima"		=>(string)$result->i_tandaterima,	
								"d_tanggalmasuk"	=>(string)$result->d_tanggalmasuk,	
								"d_tanggalmasukformat"	=>$d_tanggalmasukformat,	
								"n_srtkepada"		=>(string)$result->n_kepada,
								"nip"				=>(string)$result->nip,									
								"kd_jabatan"		=>(string)$result->kd_jabatan,
								"nm_jabatan"		=>(string)$result->nm_jabatan,
								"kd_org"			=>(string)$result->kd_org,
								"nm_org"			=>$nm_org,
								"nm_jabatan_lengkap"=>$namaJabatanLengkap, 
								"c_srtjenis"        =>(string)$result->c_srtjenis,
								"n_srtjenis"        =>(string)$result->n_srtjenis,										
								"c_srtsifat"        =>(string)$result->c_srtsifat,
								"n_srtsifat"        =>(string)$result->n_srtsifat,										
								"i_srtpengirim"     =>(string)$result->i_srtpengirim,   
								"d_tglpengirim"     =>(string)$result->d_tglpengirim,
								"d_tanggalpengirimformat" => $d_tanggalpengirimformat,
								"n_srtperihal"      =>(string)$result->n_srtperihal,    
								"n_srtlampiran"     =>(string)$result->n_srtlampiran,   
								"c_satuanlampiran"	=>(string)$result->c_satuanlampiran,
								"nm_satLampiran"	=>(string)$result->nm_satuanlampiran,
								"n_srtpengirim"     =>(string)$result->n_srtpengirim,   
								"i_agendalalusrt"   =>(string)$result->i_agendalalusrt, 
								"d_tgldariwaseskab" =>(string)$result->d_tgldariwaseskab,
								"n_srt_lokasi" 		=>(string)$result->n_srt_lokasi,
								"d_tanggalmasuk" 	=>(string)$result->d_tanggalmasuk,
								"e_retro" 			=>(string)$result->e_retro
								);
			//var_dump($hasilAkhir);
			return $hasilAkhir;						  
			 
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getIsiDisposisiByUser($dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$nTeruskanKe = $dataMasukan['n_teruskanke'];
		$kdTeruskanKe = $dataMasukan['kd_teruskanke'];
		$idAgendasrt = $dataMasukan['id_agendasrt'];
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$e_disposisi = $db->fetchOne("select e_disposisi from vm_disposisi where id_srtmasuk = '$idAgendasrt' 
											AND (kd_orgteruskanke = '$kdTeruskanKe' AND n_teruskanke = '$nTeruskanKe')");

											// echo "select e_disposisi from vm_disposisi where id_srtmasuk = '$idAgendasrt' 
											// AND (kd_orgteruskanke = '$kdTeruskanKe' OR n_teruskanke = '$nTeruskanKe')";
			return $e_disposisi;						  
			 
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	

	public function agendamasukUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			
			$kepadaArr = explode('#', $dataMasukan['n_kepada']);
			$namaKepada = $kepadaArr[1];
			if (!$dataMasukan['kd_org']) {	$kdOrgKepada = $kepadaArr[0];}
			else { $kdOrgKepada = $dataMasukan['kd_org'];}
			
			/* $paramInputSrtKepada = array("n_kepada"    	=> $namaKepada,  
										"nip"    		=> $dataMasukan['nip'],
										"kd_jabatan"    => $dataMasukan['kd_jabatan'],
										"kd_org"    	=> $kdOrgKepada,
										"i_entry"       => $dataMasukan['i_entry']); */

			$paramInputSrtKepada = array("n_kepada"    	=> $dataMasukan['n_kepada'], //$dataMasukan['n_kepada'],  
										"c_srtjenis"	=> $dataMasukan['c_srtjenis'],
										"i_entry"       =>$dataMasukan['i_entry'],
										"kd_org"        =>$dataMasukan['kd_org']);
			
			 if (($dataMasukan['n_kepada'] != 'Presiden') && ($dataMasukan['n_kepada'] != 'Seskab')){
				if ($dataMasukan['nip']){
					$paramInputSrtKepada['nip'] = $dataMasukan['nip'];
					$paramInputSrtKepada['kd_jabatan'] = $dataMasukan['kd_jabatan'];
					$paramInputSrtKepada['kd_org'] = $dataMasukan['kd_org'];
				} else {
					$paramInputSrtKepada['nip'] = null;
					$paramInputSrtKepada['kd_jabatan'] = null;
					//$paramInputSrtKepada['kd_org'] = null;
				}
			}
						
			$whereSrtKepada[] = "id_srtmasuk = '".$dataMasukan['id_agendasrt']."'";
					
			$db->update('tm_suratkepada',$paramInputSrtKepada, $whereSrtKepada);
			
			$paramInput = array("i_agendasrt"	    => $dataMasukan['i_agendasrt'],	
								"id_tandaterima"	=> $dataMasukan['id_tandaterima'],
								"c_srtjenis"        => $dataMasukan['c_srtjenis'],                          
								"c_srtsifat"        => $dataMasukan['c_srtsifat'],   
								"i_srtpengirim"     => $dataMasukan['i_srtpengirim'],  
								"n_srtpengirim"		=> $dataMasukan['n_srtpengirim'],								
								"d_tglpengirim"     => $dataMasukan['d_tglpengirim'],                         
								"n_srtperihal"      => $dataMasukan['n_srtperihal'],                          
								"n_srtlampiran"     => $dataMasukan['n_srtlampiran'],                         
								"c_satuanlampiran"  => $dataMasukan['c_satuanlampiran'],                         
								"i_agendalalusrt"   => $dataMasukan['i_agendalalusrt'],
								"d_tgldariwaseskab" => $dataMasukan['d_tgldariwaseskab'],
								"i_entry"       	=>$dataMasukan['i_entry'],
								"d_tanggalmasuk"	=> $dataMasukan['d_tanggalmasuk'],
								"e_retro"			=> $dataMasukan['e_retro']);

			if($dataMasukan['n_srt_lokasi'])
			{
				$paramInput["n_srt_lokasi"] = $dataMasukan['n_srt_lokasi'];
			}

			
			$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
		    $db->update('tm_surat_masuk', $paramInput, $where); 
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function agendamasukHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			//$n_srtkepada = $db->fetchOne("select n_srtkepada from tm_surat_masuk where id='".$dataMasukan['id']."'");
			
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry']);	
								
			$where[] = "id = '".$dataMasukan['id']."'";
			
			$db->update('tm_surat_masuk',$paramInput, $where);
			
			$whereKepada[] = "id_srtmasuk = '".$dataMasukan['id']."'";
			$db->update('tm_suratkepada',$paramInput, $whereKepada);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function getNamaSatuanLampiran($c_satuanlampiran) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select nm_satuanlampiran
							from tr_satuanlampiran
							where nm_satuanlampiran = '$c_satuanlampiran'";	
							
			
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchOne($sqlData);				
			
			//echo $sqlData;
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function jmlSuratMasuk($kdOrgLogin, $userLogin, $nipuserLogin) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (a.c_statusdeleteAgendamasuk != 'Y' or a.c_statusdeleteAgendamasuk is null) and 
								(a.c_statusdeleteKepada != 'Y' or a.c_statusdeleteKepada is null) and 
								a.c_status_bukasurat='N'";
						
			// perubahan hendar 13-10-2009 jakarta $whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin' OR a.kd_org = '1' OR a.kd_org = '2')";

	//$whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin')";
	$whereByOrg = " and (a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin')";
	if (($kdOrgLogin=='1')||($kdOrgLogin=='2'))	
	{
	
		$whereByOrg = $whereByOrg." or (upper(a.n_kepada) like '%PRESIDEN%' or 
										upper(a.n_kepada) like '%SESKAB%' or 
										upper(a.n_kepada) like '%SEKRETARIS KABINET%')";

	}		
			$where = $whereBase.$whereByOrg;
			
			$sqlProses = "select distinct a.idAgendasrt,
							a.i_agendasrt,
							a.d_tanggalmasuk,
							a.c_srtjenis,
							a.n_srtjenis,
							a.i_srtpengirim,
							a.d_tglpengirim,
							a.n_srtperihal,
							a.n_srtlampiran,
							a.n_srtpengirim,
							a.i_agendalalusrt,
							a.d_tgldariwaseskab,
							a.n_srt_lokasi,
							a.n_kepada,
							a.nip,
							a.kd_org,
							a.kd_jabatan,
							a.nm_jabatan,
							a.i_entry
						from vm_suratmasuk a ";	

			$sqlData = 'select count(*) from ('.$sqlProses.$where.') x';
			//echo $sqlData;
			$result = $db->fetchOne($sqlData);	
			
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
public function getCariData($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			/* $result = $db->fetchAll("select distinct (a.i_agendasrt), a.id,a.n_srtpengirim,a.d_tglpengirim, a.n_srtperihal,a.c_srtjenis,
									a.c_srtsifat,a.i_agendalalusrt,a.d_tgldariwaseskab, b.n_kepada
									from tm_surat_masuk a
									left join tm_suratkepada b on (a.id = b.id_srtmasuk)
									left join $cari"); */
														
	/*		$result = $db->fetchAll("select distinct (a.i_agendasrt), a.n_srtpengirim,a.d_tglpengirim, a.n_srtperihal,a.c_srtjenis,
									a.c_srtsifat,a.i_agendalalusrt,a.d_tgldariwaseskab, a.n_kepada,n_teruskandari,e_disposisi
									from vm_disposisi a where a.i_agendasrt = '30/Setkab/TU/X/2009 '");*/
									
			$result = $db->fetchAll("select distinct a.i_agendasrt, a.n_srtpengirim, a.n_srtperihal, a.n_kepada, a.d_tglpengirim, a.c_srtjenis, a.n_srtjenis, 
											a.d_tanggalmasuk, a.i_agendalalusrt
									from vm_disposisi a $cari 
									order by cast(a.i_agendasrt  as signed integer)  desc");
									
			
			//and (a.c_statusdeleteAgendamasuk != 'Y' OR a.c_statusdeleteAgendamasuk is null)
									
			
			/*  echo "select distinct a.i_agendasrt, a.n_srtpengirim, a.n_srtperihal, a.n_kepada, a.d_tglpengirim, a.c_srtjenis, a.n_srtjenis, 
											a.d_tanggalmasuk, a.i_agendalalusrt
									from vm_disposisi a $cari 
									order by cast(a.i_agendasrt  as signed integer)  desc"; */
									 
									
			for ($j=0; $j<count($result); $j++){
				$data[$j] = array("i_agendasrt" =>(string)$result[$j]->i_agendasrt,	
							"n_srtpengirim" =>(string)$result[$j]->n_srtpengirim,
							"n_kepada" =>(string)$result[$j]->n_kepada,
							"n_srtperihal" =>(string)$result[$j]->n_srtperihal,
							"d_tglpengirim" =>(string)$result[$j]->d_tglpengirim,
							"c_srtjenis" =>(string)$result[$j]->c_srtjenis,
							"n_srtjenis" =>(string)$result[$j]->n_srtjenis,
							"d_tanggalmasuk" =>(string)$result[$j]->d_tanggalmasuk,
							"i_agendalalusrt" =>(string)$result[$j]->i_agendalalusrt);
			}	
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function getCariDataDetail($cari) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			/* $result = $db->fetchAll("select distinct (a.i_agendasrt), a.id,a.n_srtpengirim,a.d_tglpengirim, a.n_srtperihal,a.c_srtjenis,
									a.c_srtsifat,a.i_agendalalusrt,a.d_tgldariwaseskab, b.n_kepada
									from tm_surat_masuk a
									left join tm_suratkepada b on (a.id = b.id_srtmasuk)
									left join $cari"); */
														
	/*		$result = $db->fetchAll("select distinct (a.i_agendasrt), a.n_srtpengirim,a.d_tglpengirim, a.n_srtperihal,a.c_srtjenis,
									a.c_srtsifat,a.i_agendalalusrt,a.d_tgldariwaseskab, a.n_kepada,n_teruskandari,e_disposisi
									from vm_disposisi a where a.i_agendasrt = '30/Setkab/TU/X/2009 '");*/
			$result = $db->fetchAll("select distinct a.*
									from vm_disposisi a $cari");
			
			/*echo "select distinct a.*
									from vm_disposisi a $cari";*/
			$jmlResult = count($result);
			for ($j = 0; $j < $jmlResult; $j++) {
			$c_srtjenis=$result[$j]->c_srtjenis;
			
			//$n_srtsifat = $db->fetchOne("select n_srtsifat from tr_sifat_surat where c_srtsifat='$c_srtsifat'" );	
			$n_srtjenis = $result[$j]->n_srtjenis;;
			$id=$result[$j]->id_srtmasuk;
			
			$n_dari = $db->fetchOne("select n_dari from tm_memo_keluar where id_srtmasuk='$id'" );
			
			$n_teruskan = $db->fetchOne("select n_teruskan from tm_memo_keluar where id_srtmasuk='$id'" );
			$d_memo = $db->fetchOne("select d_memo from tm_memo_keluar where id_srtmasuk='$id'" );
			$i_memo = $db->fetchOne("select i_memo from tm_memo_keluar where id_srtmasuk='$id'" );
			$id_memo = $db->fetchOne("select id from tm_memo_keluar where id_srtmasuk='$id'" );
			
			$nDarimemo = $db->fetchOne("select concat(level ,' ',nm_level) as nmOrg from simpeg_internal.tbl_struktur_organisasi where kd_struktur_org = '".$datamemo[0]->n_dari."'");
			$nTeruskanKememo = $db->fetchOne("select concat(level ,' ',nm_level) as nmOrg from simpeg_internal.tbl_struktur_organisasi where kd_struktur_org = '".$datamemo[0]->n_kepada."'");
			
			
			$i_srtajuan = $db->fetchOne("select i_srtajuan from tm_surat_ajuan where id_memo = '".$i_memo."'");
			$d_srtajuan = $db->fetchOne("select d_tglajuan from tm_surat_ajuan where id_memo = '".$i_memo."'");
			
			$unitAsalSrtKeluar = $db->fetchOne("select n_dari from tm_surat_ajuan where id_memo = '".$i_memo."'");
			$tujuanSrtKeluar = $db->fetchOne("select n_kepada from tm_surat_ajuankepada, tm_surat_ajuan
												where tm_surat_ajuan.id = tm_surat_ajuankepada.id_suratajuan and tm_surat_ajuan.i_srtajuan = '$i_srtajuan'
												and c_statuskepada = 'K'");
			
			
			
			$tglKirim = $db->fetchOne("select d_tglkirim from  tm_surat_ajuan where id_memo = '".$i_memo."'");
			
			$data[$j] = array("i_agendasrt" =>(string)$result[$j]->i_agendasrt,	
							"n_srtpengirim" =>(string)$result[$j]->n_srtpengirim,
							"n_kepada" =>(string)$result[$j]->n_kepada,
							"d_tglpengirim" =>(string)$result[$j]->d_tglpengirim,
							"n_srtperihal" =>(string)$result[$j]->n_srtperihal,
							"c_srtjenis" =>(string)$result[$j]->c_srtjenis,
							"n_srtjenis" =>$n_srtjenis,
							"i_agendalalusrt" =>(string)$result[$j]->i_agendalalusrt,
							"d_tgldariwaseskab" =>(string)$result[$j]->d_tgldariwaseskab,
							"kd_orgKepada" =>(string)$result[$j]->kd_orgKepada,
							"id_srtmasuk" =>(string)$result[$j]->id_srtmasuk,
							"i_srtajuan" => $i_srtajuan,
							"d_srtajuan" => $d_srtajuan,
							"unitAsalSrtKeluar" => $unitAsalSrtKeluar,
							"tujuanSrtKeluar" => $tujuanSrtKeluar,
							"tglKirim" => $tglKirim);
			}	
		     return $data;
		   } catch (Exception $e) {
	         echo $e->getMessage().'<br>';
		     return 'Data tidak ada <br>';
		   }
	 
	}
	
	public function getDisposisi1(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$globalReferensi = new globalReferensi();
		$ref_serv = Aplikasi_Referensi_Service::getInstance();
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			//cari data disposisi
			//================
			
			$dispoDari = $dataMasukan['dari'];
			$idSrtmasuk = $dataMasukan['idSrtmasuk'];;
			
			$sql = "select distinct a.n_teruskandari, a.kd_orgteruskandari, a.kd_jabatanteruskandari,
									a.n_teruskanke, a.kd_orgteruskanke, a.kd_jabatanteruskanke, 
									a.t_disposisi,
									DATE_FORMAT(a.t_disposisi, '%Y-%m-%d') as tanggalDisposisi,
									DATE_FORMAT(a.t_disposisi, '%H:%i:%s') as jamDisposisi
									from vm_disposisi a 
									where a.id_srtmasuk = '".$idSrtmasuk."'";
			if($dispoDari) {
				$sql = $sql."  and a.kd_orgteruskandari = '".$dispoDari."'";
			}	
			
			$dispo1 = $db->fetchAll($sql);
			  /* echo "select distinct a.n_teruskandari, a.kd_orgteruskandari, a.kd_jabatanteruskandari,
									a.n_teruskanke, a.kd_orgteruskanke, a.kd_jabatanteruskanke, 
									DATE_FORMAT(a.t_disposisi, '%Y-%m-%d') as tanggalDisposisi,
									DATE_FORMAT(a.t_disposisi, '%H:%i:%s') as jamDisposisi
									from vm_disposisi a 
									where a.kd_orgteruskandari = '".$dispoDari."' and a.id_srtmasuk = '".$idSrtmasuk."'";	 */					
			if (count($dispo1) > 0){
				for ($x = 0; $x< count($dispo1); $x++){
					$jabatanDari = $globalReferensi->getNamaJabatan($dispo1[$x]->kd_jabatanteruskandari);
				
					if (strtoupper($dispo1[$x]->n_teruskandari) == 'SESKAB'){
						$namaJabatanLengkapDari = 'SESKAB';
					} else {
						$namaJabatanLengkapDari = $ref_serv->getjabatanPegawai($dispo1[$x]->n_teruskandari);
					}
					
					$jabatanKe = $globalReferensi->getNamaJabatan($dispo1[$x]->kd_jabatanteruskanke);
					
					$namaJabatanLengkapKe = $ref_serv->getjabatanPegawai($dispo1[$x]->n_teruskanke);
					
					/* $namadari = $globalReferensi->getnamaPegawai($nip) */
					$hasil[$x] = array("n_teruskandari" => (string)$dispo1[$x]->n_teruskandari,
										"kd_orgteruskandari" => (string)$dispo1[$x]->kd_orgteruskandari,
										"kd_jabatanteruskandari" => (string)$dispo1[$x]->kd_jabatanteruskandari,
										"namaJabatanLengkapDari" => $namaJabatanLengkapDari,
										"n_teruskanke" => (string)$dispo1[$x]->n_teruskanke,
										"kd_jabatanteruskanke" => (string)$dispo1[$x]->kd_jabatanteruskanke,
										"kd_orgteruskanke" => (string)$dispo1[$x]->kd_orgteruskanke,
										"namaJabatanLengkapKe" => $namaJabatanLengkapKe,
										"tanggalDisposisi" => (string)$dispo1[$x]->tanggalDisposisi,
										"jamDisposisi" => (string)$dispo1[$x]->jamDisposisi,
										"t_disposisi" => (string)$dispo1[$x]->t_disposisi);
				}
			}
			else {
				$hasil = null;
			}
			
			return $hasil;
		} catch (Exception $e) {
			echo $e->getMessage().'<br>';
			return 'Data tidak ada <br>';
		}
	 
	}
	
	public function getMemo(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$globalReferensi = new globalReferensi();
		$ref_serv = Aplikasi_Referensi_Service::getInstance();
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			//cari data disposisi
			//================
			
			$memoDari= $dataMasukan['dari'];
			$jabatanDari= $dataMasukan['jabatanDari'];
			$idSrtmasuk = $dataMasukan['idSrtmasuk'];
			
			$sql = "select distinct a.i_memo, c.REV, a.d_memo,
										DATE_FORMAT(a.d_memo, '%Y-%m-%d') as dMemo,
										DATE_FORMAT(a.d_memo, '%H:%i:%s') as jamMemo,
										a.n_dari, a.kd_jabatan_dari, a.n_teruskan,a.kd_jabatan_teruskan
									from tm_memo_keluar a , tm_memo_keluarkepada b, vm_memo_keluar_terakhir c
									where a.i_memo = b.i_memo 
										and a.i_memo = c.i_memo
										and a.id_srtmasuk = '".$idSrtmasuk."'
										and c.REV in (select max(d.REV) from vm_memo_keluar_terakhir d where d.i_memo = a.i_memo) 
										and (a.c_statusmemo != 'kembali' OR a.c_statusmemo is null)";
			if($memoDari){ $sql = $sql."and a.n_dari = '".$memoDari."'  and a.kd_jabatan_dari = '".$jabatanDari."'";	}
			
			$dispo1 = $db->fetchAll($sql);
			
 			   //echo "$sql";	  	 		
			if (count($dispo1) > 0){
				for ($x = 0; $x< count($dispo1); $x++){
					$jabatanDari = $globalReferensi->getNamaJabatan($dispo1[$x]->kd_jabatan_dari);
					$namaJabatanLengkapDari = $jabatanDari." ".$globalReferensi->getNamaOrganisasi($dispo1[$x]->n_dari);
					//$namaJabatanLengkapDari = $ref_serv->getjabatanPegawai($dispo1[$x]->n_dari);
					
					$jabatanKe = $globalReferensi->getNamaJabatan($dispo1[$x]->kd_jabatan_teruskan);
					$namaJabatanLengkapKe = $jabatanKe." ".$globalReferensi->getNamaOrganisasi($dispo1[$x]->n_teruskan);
					$hasil[$x] = array("i_memo" => (string)$dispo1[$x]->i_memo,
										"REV" => (string)$dispo1[$x]->REV,
										"d_memo" => (string)$dispo1[$x]->d_memo,
										"dMemo" => (string)$dispo1[$x]->dMemo,
										"jamMemo" => (string)$dispo1[$x]->jamMemo,
										"n_dari" => (string)$dispo1[$x]->n_dari,
										"kd_jabatan_dari" => (string)$dispo1[$x]->kd_jabatan_dari,
										"namaJabatanLengkapDari" =>$namaJabatanLengkapDari,
										"n_kepada" => (string)$dispo1[$x]->n_teruskan,
										"kd_jabatan_kepada" => (string)$dispo1[$x]->kd_jabatan_teruskan,
										"namaJabatanLengkapKe" =>$namaJabatanLengkapKe
										);
				}
			}
			else {
				$hasil = null;
			}
			
			return $hasil;
		} catch (Exception $e) {
			echo $e->getMessage().'<br>';
			return 'Data tidak ada <br>';
		}
	 
	}
	
	public function getSLA($date1, $date2) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$globalReferensi = new globalReferensi();
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			//cari lama proses
			//================
			
			$sla = $db->fetchAll("select hour(timediff('$date1', '$date2'))/24 as hari,
									 hour(timediff('$date1', '$date2')) mod 24 as jam,
									 minute(timediff('$date1', '$date2'))as menit,
									 second(timediff('$date1', '$date2'))as detik");
			

		 if (count($sla) > 0){
				for ($x = 0; $x< count($sla); $x++){
					$hasil = array("hari" => (string)$sla[$x]->hari,
									"jam" => (string)$sla[$x]->jam,
										"menit" => (string)$sla[$x]->menit,
										"detik" => (string)$sla[$x]->detik);
				}
			}
			else {
				$hasil = null;
			} 
			
			return $hasil;
		} catch (Exception $e) {
			echo $e->getMessage().'<br>';
			return 'Data tidak ada <br>';
		}
	 
	}		
}
?>