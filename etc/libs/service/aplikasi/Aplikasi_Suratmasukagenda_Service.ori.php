<?php
require_once "share/globalReferensi.php";
require_once "share/gen_nosurat.php";
require_once 'Zend/Date.php';
require_once 'Zend/Locale.php';
require_once 'Zend/Date/Cities.php';
require_once 'service/aplikasi/Aplikasi_Referensi_Service.php';

class Aplikasi_Suratmasukagenda_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//======================================================================
	// List Pengumuman
	//======================================================================
	public function cariAgendamasukList(array $dataMasukan) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		$pageNumber 	= $dataMasukan['pageNumber'];
		$itemPerPage 	= $dataMasukan['itemPerPage'];
		$kategoriCari 	= $dataMasukan['kategoriCari'];
		$katakunciCari 	= $dataMasukan['katakunciCari'];
		$dTglCari1 		= $dataMasukan['dTglCari1'];
		$dTglCari2	 	= $dataMasukan['dTglCari2'];
		$dTglCari	 	= $dataMasukan['dTglCari'];
		$sortBy			= $dataMasukan['sortBy'];
		$sort			= $dataMasukan['sort'];
		$kdOrgLogin		= $dataMasukan['kdOrgLogin'];
		$userLogin		= $dataMasukan['userLogin'];
		
		//echo $userLogin;
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (a.c_statusdeleteAgendamasuk != 'Y' or a.c_statusdeleteAgendamasuk is null) and 
								(a.c_statusdeleteKepada != 'Y' or a.c_statusdeleteKepada is null) ";
			if($kategoriCari == 'semua'){
				$whereOpt = "";
			}
			else {
				if ($kategoriCari == 'periode_d_tanggalmasuk') {
					$whereOpt = "a.d_tglpengirim between '$dTglCari1' and '$dTglCari2' ";
				} else if ($kategoriCari == 'd_tanggalmasuk') {
					$whereOpt = "a.d_tglpengirim like '$dTglCari%' ";
				} else {
					$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
				}
			}
			
			// perubahan hendar 13-10-2009 jakarta $whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin' OR a.kd_org = '1' OR a.kd_org = '2')";

$whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin')";
if (($kdOrgLogin=='1')||($kdOrgLogin=='2'))	
{
	
	$whereByOrg = $whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%')";

}		
			if(($kategoriCari) && ($kategoriCari != 'semua')) { $where = $whereBase." and ".$whereOpt." and ".$whereByOrg;} 
			else { $where = $whereBase." and ".$whereByOrg;}
			
			$order = "order by ($sortBy+0) $sort ";
			$sqlProses = "select distinct a.idAgendasrt,
							a.i_agendasrt,
							a.d_tanggalmasuk,
							a.c_srtjenis,
							a.n_srtjenis,
							a.c_srtsifat,
							a.n_srtsifat,
							a.i_srtpengirim,
							a.d_tglpengirim,
							a.n_srtperihal,
							a.n_srtlampiran,
							a.n_srtpengirim,
							a.i_agendalalusrt,
							a.d_tgldariwaseskab,
							a.n_srt_lokasi,
							a.n_kepada,
							a.nip,
							a.kd_org,
							a.kd_jabatan,
							a.nm_jabatan,
							a.i_entry
						from vm_suratmasuk a
						left join tm_disposisi b on (a.id_srtmasuk = b.id_srtmasuk)";	

			if(($pageNumber==0) && ($itemPerPage==0))
			{	
			
				$sqlTotal = "select count(*) from ($sqlProses $where group by a.i_agendasrt) a";
				
				$hasilAkhir = $db->fetchOne($sqlTotal);	
			}
			else
			{
				$sqlData = $sqlProses.$where." group by a.i_agendasrt $order"." limit $xLimit offset $xOffset";
				$result = $db->fetchAll($sqlData);	
			}
			//echo $sqlData;		
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$globalReferensi = new globalReferensi();
				$namaJabatanLengkap = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);
				$nm_org = $globalReferensi->getNamaOrganisasi($result[$j]->kd_org); 
				/*if(!$result[$j]->nip)
					$n_tujuansrt = $n_kepada;
				else
					$n_tujuansrt = $result[$j]->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result[$j]->kd_org);*/
				$hasilAkhir[$j] = array("id_srtmasuk"		=>(string)$result[$j]->idAgendasrt,	
										"i_agendasrt"		=>(string)$result[$j]->i_agendasrt,	
										"d_tanggalmasuk"	=>(string)$result[$j]->d_tanggalmasuk,	
										//"n_tujuansrt"		=>$n_tujuansrt,
										"n_kepada"			=>(string)$result[$j]->n_kepada,
										"nip"				=>(string)$result[$j]->nip,
										"kd_jabatan"		=>(string)$result[$j]->kd_jabatan,
										"namaJabatan"		=>(string)$result[$j]->nm_jabatan,
										"kd_org"			=>(string)$result[$j]->kd_org,
										"namaJabatanLengkap"=>$namaJabatanLengkap,
										"nm_org"			=> $nm_org,
										"c_srtjenis"        =>(string)$result[$j]->c_srtjenis,
										"n_srtjenis"        =>(string)$result[$j]->n_srtjenis,										
										"c_srtsifat"        =>(string)$result[$j]->c_srtsifat,
										"n_srtsifat"        =>(string)$result[$j]->n_srtsifat,
										"i_srtpengirim"     =>(string)$result[$j]->i_srtpengirim,   
										"d_tglpengirim"     =>(string)$result[$j]->d_tglpengirim,   
										"n_srtperihal"      =>(string)$result[$j]->n_srtperihal,    
										"n_srtlampiran"     =>(string)$result[$j]->n_srtlampiran,   
										"n_srtpengirim"     =>(string)$result[$j]->n_srtpengirim,   
										"i_agendalalusrt"   =>(string)$result[$j]->i_agendalalusrt, 
										"d_tgldariwaseskab" =>(string)$result[$j]->d_tgldariwaseskab,
										"n_srt_lokasi" 		=>(string)$result[$j]->n_srt_lokasi,
										"i_entry" 		=>(string)$result[$j]->i_entry
										);
			}	
			unset($dataMasukan);
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	
	
	public function agendaList() {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select distinct(i_agendasrt) i_agendasrt, n_srtperihal
							from tm_surat_masuk 
							where (c_statusdelete != 'Y' or c_statusdelete is null)";

			$sqlData = $sqlProses;
			$hasilAkhir = $db->fetchCol($sqlData);	
			
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function agendamasukInsert(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->beginTransaction();
			$gen_nosurat = new gen_nosurat();
			$iAgendamasuk = $gen_nosurat->genNoAgendamasuk('AGENDA SURAT MASUK', date("Y-m-d"));
			$paramInput = array("i_agendasrt"	    => $iAgendamasuk,	
								"id_tandaterima"	=> $dataMasukan['id_tandaterima'],
								"c_srtjenis"        => $dataMasukan['c_srtjenis'],                          
								"c_srtsifat"        => $dataMasukan['c_srtsifat'],                          
								"i_srtpengirim"     => $dataMasukan['i_srtpengirim'],  
								"n_srtpengirim"		=> $dataMasukan['n_srtpengirim'],								
								"d_tglpengirim"     => $dataMasukan['d_tglpengirim'],                         
								"n_srtperihal"      => $dataMasukan['n_srtperihal'],                          
								"n_srtlampiran"     => $dataMasukan['n_srtlampiran'],                         
								"c_satuanlampiran"  => $dataMasukan['c_satuanlampiran'],                         
								"i_agendalalusrt"   => $dataMasukan['i_agendalalusrt'],
								"d_tgldariwaseskab" => $dataMasukan['d_tgldariwaseskab'],
								"n_srt_lokasi" 		=> "",
								"i_entry"       	=> $dataMasukan['i_entry'],
								"d_entry"			=> date('Y-m-d'));
								
					
			$db->insert('tm_surat_masuk',$paramInput); 
			$idSrtmasuk = $db->fetchOne("select max(id) from tm_surat_masuk where i_agendasrt = '$iAgendamasuk'");
			$paramInputSrtKepada = array("id_srtmasuk"	    => $idSrtmasuk,	
										"n_kepada"    	=> $dataMasukan['n_kepada'],  
										"nip"    		=> $dataMasukan['nip'],
										"kd_jabatan"    => $dataMasukan['kd_jabatan'],
										"kd_org"    	=> $dataMasukan['kd_org'],
										"c_srtjenis"	=> $dataMasukan['c_srtjenis'],
										"i_entry"       =>$dataMasukan['i_entry']);
			
			
			$db->insert('tm_suratkepada',$paramInputSrtKepada); 
			$db->commit();
			
			return "$iAgendamasuk~sukses";
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal";
				//return $e->getMessage();
			}
	   }
	}

	public function agendamasukIdsys($iAgendasrt)
	{
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select id	
							from tm_surat_masuk 
							where (c_statusdelete != 'Y' or c_statusdelete is null) 
								and i_agendasrt= '$iAgendasrt' ";	

			$result = $db->fetchOne($sqlProses);	
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function agendamasukPerihal($iSrtmasuk)
	{
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$sqlProses = "select n_srtperihal	
							from tm_surat_masuk 
							where (c_statusdelete != 'Y' or c_statusdelete is null) 
								and i_agendasrt= '$iSrtmasuk' ";	

			$result = $db->fetchOne($sqlProses);	
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function updateLokasiFile(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
					
			$paramInput = array("n_srt_lokasi"	=>$dataMasukan['n_srt_lokasi'],
							   "i_entry"       	=>$dataMasukan['i_entry'],
							   "d_entry"       	=>date("Y-m-d"));

									
			$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
			$db->update('tm_surat_masuk',$paramInput, $where);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function updateTanggalmasuk(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
        
		try {
			$db->beginTransaction();
			
			$suratBaru = $db->fetchOne("select count(*) from vm_suratmasuk where idAgendasrt = '".$dataMasukan['id_agendasrt']."' and d_tanggalmasuk is not null");
			/* echo "select count(*) from vm_suratmasuk where idAgendasrt = '".$dataMasukan['id_agendasrt']."' and d_tanggalmasuk is not null ||| 
			$suratBaru";
			 */
			if($suratBaru == 0){
				$paramInput = array("d_tanggalmasuk"=>new Zend_Db_Expr('NOW()'),
								   "i_entry"       	=>$dataMasukan['i_entry']);
				$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
				$db->update('tm_surat_masuk',$paramInput, $where);
			
				$db->commit();
			}
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}
	
	public function detailagendamasukById($idAgendasrt) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$where = "where idAgendasrt = '$idAgendasrt' ";
			$sqlProses = "select idAgendasrt,
							i_agendasrt,
							id_tandaterima,
							i_tandaterima,
							d_tanggalmasuk,
							c_srtjenis,
							n_srtjenis,
							c_srtsifat,
							n_srtsifat,
							i_srtpengirim,
							d_tglpengirim,
							n_srtperihal,
							n_srtlampiran,
							c_satuanlampiran,
							n_srtpengirim,
							i_agendalalusrt,
							d_tgldariwaseskab,
							n_srt_lokasi,
							n_kepada,
							nip,
							kd_org,
							kd_jabatan,
							nm_jabatan
						from vm_suratmasuk ";	

			$sqlData = $sqlProses.$where;
 			$result = $db->fetchRow($sqlData);	
			
			$globalReferensi = new globalReferensi();
			$namaJabatanLengkap = $result->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result->kd_org);
			if(!$result->nip)
				$n_tujuansrt = $n_kepada;
			else
				$n_tujuansrt = $result->nm_jabatan." ".$globalReferensi->getNamaOrganisasi($result->kd_org);
			
			$nm_org = $globalReferensi->getNamaOrganisasi($result->kd_org);
			
			$this->ref_serv = Aplikasi_Referensi_Service::getInstance();
			$n_satLampiran = $db->fetchOne("select nm_satuanlampiran from tr_satuanlampiran where id = '".$result->c_satuanlampiran."'");
			$hasilAkhir = array("id_srtmasuk"		=>(string)$result->idAgendasrt,
								"i_agendasrt"		=>(string)$result->i_agendasrt,	
								"id_tandaterima"	=>(string)$result->id_tandaterima,	 
								"i_tandaterima"		=>(string)$result->i_tandaterima,	
								"d_tanggalmasuk"	=>(string)$result->d_tanggalmasuk,	
								"n_srtkepada"		=>(string)$result->n_kepada,
								"nip"				=>(string)$result->nip,									
								"kd_jabatan"		=>(string)$result->kd_jabatan,
								"nm_jabatan"		=>(string)$result->nm_jabatan,
								"kd_org"			=>(string)$result->kd_org,
								"nm_org"			=>$nm_org,
								"nm_jabatan_lengkap"=>$namaJabatanLengkap, 
								"c_srtjenis"        =>(string)$result->c_srtjenis,
								"n_srtjenis"        =>(string)$result->n_srtjenis,										
								"c_srtsifat"        =>(string)$result->c_srtsifat,
								"n_srtsifat"        =>(string)$result->n_srtsifat,
								"i_srtpengirim"     =>(string)$result->i_srtpengirim,   
								"d_tglpengirim"     =>(string)$result->d_tglpengirim,   
								"n_srtperihal"      =>(string)$result->n_srtperihal,    
								"n_srtlampiran"     =>(string)$result->n_srtlampiran,   
								"c_satuanlampiran"	=>(string)$result->c_satuanlampiran,
								"nm_satLampiran"		=>$n_satLampiran,
								"n_srtpengirim"     =>(string)$result->n_srtpengirim,   
								"i_agendalalusrt"   =>(string)$result->i_agendalalusrt, 
								"d_tgldariwaseskab" =>(string)$result->d_tgldariwaseskab,
								"n_srt_lokasi" 		=>(string)$result->n_srt_lokasi
								);
			var_dump($hasilAkhir);
			return $hasilAkhir;						  
			 
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function agendamasukUpdate(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			$paramInputSrtKepada = array("n_kepada"    	=> $dataMasukan['n_kepada'],  
										"nip"    		=> $dataMasukan['nip'],
										"kd_jabatan"    => $dataMasukan['kd_jabatan'],
										"kd_org"    	=> $dataMasukan['kd_org'],
										"i_entry"       =>$dataMasukan['i_entry']);

						
			$whereSrtKepada[] = "id_srtmasuk = '".$dataMasukan['id_agendasrt']."'";
				
			$db->update('tm_suratkepada',$paramInputSrtKepada, $whereSrtKepada);
			
			$paramInput = array("i_agendasrt"	    => $dataMasukan['i_agendasrt'],	
								"id_tandaterima"	=> $dataMasukan['id_tandaterima'],
								"c_srtjenis"        => $dataMasukan['c_srtjenis'],                          
								"c_srtsifat"        => $dataMasukan['c_srtsifat'],                          
								"i_srtpengirim"     => $dataMasukan['i_srtpengirim'],  
								"n_srtpengirim"		=> $dataMasukan['n_srtpengirim'],								
								"d_tglpengirim"     => $dataMasukan['d_tglpengirim'],                         
								"n_srtperihal"      => $dataMasukan['n_srtperihal'],                          
								"n_srtlampiran"     => $dataMasukan['n_srtlampiran'],                         
								"c_satuanlampiran"  => $dataMasukan['c_satuanlampiran'],                         
								"i_agendalalusrt"   => $dataMasukan['i_agendalalusrt'],
								"d_tgldariwaseskab" => $dataMasukan['d_tgldariwaseskab'],
								"i_entry"       	=>$dataMasukan['i_entry']);

			if($dataMasukan['n_srt_lokasi'])
			{
				$paramInput["n_srt_lokasi"] = $dataMasukan['n_srt_lokasi'];
			}

			
			$where[] = "id = '".$dataMasukan['id_agendasrt']."'";
		    $db->update('tm_surat_masuk', $paramInput, $where); 
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function agendamasukHapus(array $dataMasukan) {
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		try {
			$db->beginTransaction();
			//$n_srtkepada = $db->fetchOne("select n_srtkepada from tm_surat_masuk where id='".$dataMasukan['id']."'");
			
			$paramInput = array("c_statusdelete"	=> 'Y',
							   "i_entry"       	=>$dataMasukan['i_entry']);	
								
			$where[] = "id = '".$dataMasukan['id']."'";
			
			$db->update('tm_surat_masuk',$paramInput, $where);
			
			$whereKepada[] = "id_srtmasuk = '".$dataMasukan['id']."'";
			$db->update('tm_suratkepada',$paramInput, $whereKepada);
			$db->commit();
			
			return 'sukses';
		} catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
				return "gagal.Data Sudah Ada.";
			}
			else
			{
				return "gagal.";
			}
	   }
	}

	public function getNamaSatuanLampiran($c_satuanlampiran) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			$globalRef = new globalReferensi;
			
			$sqlProses = "select nm_satuanlampiran
							from tr_satuanlampiran
							where nm_satuanlampiran = '$c_satuanlampiran'";	
							
			
			$sqlData = $sqlProses; //.$where;
			$result = $db->fetchOne($sqlData);				
			
			//echo $sqlData;
			return $hasilAkhir;					  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function jmlSuratMasuk($kdOrgLogin, $userLogin) {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
		
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
		 
			$xLimit=$itemPerPage;
			$xOffset=($pageNumber-1)*$itemPerPage;
			
			$whereBase = "where (a.c_statusdeleteAgendamasuk != 'Y' or a.c_statusdeleteAgendamasuk is null) and 
								(a.c_statusdeleteKepada != 'Y' or a.c_statusdeleteKepada is null) and 
								a.d_tanggalmasuk is null";
						
			// perubahan hendar 13-10-2009 jakarta $whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin' OR a.kd_org = '1' OR a.kd_org = '2')";

	$whereByOrg = "(a.i_entry = '$userLogin' or a.kd_org = '$kdOrgLogin' or b.kd_orgteruskanke = '$kdOrgLogin')";
	if (($kdOrgLogin=='1')||($kdOrgLogin=='2'))	
	{
	
		$whereByOrg = $whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%')";

	}		
			$where = $whereBase." and ".$whereByOrg;
			
			$sqlProses = "select distinct a.idAgendasrt,
							a.i_agendasrt,
							a.d_tanggalmasuk,
							a.c_srtjenis,
							a.n_srtjenis,
							a.c_srtsifat,
							a.n_srtsifat,
							a.i_srtpengirim,
							a.d_tglpengirim,
							a.n_srtperihal,
							a.n_srtlampiran,
							a.n_srtpengirim,
							a.i_agendalalusrt,
							a.d_tgldariwaseskab,
							a.n_srt_lokasi,
							a.n_kepada,
							a.nip,
							a.kd_org,
							a.kd_jabatan,
							a.nm_jabatan,
							a.i_entry
						from vm_suratmasuk a
						left join tm_disposisi b on (a.id_srtmasuk = b.id_srtmasuk)";	

			$sqlData = 'select count(*) from ('.$sqlProses.$where.') a';
			$result = $db->fetchOne($sqlData);	
			
			//echo $sqlData;		
			
			return $result;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

}
?>