<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/adm/Adm_Admsifatpenomoransuratkeluar_Service.php";
require_once "service/adm/logfile.php";
require_once "service/sso/Sso_User_Service.php";

class Adm_sifatpenomoransuratkeluarController extends Zend_Controller_Action {
	private $auditor_serv;
	private $id;
	private $kdorg;
		
    public function init() {
		// Local to this controller only; affects all actions, as loaded in init:
		//$this->_helper->viewRenderer->setNoRender(true);
		$registry = Zend_Registry::getInstance();
		$this->view->basePath = $registry->get('basepath'); 
		$this->basePath = $registry->get('basepath'); 
        $this->view->pathUPLD = $registry->get('pathUPLD');
        $this->view->procPath = $registry->get('procpath');
	   // $ssogroup = new Zend_Session_Namespace('ssogroup');
	   //echo "TEST ".$ssogroup->n_user_grp." ".$ssogroup->i_user." ".$ssogroup->i_peg_nip;
	   $this->user  = 'cdr';
	   
		$this->sifatSurat_serv = Adm_Admsifatpenomoransuratkeluar_Service::getInstance();
		$this->sso_serv = Sso_User_Service::getInstance();
		$this->Logfile = new logfile;
	    $ssogroup = new Zend_Session_Namespace('ssogroup');
	   //echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->nip." ".$ssogroup->kd_struktur_org;
		$this->user_id  = $ssogroup->user_id;
		$this->user  = $ssogroup->username;
		$this->kdOrg  = $ssogroup->kd_struktur_org;
		$this->kdJabatan = $ssogroup->kd_jabatan;
		$this->nip = $ssogroup->nip;
		$this->nama = $ssogroup->nama;
		$this->level = $ssogroup->level; 
		$this->nm_level = $ssogroup->nm_level; 
		$this->nm_jabatan = $ssogroup->nm_jabatan; 
		$this->nm_struktur_org = $ssogroup->nm_struktur_org;
		
    }
	
    public function indexAction() {
	   
    }
	
	public function sifatpenomoransuratkeluarjsAction() 
    {
	 header('content-type : text/javascript');
	 $this->render('sifatpenomoransuratkeluarjs');
    }
	
	//test OPen report
	//----------------------
	public function sifatpenomoransuratkeluarlistAction()
	{
		$currentPage = $_REQUEST['currentPage']; 
		//echo $currentPage;
 			
		if((!$currentPage) || ($currentPage == 'undefined'))
		{
			$currentPage = 1;
		} 
		
		$katakunciCari 	= $_POST['carii'];
		$kategoriCari 	= 'n_srtsifat';
		$sortBy			= 'n_srtsifat';
		$sort			= 'asc';
		
		$dataMasukan = array("kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$numToDisplay = 20;
		$this->view->numToDisplay = $numToDisplay;
		$this->view->currentPage = $currentPage;
		$this->view->totSifatSuratList = $this->sifatSurat_serv->cariSifatSuratList($dataMasukan,0,0);
		$this->view->sifatSuratList = $this->sifatSurat_serv->cariSifatSuratList($dataMasukan,$currentPage, $numToDisplay);
	}
	
	public function sifatpenomoransuratkeluarolahdataAction()
	{
		$this->view->jenisForm = $_REQUEST['jenisForm'];
		$c_srtsifat = $_REQUEST['c_srtsifat'];
		//echo "iuser --->".$iUser;
		$this->view->detailSifatSurat = $this->sifatSurat_serv->detailSifatSuratById($c_srtsifat);
	}
	
	public function sifatpenomoransuratkeluarAction()
	{
		$c_srtsifat		= $_POST['c_srtsifat'];       
		$n_srtsifat		= $_POST['n_srtsifat']; 
		$c_format		= $_POST['c_format']; 		
		$i_entry 		= $this->user; 		
		
		$dataMasukan = array("c_srtsifat"  	=>$c_srtsifat,
							"n_srtsifat"  	=>$n_srtsifat,
							"c_format"  	=>$c_format,
							 "i_entry"		=> $i_entry);
		
		$this->view->sifatSuratInsert = $this->sifatSurat_serv->sifatPenomoranSuratKeluarInsert($dataMasukan);
		$this->Logfile->createLog($this->view->namauser, "Insert data sifat penomoran surat keluar", $n_srtsifat." (".$c_srtsifat.")");
		$this->view->proses = "1";	
		$this->view->keterangan = "Sifat Penomoran Surat Keluar";
		$this->view->hasil = $this->view->sifatSuratInsert;
		
		$this->sifatpenomoransuratkeluarlistAction();
		$this->render('sifatpenomoransuratkeluarlist');
	}
	
	public function sifatpenomoransuratkeluarupdateAction()
	{
		$c_srtsifat		= $_POST['c_srtsifat'];
		$c_srtsifatH	= $_POST['c_srtsifatH'];		
		$n_srtsifat		= $_POST['n_srtsifat'];      
		$c_format		= $_POST['c_format'];
		$i_entry 		= $this->user; 
		
		$dataMasukan = array("c_srtsifat"  	=>$c_srtsifat,
							 "c_srtsifatH"  	=>$c_srtsifatH,
							 "n_srtsifat"  	=>$n_srtsifat,
							 "c_format"  	=>$c_format,
							 "i_entry"		=> $i_entry);
		
		$this->view->sifatSuratUpdate = $this->sifatSurat_serv->sifatPenomoranSuratKeluarUpdate($dataMasukan);
		$this->Logfile->createLog($this->view->namauser, "Ubah data sifat penomoran surat keluar", $n_srtsifat." (".$c_srtsifat.")");
		$this->view->proses = "2";	
		$this->view->keterangan = "Sifat Surat";
		$this->view->hasil = $this->view->sifatSuratUpdate;
		
		$this->sifatpenomoransuratkeluarlistAction();
		$this->render('sifatpenomoransuratkeluarlist');
	}
	
	public function sifatpenomoransuratkeluarHapusAction()
	{
		$c_srtsifat 		= $_REQUEST['c_srtsifat'];
		$i_entry 		= $this->user; 
		
		$dataMasukan = array("c_srtsifat" => $c_srtsifat,
							 "i_entry"		=> $i_entry);
		
		$this->view->sifatSuratUpdate = $this->sifatSurat_serv->sifatPenomoranSuratKeluarHapus($dataMasukan);
		$this->Logfile->createLog($this->view->namauser, "Hapus data sifat penomoran surat keluar", $n_srtsifat." (".$c_srtsifat.")");
		$this->view->proses = "3";	
		$this->view->keterangan = "Sifat Surat";
		$this->view->hasil = $this->view->sifatSuratUpdate;
		
		$this->sifatpenomoransuratkeluarlistAction();
		$this->render('sifatpenomoransuratkeluarlist');
	}
	

}
?>