<?php

require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_deskinfo_Service.php";
class Aplikasi_DeskinfoController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();
        $this->view->basePath = $registry->get('basepath');
        $this->deskinfo_serv = aplikasi_deskinfo_Service::getInstance();
        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;
    }

public function indexAction()
{
}
public function deskinfo($id_kuesioner)
{
        //if (!$id_kuesioner){$id_kuesioner = $_POST['idKuesioner'];}
        $this->view->id_kuesioner=$id_kuesioner;
        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $data = $this->deskinfo_serv->getTmDeskinfo($cari);
        $jmldata = count($data);
    if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
                $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
                $this->view->e_layanan=$data[$j]['e_layanan'];
                $this->view->e_layanan_publik=$data[$j]['e_layanan_publik'];
                $this->view->e_staft_pelaksana=$data[$j]['e_staft_pelaksana'];
                $this->view->e_sk_tugas=$data[$j]['e_sk_tugas'];
                $this->view->e_mohon_info=$data[$j]['e_mohon_info'];
                $this->view->e_status_perkara=$data[$j]['e_status_perkara'];
                $this->view->i_entry=$data[$j]['i_entry'];
                $this->view->d_entry=$data[$j]['d_entry'];}
                $this->view->par='Koreksi';

        $databln = $this->deskinfo_serv->getTmDeskinfobulan($cari);
        $jmldatabln = count($databln);
        if (count($databln)!=0){
            for ($j = 0; $j < $jmldatabln; $j++) {
                    $this->view->v_jan=$databln[$j]['v_jan'];
                $this->view->v_feb=$databln[$j]['v_feb'];
                $this->view->v_mar=$databln[$j]['v_mar'];
                $this->view->v_apr=$databln[$j]['v_apr'];
                $this->view->v_mei=$databln[$j]['v_mei'];
                $this->view->v_jun=$databln[$j]['v_jun'];
                $this->view->v_jul=$databln[$j]['v_jul'];
                $this->view->v_agt=$databln[$j]['v_agt'];
                $this->view->v_sep=$databln[$j]['v_sep'];
                $this->view->v_okt=$databln[$j]['v_okt'];
                $this->view->v_nop=$databln[$j]['v_nop'];
                $this->view->v_des=$databln[$j]['v_des'];
            }
        }


    }
    else{$this->view->par='Simpan';}
}
public function deskinfoAction()
{
        $id_kuesioner = $_REQUEST['idKuesioner'];
        $this->deskinfo($id_kuesioner);
}
public function deskinfojsAction()
{
    header('content-type : text/javascript');
    $this->render('deskinfojs');
}

public function maintaindatadeskinfoAction()
{

            $MaintainData1 = array("id_kuesioner"=>$_POST['id_kuesioner'],
                            "e_layanan"=>$_POST['e_layanan'],
                            "e_layanan_publik"=>$_POST['e_layanan_publik'],
                            "e_staft_pelaksana"=>$_POST['e_staft_pelaksana'],
                            "e_sk_tugas"=>$_POST['e_sk_tugas'],
                            "e_mohon_info"=>$_POST['e_mohon_info'],
                            "e_status_perkara"=>$_POST['e_status_perkara'],
                            "i_entry"=>$_POST['i_entry']);

            $MaintainData2 = array("id_kuesioner"=>$_POST['id_kuesioner'],
                                "v_jan"=>$_POST['v_jan'],
                                "v_feb"=>$_POST['v_feb'],
                                "v_mar"=>$_POST['v_mar'],
                                "v_apr"=>$_POST['v_apr'],
                                "v_mei"=>$_POST['v_mei'],
                                "v_jun"=>$_POST['v_jun'],
                                "v_jul"=>$_POST['v_jul'],
                                "v_agt"=>$_POST['v_agt'],
                                "v_sep"=>$_POST['v_sep'],
                                "v_okt"=>$_POST['v_okt'],
                                "v_nop"=>$_POST['v_nop'],
                                "v_des"=>$_POST['v_des'],
                                "i_entry"=>$_POST['i_entry']);

                                    if ($_POST['perintah']=='Koreksi')
                                    {
                                        $hasil = $this->deskinfo_serv->ubahTmDeskinfo($MaintainData1);
                                        if ($hasil=='sukses')
                                        {
                                            $cari = " and id_kuesioner ='".$_POST['id_kuesioner']."' ";

                                            $databln = $this->deskinfo_serv->getTmDeskinfobulan($cari);
                                            //echo "databln = $databln | cari = $cari";
                                            if (count($databln)==0){
                                                $hasil = $this->deskinfo_serv->tambahTmDeskinfobln($MaintainData2);
                                            }
                                            else{
                                            $hasil = $this->deskinfo_serv->ubahTmDeskinfobln($MaintainData2);
                                            }
                                        }
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";}
                                    else
                                    {
                                        $hasil = $this->deskinfo_serv->tambahTmDeskinfo($MaintainData1);
                                        if ($hasil=='sukses')
                                        {
                                            $hasil = $this->deskinfo_serv->tambahTmDeskinfobln($MaintainData2);
                                        }
                                        $this->view->par="simpan";
                                        $par="Simpan ";
                                    }



        $pesan=$par." data ".$hasil;
        $this->view->pesan = $pesan;
        $this->view->pesancek = $hasil;
        $this->deskinfo($_POST['id_kuesioner']) ;
        $this->_helper->viewRenderer('deskinfo');

}

public function hapusAction()
{
    $id_kuesioner=$_GET['id_kuesioner'];
    $MaintainData1 = array("id_kuesioner"=>$_GET['id_kuesioner']);

    $hasil = $this->deskinfo_serv->hapusTmDeskinfo($MaintainData1);
    $hasil = $this->deskinfo_serv->hapusTmDeskinfobln($MaintainData1);
    $this->view->par='Simpan';
    $this->_helper->viewRenderer('deskinfo');

}

}
?>
