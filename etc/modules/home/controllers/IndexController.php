<?php
require_once 'Zend/Controller/Action.php';
//require_once 'Zend/Session.php';
require_once "service/sso/Sso_User_Service.php";
require_once "service/adm/Adm_Admuser_Service.php";
/* require_once "service/aplikasi/Aplikasi_Pengumuman_Service.php";
require_once "service/aplikasi/Aplikasi_Suratmasukagenda_Service.php";
require_once "service/srtmemo/srtmemo_keluar_Service.php";
require_once "service/adm/Adm_Admuser_Service.php";
require_once "service/adm/Adm_Admuser_Service.php";
require_once "service/adm/logfile.php";
require_once "service/aplikasi/Aplikasi_Agenda_Service.php";
require_once "service/aplikasi/Aplikasi_Suratmasukdisposisi_Service.php" */;
require_once "share/format_date.php";

require_once "service/aplikasi/Aplikasi_Refpengadilan_Service.php";



class Home_IndexController extends Zend_Controller_Action {
    private $sso_serv;
	
    public function init() {
       // Local to this controller only; affects all actions, as loaded in init:
	   //UNTUK SETTING GLOBAL BASE PATH
	   $registry = Zend_Registry::getInstance();
	   $this->auth = Zend_Auth::getInstance();
	   $this->view->basePath = $registry->get('basepath');
	   $this->view->baseData = $registry->get('baseData');
	   $this->sso_serv = Sso_User_Service::getInstance();
	   /*$this->adm_serv = Adm_Admuser_Service::getInstance();
	    $this->pengumuman_serv = Aplikasi_Pengumuman_Service::getInstance();
	   $this->agenda_serv = Aplikasi_Agenda_Service::getInstance();
	   $this->user_serv = Adm_Admuser_Service::getInstance();
	   $this->agendamasuk_serv = Aplikasi_Suratmasukagenda_Service::getInstance();
	   $this->serv_memo = srtmemo_keluar_Service::getInstance();
	   $this->user_serv = Adm_Admuser_Service::getInstance();
	   $this->ref_serv = Aplikasi_Referensi_Service::getInstance();
	   $this->disposisi_serv = Aplikasi_Suratmasukdisposisi_Service::getInstance(); */
	   $this->refPengadilan_serv = Aplikasi_Refpengadilan_Service::getInstance();
	   $this->user_serv = Adm_Admuser_Service::getInstance();
	   //$this->Logfile = new logfile;
	  // $this->serv_notifikasi = ntfy_notifikasi_Service::getInstance();
	  
/* 	  $ssogroup = new Zend_Session_Namespace('ssogroup');
	  $errlogin = new Zend_Session_Namespace('errlogin');
 */	  
 
	$ssogroup = new Zend_Session_Namespace('ssogroup');
	  //echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->nip." ".$ssogroup->passwd;

	//if ($ssogroup->user_id && $ssogroup->username && $ssogroup->ipCurrentLogin){
		$this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		$this->passwd  = $ssogroup->passwd;
	//}
		
	  //$this->errcount = $errlogin->errcount;
	 //echo "TEST INIT INDEX ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->nip." ".$ssogroup->kd_struktur_org;
    }
	
    public function indexAction() {
		//var_dump($_POST);
		//var_dump($ssogroup);
		Zend_Session::namespaceUnset('ssogroup');	
		Zend_Session::destroy(true);
		
		$ssogroup = new Zend_Session_Namespace('ssogroup');
	   // echo "TEST ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->i_organisasi;
	    $this->userid  = $ssogroup->user_id;
		$this->username  = $ssogroup->username;
		$this->i_organisasi  = $ssogroup->i_organisasi;	
		$this->c_kategori_organisasi  = $ssogroup->c_kategori_organisasi;			
		
		//echo "TESTxxx ".$ssogroup->user_id." ".$ssogroup->username." ".$ssogroup->nip." ".$ssogroup->passwd;
			//indexAction default kepunyaan Home_IndexController dalam modul home
		/*$this->view->p = $_REQUEST['p'];
		$this->view->user_id = $_REQUEST['u'];
		$this->view->username = $this->sso_serv->getUsername($this->view->user_id);
		
		$request = $this->getRequest();   
	    $this->view->nonaktif = $_REQUEST['nonaktif'];
		$this->view->errorlogin = $_REQUEST['errorlogin'];
		
	    echo $this->view->nonaktif."xxxxxxxx".$this->view->errorloginn;
		$ssogroup = new Zend_Session_Namespace('ssogroup');
	    echo "<br>user = ".$ssogroup->user_id;
		
		$ns = new Zend_Session_Namespace('HelloWorld'); 
	    
		if(!isset($ns->yourLoginRequest)){ 
			$ns->yourLoginRequest = 1; 
		}else{ 
			$ns->yourLoginRequest++; 
		} 
		
		$this->view->checksess = $ns->yourLoginRequest;*/

		//added by Bambang 0n 01272011 

			$this->view->dataOrganisasiBanding = $this->refPengadilan_serv->organisasiTkIList('all');
			$dataMasukan = array("idPengadilanBanding" => $this->view->dataOrganisasiBanding[0]['i_organisasi']);
			$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);

			$id_organisasiTk2 = $_REQUEST['id_organisasiTk2'];
			$dataMasukan2 = array("i_organisasi" => $id_organisasiTk2);
			$this->view->dataUserTk2 = $this->user_serv->userByIdOrgList($dataMasukan2);
   }
	public function organisasitk2Action(){
		$idOrgTkI = $_REQUEST['idOrgTkI'];
		$dataMasukan = array("idPengadilanBanding" => $idOrgTkI);
		$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);

    }
	public function usertk2Action(){
		$idOrgTk2 = $_REQUEST['idOrgTk2'];
		$this->view->idOrgTk2 = $idOrgTk2;
		$dataMasukan = array("i_organisasi" => $idOrgTk2);
		$this->view->dataUserTk2 = $this->user_serv->userByIdOrgList($dataMasukan);
	}	
	public function mainAction(){
		//var_dump($ssogroup);
	    $username = $_POST['username'];
		$paswd = $_POST['password'];
		
		$idOrgTkI = $_REQUEST['idOrgTkI'];
		if (!$idOrgTkI){
			$dataMasukan = array("idPengadilanBanding" => $idOrgTkI);
			$this->view->dataOrganisasiTk2 = $this->refPengadilan_serv->pengadilanList($dataMasukan);
		}

		$idOrgTk2 = $_GET['idOrgTk2'];
		if (!$idOrgTk2){
			$this->view->idOrgTk2 = $idOrgTk2;
			$dataMasukan = array("i_organisasi" => $idOrgTk2);
			$this->view->dataUserTk2 = $this->user_serv->userByIdOrgList($dataMasukan); 
		}
		if (!$paswd){ $paswd = $this->passwd;}
		
		$ssogroup = new Zend_Session_Namespace('ssogroup');
	    // catat ip address ip address user yg login

		if (getenv("HTTP_X_FORWARDED_FOR")) 
		$ipCurrentLogin = getenv("HTTP_X_FORWARDED_FOR"); 
		else if(getenv("REMOTE_ADDR")) 
		$ipCurrentLogin = getenv("REMOTE_ADDR"); 
		 
 // $ip = getenv("REMOTE_ADDR"); 

		if (!$username){ 
			$username = $this->userid;
			//echo "<br>ffff<br>";
		}
		
		if (!$paswd){ $paswd = $this->passwd;}
		$this->view->username = $username;
		$hasiluser = $this->sso_serv->getDataUser($username, $paswd);

		if($hasiluser){
			//check apakah user sedang login aplikasi
			//==================================
			//echo $this->sso_serv->isUserLogon($username)." == ".$ipCurrentLogin;
			
			if ($this->sso_serv->isUserLogon($username) == 'login'){
			
				if ($this->sso_serv->getIpUserLogon($username) == $ipCurrentLogin) {
					$statusLogin = 'OK';
					
				} else {
					$statusLogin = 'NOT OK';
					$pesan = "Anda sedang login di komputer dengan ip ".$this->sso_serv->getIpUserLogon($username);
					$option = array("username" => $username,
									"passwd" => "",
									"pesan" => $pesan);
				}
			} else {
				$statusLogin = 'OK';					
			}
				
			if ($statusLogin == 'OK'){
				$ssogroup->user_id			= $username;	
				$ssogroup->username     	= $hasiluser->n_user;
				$ssogroup->passwd     		= $paswd;
				$ssogroup->i_organisasi		= $hasiluser->i_organisasi;
				$ssogroup->c_kategori_organisasi = $hasiluser->c_kategori_organisasi;
		
				$this->view->user_id = $ssogroup->user_id;
				$this->view->username = $ssogroup->username;
				$this->view->i_organisasi = $ssogroup->i_organisasi;
				$this->view->c_kategori_organisasi = $ssogroup->c_kategori_organisasi;
				$masukan = array("iOrganisasi"  => $this->view->i_organisasi);
				$this->view->n_organisasi = $this->refPengadilan_serv->getnamapengadilan($masukan);
				
			} else {
				$this->view->pesanKesalahan = $pesan;
				$this->indexAction();
				$this->render('index');
			}
		}  else {
			$pesan = "Nama Pengguna atau Kata Sandi Salah.";
			$this->view->pesanKesalahan = $pesan;
			$this->indexAction();
			$this->render('index');
		}
	}
	
	public function runningtextAction() {
		$pageNumber 	= 1;
		$itemPerPage 	= 20;
		$kategoriCari 	= 'c_statusaktif';
		$katakunciCari 	= 'Y';
		$sortBy			= 'e_pengumuman';
		$sort			= 'asc';
		
		$dataMasukan = array("pageNumber" => $pageNumber,
							"itemPerPage" => $itemPerPage,
							"kategoriCari" => $kategoriCari,
							"katakunciCari" => $katakunciCari,
							"sortBy" => $sortBy,
							"sort" => $sort);
		
		$this->view->pengumumanList = $this->pengumuman_serv->cariPengumumanList($dataMasukan);
	}
	
	public function notifikasiAction() {
		$this->view->jmlsrtmasuk = $this->agendamasuk_serv->jmlSuratMasuk($this->kdOrg, $this->user, $this->nip);
		$this->view->jmlmemomasuk = $this->serv_memo->jmlmemomasuk($this->kdOrg, $this->user);
		$this->view->jmldispomasuk = $this->disposisi_serv->jmlDispoMasuk($this->nip);
		$hasil = $this->agenda_serv->tulisEvent($this->view->nip);
		//var_dump($hasil);
		$this->view->totalsurat = ($this->view->jmlsrtmasuk *1) +($this->view->jmlmemomasuk *1) +($this->view->jmldispomasuk);
	}
	
	//public function notifsuratmasukAction($kdOrg) {
		
	//	$this->view->jmlsrtmasuk = $this->agendamasuk_serv->jmlSuratMasuk($kdOrg);
	//}
	
	public function testloginAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$username = $_REQUEST['username'];
		$paswd = $_REQUEST['paswd'];
		
		$hasiluser = $this->sso_serv->testLogin($username,$paswd);
		
		echo trim($hasiluser);
	}
	
	public function nonaktifuserAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$username = $_REQUEST['username'];
		
		$hasiluser = $this->sso_serv->nonAktifUser($username);
		
		echo trim($hasiluser);
	}
	
	public function headerAction() {
	   //loginAction default kepunyaan Home_IndexController dalam modul home
    }
	
	public function usergantipasswdAction()
	{
		$user_id		= $_POST['user_id'];       
		$username		= $_POST['username'];      
		$nip			= $_POST['nip'];           
		$kd_status		= $_POST['kd_status'];     
		$password		= $_POST['password'];
		$c_group		= $_POST['c_group'];
		
		
		$dataMasukan = array("user_id"  	=>$user_id,
							"username"  	=>$username,
							"nip" 			=>$nip,
							"kd_status" 	=>$kd_status,
							"password"  	=>$password,
							"c_group"  		=>$c_group);
		
		$this->view->userUpdate = $this->user_serv->userUpdate($dataMasukan);
		$this->Logfile->createLog($this->view->namauser, "Ganti Password", $username." (".$nip.")");
		$this->view->proses = "2";	
		$this->view->keterangan = "User";
		$this->view->hasil = $this->view->userUpdate;
		
		$this->halamandepanAction();
		$this->render('halamandepan');
	}
	
	public function batalAction()
	{
		$this->halamandepanAction();
		$this->render('halamandepan');
	}

	public function halamandepanAction() {
		$this->view->user_id = $ssogroup->user_id;
		$this->view->username = $ssogroup->username;
		$this->view->nip = $ssogroup->nip;
		$this->view->nama = $ssogroup->nama;
		$this->view->kd_struktur_org = $ssogroup->kd_struktur_org;
		$this->view->kd_jabatan = $ssogroup->kd_jabatan;
		$this->view->level = $ssogroup->level;
		$this->view->nm_level = $ssogroup->nm_level;
		$this->view->nm_jabatan = $ssogroup->nm_jabatan;
		$this->view->nm_struktur_org = $ssogroup->nm_struktur_org;
		$this->view->c_group =$ssogroup->c_group;
		$this->runningtextAction();
    }
	
	public function caridataAction(){
		$format_date = new format_date();
		$kdOrgLogin 	= $this->kdOrg;
		$userLogin		= $this->user;
		$nipuserLogin	= $this->nip;
		
		
		$dasar=$_GET['dasar'];
		$target=$_GET['target'];
		if(($dasar == 'd_tglpengirim') || ($dasar == 'd_tanggalmasuk')){
			$target = $format_date->convertTglHumanToMachine($target);
		}
		
		$whereBase = "where (a.c_statusdeleteAgendamasuk != 'Y' or a.c_statusdeleteAgendamasuk is null) ";
								
		if ($dasar!='semua'){
			if(($dasar == 'd_tglpengirim') || ($dasar == 'd_tanggalmasuk')){
				$cari="$whereBase and $dasar = '$target'";
			} else {
				$cari="$whereBase and $dasar like '%$target%'";
			}
		} else {
			$cari= $whereBase;
		}

		if(($kdOrgLogin!='9') && ($kdOrgLogin!='30') && ($kdOrgLogin!='31') && ($kdOrgLogin!='32') && 
			    ($kdOrgLogin!='170') && ($kdOrgLogin!='174') && ($kdOrgLogin!='178') && ($kdOrgLogin!='182') && ($kdOrgLogin!='186') &&
				($kdOrgLogin!='190') && ($kdOrgLogin!='194') && ($kdOrgLogin!='197') &&
				($kdOrgLogin!='171') && ($kdOrgLogin!='172') && ($kdOrgLogin!='173') &&
				($kdOrgLogin!='175') && ($kdOrgLogin!='176') && ($kdOrgLogin!='177') &&
				($kdOrgLogin!='179') && ($kdOrgLogin!='180') && ($kdOrgLogin!='181') &&
				($kdOrgLogin!='183') && ($kdOrgLogin!='184') && ($kdOrgLogin!='158') &&
				($kdOrgLogin!='187') && ($kdOrgLatogin!='188') && ($kdOrgLogin!='189') &&
				($kdOrgLogin!='191') && ($kdOrgLogin!='192') && ($kdOrgLogin!='193') &&
				($kdOrgLogin!='195') && ($kdOrgLogin!='196') && ($kdOrgLogin!='198') && ($kdOrgLogin!='199'))
			{
				$cari = "$cari and (a.kd_orgKepada = '$kdOrgLogin' or 
								(a.kd_orgteruskanke = '$kdOrgLogin' and a.n_teruskanke='$nipuserLogin'))";
			}
			
			if (($kdOrgLogin=='0')||($kdOrgLogin=='1'))	
			{
				//$whereByOrg = $whereByOrg."or(upper(n_kepada) like '%PRESIDEN%' or upper(n_kepada) like '%SESKAB%' or upper(n_kepada) like '%SEKRETARIS KABINET%')";
				$cari = "$cari or (a.kd_orgKepada = '0' or a.umkd_orgKepada ='1') or
										(upper(a.n_kepada) = 'PRESIDEN' or UPPER(a.n_kepada) = 'SESKAB')";
		        //$whereByOrg = '';
			}
		$this->view->suratcari = $this->agendamasuk_serv->getCariData($cari);
	
	}
	public function caridatadetilAction(){
		$i_agendasrt=$_GET['i_agendasrt'];
		$cari="where a.i_agendasrt ='$i_agendasrt'";
	
		$this->view->suratcari = $this->agendamasuk_serv->getCariDataDetail($cari);
		
		$suratMasuk = $this->view->suratcari;
		
		
		if(count($suratMasuk) > 0){
			$cari = array("dari" => $suratMasuk[0]['kd_orgKepada'],
						  "idSrtmasuk" => $suratMasuk[0]['id_srtmasuk']);
						  
			$dispo1 = $this->agendamasuk_serv->getDisposisi1($cari);
		}
		//var_dump($dispo1);
	
		$this->view->disposisi1 = $dispo1;
		
		$cari2 = array("dari" => $suratMasuk[0]['kd_orgKepada'],
						  "idSrtmasuk" => $suratMasuk[0]['id_srtmasuk'],
						  "kd_jabatandari" => $suratMasuk[0]['kd_jabatanKepada']);
		//var_dump($suratMasuk);
		$this->view->memo = $this->agendamasuk_serv->getMemo($cari2);
		
	}
	
	public function logoutAction(){
		$this->sso_serv->emptyIpLogin($this->user);
		//Zend_Session::destroy(true);
		Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index');
		/* $this->indexAction();
		$this->render('index'); */
	}
}
?>