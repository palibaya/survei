<?php
require_once 'Zend/Controller/Action.php';
require_once 'Zend/Auth.php';
require_once "service/aplikasi/Aplikasi_software_Service.php";

class Aplikasi_SoftwareController extends Zend_Controller_Action {

public function init() {
        $registry = Zend_Registry::getInstance();

        $this->view->basePath = $registry->get('basepath');
        $this->software_serv = aplikasi_software_Service::getInstance();
        $ssogroup = new Zend_Session_Namespace('ssogroup');
        $this->userid  = $ssogroup->user_id;
        $this->username  = $ssogroup->username;
        $this->i_organisasi  = $ssogroup->i_organisasi;

    }

public function indexAction()
{
}
public function software($id_kuesioner)
{

        $cari = " and id_kuesioner ='$id_kuesioner' ";
        $this->view->datasoftware = $this->software_serv->getTmSoftwareAdm($cari);
        $this->view->datasoftwarekendala = $this->software_serv->getTmSoftwareKendala($cari);

        $data = $this->software_serv->getTmSoftware($cari);
        $jmldata = count($data);
if (count($data)!=0){
        for ($j = 0; $j < $jmldata; $j++) {
            $this->view->id_kuesioner=$data[$j]['id_kuesioner'];
            $this->view->i_xp=$data[$j]['i_xp'];
            $this->view->i_vista=$data[$j]['i_vista'];
            $this->view->i_seven=$data[$j]['i_seven'];
            $this->view->i_linux=$data[$j]['i_linux'];
            $this->view->i_dos=$data[$j]['i_dos'];
            $this->view->i_ext=$data[$j]['i_ext'];
            $this->view->i_browser=$data[$j]['i_browser'];
            $this->view->i_pdfreader=$data[$j]['i_pdfreader'];
            $this->view->i_office=$data[$j]['i_office'];
            $this->view->i_kompresi=$data[$j]['i_kompresi'];
            $this->view->i_virus=$data[$j]['i_virus'];
            $this->view->c_website=$data[$j]['c_website'];
            $this->view->n_website=$data[$j]['n_website'];
            $this->view->c_sw_platform=$data[$j]['c_sw_platform'];
            $this->view->c_sw_database=$data[$j]['c_sw_database'];
            $this->view->c_sk=$data[$j]['c_sk'];
            $this->view->c_struktur=$data[$j]['c_struktur'];
            $this->view->c_nemail=$data[$j]['c_nemail'];
            $this->view->n_sw_email=$data[$j]['n_sw_email'];
        }
        $this->view->par='Koreksi';
}
else{$this->view->par='Simpan';}
$this->view->id_kuesioner=$id_kuesioner;

}
public function softwareAction()
{
    $id_kuesioner = $_REQUEST['idKuesioner'];
    $this->software($id_kuesioner);
}
public function softwarejsAction()
{
    header('content-type : text/javascript');
    $this->render('softwarejs');
}

public function maintaindatasoftwareAction()
{

            $MaintainData1 = array(
                    "id_kuesioner"=>$_POST['id_kuesioner'],
                    "i_xp"=>$_POST['i_xp'],
                    "i_vista"=>$_POST['i_vista'],
                    "i_seven"=>$_POST['i_seven'],
                    "i_linux"=>$_POST['i_linux'],
                    "i_dos"=>$_POST['i_dos'],
                    "i_ext"=>$_POST['i_ext'],
                    "i_browser"=>$_POST['i_browser'],
                    "i_pdfreader"=>$_POST['i_pdfreader'],
                    "i_office"=>$_POST['i_office'],
                    "i_kompresi"=>$_POST['i_kompresi'],
                    "i_virus"=>$_POST['i_virus'],
                    "c_website"=>$_POST['c_website'],
                    "n_website"=>$_POST['n_website'],
                    "c_sw_platform"=>$_POST['c_sw_platform'],
                    "c_sw_database"=>$_POST['c_sw_database'],
                    "c_sk"=>$_POST['c_sk'],
                    "c_struktur"=>$_POST['c_struktur'],
                    "c_nemail"=>$_POST['c_nemail'],
                    "n_sw_email"=>$_POST['n_sw_email'],
                    "i_entry"=>$_POST['i_entry']);

                    if ($_POST['perintah']=='Koreksi')
                    {$hasil = $this->software_serv->ubahTmSoftware($MaintainData1);}
                    else
                    {$hasil = $this->software_serv->tambahTmSoftware($MaintainData1);}



    if ($hasil =='sukses')
    {
            for($x=0;$x<5;$x++)
            {
                $counttable=0;
                switch ($x)
                {
                    case 0:
                        $counttable=$_POST['counttableperkara'];
                        $jnsapl="jnsaplperkara_";
                        $platform="platformperkara_";
                        $datbs="datbsperkara_";
                        $naplikasi="perkara_";
                        $naplikasi2="perkara2_";
                        $c_adm="1";

                      break;
                    case 1:
                        $counttable=$_POST['counttablekeuangan'];
                        $jnsapl="jnsaplkeuangan_";
                        $platform="platformkeuangan_";
                        $datbs="datbskeuangan_";
                        $naplikasi="keuangan_";
                        $naplikasi2="keuangan2_";
                        $c_adm="2";
                      break;
                    case 2:

                        $counttable=$_POST['counttablekepegawaian'];
                        $jnsapl="jnsaplkepegawaian_";
                        $platform="platformkepegawaian_";
                        $datbs="datbskepegawaian_";
                        $naplikasi="kepegawaian_";
                        $naplikasi2="kepegawaian2_";
                        $c_adm="3";
                      break;
                    case 3:
                     $counttable=$_POST['counttableaset'];
                     $jnsapl="jnsaplaset_";
                     $platform="platformaset_";
                     $datbs="datbsaset_";
                     $naplikasi="aset_";
                     $naplikasi2="aset2_";
                     $c_adm="4";
                      break;
                    case 4:
                        $counttable=$_POST['counttablerencanaanggaran'];
                        $jnsapl="jnsaplrencanaanggaran_";
                        $platform="platformrencanaanggaran_";
                        $datbs="datbsrencanaanggaran_";
                        $naplikasi="rencanaanggaran_";
                        $naplikasi2="rencanaanggaran2_";
                        $c_adm="5";
                      break;

                }
                $con=1;
                //echo "<br>".$counttable;
                for($xi=0;$xi<$counttable;$xi++)
                    {
                        $c_jns_aplikasi=$_POST[$jnsapl.$con];
                        $c_platform=$_POST[$platform.$con];
                        $c_database=$_POST[$datbs.$con];
                        $n_aplikasi=$_POST[$naplikasi.$con];
                        $n_aplikasi2=$_POST[$naplikasi2.$con];
                        //echo " $jnsapl$con  $jenisaplikasi $n_aplikasi<br>";
                         if ($n_aplikasi)
                             {
                                 $MaintainData = array(
                                                "id_kuesioner"=>$_POST['id_kuesioner'],
                                                "c_adm"=>$c_adm,
                                                "n_aplikasi"=>$n_aplikasi,
                                                "n_aplikasi2"=>$n_aplikasi2,
                                                "c_platform"=>$c_platform,
                                                "c_database"=>$c_database,
                                                "c_jns_aplikasi"=>$c_jns_aplikasi,
                                                "i_entry"=>$data['i_entry'] );

                                    if ($_POST['perintah']=='Koreksi')
                                    {
                                        if ($xi==0){$hasil = $this->software_serv->hapusTmSoftwareAdm2($MaintainData);}
                                        $hasil = $this->software_serv->tambahTmSoftwareAdm($MaintainData);
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";}
                                    else
                                    {   $hasil = $this->software_serv->tambahTmSoftwareAdm($MaintainData);
                                        $this->view->par="simpan";
                                        $par="Simpan ";}


                             }
                        $con++;
                    }
            }
    }

    if ($hasil =='sukses')
    {
        $counttablekendala=$_POST['counttablekendala'];
        $con=1;
        for($xi=0;$xi<$counttablekendala;$xi++)
        {

         if ($_POST['aplkendala_'.$con])
            {
                                 $MaintainData = array(
                                                "id_kuesioner"=>$_POST['id_kuesioner'],
                                                "n_aplikasi_kendala"=>$_POST['aplkendala_'.$con],
                                                "n_kendala"=>$_POST['kendala_'.$con],
                                                "i_entry"=>$data['i_entry'] );
                                    if ($_POST['perintah']=='Koreksi')
                                    {//$hasil = $this->software_serv->ubahTmSoftwareKendala($MaintainData);
                                        if ($xi==0){
                                        $hasil = $this->software_serv->hapusTmSoftwareKendala($MaintainData);}
                                        if ($hasil=='sukses'){ $hasil = $this->software_serv->tambahTmSoftwareKendala($MaintainData);}
                                        $this->view->par="Koreksi";
                                        $par="Koreksi ";}
                                    else
                                    {   $hasil = $this->software_serv->tambahTmSoftwareKendala($MaintainData);
                                        $this->view->par="simpan";
                                        $par="Simpan ";}


            }
            $con++;
        }

    }

        $pesan=$par." data ".$hasil;
        $this->view->pesan = $pesan;
        $this->view->pesancek = $hasil;
        $this->software($_POST['id_kuesioner']);
    $this->_helper->viewRenderer('software');

}

public function hapusAction()
{
    $id_kuesioner=$_GET['id_kuesioner'];
    $MaintainData1 = array("id_kuesioner"=>$_GET['id_kuesioner']);
    $hasil = $this->software_serv->hapusTmSoftware($MaintainData1);
    $hasil = $this->software_serv->hapusTmSoftwareAdm($MaintainData1);
    $hasil = $this->software_serv->hapusTmSoftwareKendala($MaintainData1);
    $this->view->par='Simpan';
    $this->_helper->viewRenderer('software');

}
    public function softwarejuduliconAction()
    {
        //untuk mengeluarkan judul dan icon Kondisi Sistem TI
    }

}
?>
