<?php
class Aplikasi_Sdm_Service {
    private static $instance;
   
    // A private constructor; prevents direct creation of object
    private function __construct() {
       //echo 'I am constructed';
    }

    // The singleton method
    public static function getInstance() {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
    }

	//MA
	//======
	public function detailSdmTerakhir($data){
		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$idKuesioner = $data['idKuesioner'];
			$jmldataSDM = $db->fetchOne("select count(*) from tm_sdm where id_kuesioner = '$idKuesioner'");
			
			if($jmldataSDM == 0){
				$hasilAkhir = null;
			} else {
				$sqlProses = "SELECT id_kuesioner, i_jumlahhakim, i_jumlahpns, i_jumlahpp, i_jumlahhonorer,
							       i_jumlahpeneliti,i_jumlahwidyaiswara, i_jumlahpranatakomputer, i_jumlahauditor,
							       i_stafit_sertifikasi_software, i_stafit_sertifikasi_hardware,
							       i_stafit_sertifikasi_network, i_user_siap, i_user_sdm, i_user_sai,
							       i_user_rkakl, i_user_sabmn, i_user_website, i_hakim_pc, i_pp_pc,
							       i_pns_pc, i_honorer_pc
							  FROM tm_sdm
							  WHERE id_kuesioner = '$idKuesioner'";	
								
				$result = $db->fetchRow($sqlProses);				
				
				$jmlResult = count($result);
				
				$hasilAkhir = array("id_kuesioner"  	=>(string)$result->id_kuesioner,
									"i_jumlahhakim"  	=>(string)$result->i_jumlahhakim,
									"i_jumlahpns"  	=>(string)$result->i_jumlahpns,
									"i_jumlahpp"  	=>(string)$result->i_jumlahpp,
									"i_jumlahhonorer"  	=>(string)$result->i_jumlahhonorer,
									"i_jumlahpeneliti" => $result->i_jumlahpeneliti,
									"i_jumlahwidyaiswara" => $result->i_jumlahwidyaiswara,
									"i_jumlahpranatakomputer" => $result->i_jumlahpranatakomputer,
									"i_jumlahauditor" => $result->i_jumlahauditor,
									"i_stafit_sertifikasi_software"  	=>(string)$result->i_stafit_sertifikasi_software,
									"i_stafit_sertifikasi_hardware"  	=>(string)$result->i_stafit_sertifikasi_hardware,
									"i_stafit_sertifikasi_network"  	=>(string)$result->i_stafit_sertifikasi_network,
									"i_user_siap"  	=>(string)$result->i_user_siap,
									"i_user_sdm"  	=>(string)$result->i_user_sdm,
									"i_user_sai"  	=>(string)$result->i_user_sai,
									"i_user_rkakl"  	=>(string)$result->i_user_rkakl,
									"i_user_sabmn"  	=>(string)$result->i_user_sabmn,
									"i_user_website"  	=>(string)$result->i_user_website,
									"i_hakim_pc"  	=>(string)$result->i_hakim_pc,
									"i_pp_pc"  	=>(string)$result->i_pp_pc,
									"i_pns_pc"  	=>(string)$result->i_pns_pc,
									"i_honorer_pc"  	=>(string)$result->i_honorer_pc
									);
			}
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function insertSdm(array $data) {
	   $registry = Zend_Registry::getInstance();
	   $db = $registry->get('db');
	   try {
			$db->beginTransaction();
			
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$idKuesioner = $data['idKuesioner'];
			$jmldataSDM = $db->fetchOne("select count(*)  from tm_sdm where id_kuesioner = '$idKuesioner'");
			
			$idKuesioner = $data['idKuesioner'];
			$iJumlahhakim = $data['iJumlahhakim'];
			$iJumlahpp = $data['iJumlahpp'];
			$iJumlahpns = $data['iJumlahpns'];
			$iJumlahhonorer = $data['iJumlahhonorer'];
			$iJumlahpeneliti = $data['iJumlahpeneliti'];
			$iJumlahwidyaiswara = $data['iJumlahwidyaiswara'];
			$iJumlahpranatakomputer = $data['iJumlahpranatakomputer'];
			$iJumlahauditor = $data['iJumlahauditor'];
			$iStafitSertifikasiSoftware = $data['iStafitSertifikasiSoftware'];
			$iStafitSertifikasiHardware = $data['iStafitSertifikasiHardware'];
			$iStafitSertifikasiNetwork = $data['iStafitSertifikasiNetwork'];
			$iUserSiap = $data['iUserSiap'];
			$iUserSdm = $data['iUserSdm'];
			$iUserSai = $data['iUserSai'];
			$iUserRkakl = $data['iUserRkakl'];
			$iUserSabmn = $data['iUserSabmn'];
			$iUserWebsite = $data['iUserWebsite'];
			$iHakimPc = $data['iHakimPc'];
			$iPpPc = $data['iPpPc'];
			$iPnsPc = $data['iPnsPc'];
			$iHonorerPc = $data['iHonorerPc'];
			$i_entry = $data['i_entry'];
	     
			if(!$jmldataSDM){
				$paramInsertSdm = array("id_kuesioner" => $idKuesioner,
									"i_jumlahhakim" => $iJumlahhakim,
									"i_jumlahpns" => $iJumlahpns,
									"i_jumlahpp" => $iJumlahpp,
									"i_jumlahhonorer" => $iJumlahhonorer,
									"i_jumlahpeneliti" => $iJumlahpeneliti,
									"i_jumlahwidyaiswara" => $iJumlahwidyaiswara,
									"i_jumlahpranatakomputer" => $iJumlahpranatakomputer,
									"i_jumlahauditor" => $iJumlahauditor,
									"i_stafit_sertifikasi_software" => $iStafitSertifikasiSoftware,
									"i_stafit_sertifikasi_hardware" => $iStafitSertifikasiHardware,
									"i_stafit_sertifikasi_network" => $iStafitSertifikasiNetwork,
									"i_user_siap" => $iUserSiap,
									"i_user_sdm" => $iUserSdm,
									"i_user_sai" => $iUserSai,
									"i_user_rkakl" => $iUserRkakl,
									"i_user_sabmn" => $iUserSabmn,
									"i_user_website" => $iUserWebsite,
									"i_hakim_pc" => $iHakimPc,
									"i_pp_pc" => $iPpPc,
									"i_pns_pc" => $iPnsPc,
									"i_honorer_pc" => $iHonorerPc,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);

				$db->insert('tm_sdm', $paramInsertSdm);
			} else {
				$paramUpdateSdm = array("i_jumlahhakim" => $iJumlahhakim,
									"i_jumlahpns" => $iJumlahpns,
									"i_jumlahpp" => $iJumlahpp,
									"i_jumlahhonorer" => $iJumlahhonorer,
									"i_jumlahpeneliti" => $iJumlahpeneliti,
									"i_jumlahwidyaiswara" => $iJumlahwidyaiswara,
									"i_jumlahpranatakomputer" => $iJumlahpranatakomputer,
									"i_jumlahauditor" => $iJumlahauditor,
									"i_stafit_sertifikasi_software" => $iStafitSertifikasiSoftware,
									"i_stafit_sertifikasi_hardware" => $iStafitSertifikasiHardware,
									"i_stafit_sertifikasi_network" => $iStafitSertifikasiNetwork,
									"i_user_siap" => $iUserSiap,
									"i_user_sdm" => $iUserSdm,
									"i_user_sai" => $iUserSai,
									"i_user_rkakl" => $iUserRkakl,
									"i_user_sabmn" => $iUserSabmn,
									"i_user_website" => $iUserWebsite,
									"i_hakim_pc" => $iHakimPc,
									"i_pp_pc" => $iPpPc,
									"i_pns_pc" => $iPnsPc,
									"i_honorer_pc" => $iHonorerPc,
									"i_entry" => $i_entry,
									"d_entry" => date('Y-m-d')
									);
				$whereUpdatesdm[] = "id_kuesioner = '$idKuesioner'";
				//var_dump($paramUpdateSdm);

				$db->update('tm_sdm', $paramUpdateSdm, $whereUpdatesdm);
			}
		 $db->commit();
		 unset($paramInsert);
	     return 'sukses';
	   } catch (Exception $e) {
			$db->rollBack();
			$errmsgArr = explode(":",$e->getMessage());
			$errMsg = $errmsgArr[0];

			if($errMsg == "SQLSTATE[23000]")
			{
			return "gagal.Data Sudah Ada.";
			}
			else
			{
			return "gagal.";
			}
	   }
	}
	
	public function pengadilanbandingList() {

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			/* $whereBase = "where c_statusdelete != 'Y' or c_statusdelete is null ";
			$whereOpt = "$kategoriCari like '%$katakunciCari%' ";
			if($kategoriCari) { $where = $whereBase." and ".$whereOpt;} 
			else { $where = $whereBase;}
			$order = "order by $sortBy $sort "; */
			$sqlProses = "select id_pengadilan,
								n_pengadilan
							from tm_pengadilan
							where (id_pengadilan_banding is null) or (id_pengadilan_banding = '' )
							order by id_pengadilan";	
							
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("id_pengadilan"  	=>(string)$result[$j]->id_pengadilan,
									   "n_pengadilan"  	=>(string)$result[$j]->n_pengadilan
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function pengadilanList($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select id_pengadilan,
								n_pengadilan
							from tm_pengadilan
							where id_pengadilan_banding = '$idPengadilanBanding' 
							order by id_pengadilan";	
							
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("id_pengadilan"  	=>(string)$result[$j]->id_pengadilan,
									   "n_pengadilan"  	=>(string)$result[$j]->n_pengadilan
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getalamatpengadilan($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select n_alamat
							from tm_pengadilan
							where id_pengadilan = '$idPengadilanBanding'";	
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("n_alamat" =>(string)$result[$j]->n_alamat
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	
	public function getnotelppengadilan($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select no_telepon
							from tm_pengadilan
							where id_pengadilan = '$idPengadilanBanding'";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("no_telepon" =>(string)$result[$j]->no_telepon
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}

	public function getnofaxpengadilan($dataMasukan) {
	
		$idPengadilanBanding = $dataMasukan['idPengadilanBanding'];

		$registry = Zend_Registry::getInstance();
		$db = $registry->get('db');
	   
		try {
			$db->setFetchMode(Zend_Db::FETCH_OBJ); 
			
			$sqlProses = "select no_fax
							from tm_pengadilan
							where id_pengadilan = '$idPengadilanBanding'";	
			//echo $sqlProses;
			$result = $db->fetchAll($sqlProses);				
			
			$jmlResult = count($result);
			
			for ($j = 0; $j < $jmlResult; $j++) {
				$hasilAkhir[$j] = array("no_fax" =>(string)$result[$j]->no_fax
										);
				//var_dump($hasilAkhir);				
			}	
			return $hasilAkhir;						  
			
	   } catch (Exception $e) {
         echo $e->getMessage().'<br>';
	     return 'gagal <br>';
	   }
	}
	////////////////////////////////////////
}
?>